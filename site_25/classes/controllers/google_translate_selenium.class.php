<?
// no direct access
defined( '_EXEC' ) or die( 'Restricted access' );

//require_once 'PHPUnit/Autoload.php';
require_once($_SERVER['DOCUMENT_ROOT'] . '/frameworks/PHPUnit/Autoload.php');
//require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/antigate.class.php');

//require_once 'PHPUnit/Extensions/SeleniumTestCase/Autoload.php';
//require_once 'PHPUnit/Extensions/SeleniumCommon/Autoload.php'; 
//require_once 'PHPUnit/Extensions/SeleniumTestCase.php';
 
class Google_translate_selenium extends PHPUnit_Extensions_SeleniumTestCase{

	protected $antigate;
	
	function __construct(){
		parent::__construct();
		
		//$this->antigate = new Antigate;		
	}
	
	
    public function setUp(){
        $this->setBrowser('*googlechrome');//iexplore // googlechrome opera
		//в разных браузерах вёрстка может отличаться, при смене нужно проверять селекторы 
        $this->setBrowserUrl('http://translate.google.com/'); //?langpair=#ru/be/%D0%B4%D0%B2%D0%BA
		$this->start();
		
		//$this->antigate = new Antigate;
    }
 
    public function get_translate($text, $from, $to){
	
		if( !empty($text) ){
			$url = "http://translate.google.com/?langpair=#$from/$to/";		
			//$text = 'хлеб';
			//$words = trim($words);
			try{
				$this->open($url);	
				sleep(2);
				$this->type('css=textarea#source', $text);
				sleep(1);
				$this->click('css=input#gt-submit');
				sleep(7);
				$result = $this->getText('css=span#result_box');
			}catch(Exception $e){
				try{
					sleep(3);
					$result = $this->getText('css=span#result_box');
				}catch(Exception $e){
					
				}
			}
			//sleep(3);
			//$tmp_html = $this->getHtmlSource();
			$result = strip_tags($result);
			$result = trim($result);
			//echo '<hr>$result = ' . $result;		
			//sleep(5);
		}
		return $result;
    }	
	
	
	public function tearDown(){
		$this->stop();
	}
}
?>