<?
// no direct access
defined( '_EXEC' ) or die( 'Restricted access' );

require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/google_translate_selenium.class.php');

class Translate extends Google_translate_selenium{

	/*
	protected $UA = array (
			"Mozilla/5.0 (Windows; U; Windows NT 6.0; fr; rv:1.9.1b1) Gecko/20081007 Firefox/3.1b1",
			"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.0",
			"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.19 (KHTML, like Gecko) Chrome/0.4.154.18 Safari/525.19",
			"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.2.149.27 Safari/525.13",
			"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)",
			"Mozilla/4.0 (compatible; MSIE 7.0b; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.40607)",
			"Mozilla/4.0 (compatible; MSIE 7.0b; Windows NT 5.1; .NET CLR 1.1.4322)",
			"Mozilla/4.0 (compatible; MSIE 7.0b; Windows NT 5.1; .NET CLR 1.0.3705; Media Center PC 3.1; Alexa Toolbar; .NET CLR 1.1.4322; .NET CLR 2.0.50727)",
			"Mozilla/45.0 (compatible; MSIE 6.0; Windows NT 5.1)",
			"Mozilla/4.08 (compatible; MSIE 6.0; Windows NT 5.1)",
			"Mozilla/4.01 (compatible; MSIE 6.0; Windows NT 5.1)",
			"Opera/10.00 (Windows NT 5.1; U; ru) Presto/2.2.0"
	);
			
    
	function getRandomUserAgent ( ) {
	    srand((double)microtime()*1000000);	   
	    return  $this->UA[rand(0,count( $this->UA)-1)];
	}
	*/
	
	/*
	function getContent ($url) {	//, $text
	
	    $ch = curl_init();
		//curl_setopt($ch, CURLOPT_POST, true);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, "text=" . urlencode($text) );
		
		
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_HEADER, 0);
	    curl_setopt($ch, CURLOPT_USERAGENT, $this->getRandomUserAgent());
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);	 
		curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt ($ch, CURLOPT_TIMEOUT, 10);		
	    $output = curl_exec($ch);
	    $info = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	    curl_close($ch);
	    if ($output === false || $info != 200) {
	      $output = null;
	    }
	    return $output;
	 
	}
	*/
		
    function translate_text($text, $from, $to){
	
		if( !empty($text) ){
		
			$this->setUp();
			$translate_text = $this->get_translate($text, $from, $to);
			$this->tearDown();
			
			###попытка вылечить баг с незакрытием окон
			sleep(1);
			$this->tearDown();
			sleep(2);
		}
		
		return $translate_text;
	
	
	
		/*
		$text = strip_tags(trim($text));
		$text = urlencode($text);
		//$f = $this->getContent("http://translate.google.com/?langpair=#$from/$to/" . $text); //text=" . urlencode($text) . "&	
		$f = $this->getContent("http://translate.google.com/?langpair=#ru/be/%D0%B4%D0%B2%D0%BA"); //text=" . urlencode($text) . "&			
		//translate_t
		echo '<hr>$f' . $f;
		
		$f = strstr($f, "TRANSLATED_TEXT='");	
		$f = strstr($f, "';INPUT_TOOL_PATH='", true);
		$f = str_replace("TRANSLATED_TEXT='", '', $f);
		
		//убираем пробелы между многоточие и словом и добавляем br после него
		$f = preg_replace('|([а-яА-ЯёЁa-z0-9)(]) *?\.\.\.|', '$1...<br>', $f);
		

		return $f;
		*/
    }
	
}
?>