<?php
// no direct access
defined( '_EXEC' ) or die( 'Restricted access' );

class Mysql
{
	public $db_id = false;
    private static $instance = null;
	public $connected = false;
	public $query_num = 0;
	public $query_list = array();
	public $mysql_error = '';
	public $mysql_version = '';
	public $mysql_error_num = 0;
	public $mysql_extend = "MySQL";
	public $MySQL_time_taken = 0;
	public $query_id = false;
        //var $db_config = array();

    function connect($db_config, $show_error=1){
            //$this->db_config = $db_config;
            $db_user = $db_config['user'];
            $db_pass = $db_config['pass']; 
            $db_name = $db_config['name']; 
            $db_location = $db_config['host'];
            if(!$this->db_id = @mysql_connect($db_location, $db_user, $db_pass)) {
			if($show_error == 1) {
				$this->display_error(mysql_error(), mysql_errno());
			} else {
				return false;
			}
		}

		if(!@mysql_select_db($db_name, $this->db_id)) {
			if($show_error == 1) {
				$this->display_error(mysql_error(), mysql_errno());
			} else {
				return false;
			}
		}

		$this->mysql_version = mysql_get_server_info();

		if(!defined('COLLATE'))
		{
			define ("COLLATE", "utf8");
		}

		if (version_compare($this->mysql_version, '4.1', ">=")) mysql_query("/*!40101 SET NAMES '" . COLLATE . "' */");

		$this->connected = true;

		return true;
	}
        
    public static function getInstance(){
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }
/**
 *
 * @param type $query
 * @param type $show_error
 * @return type 
 */
	function query($query, $show_error=true){
		$time_before = $this->get_real_time();

		//if(!$this->connected) $this->connect(DBUSER, DBPASS, DBNAME, DBHOST);

		if(!($this->query_id = mysql_query($query, $this->db_id) )) {

			$this->mysql_error = mysql_error();
			$this->mysql_error_num = mysql_errno();

			if($show_error) {
				$this->display_error($this->mysql_error, $this->mysql_error_num, $query);
			}
		}

		$this->MySQL_time_taken += $this->get_real_time() - $time_before;


			$this->query_list[] = array( 'time'  => ($this->get_real_time() - $time_before),
										 'query' => $query,
										 'num'   => (count($this->query_list) + 1));

		$this->query_num ++;
                $this->query_time +=($this->get_real_time() - $time_before);

		return $this->query_id;
	}
/**
 *
 * @param type $query_id
 * @return type 
 */
	function get_row($query_id = ''){
		if ($query_id == '') $query_id = $this->query_id;

		return mysql_fetch_assoc($query_id);
	}
/**
 *
 * @param type $query_id
 * @return type 
 */
	function get_array($query_id = ''){
		if ($query_id == '') $query_id = $this->query_id;

		return mysql_fetch_array($query_id);
	}

/**
 *
 * @param type $query
 * @param type $multi
 * @return type 
 */
	function super_query($query, $multi = false){

		if(!$multi) {

			$this->query($query);
			$data = $this->get_row();
			$this->free();
			return $data;

		} else {
			$this->query($query);

			$rows = array();
			while($row = $this->get_row()) {
				$rows[] = $row;
			}

			$this->free();

			return $rows;
		}
	}
        
        /**
         *
         * @param type $query
         * @param type $id
         * @return string 
         */
    function selects_query($query,$id =null){

		
		$this->query($query);

		$rows = array();
		while($row = $this->get_row()) {
                            
            if($row['id'] == $id)$row['selected'] = ' selected="selected" ';
			//$row = index::cp1251toUTFiconvArr($row);
            $rows[] = $row;
		}

		$this->free();

		return $rows;		
	}
/**
 *
 * @param type $query_id
 * @return type 
 */
	function num_rows($query_id = ''){

		if ($query_id == '') $query_id = $this->query_id;

		return mysql_num_rows($query_id);
	}
/**
 *
 * @return type 
 */
	function insert_id(){
		return mysql_insert_id($this->db_id);
	}
/**
 *
 * @param type $query_id
 * @return type 
 */
	function get_result_fields($query_id = ''){

		if ($query_id == '') $query_id = $this->query_id;

		while ($field = mysql_fetch_field($query_id))
		{
            $fields[] = $field;
		}

		return $fields;
   	}
/**
 *
 * @param type $source
 * @return type 
 */
	public function safesql( $source ){
		if ($this->db_id) return mysql_real_escape_string($source, $this->db_id);
		else return mysql_escape_string($source);
	}
/**
 *
 * @param type $query_id 
 */
	function free( $query_id = '' ){

		if ($query_id == '') $query_id = $this->query_id;

		@mysql_free_result($query_id);
	}
/**
 * 
 */
	function close(){
		@mysql_close($this->db_id);
	}
/**
 *
 * @return type 
 */
	function get_real_time(){
		list($seconds, $microSeconds) = explode(' ', microtime());
		return ((float)$seconds + (float)$microSeconds);
	}
/**
 *
 * @param type $error
 * @param type $error_num
 * @param type $query 
 */
	function display_error($error, $error_num, $query = ''){
		if($query){
			// Safify query
			$query = preg_replace("/([0-9a-f]){32}/", "********************************", $query); // Hides all hashes
			$query_str = "$query";
		}

		echo '<?xml version="1.0" encoding="utf-8"?>
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<title>MySQL Fatal Error</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<style type="text/css">
		<!--
		body {
			font-family: Verdana, Arial, Helvetica, sans-serif;
			font-size: 10px;
			font-style: normal;
			color: #000000;
		}
		-->
		</style>
		</head>
		<body>
			<font size="4">MySQL Error!</font>
			<br />------------------------<br />
			<br />

			<u>The Error returned was:</u>
			<br />
				<strong>'.$error.'</strong>

			<br /><br />
			</strong><u>Error Number:</u>
			<br />
				<strong>'.$error_num.'</strong>
			<br />
				<br />

			<textarea name="" rows="10" cols="52" wrap="virtual">'.$query_str.'</textarea><br />

		</body>
		</html>';

		exit();
	}

}


?>