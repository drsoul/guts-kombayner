<?
// no direct access
defined( '_EXEC' ) or die( 'Restricted access' );

require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/antigate.class.php');
 
class Google_captcha{

	protected $antigate;
	
	function __construct(){		
		
		$this->antigate = new Antigate;		
	}
	
	
	//проверяет сработала ли капча, если да то производит её распознание
	//в любом случае возращает html по запросу $google_query_url, либо переданный, либо полученный после прохождения капчи
	//пример $google_query_url = 'http://www.google.ru/search?q=sword&hl=ru&newwindow=1&safe=active&tbas=0&biw=1066&bih=747&tbm=vid&prmd=imvnse&source=lnms&sa=X&oi=mode_link&ct=mode&cd=4';
	function work($html_source, $google_query_url){
	
		if(strpos($html_source, 'sorry/Captcha') !== false OR strpos($html_source, '<H1>302 Moved</H1>') !== false){
	
			$useragent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.0.1) Gecko/2008070208';
			$cookie_path = $_SERVER['DOCUMENT_ROOT'] . '/cookie/google.txt';
		
			//$query = 'хлеб';
			//$query = urlencode( $query );
			//$url = "http://www.google.ru/search?q=" . $query . "&hl=ru&newwindow=1&safe=active&tbas=0&biw=1066&bih=747&tbm=vid&prmd=imvnse&source=lnms&sa=X&oi=mode_link&ct=mode&cd=4";
			$url = $google_query_url;
			//$img = 'http://www.google.ru/sorry/image?id=14490407403469528858&hl=ru';
			//$url = urlencode( $url );
			$url_1 = 'http://www.google.ru/sorry/Captcha?continue=' . $url;
			//echo '$url = ' . $url_1;
					
			####### заход на форму каптчи и получение адреса изображения
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url_1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
			curl_setopt($ch, CURLOPT_TIMEOUT, 1);	
			//curl_setopt($ch, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'] . '/cookie/google.txt');
			//curl_setopt($ch, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . '/cookie/google.txt');				
			curl_setopt($ch, CURLOPT_REFERER, $url);
			// curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
			//curl_setopt($ch, CURLOPT_PROXY, $proxy_now );
			$html = curl_exec($ch);
			curl_close($ch);							
					
			preg_match($pattern = '|(?<=<img src=").*?(?=")|uis', $html, $match);
					
			//echo '$url_1 = ' . $url_1;
			//echo '<hr>' . $html;
			//die;					
				
			if( !empty($match[0]) ){//обнаружили адрес картинки капчи	
					
				$img = 'http://www.google.ru' . $match[0];		
					
				preg_match($pattern = '|(?<=name="id" value=").*?(?=">)|uis', $html, $match);
				$id_captcha = $match[0];
						
				$this->antigate->setup($filename = 'google.jpg', $apikey, $is_verbose, $domain,
				$rtimeout, $mtimeout, $is_phrase, $is_regsense, $is_numeric,
				$min_len, $max_len, $is_russian);
				
				
				sleep(3);
						
				######закачка изображения и кук с ним
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $img);
				curl_setopt($ch, CURLOPT_TIMEOUT, 10);
				curl_setopt($ch, CURLOPT_TIMEOUT, 10);						
				curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_path);
				curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_path);
				//curl_setopt($ch, CURLOPT_PROXY, $proxy_now );
				curl_setopt($ch, CURLOPT_REFERER, $url_1);
				$file = curl_exec($ch);
				curl_close($ch);
						
				//пищем картинку капчи в файл для отсылки в сервис распознания
				file_put_contents($this->antigate->get_path_img(), $file );				
				$text_img = $this->antigate->recognize();
						
				/*
				####создание формы с данными по шаблону
				$form_captcha_template_path = 'E:\WebServers1\home\unique_site_gen_45\www\tmp\form_captcha_template.html';
				$form_captcha_path = 'E:\WebServers1\home\unique_site_gen_45\www\tmp\form_captcha.html';
				$google_cookie_path = 'E:\WebServers1\home\unique_site_gen_45\www\cookie\google.txt';					
						
				$form_captcha = file_get_contents($form_captcha_template_path);
						
				$js_cookie_string = get_js_cookie_string($google_cookie_path);
						
				$form_captcha = str_replace('_continue_', $url, $form_captcha);
				$form_captcha = str_replace('_id_', $id_captcha, $form_captcha);
				$form_captcha = str_replace('_captcha_', $text_img, $form_captcha);
				$form_captcha = str_replace('_cookie_', $js_cookie_string, $form_captcha);				
						
				file_put_contents($form_captcha_path, $form_captcha);
				*/
						
				//die;
				if( $text_img !== false){	
				
					//отправка формы с капчей
					$ch = curl_init();
					$form_data = $url_1 . '&id=' . $id_captcha . '&captcha=' . $text_img . '&submit=Отправить';
					curl_setopt($ch, CURLOPT_URL, $form_data);
					curl_setopt($ch, CURLOPT_TIMEOUT, 10);
					curl_setopt($ch, CURLOPT_TIMEOUT, 10);					
					curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_path);
					curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_path);
					//curl_setopt($ch, CURLOPT_PROXY, $proxy_now );
					curl_setopt($ch, CURLOPT_REFERER, $url_1);
					curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
					$html = curl_exec($ch);
					curl_close($ch);
						
					/*	
					echo '<hr> html = ' . $html;					
					echo '<br>form_data = ' . $form_data;
					//die;
					*/
					
					/*
					#####отправка локальной формы с куками и данными -- другой метод отсылки формы - не рабочий
					$ch = curl_init();
					$local_form = 'file:///' . $form_captcha_path;
					curl_setopt($ch, CURLOPT_URL, $local_form);
					curl_setopt($ch, CURLOPT_TIMEOUT, 10);
					curl_setopt($ch, CURLOPT_TIMEOUT, 10);					
					curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.0.1) Gecko/2008070208');
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_COOKIEJAR, $_SERVER['DOCUMENT_ROOT'] . '/cookie/google.txt');
					curl_setopt($ch, CURLOPT_COOKIEFILE, $_SERVER['DOCUMENT_ROOT'] . '/cookie/google.txt');
					//curl_setopt($ch, CURLOPT_PROXY, $proxy_now );
					curl_setopt($ch, CURLOPT_REFERER, $url_1);
							
					$file = curl_exec($ch);
					curl_close($ch);
					*/
							
							
					//$html = str_replace('action="Captcha"', 'action="http://www.google.ru/sorry/Captcha"', $html);
				}else{
					return $html_source; //просто возвращаем полученный html, т.к. при распознании капчи произошла ошибка
				}
				/*			
				echo '<hr>' . $html;
				echo '<br>$img = ' . $img;
				echo '<br>$antigate->get_path_img() = ' . $antigate->get_path_img();
				echo '<br>$text_img = ' . $text_img;
				echo '<br>$id_captcha = ' . $id_captcha;
				echo '<br>$url = ' . $url;
				echo '<br>$file = ' . $file;
				*/	
				return $html;
						
			}else{
				return false;
			}
			
		}else{
			return $html_source; //просто возвращаем полученный html, т.к. капч не обнаружено
		}
	
	}
}
?>