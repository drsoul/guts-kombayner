User-agent: *
Allow: /
Disallow: /css/
Disallow: /js/
Disallow: /plugins/
Disallow: /templates/
Disallow: /templates_c/
Disallow: /classes/
Disallow: /money_house/
Disallow: /images/
Host: site_1