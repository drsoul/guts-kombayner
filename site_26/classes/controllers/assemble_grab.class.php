<?
// no direct access
defined( '_EXEC' ) or die( 'Restricted access' );

require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/abstract_auth.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/auth_rambler.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/grabs.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/parsers.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/automatize.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/templates.class.php');
//require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/translate.class.php'); // подключается по условию ниже, иначе fatal error
//require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/google_captcha.class.php');

require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/models/root_model.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/models/sql_model.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/models/dbconfig.class.php');

require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/smarty/Smarty.class.php');

class Assemble_grab{
	
	protected $auth_rambler;
	protected $grabs;
	public $parsers;
	protected $sql_model;
	protected $smarty;
	protected $automatize;
	protected $depth_text_google_grab = 10;
	protected $depth_text_eng_google_grab = 10;
	protected $depth_text_ua_google_grab = 10;
	protected $depth_text_by_google_grab = 10;	
	protected $depth_text_rusarticles_grab = 2;
	protected $method_grab_cat = 1;
	protected $wordstat_yandex;
	protected $wordstat_yandex_selenium;
	protected $adwords_keys_selenium;
	
	protected $ad_mail;
	protected $ad_pass;
	//protected $google_captcha;	
	
	
	public function set_depth_text_google_grab($depth_text_google_grab = 10){
	
		$this->depth_text_google_grab = $depth_text_google_grab;
		
		return true;
	}
	
	
	public function set_depth_text_rusarticles_grab($depth_text_rusarticles_grab = 2){
	
		$this->depth_text_rusarticles_grab = $depth_text_rusarticles_grab;
		
		return true;
	}
	
	
	function Assemble_grab(){
	
		set_time_limit(0);
		
		//ini_set("max_execution_time", "3600");
		
		$this->auth_rambler = new Auth_rambler;
		$this->grabs = new Grabs;
		$this->parsers = new Parsers;
		$this->sql_model = new Sql_model;
		$this->smarty = new Smarty;
		$this->automatize = new Automatize;
		$this->templates = new Templates;	
		//$this->google_captcha = new Google_captcha;	

		$this->config = New Dbconfig();
		
		
		$params = $this->sql_model->select_params();		
		foreach($params as $key => $value){
			if($value['name'] == 'method_grab_cat'){
				$this->method_grab_cat = $value['value'];
			}			
			
			if($value['name'] == 'ad_mail'){
				$this->ad_mail = $value['value'];
			}

			if($value['name'] == 'ad_pass'){
				$this->ad_pass = $value['value'];
			}			
			//здесь раньше было объявление экземпляра класса Translate,
			//но при этом не работали settings если стоял 2й способ сбора контента и не были подключеные все нужные библиотеки
			//перенёс в activate_translater

		}
	}
	
	
	public function activate_translater(){
	
		if($this->method_grab_cat == 2){
			require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/translate.class.php');
			$this->translate = New Translate();
		}
	}

	
	
	//главный цикл для обновлятора
	function grab_main_loop_second($id_site){		

		$phrases_primary_grab_0 = $this->sql_model->select_phrases_primary_grab_0($id_site);		
		
		foreach($phrases_primary_grab_0 as $key => $value){		
		
			$this->sql_model->set_id_sites($id_site);	
			$this->parsers->set_id_sites($id_site);	
			$this->grabs->set_id_sites($id_site);	
			
				
			//сбор поисковых фраз
			//$phrases = $this->adstat_rambler_grab($value['search_phrase']);
			$phrases = $this->sql_model->select_phrases_by_id_site($id_site, $limit = 150, $offset = 0);
			//$phrases = $this->wordstat_yandex_grab($value['search_phrase']);
			if( !empty($phrases) ){
				
				$this->sql_model->logs_grab($id_site, $title = 'Сайт');
				$this->sql_model->logs_grab($phrases, $title = 'Найденные фразы');
				/*
				echo '<pre>';
				print_r($phrases);
				echo '</pre>';
				*/				
				
				/*
				$this->texts_tr_by_grab($phrases, $id_site, $their_requests = 0);
				$this->sql_model->logs_grab('Ok!', $title = 'Тексты переведённые by');	
				
			
				$this->texts_tr_ua_grab($phrases, $id_site, $their_requests = 0);
				$this->sql_model->logs_grab('Ok!', $title = 'Тексты переведённые ua');
				
								
				//сбор текстов переведённых
				$this->texts_tr_grab($phrases, $id_site, $their_requests = 0);
				$this->sql_model->logs_grab('Ok!', $title = 'Тексты переведённые');		
				*/
				
				//сбор текстов
				$this->texts_grab($phrases, $id_site, $their_requests = 0);
				$this->sql_model->logs_grab('Ok!', $title = 'Тексты');
				
								
				//echo '<br>сбор тектов прошёл<br>';
				//сбор текстов
				//передаём экземпляр чтобы вместе с ним передалось свойство id_sites
				$this->fotos_grab($phrases, $id_site, $their_requests = 0);			
				$this->sql_model->logs_grab('Ok!', $title = 'Фотографии');
				//echo '<br>сбор фоток прошёл<br>';			
				//сбор видео
				
				
				$ids_videos = $this->videos_grab($phrases, $id_site, $their_requests = 0);
				$this->sql_model->logs_grab($ids_videos, $title = 'Видео');
				
				//echo '<br>сбор видео прошёл<br>';
					
				$this->automatize->bind_images_on_texts($imgs_on_text_from = 1, $imgs_on_text_to = 3);
				$this->sql_model->logs_grab('Ok!', $title = 'bind_images_on_texts');
				//echo '<br>привязка изображений прошла<br>';
				
			}
		}
	}
	
	
	//главный цикл сбора и вставки контента в базу
	function grab_main_loop($sites_and_phrases_primary_grab_0, $fragment_grab = 0, $cenzor_count = 30, $cenzor_count_max = 100){	
		
		$this->activate_translater(); //объявление экземпляра класса translate
		
		$time_sleep = 3;
		//error_reporting(E_ERROR | E_PARSE);
		//error_reporting(E_ALL);
		//ini_set('display_errors', 'on');
		//ob_start();
		
		
		if($this->method_grab_cat == 2){//если стоит автоматический способ сбора категорий то подключаем классы
			require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/wordstat_yandex.class.php');
			require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/wordstat_yandex_selenium.class.php');
			require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/adwords_keys_selenium.class.php');
			
			$this->wordstat_yandex = new Wordstat_yandex;
			$this->wordstat_yandex_selenium = new Wordstat_yandex_selenium;		
			$this->adwords_keys_selenium = new Adwords_keys_selenium;
		}
		
		
		//увеличиваем глубину сбора сайтов в гугле
		$this->set_depth_text_google_grab($depth_text_google_grab = 10);
		
		//add 03_12_12-16_48
		//создаём шаблоны в отдельном цикле чтобы не дублировать создание шаблонов в основном цикле (он ниже)
		$sites_primary_grab_0 = $this->sql_model->select_sites_by_primary_grab($primary_grab = 0);			
		
		$this->sql_model->logs_grab($sites_and_phrases_primary_grab_0, $title = 'Сайты и первичные фразы');	
		
		
		//создание шаблона сайта
		foreach($sites_primary_grab_0 as $key => $value){			
			$this->automatize->create_template($value['id_sites']);	
			$this->sql_model->logs_grab($value['id_sites'], $title = 'Создан шаблон для сайта');
		}	
		//конец - создание шаблона сайта
						
		$temp_id_sites = 0;
		$count_phrases = count($sites_and_phrases_primary_grab_0);
		$phrases_loop = 1;
		$loop_sites_phrases = 1;
		foreach($sites_and_phrases_primary_grab_0 as $key => $value){
						
			$this->sql_model->set_id_sites($value['id_sites']);		
			$this->parsers->set_id_sites($value['id_sites']);
			$this->grabs->set_id_sites($value['id_sites']);
			
			//сбор поисковых фраз
			
			//если автоматический то пользуем сборщик на основе селениум сервер
			if($this->method_grab_cat == 2){

				$phrases = $this->wordstat_yandex_grab_selenium($value['search_phrase']);
				
			}else{//если ручной метод сбора кат, то пользуем категории из бд
			
				$phrases_raw = $this->sql_model->select_phrases_by_id_site_only($value['id_sites'], $limit = 150, $offset = 0);
				
				//меняем формат массива
				foreach($phrases_raw as $key_1 => $value_1){
					if( !empty($value_1['search_phrase']) ){
						$phrases[] = $value_1['search_phrase'];
					}
				}
				
				unset($phrases_raw);
				
			}
			//$phrases = $this->sql_model->select_phrases_by_id_site($id_site, $limit = 150, $offset = 0);
			
			//$phrases = $this->adstat_rambler_grab($value['search_phrase']);
			//$phrases = $this->wordstat_yandex_grab($value['search_phrase'], $depth = 2);
		
	
		/*
		echo '<pre>';
		
		echo '<br>$value[id_sites] = ' . $value['id_sites'];
		echo '<br>$this->method_grab_cat = ' . $this->method_grab_cat;
		echo 'Ok!';
		print_r($phrases);
		echo '</pre>';
		die;
		*/
		
			
			if( !empty($phrases) ){
				
				$this->sql_model->logs_grab($value['id_sites'], $title = 'Сайт');
				$this->sql_model->logs_grab($phrases, $title = 'Найденные фразы');	
													
				
				if($this->method_grab_cat == 2){
				
					#####сбор текстов с Белоруси и перевод
					$count_texts_all['count'] = 0;				
					$count_texts_all = $this->sql_model->select_count_all_texts_tr_by_id_site($value['id_sites']);
					
					$flag_grab = 1;
					//проверяем включение мода фрагментарного грабинга, если включен проверяем цензор на то что собрано меньше минимума статей,
					//если да то позволяем сбор статей
					if($fragment_grab == 1 AND $count_texts_all['count'] > $cenzor_count){
						$flag_grab = 0;					
					}
					
					//проверяем количество статей на максимальный цензор, если собрано больше то останавливаем сбор, идём дальше
					if($count_texts_all['count'] < $cenzor_count_max AND $flag_grab == 1){
						sleep($time_sleep);
						$this->texts_tr_by_grab($phrases, $value['id_sites'], $their_requests = 1);
						$this->sql_model->logs_grab('Ok!', $title = 'Тексты переведённые by');
					}
					
					
					
					#####сбор текстов с Украины и перевод	
					$count_texts_all['count'] = 0;				
					$count_texts_all = $this->sql_model->select_count_all_texts_tr_ua_id_site($value['id_sites']);
					
					$flag_grab = 1;
					//проверяем включение мода фрагментарного грабинга, если включен проверяем цензор на то что собрано меньше минимума статей,
					//если да то позволяем сбор статей
					if($fragment_grab == 1 AND $count_texts_all['count'] > $cenzor_count){
						$flag_grab = 0;					
					}
					
					//проверяем количество статей на максимальный цензор, если собрано больше то останавливаем сбор, идём дальше
					if($count_texts_all['count'] < $cenzor_count_max AND $flag_grab == 1){
						sleep($time_sleep);
						$this->texts_tr_ua_grab($phrases, $value['id_sites'], $their_requests = 1);
						$this->sql_model->logs_grab('Ok!', $title = 'Тексты переведённые ua');
					}


					#####сбор текстов
					$count_texts_all['count'] = 0;				
					$count_texts_all = $this->sql_model->select_count_all_texts_id_site($value['id_sites']);
					
					$flag_grab = 1;
					//проверяем включение мода фрагментарного грабинга, если включен проверяем цензор на то что собрано меньше минимума статей,
					//если да то позволяем сбор статей
					if($fragment_grab == 1 AND $count_texts_all['count'] > $cenzor_count){
						$flag_grab = 0;					
					}
					
					//проверяем количество статей на максимальный цензор, если собрано больше то останавливаем сбор, идём дальше
					if($count_texts_all['count'] < $cenzor_count_max AND $flag_grab == 1){
						sleep($time_sleep);
						$this->texts_grab($phrases, $value['id_sites'], $their_requests = 1);
						$this->sql_model->logs_grab('Ok!', $title = 'Тексты');
					}					
					#####сбор текстов - конец					
			
									
					/*
					#####сбор текстов с буржуйнета и перевод
					$count_texts_all['count'] = 0;				
					$count_texts_all = $this->sql_model->select_count_all_texts_tr_id_site($value['id_sites']);
					
					$flag_grab = 1;
					//проверяем включение мода фрагментарного грабинга, если включен проверяем цензор на то что собрано меньше минимума статей,
					//если да то позволяем сбор статей
					if($fragment_grab == 1 AND $count_texts_all['count'] > $cenzor_count){
						$flag_grab = 0;					
					}
					
					//проверяем количество статей на максимальный цензор, если собрано больше то останавливаем сбор, идём дальше
					if($count_texts_all['count'] < $cenzor_count_max AND $flag_grab == 1){
						sleep($time_sleep);
						$this->texts_tr_grab($phrases, $value['id_sites'], $their_requests = 1);
						$this->sql_model->logs_grab('Ok!', $title = 'Тексты переведённые');
					}
					*/
					
				}else{
					#####сбор текстов
					$count_texts_all['count'] = 0;				
					$count_texts_all = $this->sql_model->select_count_all_texts_id_site($value['id_sites']);
					
					$flag_grab = 1;
					//проверяем включение мода фрагментарного грабинга, если включен проверяем цензор на то что собрано меньше минимума статей,
					//если да то позволяем сбор статей
					if($fragment_grab == 1 AND $count_texts_all['count'] > $cenzor_count){
						$flag_grab = 0;					
					}
					
					//проверяем количество статей на максимальный цензор, если собрано больше то останавливаем сбор, идём дальше
					if($count_texts_all['count'] < $cenzor_count_max AND $flag_grab == 1){
						sleep($time_sleep);
						$this->texts_grab($phrases, $value['id_sites'], $their_requests = 1);
						$this->sql_model->logs_grab('Ok!', $title = 'Тексты');
					}	
				}
				
				/*
				#####сбор текстов
				$count_texts_all['count'] = 0;				
				$count_texts_all = $this->sql_model->select_count_all_texts_id_site($value['id_sites']);
				
				$flag_grab = 1;
				//проверяем включение мода фрагментарного грабинга, если включен проверяем цензор на то что собрано меньше минимума статей,
				//если да то позволяем сбор статей
				if($fragment_grab == 1 AND $count_texts_all['count'] > $cenzor_count){
					$flag_grab = 0;					
				}
				
				//проверяем количество статей на максимальный цензор, если собрано больше то останавливаем сбор, идём дальше
				if($count_texts_all['count'] < $cenzor_count_max AND $flag_grab == 1){
					sleep($time_sleep);
					$this->texts_grab($phrases, $value['id_sites'], $their_requests = 1);
					$this->sql_model->logs_grab('Ok!', $title = 'Тексты');
				}				
				*/
									
				#####сбор фото
				//передаём экземпляр чтобы вместе с ним передалось свойство id_sites
				$count_texts_all['count'] = 0;				
				$count_texts_all = $this->sql_model->select_count_all_images_id_site($value['id_sites']);
				
				$flag_grab = 1;
				//проверяем включение мода фрагментарного грабинга, если включен проверяем цензор на то что собрано меньше минимума статей,
				//если да то позволяем сбор статей
				if($fragment_grab == 1 AND $count_texts_all['count'] > $cenzor_count){
					$flag_grab = 0;					
				}
				
				//проверяем количество статей на максимальный цензор, если собрано больше то останавливаем сбор, идём дальше
				if($count_texts_all['count'] < $cenzor_count_max AND $flag_grab == 1){
					sleep($time_sleep);
					$ids_images = $this->fotos_grab($phrases, $value['id_sites'], $their_requests = 1);
					$this->sql_model->logs_grab($ids_images, $title = 'Фотографии');
				}				

					
				
				#####сбор видео
				$count_texts_all['count'] = 0;				
				$count_texts_all = $this->sql_model->select_count_all_videos_id_site($value['id_sites']);
				
				$flag_grab = 1;
				//проверяем включение мода фрагментарного грабинга, если включен проверяем цензор на то что собрано меньше минимума статей,
				//если да то позволяем сбор статей
				if($fragment_grab == 1 AND $count_texts_all['count'] > $cenzor_count){
					$flag_grab = 0;					
				}
				
				//проверяем количество статей на максимальный цензор, если собрано больше то останавливаем сбор, идём дальше
				if($count_texts_all['count'] < $cenzor_count_max AND $flag_grab == 1){
					sleep($time_sleep);
					$ids_videos = $this->videos_grab($phrases, $value['id_sites'], $their_requests = 1);														
					$this->sql_model->logs_grab($ids_videos, $title = 'Видео');
				}					
				
				
				unset($phrases);			
							
				//update 30_11_12-08_24
				//проверка чтобы не обновлять статус у сайта после каждого цикла по ключевой фразе, а только после завершения работы по данному сайту
						
				$this->action_after_grab($temp_id_sites, $value['id_sites'], $count_phrases, $phrases_loop);
				
				$phrases_loop++;
				$temp_id_sites = $value['id_sites'];
				
				
				//создание файла с тегами для соц. закладок
				//$this->templates->create_file_for_bagbookmark($value['id_sites']); - больше здесь не требуется
			}
			
			
			//костыль чтобы последний сайт при грабе менял статус сбора контента после сбора
			if($count_phrases <= $loop_sites_phrases){
				
				$this->automatize->update_site_primary_grab($value['id_sites']);	
				$this->automatize->logs_grab('Ok!', $title = 'update_site_primary_grab_finish');	
				
				//обновление публикации контента
				$this->sql_model->unpublish_texts();
				$this->sql_model->unpublish_texts_tr();
				$this->sql_model->unpublish_videos();
				$this->sql_model->unpublish_images();

				$sites_1 = $this->sql_model->select_all_info_sites();
				
				foreach($sites_1 as $key_1 => $value_1){		
					$this->automatize->updater_status($limit_from = 80, $limit_to = 85, $percent = true, $value_1['id']);
				}
				//обновление публикации контента - конец
			}
			$loop_sites_phrases++;
			//конец костыля
		}	
		
		
		
	}

	
	protected function action_after_grab($temp_id_sites, $id_sites, $count_phrases, $phrases_loop){
	
			if($temp_id_sites != $id_sites AND $temp_id_sites != 0){				
				
				$url =  'http://' . $_SERVER['HTTP_HOST'] . '/admin/their_requests.php?command=action_after_grab_1';		
				
				$params['id_sites'] = $id_sites;
				$params['temp_id_sites'] = $temp_id_sites;				
				$login = $this->config->user['name'];
				$password = md5($this->config->user['pass']);
				//$params['domain'] = 'adm.ekstrim.borrba.ru';		
				
				$html = $this->automatize->curl_secure_connect($url, $login, $password, $params);
				unset($html, $login, $password, $params, $url);				
				
			}elseif($count_phrases == $phrases_loop){

				$url =  'http://' . $_SERVER['HTTP_HOST'] . '/admin/their_requests.php?command=action_after_grab_2';		
				
				$params['id_sites'] = $id_sites;
				$login = $this->config->user['name'];
				$password = md5($this->config->user['pass']);
				//$params['domain'] = 'adm.ekstrim.borrba.ru';		
				
				$html = $this->automatize->curl_secure_connect($url, $login, $password, $params);
				unset($html, $login, $password, $params, $url);			
				
			}
	}
	
	
	public function adstat_rambler_grab($search_phrase, $count_pages = 3){
		
		//перекодируем т.к. рамблер вордстат в cp1251
		$search_phrase = mb_convert_encoding($search_phrase, 'CP1251', 'UTF8');
		//$search_phrase = 'варенье из кабачков';

		
		//второй параметр - количество страниц парса адстата рамблера
		$html = $this->auth_rambler->browser_adstat_rambler($search_phrase, $count_pages);
		/*	
		echo '<pre>';
		print_r($html);
		echo '</pre>';
		*/
		
		$phrases = $this->parsers->parser_adstat_rambler($html, $search_phrase);
		
		
		//до этого была cp1251 делаем utf-8
		foreach ($phrases as $key => $value){
			$phrases[$key] = mb_convert_encoding($value, "UTF-8", "CP1251");
		}		
		/*
		##отладка
		echo '<pre>';
		print_r($phrases);
		echo '</pre>';
		*/	
		
		return $phrases;
	}
	
	
	public function wordstat_yandex_grab($search_phrase, $depth = 2, $id_sites = 0){
		
		if($this->method_grab_cat == 2){//если стоит автоматический способ сбора категорий то подключаем классы
		
			//до 05_12_12 было $depth = 2 в нижеидущем методе
			$html = $this->wordstat_yandex->wordstat_yandex_grab_html($search_phrase, $depth);
			/*
			echo '<pre>';
			print_r($html);
			echo '</pre>';
			*/
			$temp_id_sites = $this->parsers->get_id_sites();
			if( empty($temp_id_sites) ){
				$this->parsers->set_id_sites($id_sites);
			}
			$search_phrases = $this->parsers->wordstat_yandex_parse($html, $search_phrase, $limit_words = 5, $del_phrases_with_dig = false, $full_inclusion = true, $limit_phrases = 150, $strict_key_phrase_del = 1, $strict_word_ending = false);
				
			return $search_phrases;
			
		}else{
			return false;
		}
	}
	
	
	public function wordstat_yandex_grab_selenium($search_phrase, $depth = 2, $id_sites = 0){
		
		if($this->method_grab_cat == 2){//если стоит автоматический способ сбора категорий то подключаем классы
		
			//до 05_12_12 было $depth = 2 в нижеидущем методе
			
			/*
			$this->wordstat_yandex_selenium->setUp();
			$html = $this->wordstat_yandex_selenium->get_wordstat_html($search_phrase, $depth);
			$this->wordstat_yandex_selenium->tearDown();
			*/

			$this->adwords_keys_selenium->setUp();
			$search_phrases = $this->adwords_keys_selenium->get_keys_html($search_phrase, $this->ad_mail, $this->ad_pass);
			$this->adwords_keys_selenium->tearDown();
			
			/*
			//тест
			echo '<pre>';
			print_r($html);
			echo '</pre>';
			*/
			
			/*
			$temp_id_sites = $this->parsers->get_id_sites();
			if( empty($temp_id_sites) ){
				$this->parsers->set_id_sites($id_sites);
			}
			$search_phrases = $this->parsers->wordstat_yandex_parse($html, $search_phrase, $limit_words = 5, $del_phrases_with_dig = false, $full_inclusion = true, $limit_phrases = 150, $strict_key_phrase_del = 1, $strict_word_ending = false);
			*/
			/*
			//тест
			echo '<pre>';
			print_r($search_phrases);
			echo '</pre>';
			die;
			*/
			
			return $search_phrases;
			
		}else{
			return false;
		}
	}
	

	public function texts_grab($phrases, $id_site, $their_requests = 1, $sleep = 2){	

		if($their_requests == 1){
		
			foreach($phrases as $key => $value_search_phrase){
			
				sleep($sleep);
				//time_nanosleep(0, $sleep);
														
				//add 19_11_12 11_25
				//служит для того чтобы в названия категорий не попадали пустоты
				if($value_search_phrase == ' ' OR empty($value_search_phrase) ){
					break;
				}else{
					$url =  'http://' . $_SERVER['HTTP_HOST'] . '/admin/their_requests.php?command=text_grab';		
					
					$params['value_search_phrase'] = $value_search_phrase;
					$params['depth_text_google_grab'] = $this->depth_text_google_grab;
					$params['depth_text_rusarticles_grab'] = $this->depth_text_rusarticles_grab;
					$params['id_site'] = $id_site;
					
					$login = $this->config->user['name'];
					$password = md5($this->config->user['pass']);
					//$params['domain'] = 'adm.ekstrim.borrba.ru';		
					
					$html = $this->automatize->curl_secure_connect($url, $login, $password, $params);
					unset($html, $login, $password, $params, $url);
				}		
			}
				
		}else{
			
			foreach($phrases as $key => $value_search_phrase){	
			
				sleep($sleep);
				//time_nanosleep(0, $sleep);
			
				$depth_text_google_grab = $this->depth_text_google_grab;				
				$depth_text_rusarticles_grab = $this->depth_text_rusarticles_grab;				
				
				//чтобы фразы вставлялись в базу с нужным id сайта
				$this->parsers->set_id_sites($id_site);
				
				//перекодирум в utf8 т.к. данные от рамблер вордстата мы получили в cp1251
				//$value_search_phrase_encode = mb_convert_encoding($value_search_phrase, 'UTF8', 'CP1251');	
				//$value_search_phrase_encode = $value_search_phrase;
				
				//########сбор статей c google
				//второй параметр задаёт глубину парсинг гугла
				$html = $this->parsers->google_main_grab($value_search_phrase, $depth_text_google_grab);
				/*
				echo '<pre><b>value_search_phrase_encode</b>';
				print_r($value_search_phrase_encode);
				echo '</pre>';
				*/
				$results = $this->parsers->google_main_parse($html);
				$this->parsers->logs_grab($results, $title = 'google_main_parse');
				//print_r($results);
				/*
				echo '<pre><b>results_main_parse</b>';
				print_r($results);
				echo '</pre>';
				*/
				$results = $this->parsers->google_result_censor($results[0], $value_search_phrase);
				$this->parsers->logs_grab($results, $title = 'google_result_censor');
				/*
				echo '<pre><b>results_result_censor</b>';
				print_r($results);
				echo '</pre>';
				*/
				
				$texts_and_seach_phrase_array = $this->parsers->donor_parser($results);
				$this->parsers->logs_grab($texts_and_seach_phrase_array, $title = 'donor_parser');
				/*
				echo '<pre><b>google_donor_parser<b>';
				print_r($texts_and_seach_phrase_array);
				echo '</pre>';
				*/
				
				$texts_and_seach_phrase_array = $this->parsers->full_text_assembler($texts_and_seach_phrase_array);
				$this->parsers->logs_grab($texts_and_seach_phrase_array, $title = 'full_text_assembler');
				/*
				echo '<pre><b>google_full_text_assembler<b>';
				print_r($texts_and_seach_phrase_array);
				echo '</pre>';
				*/
				$id_texts = $this->parsers->insert_texts_and_search_phrases($texts_and_seach_phrase_array);
				$this->parsers->logs_grab($id_texts, $title = 'insert_texts_and_search_phrases');
				/*
				echo '<pre><b>id_texts<b>';
				print_r($id_texts);
				echo '</pre>';
				*/
				
					
				//echo '<b>СЛОВО - </b>' . $value_search_phrase . '!!<br>';
				
				//########сбор статей rusarticles
				$html = $this->parsers->rusarticles_main_grab($value_search_phrase, $depth_text_rusarticles_grab);	
				
				//echo $html . '!!!!';	
				$urls_articles = $this->parsers->rusarticles_urls($html);
				$this->parsers->logs_grab($urls_articles, $title = 'rusarticles_urls');

				
				$texts_and_seach_phrase_array_1 = $this->parsers->mass_text_and_title_article($urls_articles, $value_search_phrase);
				$this->parsers->logs_grab($texts_and_seach_phrase_array_1, $title = 'mass_text_and_title_article');
				
				$id_texts = $this->parsers->insert_texts_and_search_phrases($texts_and_seach_phrase_array_1);	
			}
		}		
	}
	
	
	public function texts_tr_grab($phrases, $id_site, $their_requests = 1, $sleep = 5){

		$sep = '';
		foreach($phrases as $key => $value_search_phrase){
			$string_search_phrase .= $sep . $value_search_phrase;
			$sep = ' zzzzxxxxzzzz ';
		}
		
		$string_search_phrase = $this->translate->translate_text($string_search_phrase, $from = 'ru', $to = 'en');	
		$phrases_translate = explode(' zzzzxxxxzzzz ', $string_search_phrase);
		


		if($their_requests == 1){
		
			foreach($phrases_translate as $key => $value_search_phrase){
			
				sleep($sleep);
				//time_nanosleep(0, $sleep);
														
				//add 19_11_12 11_25
				//служит для того чтобы в названия категорий не попадали пустоты
				if($value_search_phrase == ' ' OR empty($value_search_phrase) ){
					break;
				}else{
					$url =  'http://' . $_SERVER['HTTP_HOST'] . '/admin/their_requests.php?command=text_tr_grab';		
					
					//$params['value_search_phrase'] = $value_search_phrase;
					$params['value_search_phrase_en'] = $value_search_phrase;
					$params['value_search_phrase_rus'] = $phrases[$key];
					$params['sleep'] = $sleep;
					$params['depth_text_eng_google_grab'] = $this->depth_text_eng_google_grab;				
					$params['id_site'] = $id_site;
					
					$login = $this->config->user['name'];
					$password = md5($this->config->user['pass']);
					//$params['domain'] = 'adm.ekstrim.borrba.ru';		
					
					$html = $this->automatize->curl_secure_connect($url, $login, $password, $params);
					unset($html, $login, $password, $params, $url);
				}		
			}
				
		}else{
			/*
			//отладка
			print_r($phrases);
			*/
			foreach($phrases_translate as $key => $value_search_phrase){	
				
				sleep($sleep);
				//time_nanosleep(0, $sleep);
			
				$depth_text_eng_google_grab = $this->depth_text_eng_google_grab;	


				$value_search_phrase_rus = $phrases[$key];
				$value_search_phrase_eng = $value_search_phrase;					
						
				
				//чтобы фразы вставлялись в базу с нужным id сайта
				$this->parsers->set_id_sites($id_site);
				
					//$value_search_phrase_rus = $value_search_phrase;
					
					//$value_search_phrase_rus = 'варенье';
					//$value_search_phrase_eng = $this->translate->translate_text($value_search_phrase_rus, $from = 'ru', $to = 'en');
					//echo '$value_search_phrase_eng' . $value_search_phrase_eng;
					
					/*
					//отладка
					echo '$value_search_phrase_eng=' . $value_search_phrase_eng;
					*/
					
					//перекодирум в utf8 т.к. данные от рамблер вордстата мы получили в cp1251
					//$value_search_phrase_encode = mb_convert_encoding($value_search_phrase, 'UTF8', 'CP1251');	
					//$value_search_phrase_encode = $value_search_phrase;
					
					//########сбор статей c google
					//второй параметр задаёт глубину парсинг гугла
					$html = $this->parsers->google_main_grab($value_search_phrase_eng, $depth_text_eng_google_grab);
					
					/*
					//отладка
					echo '$depth_text_eng_google_grab=' . $depth_text_eng_google_grab;
					
					//отладка
					echo '<pre><b>html</b>';
					print_r($html);
					echo '</pre>';
					*/
					
					$results = $this->parsers->google_main_parse($html);
					$this->parsers->logs_grab($results, $title = 'google_main_parse');
					//print_r($results);
					
					/*
					//отладка
					echo '<pre><b>results_main_parse</b>';
					print_r($results);
					echo '</pre>';
					*/
					
					$results = $this->parsers->google_result_censor($results[0], $value_search_phrase_eng);
					$this->parsers->logs_grab($results, $title = 'google_result_censor');
					/*
					echo '<pre><b>results_result_censor</b>';
					print_r($results);
					echo '</pre>';
					*/
					
					
					
					
					
					$texts_and_seach_phrase_array = $this->parsers->donor_parser_eng($results);
					$this->parsers->logs_grab($texts_and_seach_phrase_array, $title = 'donor_parser_eng');
					/*
					echo '<pre><b>google_donor_parser<b>';
					print_r($texts_and_seach_phrase_array);
					echo '</pre>';				
					*/
					
					
					
					$texts_and_seach_phrase_array = $this->parsers->full_text_assembler_eng($texts_and_seach_phrase_array);
					$this->parsers->logs_grab($texts_and_seach_phrase_array, $title = 'full_text_assembler_eng');
					/*
					echo '<pre><b>google_full_text_assembler<b>';
					print_r($texts_and_seach_phrase_array);
					echo '</pre>';
					*/
					foreach($texts_and_seach_phrase_array['texts'] as $key => $value){
						if( is_array($value['text']) ){
							//меняем br на условные символы чтобы переводчик не удалил их
							$value['text']['texts'][0]['text'] = str_replace('zzzbrzzz', '<__>' , $value['text']['texts'][0]['text']);
							
							$text_for_translate = $value['text']['texts'][0]['title'] . ' _______ ' . $value['text']['texts'][0]['text'];		
							//echo '<br>text_for_translate!!!!' . $text_for_translate;
							
							sleep($sleep);
							$text_translate = $this->translate->translate_text($text_for_translate, $from = 'en', $to = 'ru');		
							//echo '<br>text_translate!!!!' . $text_translate;						
							$text_translate_array = explode(' _______ ', $text_translate);	
							
							//проверям тексты на наличие русской ключевой фразы
							$temp_text = $this->parsers->key_phrase_censor_translate_text_eng($text_translate_array[1], $value_search_phrase_rus);
							
							if( $temp_text !== false ){
								//возвращаем br
								$text_translate_array[1] = str_replace('<__>', '<br>' , $text_translate_array[1]);
								
								$texts_and_seach_phrase_array['texts'][$key]['text']['texts'][0]['title'] = $text_translate_array[0];
								$texts_and_seach_phrase_array['texts'][$key]['text']['texts'][0]['text'] = $text_translate_array[1];// . ' ------------------ ' . $value_search_phrase_rus;
							}else{
								unset($texts_and_seach_phrase_array['texts'][$key]);
							}

						}else{
							//меняем br на условные символы чтобы переводчик не удалил их
							$value['text'] = str_replace('zzzbrzzz', '<__>' , $value['text']);
							
							$text_for_translate = $value['title'] . ' _______ ' . $value['text'];
							
							sleep($sleep);
							$text_translate = $this->translate->translate_text($text_for_translate, $from = 'en', $to = 'ru');					
							$text_translate_array = explode(' _______ ', $text_translate);	

							//проверям тексты на наличие русской ключевой фразы
							$temp_text = $this->parsers->key_phrase_censor_translate_text_eng($text_translate_array[1], $value_search_phrase_rus);
							
							if( $temp_text !== false ){		
								//возвращаем br
								$text_translate_array[1] = str_replace('<__>', '<br>' , $text_translate_array[1]);
								
								$texts_and_seach_phrase_array['texts'][$key]['title'] = $text_translate_array[0];
								$texts_and_seach_phrase_array['texts'][$key]['text'] = $text_translate_array[1];// . ' ------------------ ' . $value_search_phrase_rus;
							}else{
								unset($texts_and_seach_phrase_array['texts'][$key]);
							}
						}					
						//echo '<br>' . $this->translate->translate_text($text_for_translate, $from = 'en', $to = 'ru');
					}
					
					$texts_and_seach_phrase_array['key_phrase'] = $value_search_phrase_rus;
					//echo '$texts_and_seach_phrase_array' . $texts_and_seach_phrase_array['key_phrase'];
					/*
					echo '<pre><b>texts_and_seach_phrase_array<b>';
					print_r($texts_and_seach_phrase_array);
					echo '</pre>';
					*/
					
					
					
					$id_texts = $this->parsers->insert_texts_and_search_phrases_eng($texts_and_seach_phrase_array);
					$this->parsers->logs_grab($id_texts, $title = 'insert_texts_and_search_phrases_eng');				
			}
		}		
	}
	
	
	public function texts_tr_ua_grab($phrases, $id_site, $their_requests = 1, $sleep = 5){	
	
		$sep = '';
		foreach($phrases as $key => $value_search_phrase){
			$string_search_phrase .= $sep . $value_search_phrase;
			$sep = ' zzzzxxxxzzzz ';
		}
		
		$string_search_phrase = $this->translate->translate_text($string_search_phrase, $from = 'ru', $to = 'uk');	
		$phrases_translate = explode(' zzzzxxxxzzzz ', $string_search_phrase);
		
		

		if($their_requests == 1){
		
			foreach($phrases_translate as $key => $value_search_phrase){
			
				sleep($sleep);
				//time_nanosleep(0, $sleep);
														
				//add 19_11_12 11_25
				//служит для того чтобы в названия категорий не попадали пустоты
				if($value_search_phrase == ' ' OR empty($value_search_phrase) ){
					break;
				}else{
					$url =  'http://' . $_SERVER['HTTP_HOST'] . '/admin/their_requests.php?command=text_tr_ua_grab';		
					
					//$params['value_search_phrase'] = $value_search_phrase;
					$params['value_search_phrase_ua'] = $value_search_phrase;
					$params['value_search_phrase_rus'] = $phrases[$key];
					$params['sleep'] = $sleep;
					$params['depth_text_ua_google_grab'] = $this->depth_text_ua_google_grab;				
					$params['id_site'] = $id_site;
					
					$login = $this->config->user['name'];
					$password = md5($this->config->user['pass']);
					//$params['domain'] = 'adm.ekstrim.borrba.ru';		
					
					$html = $this->automatize->curl_secure_connect($url, $login, $password, $params);
					unset($html, $login, $password, $params, $url);
				}		
			}
				
		}else{
			/*
			//отладка
			print_r($phrases);
			*/
			foreach($phrases_translate as $key => $value_search_phrase){	
			
				sleep($sleep);
				//time_nanosleep(0, $sleep);
			
				$depth_text_ua_google_grab = $this->depth_text_ua_google_grab;	

				$value_search_phrase_rus = $phrases[$key];
				$value_search_phrase_ua = $value_search_phrase;				
						
				
				//чтобы фразы вставлялись в базу с нужным id сайта
				$this->parsers->set_id_sites($id_site);
				
					//$value_search_phrase_rus = $value_search_phrase;
					
					//$value_search_phrase_rus = 'варенье';
					//$value_search_phrase_ua = $this->translate->translate_text($value_search_phrase_rus, $from = 'ru', $to = 'uk');
					//echo '$value_search_phrase_ua=' . $value_search_phrase_ua;
					
					/*
					//отладка
					echo '$value_search_phrase_eng=' . $value_search_phrase_eng;
					*/
					
					//перекодирум в utf8 т.к. данные от рамблер вордстата мы получили в cp1251
					//$value_search_phrase_encode = mb_convert_encoding($value_search_phrase, 'UTF8', 'CP1251');	
					//$value_search_phrase_encode = $value_search_phrase;
					
					//########сбор статей c google
					//второй параметр задаёт глубину парсинг гугла
					$html = $this->parsers->google_main_grab($value_search_phrase_ua, $depth_text_ua_google_grab, $lang = 'ua'); //. ' site:.ua'
					
					
					//отладка
					//echo '$depth_text_ua_google_grab=' . $depth_text_ua_google_grab;
					/*
					//отладка
					echo '<pre><b>html</b>';
					print_r($html);
					echo '</pre>';
					*/
					
					$results = $this->parsers->google_main_parse($html);
					$this->parsers->logs_grab($results, $title = 'google_main_parse_ua');
					//print_r($results);
					
					/*
					//отладка
					echo '<pre><b>results_main_parse</b>';
					print_r($results);
					echo '</pre>';
					*/
					
					$results = $this->parsers->google_result_censor($results[0], $value_search_phrase_ua);
					$this->parsers->logs_grab($results, $title = 'google_result_censor_ua');
					/*
					echo '<pre><b>results_result_censor</b>';
					print_r($results);
					echo '</pre>';
					*/
					
					
					
					
					
					$texts_and_seach_phrase_array = $this->parsers->donor_parser_ua($results);
					$this->parsers->logs_grab($texts_and_seach_phrase_array, $title = 'donor_parser_ua');
					
					/*
					echo '<pre><b>donor_parser_ua<b>';
					print_r($texts_and_seach_phrase_array);
					echo '</pre>';				
					*/
					
					
					
					$texts_and_seach_phrase_array = $this->parsers->full_text_assembler_ua($texts_and_seach_phrase_array);
					$this->parsers->logs_grab($texts_and_seach_phrase_array, $title = 'full_text_assembler_ua');
					
					/*
					echo '<pre><b>full_text_assembler_ua<b>';
					print_r($texts_and_seach_phrase_array);
					echo '</pre>';
					*/
					
					foreach($texts_and_seach_phrase_array['texts'] as $key => $value){
						if( is_array($value['text']) ){
							//меняем br на условные символы чтобы переводчик не удалил их
							$value['text']['texts'][0]['text'] = str_replace('zzzbrzzz', '<__>' , $value['text']['texts'][0]['text']);
							
							$text_for_translate = $value['text']['texts'][0]['title'] . ' _______ ' . $value['text']['texts'][0]['text'];
							
							//отладка
							//echo '<br>text_for_translate!!!!' . $text_for_translate;
							sleep($sleep);
							$text_translate = $this->translate->translate_text($text_for_translate, $from = 'uk', $to = 'ru');	
							
							//отладка
							//echo '<br>text_translate!!!!' . $text_translate;
							//echo 1;
							
							$text_translate_array = explode('_______ ', $text_translate);	
							
							//проверям тексты на наличие русской ключевой фразы
							$temp_text = $this->parsers->key_phrase_censor_translate_text_eng($text_translate_array[1], $value_search_phrase_rus);
							
							//отладка
							$temp_text = 1;
							//echo '<br><br>$text_translate_array = ' . $temp_text;
							//echo '<br><br>$text_translate_array[1] = ' . $text_translate_array[1];
							
							if( $temp_text !== false ){
								//возвращаем br
								$text_translate_array[1] = str_replace('<__>', '<br>' , $text_translate_array[1]);
								
								$texts_and_seach_phrase_array['texts'][$key]['text']['texts'][0]['title'] = $text_translate_array[0];
								$texts_and_seach_phrase_array['texts'][$key]['text']['texts'][0]['text'] = $text_translate_array[1];// . ' ------------------ ' . $value_search_phrase_rus;
							}else{
								unset($texts_and_seach_phrase_array['texts'][$key]);
							}

						}else{
							//меняем br на условные символы чтобы переводчик не удалил их
							$value['text'] = str_replace('zzzbrzzz', '<__>' , $value['text']);
							
							$text_for_translate = $value['title'] . ' _______ ' . $value['text'];
							//отладка
							//echo '<br>text_for_translate!!!!' . $text_for_translate;
							sleep($sleep);
							$text_translate = $this->translate->translate_text($text_for_translate, $from = 'uk', $to = 'ru');
							//отладка
							//echo '<br>text_translate!!!!' . $text_translate;	
							//echo 1;
							
							$text_translate_array = explode('_______ ', $text_translate);	

							//проверям тексты на наличие русской ключевой фразы
							$temp_text = $this->parsers->key_phrase_censor_translate_text_eng($text_translate_array[1], $value_search_phrase_rus);
							
							//отладка
							//echo '<br><br>$text_translate_array = ' . $temp_text;
							//echo '<br><br>$text_translate_array[1] = ' . $text_translate_array[1];
							
							if( $temp_text !== false ){		
								//возвращаем br
								$text_translate_array[1] = str_replace('<__>', '<br>' , $text_translate_array[1]);
								
								$texts_and_seach_phrase_array['texts'][$key]['title'] = $text_translate_array[0];
								$texts_and_seach_phrase_array['texts'][$key]['text'] = $text_translate_array[1];// . ' ------------------ ' . $value_search_phrase_rus;
							}else{
								unset($texts_and_seach_phrase_array['texts'][$key]);
							}
						}					
						//echo '<br>' . $this->translate->translate_text($text_for_translate, $from = 'en', $to = 'ru');
					}
					
					$texts_and_seach_phrase_array['key_phrase'] = $value_search_phrase_rus;
					//echo '$texts_and_seach_phrase_array' . $texts_and_seach_phrase_array['key_phrase'];
					
					/*
					echo '<pre><b>texts_and_seach_phrase_array<b>';
					print_r($texts_and_seach_phrase_array);
					echo '</pre>';
					*/
					
					
					
					$id_texts = $this->parsers->insert_texts_and_search_phrases_ua($texts_and_seach_phrase_array);
					$this->parsers->logs_grab($id_texts, $title = 'insert_texts_and_search_phrases_ua');		
										
			}
		}		
	}
	
	
	public function texts_tr_by_grab($phrases, $id_site, $their_requests = 1, $sleep = 5){	

		$sep = '';
		foreach($phrases as $key => $value_search_phrase){
			$string_search_phrase .= $sep . $value_search_phrase;
			$sep = ' zzzzxxxxzzzz ';
		}
		
		$string_search_phrase = $this->translate->translate_text($string_search_phrase, $from = 'ru', $to = 'be');	
		$phrases_translate = explode(' zzzzxxxxzzzz ', $string_search_phrase);
		
		/*
		//отладка
		echo '<pre><b>phrases</b>';
		print_r($phrases);
		echo '</pre>';
		//die;
		
		
		//отладка
		echo '<pre><b>string_search_phrase</b>';
		print_r($string_search_phrase);
		echo '</pre>';

		//отладка
		echo '<pre><b>phrases_translate</b>';
		print_r($phrases_translate);
		echo '</pre>';		
		die;
		*/
		
		//изменено вернуть
		if($their_requests == 1){
		
			foreach($phrases_translate as $key => $value_search_phrase){
			
				sleep($sleep);
				//time_nanosleep(0, $sleep);
														
				//add 19_11_12 11_25
				//служит для того чтобы в названия категорий не попадали пустоты
				if($value_search_phrase == ' ' OR empty($value_search_phrase) ){
					break;
				}else{
					$url =  'http://' . $_SERVER['HTTP_HOST'] . '/admin/their_requests.php?command=text_tr_by_grab';		
					
					//$params['value_search_phrase'] = $value_search_phrase;
					$params['value_search_phrase_by'] = $value_search_phrase;
					$params['value_search_phrase_rus'] = $phrases[$key];
					$params['sleep'] = $sleep;
					$params['depth_text_by_google_grab'] = $this->depth_text_by_google_grab;				
					$params['id_site'] = $id_site;					
					
					$login = $this->config->user['name'];
					$password = md5($this->config->user['pass']);
					//$params['domain'] = 'adm.ekstrim.borrba.ru';		
					
					$html = $this->automatize->curl_secure_connect($url, $login, $password, $params);
					unset($html, $login, $password, $params, $url);
				}		
			}
				
		}else{
			/*
			//отладка
			print_r($phrases);
			*/
			foreach($phrases_translate as $key => $value_search_phrase){
			
				$value_search_phrase_rus = $phrases[$key];
				$value_search_phrase_by = $value_search_phrase;
			
				sleep($sleep);
				//time_nanosleep(0, $sleep);
				
				$depth_text_by_google_grab = $this->depth_text_by_google_grab;				
						
				
				//чтобы фразы вставлялись в базу с нужным id сайта
				$this->parsers->set_id_sites($id_site);
				
					//$value_search_phrase_rus = $value_search_phrase;
					
					//отладка 
					//echo '$value_search_phrase_rus=' . $value_search_phrase_rus;
					
					
					//$value_search_phrase_rus = 'варенье';
					//$value_search_phrase_by = $this->translate->translate_text($value_search_phrase_rus, $from = 'ru', $to = 'be');
					//echo '$value_search_phrase_ua=' . $value_search_phrase_ua;
					
					/*
					//отладка 
					echo '$value_search_phrase_by=' . $value_search_phrase_by;
					//die;
					*/
					
					//перекодирум в utf8 т.к. данные от рамблер вордстата мы получили в cp1251
					//$value_search_phrase_encode = mb_convert_encoding($value_search_phrase, 'UTF8', 'CP1251');	
					//$value_search_phrase_encode = $value_search_phrase;
					
					//########сбор статей c google
					//второй параметр задаёт глубину парсинг гугла
					$html = $this->parsers->google_main_grab($value_search_phrase_by, $depth_text_by_google_grab, $lang = 'by'); //. ' site:.ua'
					
					
					//отладка
					//echo '$depth_text_ua_google_grab=' . $depth_text_ua_google_grab;
					
					/*
					//отладка
					echo '<pre><b>html</b>';
					print_r($html);
					echo '</pre>';
					//die;
					*/
					
					$results = $this->parsers->google_main_parse($html);
					$this->parsers->logs_grab($results, $title = 'google_main_parse_by');
					//print_r($results);
					
					/*
					//отладка 
					echo '<pre><b>results_main_parse</b>';
					print_r($results);
					echo '</pre>';
					//die;
					*/
					
					
					$results = $this->parsers->google_result_censor($results[0], $value_search_phrase_by);
					$this->parsers->logs_grab($results, $title = 'google_result_censor_by');
					
					/*
					//отладка
					echo '<pre><b>results_result_censor</b>';
					print_r($results);
					echo '</pre>';
					//die;
					*/
					
					
					
					
					
					$texts_and_seach_phrase_array = $this->parsers->donor_parser_by($results);
					$this->parsers->logs_grab($texts_and_seach_phrase_array, $title = 'donor_parser_by');
					
					/*
					echo '<pre><b>donor_parser_ua<b>';
					print_r($texts_and_seach_phrase_array);
					echo '</pre>';				
					die;
					*/
					
					
					$texts_and_seach_phrase_array = $this->parsers->full_text_assembler_by($texts_and_seach_phrase_array);
					$this->parsers->logs_grab($texts_and_seach_phrase_array, $title = 'full_text_assembler_by');
					
					
					/*
					echo '<pre><b>full_text_assembler_ua<b>';
					print_r($texts_and_seach_phrase_array);
					echo '</pre>';
					//die;
					*/
					
					
					foreach($texts_and_seach_phrase_array['texts'] as $key => $value){
						if( is_array($value['text']) ){
							//меняем br на условные символы чтобы переводчик не удалил их
							$value['text']['texts'][0]['text'] = str_replace('zzzbrzzz', '<__>' , $value['text']['texts'][0]['text']);
							
							$text_for_translate = $value['text']['texts'][0]['title'] . ' _______ ' . $value['text']['texts'][0]['text'];
							
							//отладка
							//echo '<br>text_for_translate!!!!' . $text_for_translate;
							sleep($sleep);
							$text_translate = $this->translate->translate_text($text_for_translate, $from = 'be', $to = 'ru');	
							
							/*
							//отладка
							echo '<br>text_translate!!!!' . $text_translate;
							die;
							//echo 1;
							*/
							
							$text_translate_array = explode('_______ ', $text_translate);	
							
							//проверям тексты на наличие русской ключевой фразы
							//временно отключено
							$temp_text = 1;
							$temp_text = $this->parsers->key_phrase_censor_translate_text_eng($text_translate_array[1], $value_search_phrase_rus);
							
							//отладка
							
							//echo '<br><br>$text_translate_array = ' . $temp_text;
							//echo '<br><br>$text_translate_array[1] = ' . $text_translate_array[1];
							
							if( $temp_text !== false ){
								//возвращаем br
								$text_translate_array[1] = str_replace('<__>', '<br>' , $text_translate_array[1]);
								
								$texts_and_seach_phrase_array['texts'][$key]['text']['texts'][0]['title'] = $text_translate_array[0];
								$texts_and_seach_phrase_array['texts'][$key]['text']['texts'][0]['text'] = $text_translate_array[1];// . ' ------------------ ' . $value_search_phrase_rus;
							}else{
								unset($texts_and_seach_phrase_array['texts'][$key]);
							}

						}else{
							//меняем br на условные символы чтобы переводчик не удалил их
							$value['text'] = str_replace('zzzbrzzz', '<__>' , $value['text']);
							
							$text_for_translate = $value['title'] . ' _______ ' . $value['text'];
							//отладка
							//echo '<br>text_for_translate!!!!' . $text_for_translate;
							sleep($sleep);
							$text_translate = $this->translate->translate_text($text_for_translate, $from = 'be', $to = 'ru');
							
							/*
							//отладка
							echo '<br>text_translate!!!!' . $text_translate;	
							die;
							//echo 1;
							*/
							
							$text_translate_array = explode('_______ ', $text_translate);	

							//проверям тексты на наличие русской ключевой фразы
							//временно отключено
							$temp_text = 1;
							$temp_text = $this->parsers->key_phrase_censor_translate_text_eng($text_translate_array[1], $value_search_phrase_rus);
							
							//отладка
							//echo '<br><br>$text_translate_array = ' . $temp_text;
							//echo '<br><br>$text_translate_array[1] = ' . $text_translate_array[1];
							
							if( $temp_text !== false ){		
								//возвращаем br
								$text_translate_array[1] = str_replace('<__>', '<br>' , $text_translate_array[1]);
								
								$texts_and_seach_phrase_array['texts'][$key]['title'] = $text_translate_array[0];
								$texts_and_seach_phrase_array['texts'][$key]['text'] = $text_translate_array[1];// . ' ------------------ ' . $value_search_phrase_rus;
							}else{
								unset($texts_and_seach_phrase_array['texts'][$key]);
							}
						}					
						//echo '<br>' . $this->translate->translate_text($text_for_translate, $from = 'en', $to = 'ru');
					}
					
					$texts_and_seach_phrase_array['key_phrase'] = $value_search_phrase_rus;
					//echo '$texts_and_seach_phrase_array' . $texts_and_seach_phrase_array['key_phrase'];
					
					/*
					echo '<pre><b>texts_and_seach_phrase_array<b>';
					print_r($texts_and_seach_phrase_array);
					echo '</pre>';
					die;
					*/
					
					
					$id_texts = $this->parsers->insert_texts_and_search_phrases_by($texts_and_seach_phrase_array);
					$this->parsers->logs_grab($id_texts, $title = 'insert_texts_and_search_phrases_by');		
										
			}
		}		
	}
	
	
	public function texts_eng_grab($phrases, $id_site, $their_requests = 1){
	
		###тест
		echo '<pre><b>phrases</b>';
		print_r($phrases);
		echo '</pre>';
		
		$phrases_imp = implode('. ', $phrases);
		
		###тест
		echo '<pre><b>phrases</b>';
		print_r($phrases_imp);
		echo '</pre>';
		
		$phrases_imp_eng = $this->translate->translate_text($phrases_imp, 'ru', 'en');
		###тест
		echo '$phrases_imp_eng=' . $phrases_imp_eng;
		
		$this->sql_model->logs_grab($phrases_imp_eng, $title = 'phrases_imp_eng');
		if( !empty($phrases_imp_eng) ){
			$phrases_eng = explode('. ', $phrases_imp_eng);
		}
		
		###тест
		echo '<pre><b>phrases_eng</b>';
		print_r($phrases_eng);
		echo '</pre>';
		
		foreach($phrases as $key => $value){			
			$phrases_ru_en[$key]['ru'] = $value;
			$phrases_ru_en[$key]['en'] = $phrases_eng[$key];
		}
		
		###тест
		echo '<pre><b>phrases_ru_en</b>';
		print_r($phrases_ru_en);
		echo '</pre>';
		
		if($their_requests == 1){
				
			foreach($phrases_eng as $key => $value_search_phrase){
				//служит для того чтобы в названия категорий не попадали пустоты
				if($value_search_phrase == ' ' OR empty($value_search_phrase) ){
					break;
				}else{
					$url =  'http://' . $_SERVER['HTTP_HOST'] . '/admin/their_requests.php?command=texts_eng_grab';		
					
					$params['value_search_phrase'] = $value_search_phrase;
					$params['depth_text_eng_google_grab'] = $this->depth_text_eng_google_grab;					
					$params['id_site'] = $id_site;
						
					$login = $this->config->user['name'];
					$password = md5($this->config->user['pass']);
					//$params['domain'] = 'adm.ekstrim.borrba.ru';		
						
					$html = $this->automatize->curl_secure_connect($url, $login, $password, $params);
					unset($html, $login, $password, $params, $url);
				}	
			}			
			
		}else{
			foreach($phrases_eng as $key => $value_search_phrase){
				
				
				$depth_text_eng_google_grab = $this->depth_text_eng_google_grab;					
				
				//чтобы фразы вставлялись в базу с нужным id сайта
				$this->parsers->set_id_sites($id_site);
				//$value_search_phrase_eng = 'jam';
				
				$value_search_phrase_eng = $this->translate->translate_text($value_search_phrase, $from = 'ru', $to = 'en');
				//echo '$value_search_phrase_eng' . $value_search_phrase_eng;
				
				//перекодирум в utf8 т.к. данные от рамблер вордстата мы получили в cp1251
				//$value_search_phrase_encode = mb_convert_encoding($value_search_phrase, 'UTF8', 'CP1251');	
				//$value_search_phrase_encode = $value_search_phrase;
				
				//########сбор статей c google
				//второй параметр задаёт глубину парсинг гугла
				$html = $this->parsers->google_main_grab($value_search_phrase_eng, $depth_text_eng_google_grab);
				/*
				echo '<pre><b>value_search_phrase_encode</b>';
				print_r($html);
				echo '</pre>';
				*/
				$results = $this->parsers->google_main_parse($html);
				$this->parsers->logs_grab($results, $title = 'google_main_parse');
				//print_r($results);
				
				/*
				echo '<pre><b>results_main_parse</b>';
				print_r($results);
				echo '</pre>';
				die;
				*/
				
				$results = $this->parsers->google_result_censor($results[0], $value_search_phrase_eng);
				$this->parsers->logs_grab($results, $title = 'google_result_censor');
				/*
				echo '<pre><b>results_result_censor</b>';
				print_r($results);
				echo '</pre>';
				*/			
				
				$texts_and_seach_phrase_array = $this->parsers->donor_parser_eng($results);
				$this->parsers->logs_grab($texts_and_seach_phrase_array, $title = 'donor_parser_eng');
				/*
				echo '<pre><b>google_donor_parser<b>';
				print_r($texts_and_seach_phrase_array);
				echo '</pre>';				
				*/
				
				
				
				$texts_and_seach_phrase_array = $this->parsers->full_text_assembler_eng($texts_and_seach_phrase_array);
				$this->parsers->logs_grab($texts_and_seach_phrase_array, $title = 'full_text_assembler_eng');
				/*
				echo '<pre><b>google_full_text_assembler<b>';
				print_r($texts_and_seach_phrase_array);
				echo '</pre>';
				*/
				foreach($texts_and_seach_phrase_array['texts'] as $key => $value){
					if( is_array($value['text']) ){
						$text_for_translate = $value['text']['texts'][0]['title'] . ' _______ ' . $value['text']['texts'][0]['text'];		
						
						$text_translate = $this->translate->translate_text($text_for_translate, $from = 'en', $to = 'ru');		
												
						$text_translate_array = explode(' _______ ', $text_translate);		
						$texts_and_seach_phrase_array['texts'][$key]['text']['texts'][0]['title'] = $text_translate_array[0];
						$texts_and_seach_phrase_array['texts'][$key]['text']['texts'][0]['text'] = $text_translate_array[1];
					}else{
						$text_for_translate = $value['title'] . ' _______ ' . $value['text'];
						
						$text_translate = $this->translate->translate_text($text_for_translate, $from = 'en', $to = 'ru');					
						$text_translate_array = explode(' _______ ', $text_translate);		
						$texts_and_seach_phrase_array['texts'][$key]['title'] = $text_translate_array[0];
						$texts_and_seach_phrase_array['texts'][$key]['text'] = $text_translate_array[1];
					}					
					
				}
				
				$texts_and_seach_phrase_array['key_phrase'] = $phrases_ru_en[$key]['ru'];
				//echo '$texts_and_seach_phrase_array' . $texts_and_seach_phrase_array['key_phrase'];
				
				/*
				###тест
				echo '<pre><b>texts_and_seach_phrase_array<b>';
				print_r($texts_and_seach_phrase_array);
				echo '</pre>';
				*/
				
				
				
				$id_texts = $this->parsers->insert_texts_and_search_phrases_eng($texts_and_seach_phrase_array);
				$this->parsers->logs_grab($id_texts, $title = 'insert_texts_and_search_phrases_eng');
			}
		}		
	}
	
	
	


	public function fotos_grab($phrases, $id_site, $their_requests = 1, $sleep = 2){
		//echo 'Начался сбор фото<br>';
		//сбор фото
		if($their_requests == 1){
			foreach($phrases as $key => $value_search_phrase){

				sleep($sleep);
				//time_nanosleep(0, $sleep);
			
				if($value_search_phrase == ' ' OR empty($value_search_phrase) ){
					break;
				}else{
					$url =  'http://' . $_SERVER['HTTP_HOST'] . '/admin/their_requests.php?command=fotos_grab';					
					$params['value_search_phrase'] = $value_search_phrase;			
					$params['id_site'] = $id_site;
					$login = $this->config->user['name'];
					$password = md5($this->config->user['pass']);					
					
					$html = $this->automatize->curl_secure_connect($url, $login, $password, $params);
					unset($html, $login, $password, $params, $url);	
				}					
			}
		}else{
			foreach($phrases as $key => $value_search_phrase){

				sleep($sleep);
				//time_nanosleep(0, $sleep);
				
				//echo 'Cбор фото цикл' . $key . '<br>';
				//перекодирум в utf8 т.к. данные от рамблер вордстата мы получили в cp1251
				//$value_search_phrase_encode = mb_convert_encoding($value_search_phrase, 'UTF8', 'CP1251');					
					
				//чтобы фразы вставлялись в базу с нужным id сайта
				$this->parsers->set_id_sites($id_site);	
					
				//заберает html с изображениями из гугл
				$html_imgs = $this->parsers->google_img_html_grab($value_search_phrase);
					
				//устанавливаем поисковую фразу для проверки её вхождения в описание изображения
				$this->parsers->set_key_phrase($value_search_phrase);
				//парсить урлы фоток из куска html
				$urls_array = $this->parsers->google_img_parse($html_imgs);
				$this->parsers->logs_grab($urls_array, $title = 'google_img_parse');
				/*	## отладка			
				echo '<pre>';
				print_r($html_imgs);
				echo '</pre>';
				*/
					
				/*
				echo '<pre>$urls_array';
				print_r($urls_array);
				echo '</pre>';
				*/
				
				$id_search_phrase = $this->parsers->select_or_insert_search_phrase($value_search_phrase);
				$this->parsers->logs_grab($id_search_phrase, $title = 'select_or_insert_search_phrase');
				//скачивает фотки с серверов гугл
				$this->parsers->google_img_grab($urls_array, $value_search_phrase, $id_search_phrase, $id_site);	
			}
		}		
	}	
	

	public function videos_grab($phrases, $id_site, $their_requests = 1, $sleep = 4){

		if($their_requests == 1){
			//сбор видео
			foreach($phrases as $key => $value_search_phrase){
			
				sleep($sleep);
				//time_nanosleep(0, $sleep);
			
				if($value_search_phrase == ' ' OR empty($value_search_phrase) ){
					break;
				}else{
					$url =  'http://' . $_SERVER['HTTP_HOST'] . '/admin/their_requests.php?command=videos_grab';					
					$params['value_search_phrase'] = $value_search_phrase;			
					$params['id_site'] = $id_site;
					$login = $this->config->user['name'];
					$password = md5($this->config->user['pass']);					
					
					$html = $this->automatize->curl_secure_connect($url, $login, $password, $params);
					unset($html, $login, $password, $params, $url);		
				}			
			}
		}else{
			foreach($phrases as $key => $value_search_phrase){
			
				sleep($sleep);
				//time_nanosleep(0, $sleep);
				
				//echo 'Cбор фото цикл' . $key . '<br>';
				//перекодирум в utf8 т.к. данные от рамблер вордстата мы получили в cp1251
				//$value_search_phrase_encode = mb_convert_encoding($value_search_phrase, 'UTF8', 'CP1251');				
				
				//чтобы фразы вставлялись в базу с нужным id сайта
				$this->parsers->set_id_sites($id_site);
				
				//перекодирум в utf8 т.к. данные от рамблер вордстата мы получили в cp1251
				//$value_search_phrase_encode = mb_convert_encoding($value_search_phrase, 'UTF8', 'CP1251');				
					
				//установка ключевого слова для проверки его наличия в описании видео и в названии
				$this->parsers->set_key_phrase($value_search_phrase);			

				$html_videos = $this->parsers->google_video_html_grab($value_search_phrase);		
				//echo $html_videos;
				$this->parsers->logs_grab('Ok!', $title = 'google_video_html_grab');
					
				$html_video_fragment = $this->parsers->google_video_parse($html_videos);
				$this->parsers->logs_grab('Ok!', $title = 'google_video_parse');
				//$this->sql_model->logs_grab($html_video_fragment, $title = 'google_video_parse');
				//echo $html_video_fragment;
					
				/*
				###отладка
				echo '<pre>html_video_fragment';
				print_r($html_video_fragment);
				echo '<pre>';
				*/
					
				$id_search_phrase = $this->parsers->select_or_insert_search_phrase($value_search_phrase);
					
				//echo '$id_search_phrase: ' . $id_search_phrase . '<br>';
					
				$this->parsers->google_fragments_html_video_parse($html_video_fragment, $id_search_phrase, $id_site);
				$this->parsers->logs_grab('Ok!', $title = 'google_fragments_html_video_parse');
			}
		}
	}
}

?>