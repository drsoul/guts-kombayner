<?
define( '_EXEC', 1 );

//это тестовый сайт?
$this_test_site = 1;
//индентификатор сайта
$id_site = 1;
//статей на страницу
$per_page = 5;	
//число ссылок от активной страницы
$num_page = 4;	
//количество изображений в шапке
$header_images_count = 7;	
//изображений на страницу галереи
$per_page_gallery = 24;
//количество пунктов меню
$menu_items = 100;

require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/smarty/Smarty.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/models/sql_model.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/helpers.class.php');

$smarty = new Smarty;
$sql = new Sql_model;
$helpers = new Helpers;

$smarty_cat_list->debugging = false;
$smarty_cat_list->caching = false;
$smarty_cat_list->cache_lifetime = 120;

$smarty_cat_list->template_dir = $_SERVER['DOCUMENT_ROOT'] . "/site/templates/";
$smarty_cat_list->compile_dir = $_SERVER['DOCUMENT_ROOT'] . "/site/templates_c/";
$smarty_cat_list->config_dir = $_SERVER['DOCUMENT_ROOT'] . "/site/configs/";
$smarty_cat_list->cache_dir = $_SERVER['DOCUMENT_ROOT'] . "/site/cache/";


$smarty->assign("id_site", $id_site);

$site = $sql->select_site($id_site);

//url который будет подставляться в ссылки
$url = 'http://' . $site['domain'] . '/';

//домен
$smarty->assign("domain", $site['domain']);
//делаем отдельный домен для изображений из-за проблемы с поддоменами
$domain_array = explode('/', $site['domain']);
if(is_array($domain_array)){
	$domain_for_img = $domain_array[0];
}else{
	$domain_for_img = $domain_array;
}
$smarty->assign("domain_for_img", $domain_for_img);

//название сайта
$smarty->assign("name_site", $site['name']);


//биржи ссылок
if($site['links_sape'] == 1){

    if (!defined('SAPE_USER')){
       define('SAPE_USER', 'sape'); 
    }
	
	if($this_test_site == 1){
		require_once($_SERVER['DOCUMENT_ROOT'].'/site/money_house/' . SAPE_USER . '/sape.php'); 
	}else{
		require_once($_SERVER['DOCUMENT_ROOT'].'/money_house/' . SAPE_USER . '/sape.php'); 
	}    
	
    $o['charset'] = 'UTF-8';
    $sape = new SAPE_client($o);
    unset($o);	

	$links_block_1 = $sape->return_links();
	
	//add 10_11_12 08_00
	//чтобы можно было применить проверку на пустоту и не выводить ссылочный блок, если нет ссылок
	preg_match ('|<!--.+-->|us', $links_block_1, $matches);
	$links_block_1_comment = $matches[0];
	$links_block_1 = preg_replace('|<!--.+-->|us', '', $links_block_1);
	
	$smarty->assign("links_block_1_comment", $links_block_1_comment);
	$smarty->assign("links_block_1", $links_block_1);
	
}

if($site['links_mainlink'] == 1){

    if (!defined('MAINLINK_USER')){
       define('MAINLINK_USER', 'mainlink'); 
    }
	
	if($this_test_site == 1){
		require_once($_SERVER['DOCUMENT_ROOT'].'/site/money_house/' . MAINLINK_USER . '/mainlink.php');
	}else{
		require_once($_SERVER['DOCUMENT_ROOT'].'/money_house/' . MAINLINK_USER . '/mainlink.php');
	}  	
	/*
	//было до 25_01_13-13_11
	$ml->Set_Config(array(charset=>'utf-8'));//, debugmode=>true));
	$ml->Get_Links();
	$links_block_2 = $ml->Get_Links();	
	*/
	
	$ml_conf['USERNAME'] = '3EAFE7A979C801DE51730FA748B5F9AE';
	$ml_conf['charset'] = 'utf';
	
	$client_lnk = new MLClient($ml_conf); 
	$links_block_2 = $client_lnk->build_links(); 
	
	
	$links_block_2_test = preg_replace('|<!--.+-->|us', '', $links_block_2);	
	
}

if($site['links_linkfeed'] == 1){

	define('LINKFEED_USER', 'linkfeed');

	if($this_test_site == 1){
		require_once($_SERVER['DOCUMENT_ROOT'].'/site/money_house/' . LINKFEED_USER . '/linkfeed.php');
	}else{
		require_once($_SERVER['DOCUMENT_ROOT'].'/money_house/' . LINKFEED_USER . '/linkfeed.php');
	}  	
	
	global $linkfeed;	
	$linkfeed = new LinkfeedClient();	
	
	
	//add 10_11_12 08_00
	//чтобы можно было применить проверку на пустоту и не выводить ссылочный блок, если нет ссылок
	if( empty($links_block_2_test) ){
		$separator_links = '';
	}else{
		$separator_links = '<br>';
	}	
	unset($links_block_2_test);
	
	preg_match ('|<!--.+-->|us', $links_block_2, $matches);
	$links_block_2_comment_mainlink = $matches[0];
	
	$linkfeed_str_code = $linkfeed->return_links();
	preg_match ('|<!--.+-->|us', $linkfeed_str_code, $matches);
	$links_block_2_comment_linkfeed = $matches[0];
	
	$links_block_2 .= $separator_links . $linkfeed_str_code;
	
	//add 10_11_12 08_00
	//чтобы можно было применить проверку на пустоту и не выводить ссылочный блок, если нет ссылок

	$links_block_2 = preg_replace('|<!--.+-->|us', '', $links_block_2);		
}

$smarty->assign("links_block_2", $links_block_2);
$smarty->assign("links_block_2_comment_mainlink", $links_block_2_comment_mainlink);
$smarty->assign("links_block_2_comment_linkfeed", $links_block_2_comment_linkfeed);

if($site['links_setlinks'] == 1){

	define('SETLINKS_USER', 'setlinks');

	if($this_test_site == 1){
		require_once($_SERVER['DOCUMENT_ROOT'].'/site/money_house/' . SETLINKS_USER . '/slclient.php');
	}else{
		require_once($_SERVER['DOCUMENT_ROOT'].'/money_house/' . SETLINKS_USER . '/slclient.php');
	}  
	
	$sl = new SLClient();
	$links_block_3 = $sl->GetLinks();
	
	//add 10_11_12 08_00
	//чтобы можно было применить проверку на пустоту и не выводить ссылочный блок, если нет ссылок
	preg_match ('|<!--.+-->|us', $links_block_3, $matches);
	$links_block_3_comment = $matches[0];
	
	$links_block_3 = preg_replace('|<!--.+-->|us', '', $links_block_3);		
	
	$smarty->assign("links_block_3_comment", $links_block_3_comment);
	
	$smarty->assign("links_block_3", $links_block_3);
}


//блок рекламы
if($site['ads'] == 1){
	
	//update 02_11_12-02_39
	$site_ads = $sql->select_site_ads($id_site);
	
	$ads_block_1 = '';
	$ads_block_2 = '';
	$ads_block_3 = '';
	
	foreach($site_ads as $key => $value){
		if($value['id_position'] == 1){
			$ads_block_1 .= $value['code'] . ' ';
		}elseif($value['id_position'] == 2){
			$ads_block_2 .= $value['code'] . ' ';
		}elseif($value['id_position'] == 3){
			$ads_block_3 .= $value['code'] . ' ';
		}
	}
	
	$smarty->assign("ads_block_1", $ads_block_1);
	$smarty->assign("ads_block_2", $ads_block_2);
	$smarty->assign("ads_block_3", $ads_block_3);
	
	/*
	if(!empty($site_ads[0])){
		
		$ads_block_1 = $site_ads[0]['code'];
		$smarty->assign("ads_block_1", $ads_block_1);	
	}
	if(!empty($site_ads[1])){
		
		$ads_block_2 = $site_ads[1]['code'];
		$smarty->assign("ads_block_2", $ads_block_2);	
	}
	if(!empty($site_ads[2])){
		
		$ads_block_3 = $site_ads[2]['code'];
		$smarty->assign("ads_block_3", $ads_block_3);	
	}
	*/
	
}

header("Content-Type: text/html; charset=utf-8");
/*
$texts_imges_videos = $sql->select_phrases_texts_images_videos_by_id_site($id_site);
$smarty->assign("tests_imges_videos", $texts_imges_videos);
*/

if(!empty($_GET['article'])){
	$_GET['article'] = (int)$_GET['article'];
	$_GET['p'] = (int)$_GET['p'];
	//текст
	$text_and_images = $sql->select_text_and_images_by_id($id_site, $_GET['article']);
	$smarty->assign("text_and_images", $text_and_images);
	
	//изображения  
	$images = $sql->select_images_by_id_site($id_site, $header_images_count, 0);
	$smarty->assign("images", $images);
	
	//видео
	$videos = $sql->select_videos_by_id_site($id_site, $per_page, 0, $text_and_images[0]['id_phrase']);
	$smarty->assign("videos", $videos);	
	
	
	//поисковые фразы
	$phrases = $sql->select_phrases_by_id_site($id_site, $menu_items, 0);
	$smarty->assign("phrases", $phrases);	
	
	//установка мета
	$smarty->assign("meta_title", $text_and_images[0]['text_title']);
	$smarty->assign("meta_keywords", $text_and_images[0]['text_title']);
	$smarty->assign("meta_description", $text_and_images[0]['text_title']);
	
	//404
	if(empty($text_and_images[0])){
		error_404($smarty, $sql, $helpers, $id_site, $per_page, $menu_items, $header_images_count, $num_page, $url, $site);
		die;
	}	
	
	$smarty->display('article.html');
	
}elseif(!empty($_GET['video'])){
	$_GET['video'] = (int)$_GET['video'];
	$_GET['p'] = (int)$_GET['p'];
	//текст
	$video = $sql->select_video_by_id($id_site, $_GET['video']);
	$smarty->assign("video", $video);
	
	//изображения
	$images = $sql->select_images_by_id_site($id_site, $header_images_count, 0);
	$smarty->assign("images", $images);	

	//видео
	$videos = $sql->select_videos_by_id_site($id_site, $per_page, 0, $video['id_phrase']);
	$smarty->assign("videos", $videos);
	
	//поисковые фразы
	$phrases = $sql->select_phrases_by_id_site($id_site, $menu_items, 0);
	$smarty->assign("phrases", $phrases);
	
	//установка мета
	$smarty->assign("meta_title", $videos[0]['title']);
	$smarty->assign("meta_keywords", $videos[0]['description']);
	$smarty->assign("meta_description", $videos[0]['description']);
	
	//404
	if(empty($video['id_video'])){
		error_404($smarty, $sql, $helpers, $id_site, $per_page, $menu_items, $header_images_count, $num_page, $url, $site);
		die;
	}
	
	
	$smarty->display('video.html');
	
}elseif(!empty($_GET['cat'])){
	$_GET['cat'] = (int)$_GET['cat'];
	$_GET['p'] = (int)$_GET['p'];
	//номер строки с которой началась выборка из базы	
	$id_cat = $helpers->get_int($_GET['cat']);
	$smarty->assign("id_cat", $id_cat);
	
	//номер строки с которой началась выборка из базы
	$offset = $helpers->get_int($_GET['p']);
	
	//тексты категории
	$texs = $sql->select_texts_by_id_phrase($id_cat, $per_page, $offset);
	$smarty->assign("texs", $texs);
	
	//изображения
	$images = $sql->select_images_by_id_site($id_site, $header_images_count, 0);
	$smarty->assign("images", $images);
		
	//видео
	$videos = $sql->select_videos_by_id_phrase($id_cat, $per_page, $offset);	
	$smarty->assign("videos", $videos);
	
	//поисковые фразы
	$phrases = $sql->select_phrases_by_id_site($id_site, $menu_items, 0);
	$smarty->assign("phrases", $phrases);
	
	
	if($offset != 0 OR !empty($offset)){
		$meta_add_for_uniq = ' - ' . $offset;
	}	
	//установка мета
	if(!empty($texs[0]['search_phrase'])){
		$smarty->assign("meta_title", $texs[0]['search_phrase'].$meta_add_for_uniq);
		$smarty->assign("meta_keywords", $texs[0]['search_phrase'].$meta_add_for_uniq);
		$smarty->assign("meta_description", $texs[0]['search_phrase'].$meta_add_for_uniq);
	}else{
		$smarty->assign("meta_title", $videos[0]['search_phrase'].$meta_add_for_uniq);
		$smarty->assign("meta_keywords", $videos[0]['search_phrase'].$meta_add_for_uniq);
		$smarty->assign("meta_description", $videos[0]['search_phrase'].$meta_add_for_uniq);
	}
	
	
	//выбираем общее количество статей для сайта
	$count_articles = $sql->select_count_texts_by_id_phrase($id_cat);
	$count_videos = $sql->select_count_videos_by_id_phrase($id_cat);

	//находим общее число строк по большему количеству строку в статьях или видео
	$total = $helpers->get_big($count_articles['count'], $count_videos['count']);
	//$total = $total - 1;
	
	//echo $count_articles;
	//echo '<br>' . print_r($count_videos);
	
	
	$select_phrase_test = $sql->select_phrase_for_cat($id_cat, $id_site);
	if(empty($select_phrase_test[0])){
		error_404($smarty, $sql, $helpers, $id_site, $per_page, $menu_items, $header_images_count, $num_page, $url, $site);
		die;
	}
	
	
	$url = $url . '?cat=' . $id_cat;
	
	$pagination = $helpers->pagination($total, $per_page, $num_page, $offset, $url, 1, 2);
	$smarty->assign("pagination", $pagination);
	
	$smarty->display('category.html');
	
}elseif(!empty($_GET['gal'])){
	$_GET['gal'] = (int)$_GET['gal'];
	$_GET['p'] = (int)$_GET['p'];
	//номер строки с которой началась выборка из базы	
	$id_cat = $helpers->get_int($_GET['gal']);
	$smarty->assign("id_cat", $id_cat);	
	//для header
	$smarty->assign("gal", 1);	
	
	//номер строки с которой началась выборка из базы
	$offset = $helpers->get_int($_GET['p']);	
	
	//изображения
	$images = $sql->select_images_by_id_site($id_site, $header_images_count, 0);
	$smarty->assign("images", $images);	
	
	//галлерея
	//$gallery = $sql->select_images_by_id_site($id_site, 24, $header_images_count);
	$gallery = $sql->select_images_by_id_site_and_phrase($id_site, $id_cat, $per_page_gallery, $offset);
	$smarty->assign("gallery", $gallery);
	
	$cat_title = $helpers->first_char_big($gallery[0]['search_phrase']);
	
	if(empty($cat_title)){
		$phrase = $sql->select_phrase_for_gal($id_cat, $id_site);		
		$cat_title = $helpers->first_char_big($phrase['search_phrase']);		
		$smarty->assign("cat_title", $cat_title);
	}else{
		$smarty->assign("cat_title", $cat_title);
	}
	
	/*
	//видео
	$videos = $sql->select_videos_by_id_phrase($id_cat, $per_page, $offset);	
	$smarty->assign("videos", $videos);
	*/
	
	//поисковые фразы
	$phrases = $sql->select_phrases_by_id_site($id_site, $menu_items, 0);
	$smarty->assign("phrases", $phrases);
	
	
	if($offset != 0 OR !empty($offset)){
		$meta_add_for_uniq = ' - ' . $offset;
	}
	
	//установка мета
	if(empty($gallery[0]['search_phrase'])){
		$select_phrase_test = $sql->select_phrase_for_cat($id_cat, $id_site);
		$smarty->assign("meta_title", $select_phrase_test[0]['search_phrase'].$meta_add_for_uniq);
		$smarty->assign("meta_keywords", $select_phrase_test[0]['search_phrase'].$meta_add_for_uniq);
		$smarty->assign("meta_description", $select_phrase_test[0]['search_phrase'].$meta_add_for_uniq);
	}else{		
		$smarty->assign("meta_title", $gallery[0]['search_phrase'].$meta_add_for_uniq);
		$smarty->assign("meta_keywords", $gallery[0]['search_phrase'].$meta_add_for_uniq);
		$smarty->assign("meta_description", $gallery[0]['search_phrase'].$meta_add_for_uniq);
	}

	
	
	//выбираем общее количество статей для сайта
	//$count_articles = $sql->select_count_texts_by_id_phrase($id_cat);
	//$count_videos = $sql->select_count_videos_by_id_phrase($id_cat);
	$count_gallery = $sql->select_count_images_by_id_site_and_phrase($id_site, $id_cat);
	
	$total = $count_gallery['count'];
	//$total = $total - 1;	
	
	foreach($phrases as $key => $value){
		$phrases_id[] = $value['id'];
	}
	$result_in = in_array($id_cat, $phrases_id);
	
	//print_r($phrases_id);
	if($total == 0 AND $result_in === true){
		$smarty->assign("gallery_cat_empty", "1");
	}elseif($offset + 1 > $total OR $result_in === false){
		error_404($smarty, $sql, $helpers, $id_site, $per_page, $menu_items, $header_images_count, $num_page, $url, $site);
		die;
	}
	
	
	$url = $url . '?gal=' . $id_cat;
	
	$pagination = $helpers->pagination($total, $per_page_gallery, $num_page, $offset, $url, 1, 2);
	$smarty->assign("pagination", $pagination);
	
	$smarty->display('gallery.html');
	
}elseif(!empty($_GET['p']) OR (count($_GET) == 0) OR ($_GET['p'] == 0)){

	$_GET['p'] = (int)$_GET['p'];
	//номер строки с которой началась выборка из базы
	//$offset = (!empty($_GET['p']))? intval($_GET['p']): 0;
	$offset = $helpers->get_int($_GET['p']);
	
	//тексты
	$texs = $sql->select_texts_by_id_site($id_site, $per_page, $offset);
	$smarty->assign("texs", $texs);

	//поисковые фразы
	$phrases = $sql->select_phrases_by_id_site($id_site, $menu_items, 0);
	$smarty->assign("phrases", $phrases);

	//видео
	$videos = $sql->select_videos_by_id_site($id_site, $per_page, $offset);
	$smarty->assign("videos", $videos);

	//изображения
	$images = $sql->select_images_by_id_site($id_site, $header_images_count, 0);
	$smarty->assign("images", $images);	
	
	//выбираем общее количество статей для сайта
	$count_articles = $sql->select_count_texts_by_id_site($id_site);
	$count_videos = $sql->select_count_videos_by_id_site($id_site);

	//находим общее число строк по большему количеству строку в статьях или видео
	$total = $helpers->get_big($count_articles['count'], $count_videos['count']);
	//$total = $total - 1;
	//echo $count_articles['count'];
	//echo '<br>' . $count_videos['count'];
	
	//если введено число отступа большее чем есть информации в базе выводим 404 ошибку
	if($offset + 1 > $total){
		error_404($smarty, $sql, $helpers, $id_site, $per_page, $menu_items, $header_images_count, $num_page, $url, $site);
		die;
	}
	
	$pagination = $helpers->pagination($total, $per_page, $num_page, $offset, $url, 2, 1);
	$smarty->assign("pagination", $pagination);
	
	if($offset != 0 OR !empty($offset)){
		$meta_add_for_uniq = ' - ' . $offset;
	}	
	
	//установка мета
	$smarty->assign("meta_title", $site['name'].$meta_add_for_uniq);
	$smarty->assign("meta_keywords", $site['name'].$meta_add_for_uniq);
	$smarty->assign("meta_description", $site['name'].$meta_add_for_uniq);
	
	if('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] == 'http://' . $site['domain'] . '/'){
		$main_link = 1;
	}else{
		$main_link = 2;
	}
	$smarty->assign("main_link", $main_link);

	
	
	$smarty->display('index.html');
}else{	
	error_404($smarty, $sql, $helpers, $id_site, $per_page, $menu_items, $header_images_count, $num_page, $url, $site);
}

//страница для 404 ошибки
function error_404($smarty, $sql, $helpers, $id_site, $per_page, $menu_items, $header_images_count, $num_page, $url, $site){
	
	header("HTTP/1.0 404 Not Found"); 
	
	$smarty->assign("error404", '1');
	
	//номер строки с которой началась выборка из базы
	//$offset = (!empty($_GET['p']))? intval($_GET['p']): 0;
	$offset = 10;
	
	//тексты
	$texs = $sql->select_texts_by_id_site($id_site, $per_page, $offset);
	$smarty->assign("texs", $texs);

	//поисковые фразы
	$phrases = $sql->select_phrases_by_id_site($id_site, $menu_items, 0);
	$smarty->assign("phrases", $phrases);

	//видео
	$videos = $sql->select_videos_by_id_site($id_site, $per_page, $offset);
	$smarty->assign("videos", $videos);

	//изображения
	$images = $sql->select_images_by_id_site($id_site, $header_images_count, 0);
	$smarty->assign("images", $images);	
	
	//выбираем общее количество статей для сайта
	$count_articles = $sql->select_count_texts_by_id_site($id_site);
	$count_videos = $sql->select_count_videos_by_id_site($id_site);

	//находим общее число строк по большему количеству строку в статьях или видео
	$total = $helpers->get_big($count_articles['count'], $count_videos['count']);
	//$total = $total - 1;
	//echo $count_articles['count'];
	//echo '<br>' . $count_videos['count'];
	
	$pagination = $helpers->pagination($total, $per_page, $num_page, $offset, $url, 2, 1);
	$smarty->assign("pagination", $pagination);
	
	//установка мета
	$smarty->assign("meta_title", $site['name']);
	$smarty->assign("meta_keywords", $site['name']);
	$smarty->assign("meta_description", $site['name']);
	
	if('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] == 'http://' . $site['domain'] . '/'){
		$main_link = 1;
	}else{
		$main_link = 2;
	}
	
	$smarty->assign("main_link", $main_link);		
	$smarty->display('index.html');
}
?>
