<?
// no direct access
defined( '_EXEC' ) or die( 'Restricted access' );

//корневая модель для всех классов модели
class Root_model {

	protected $dbconfig;
	protected $mysql;
	
	protected $prefix_table = '';
	
	////конструктор
	function Root_model()
    {	
		
		$dbconfig_path = $_SERVER['DOCUMENT_ROOT'] . '/classes/models/dbconfig.class.php';
		$mysql = $_SERVER['DOCUMENT_ROOT'] . '/classes/models/mysql.class.php';
		
		require_once($dbconfig_path);
		require_once($mysql);
		
		$this->dbconfig = new Dbconfig;
		$this->mysql = new Mysql;
		
		$dbconfig_array = $this->dbconfig->init();
		$this->mysql->connect($dbconfig_array);
		
		/*	
		mysql_query ("set character_set_client='cp1251'"); 
		mysql_query ("set character_set_results='cp1251'"); 
		mysql_query ("set collation_connection='cp1251_general_ci'");	
		*/				
		
		mysql_query ("set character_set_client='utf8'"); 
		mysql_query ("set character_set_results='utf8'"); 
		mysql_query ("set collation_connection='utf8_general_ci'");
		
    }	
	
	
	////обрезание пробелов и экранирование
	public function safe($text){
		$text = trim($text);
		$text = $this->mysql->safesql($text);
		//$text = mysql_real_escape_string($text);
	
		return $text;
	}
	
	
	////вставка и возвращение вставленного id
	protected function ins_ret($query){	
		$this->mysql->query($query, true);	
		$insert_id = $this->mysql->insert_id();
		
		return $insert_id;
	}
	
	
	////выборка 1й строки результата запроса
	protected function sel_1_row($query){
		$query_id = $this->mysql->query($query, true);	
		$row = $this->mysql->get_row($query_id);
		
		return $row;
	}
	
	
	////выборка массива сттрок
	protected function sel_rows($query){
		$rows = $this->mysql->super_query($query, true);	
		
		return $rows;
	}
	
	
	////отправка запроса
	protected function query($query){
		$query_id = $this->mysql->query($query, true);
		
		return $query_id;
	}
	
	////количество затронутых строк
	protected function num_rows($query_id = ''){
		$num_rows = $this->mysql->num_rows($query_id);
		
		return $num_rows;
	}
}
?>