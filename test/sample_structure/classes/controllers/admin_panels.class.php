<?
// no direct access
defined( '_EXEC' ) or die( 'Restricted access' );

require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/models/sql_model.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/helpers.class.php');

class Admin_panels extends Sql_model{	
	
	function Admin_panels (){
		parent::__construct();
		$this->helpers = New Helpers;		
	}

	// create subdomain
	function subdomain($login_cpanel, $password_cpanel, $theme_cpanel = 'x3', $up_domain, $sub_domain, $number_farm, $id_site){
	
		$host = 'localhost';
		$port = 2082;
	
		/*
		$slash = urlencode( '/' );
		$down_line = urlencode( '_' );
		$path = 'domains' . $slash . 'sites' . $down_line . '6' . $slash . '/test';
		*/ 
		$path = 'domains/sites_' . $number_farm . '/site_' . $id_site;
		$path = urlencode($path);
		  
		$request = "/frontend/$theme_cpanel/subdomain/doadddomain.html?rootdomain=$up_domain&domain=$sub_domain&dir=" . $path;

		$sock = fsockopen($host, $port);
		if(!$sock) {
			print('Socket error');
			exit();
		}

		$authstr = "$login_cpanel:$password_cpanel";
		$pass = base64_encode($authstr);
		$in = "GET $request\r\n";
		$in .= "HTTP/1.0\r\n";
		$in .= "Host:$host\r\n";
		$in .= "Authorization: Basic $pass\r\n";
		$in .= "\r\n";
		 
		fputs($sock, $in);
		while (!feof($sock)) {
			$result .= fgets ($sock, 128);
		}
		fclose($sock);

		return $result;
	}
	
	
	function subdomain_isp($login_panel, $password_panel, $hid_user, $up_domain, $sub_domain, $number_farm, $id_site, $hid_email, $port){
	
		//$hid_email = 'mail@mail.ru';
		$domainwww = $sub_domain . '.' . $up_domain;			
		$root = $login_panel;
		$rootpass = $password_panel;

		//$server = '78.46.202.72:1500';
		if( !empty($port) ){
			$server = $_SERVER['SERVER_ADDR'] . ':' . $port;
		}else{
			$server = $_SERVER['SERVER_ADDR'];
		}
		
		$url = "https://" . $server . "/manager/ispmgr?authinfo=".$root.":".$rootpass."&out=xml&func=wwwdomain.edit";

		$add_domain["sok"] = "yes";
		$add_domain["elid"] = "";

		$add_domain["domain"] = $domainwww;
		$add_domain["alias"] = "www.".$domainwww;
			
		$add_domain["docroot"] = 'domains/sites_' . $number_farm . '/site_' . $id_site;
		$add_domain["owner"] = $hid_user;
		#add_domain["version"].

		$add_domain["ip"] = $_SERVER['SERVER_ADDR'];
		//$add_domain["ip"] = '78.46.202.72';
		#ip6 - IPv6-адрес. Параметр зависим от возможности ipv6..
		#pool - Пул приложений. Параметр зависим от возможности windows..
		$add_domain["admin"] = $hid_email;
		$add_domain["charset"] = "UTF-8";
		$add_domain["index"] = "index.php";
		#autosubdomain - Авто поддомены. Параметр зависим от возможности asd..

		#Возможные значения :.
		$add_domain["asdnone"] = "on";
		#asddir = "/var/www/".$hid_user."/data/www/".$domainwww;
		#asdsubdir - В поддиректории WWW домена.
		$add_domain["php"] = "phpmod";

		#Возможные значения :.
		#phpnone - Нет поддержки PHP.
		#phpmod - PHP как модуль Apache.
		#phpcgi - PHP как CGI.
		$add_domain["phpfcgi"] = "off";
		#- PHP как FastCGI.
		$add_domain["cgi"] = "off" ;
		#Cgi-bin. (Необязательный параметр. Чтобы включить данную опцию используйте значение "on".) Параметр зависим от возможности cgi..
		#wsgi - wsgi-scripts. (Необязательный параметр. Чтобы включить данную опцию используйте значение "on".) Параметр зависим от возможности wsgi..
		#ssi - SSI. (Необязательный параметр. Чтобы включить данную опцию используйте значение "on".) Параметр зависим от возможности ssi..

		#ssiext - Расширения файлов SSI. Параметр зависим от возможности ssi..
		#frp - FrontPage. (Необязательный параметр. Чтобы включить данную опцию используйте значение "on".) Параметр зависим от возможности frp..

		#fppasswd - Пароль для FrontPage. Параметр зависим от возможности frp..
		#ror - Ruby on rails. (Необязательный параметр. Чтобы включить данную опцию используйте значение "on".) Параметр зависим от возможности ror..

		#ssl - SSL. (Необязательный параметр. Чтобы включить данную опцию используйте значение "on".) Параметр зависим от возможности ssl..

		#sslport - SSL порт. Параметр зависим от возможности ssl..
		#cert - SSL сертификат. Параметр зависим от возможности ssl..
		#switchispmgr - Отключить ISPmanager. (Необязательный параметр. Чтобы включить данную опцию используйте значение "on".).
		$add_domain["logrequests"] = "on";

		foreach( $add_domain as $k => $v ){
			 $url .= '&'.$k.'='.urlencode($v);
		}
		# echo $url;
		# exit();
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		// Get the response and close the channel.
		$response = curl_exec($ch);
		curl_close($ch);	
		
		
		if( mb_strpos($response, $needle = '<doc><ok/></doc>', $offset = 0, $encoding = 'utf8') ){
			return 'был создан!';
		}else{
			return 'NO_ADD';
		}
		
	}
		
	
	public function add_domain_cpanel($domain, $domain_zone, $login_cpanel, $password_cpanel, $theme_cpanel, $number_farm, $id_site){
	
		//$domain = 'nizhnee-belio.ru';
		//$login_cpanel = "stiheeru";
		//$password_cpanel = "12369874123698741";
		//$domain_array = explode('.', $domain);
		$user_domain = $domain;
		
		$full_domain = $domain . $domain_zone;
		//pass ftp
		$password = $this->helpers->gen_random_str(12);

		$host = 'localhost';
		$port = 2082;
		
		//$theme_cpanel = 'x3';
		/*
		$path = 'domains/sites_test/site_test';
		$path = urlencode($path);
		*/
		
		$go = 'Добавить домен';
		$go = urlencode($go);
	
		/*
		###отладка
		echo '$domain - ' . $domain . '<br>';
		echo '$login_cpanel - ' . $login_cpanel . '<br>';
		echo '$password_cpanel - ' . $password_cpanel . '<br>';
		echo '$user_domain - ' . $user_domain . '<br>';
		echo '$password - ' . $password . '<br>';
		echo '$host - ' . $host . '<br>';
		echo '$port - ' . $port . '<br>';
		echo '$path - ' . $path . '<br>';
		echo '$go - ' . $go . '<br>';
		echo '$theme_cpanel - ' . $theme_cpanel . '<br>';
		echo '$number_farm - ' . $number_farm . '<br>';
		echo '$id_site - ' . $id_site . '<br>';
		*/
		
		$path = 'domains/sites_' . $number_farm . '/site_' . $id_site;
		$path = urlencode($path);		

		$request = "/frontend/$theme_cpanel/addon/doadddomain.html?domain=$full_domain&user=$user_domain&dir=$path&pass=$password&pass2=$password&go=$go";
				
		###отладка
		//echo '$request - ' . $request;
		
		$sock = fsockopen($host, $port);
		if(!$sock) {
			print('Socket error');
			exit();
		}

		$authstr = "$login_cpanel:$password_cpanel";
		$pass = base64_encode($authstr);
		$in = "GET $request\r\n";
		$in .= "HTTP/1.0\r\n";
		$in .= "Host:$host\r\n";
		$in .= "Authorization: Basic $pass\r\n";
		$in .= "\r\n";
		 
		fputs($sock, $in);
		while (!feof($sock)) {
			$result .= fgets ($sock, 128);
		}
		fclose($sock);
		
		###отладка
		//echo $result;
		
		
		if( mb_strpos($result, $needle = '” создан.', $offset = 0, $encoding = 'utf8') ){
			return 'SUCCESS';
		}else{
			return 'NO_ADD';
		}
	}
	
	
	
	public function add_domain_isp($domain, $domain_zone, $root, $rootpass, $hid_user, $number_farm, $id_site, $hid_email, $port){
	
		//$domain = 'nizhnee-belio.ru';
		//$login_cpanel = "stiheeru";
		//$password_cpanel = "12369874123698741";
		//$domain_array = explode('.', $domain);
		//$user_domain = $domain;
		
		$domainwww = $domain . $domain_zone;			
		
		if( !empty($port) ){
			$server = $_SERVER['SERVER_ADDR'] . ':' . $port;
		}else{
			$server = $_SERVER['SERVER_ADDR'];
		}

		//$server = '78.46.202.72:1500';
		$url = "https://" . $server . "/manager/ispmgr?authinfo=".$root.":".$rootpass."&out=xml&func=wwwdomain.edit";

		$add_domain["sok"] = "yes";
		$add_domain["elid"] = "";

		$add_domain["domain"] = $domainwww;
		$add_domain["alias"] = "www.".$domainwww;
			
		$add_domain["docroot"] = 'domains/sites_' . $number_farm . '/site_' . $id_site;
		$add_domain["owner"] = $hid_user;
		#add_domain["version"].

		$add_domain["ip"] = $_SERVER['SERVER_ADDR'];
		//$add_domain["ip"] = '78.46.202.72';
		#ip6 - IPv6-адрес. Параметр зависим от возможности ipv6..
		#pool - Пул приложений. Параметр зависим от возможности windows..
		$add_domain["admin"] = $hid_email;
		$add_domain["charset"] = "UTF-8";
		$add_domain["index"] = "index.php";
		#autosubdomain - Авто поддомены. Параметр зависим от возможности asd..

		#Возможные значения :.
		$add_domain["asdnone"] = "on";
		#asddir = "/var/www/".$hid_user."/data/www/".$domainwww;
		#asdsubdir - В поддиректории WWW домена.
		$add_domain["php"] = "phpmod";

		#Возможные значения :.
		#phpnone - Нет поддержки PHP.
		#phpmod - PHP как модуль Apache.
		#phpcgi - PHP как CGI.
		$add_domain["phpfcgi"] = "off";
		#- PHP как FastCGI.
		$add_domain["cgi"] = "off" ;
		#Cgi-bin. (Необязательный параметр. Чтобы включить данную опцию используйте значение "on".) Параметр зависим от возможности cgi..
		#wsgi - wsgi-scripts. (Необязательный параметр. Чтобы включить данную опцию используйте значение "on".) Параметр зависим от возможности wsgi..
		#ssi - SSI. (Необязательный параметр. Чтобы включить данную опцию используйте значение "on".) Параметр зависим от возможности ssi..

		#ssiext - Расширения файлов SSI. Параметр зависим от возможности ssi..
		#frp - FrontPage. (Необязательный параметр. Чтобы включить данную опцию используйте значение "on".) Параметр зависим от возможности frp..

		#fppasswd - Пароль для FrontPage. Параметр зависим от возможности frp..
		#ror - Ruby on rails. (Необязательный параметр. Чтобы включить данную опцию используйте значение "on".) Параметр зависим от возможности ror..

		#ssl - SSL. (Необязательный параметр. Чтобы включить данную опцию используйте значение "on".) Параметр зависим от возможности ssl..

		#sslport - SSL порт. Параметр зависим от возможности ssl..
		#cert - SSL сертификат. Параметр зависим от возможности ssl..
		#switchispmgr - Отключить ISPmanager. (Необязательный параметр. Чтобы включить данную опцию используйте значение "on".).
		$add_domain["logrequests"] = "on";

		foreach( $add_domain as $k => $v ){
			 $url .= '&'.$k.'='.urlencode($v);
		}
		# echo $url;
		# exit();
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		// Get the response and close the channel.
		$response = curl_exec($ch);
		curl_close($ch);	
		
		
		if( mb_strpos($response, $needle = '<doc><ok/></doc>', $offset = 0, $encoding = 'utf8') ){
			return 'SUCCESS';
		}else{
			return 'NO_ADD';
		}
				
	}
	
	
	//$sub_domains_and_ids_sites_array - субдомены
	//$up_domains_array - домены, если передан один в первом элементе массива то он станет верхним для всех субдоменов
	//если передано больше одного элемента массива то домены верхнего уровня будут чередоваться
	//$login_cpanel - логин к админке
	//$password_cpanel - пароль к админке
	//$number_farm - номер фермы сайтов, должен быть уникален для акаунта админки, чтобы папки доменов не пересекались
	//$theme_cpanel - скин админки, по умолчанию x3, у вас может быть что-то другое например - x
	function mass_subdomain($sub_domains_and_ids_sites_array, $up_domains_array, $login_cpanel, $password_cpanel, $number_farm, $theme_cpanel, $rand_up_domains = 0){
		
		//$sub_domains_and_ids_sites_array['index']['domain'] - домен
		//$sub_domains_and_ids_sites_array['index']['id'] - id сайта
		
		$count_domains = count($up_domains_array);
		$count_subdomains = count($sub_domains_and_ids_sites_array);
		
		
		$count_loop_up = 0;
		$count_loop = 0;
		if( is_array($sub_domains_and_ids_sites_array) ){
			foreach($sub_domains_and_ids_sites_array as $key => $value){
				
				$html = null;
				$up_domain = null;
				if( is_array($up_domains_array) ){
					if($count_domains > 1){			
						if($rand_up_domains != 1){
						
							$up_domain = $up_domains_array[$count_loop_up];

							$html = strip_tags( $this->subdomain($login_cpanel, $password_cpanel, $theme_cpanel, $up_domain, $value['domain'], $number_farm, $value['id']) );
							
							$count_loop_up++;
							
							//если достигли последнего элемента в массиве верхнинх доменов обнуляем счётчик и начинаем проход с первого домена
							if($count_loop_up == $count_domains){
								$count_loop_up = 0;
							}
							
						}else{
						
							$number_domain = rand(0, $count_domains - 1);
							
							$up_domain = $up_domains_array[$number_domain];
							
							$html = strip_tags( $this->subdomain($login_cpanel, $password_cpanel, $theme_cpanel, $up_domain, $value['domain'], $number_farm, $value['id']) );
							
						}
						
					}else{
						$up_domain = $up_domains_array[0];
						$html = strip_tags( $this->subdomain($login_cpanel, $password_cpanel, $theme_cpanel, $up_domain, $value['domain'], $number_farm, $value['id']) );	
					}
				}else{
					$up_domain = $up_domains_array;
					$html = strip_tags( $this->subdomain($login_cpanel, $password_cpanel, $theme_cpanel, $up_domain, $value['domain'], $number_farm, $value['id']) );
				}
				
				//проверяем выдачу страницы, если находим 'был создан!' значит домен было создан, пишем статус - ok
				if( mb_strpos($html, $needle = 'был создан!', $offset = 0, $encoding = 'utf-8') !== false){
					$result[$count_loop]['status'] = '<span style="color:green;font-weight:bold;">Удачно!</span>';
					
					//обдновляем домен в базе
					$this->update_domain_by_id_site($value['id'], $value['domain'] . '.' . $up_domain);
				}else{
					$result[$count_loop]['status'] = '<span style="color:red;font-weight:bold;">Ошибка!</span>';
				}

				$result[$count_loop]['id'] = $value['id'];
				$result[$count_loop]['domain'] = $value['domain'] . '.' . $up_domain;			
				
				
				
				$count_loop++;
				time_nanosleep(3, 500000000); //задержка полсекунды
			}
		}else{
			$result = 'Поддомены не были созданы! В базе нет готовых сайтов с доменом по умолчанию (site_1).';
		}
	

		//$result = subd('localhost',2082,$cpaneluser,$cpanelpass,$request);
		//$show = strip_tags($result);
		return $result;
	}
	
	
		//$sub_domains_and_ids_sites_array - субдомены
	//$up_domains_array - домены, если передан один в первом элементе массива то он станет верхним для всех субдоменов
	//если передано больше одного элемента массива то домены верхнего уровня будут чередоваться
	//$login_cpanel - логин к админке
	//$password_cpanel - пароль к админке
	//$number_farm - номер фермы сайтов, должен быть уникален для акаунта админки, чтобы папки доменов не пересекались
	//$theme_cpanel - скин админки, по умолчанию x3, у вас может быть что-то другое например - x
	function mass_subdomain_isp($sub_domains_and_ids_sites_array, $up_domains_array, $login_panel, $password_panel, $number_farm, $hid_user, $hid_email, $port, $rand_up_domains = 0){
		
		//$sub_domains_and_ids_sites_array['index']['domain'] - домен
		//$sub_domains_and_ids_sites_array['index']['id'] - id сайта
		
		$count_domains = count($up_domains_array);
		$count_subdomains = count($sub_domains_and_ids_sites_array);
		
		
		$count_loop_up = 0;
		$count_loop = 0;
		if( is_array($sub_domains_and_ids_sites_array) ){
			foreach($sub_domains_and_ids_sites_array as $key => $value){
				
				$html = null;
				$up_domain = null;
				if( is_array($up_domains_array) ){
					if($count_domains > 1){			
						if($rand_up_domains != 1){
						
							$up_domain = $up_domains_array[$count_loop_up];

							$html = strip_tags( $this->subdomain_isp($login_panel, $password_panel, $hid_user, $up_domain, $value['domain'], $number_farm, $value['id'], $hid_email, $port) );
							
							$count_loop_up++;
							
							//если достигли последнего элемента в массиве верхнинх доменов обнуляем счётчик и начинаем проход с первого домена
							if($count_loop_up == $count_domains){
								$count_loop_up = 0;
							}
							
						}else{
						
							$number_domain = rand(0, $count_domains - 1);
							
							$up_domain = $up_domains_array[$number_domain];
							
							$html = strip_tags( $this->subdomain_isp($login_panel, $password_panel, $hid_user, $up_domain, $value['domain'], $number_farm, $value['id'], $hid_email, $port) );
							
						}
						
					}else{
						$up_domain = $up_domains_array[0];
						$html = strip_tags( $this->subdomain_isp($login_panel, $password_panel, $hid_user, $up_domain, $value['domain'], $number_farm, $value['id'], $hid_email, $port) );	
					}
				}else{
					$up_domain = $up_domains_array;
					$html = strip_tags( $this->subdomain_isp($login_panel, $password_panel, $hid_user, $up_domain, $value['domain'], $number_farm, $value['id'], $hid_email, $port) );
				}
				
				//проверяем выдачу страницы, если находим 'был создан!' значит домен было создан, пишем статус - ok
				if( mb_strpos($html, $needle = 'был создан!', $offset = 0, $encoding = 'utf-8') !== false){
					$result[$count_loop]['status'] = '<span style="color:green;font-weight:bold;">Удачно!</span>';
					
					//обдновляем домен в базе
					$this->update_domain_by_id_site($value['id'], $value['domain'] . '.' . $up_domain);
				}else{
					$result[$count_loop]['status'] = '<span style="color:red;font-weight:bold;">Ошибка!</span>';
				}

				$result[$count_loop]['id'] = $value['id'];
				$result[$count_loop]['domain'] = $value['domain'] . '.' . $up_domain;			
				
				
				
				$count_loop++;
				time_nanosleep(3, 500000000); //задержка полсекунды
			}
		}else{
			$result = 'Поддомены не были созданы! В базе нет готовых сайтов с доменом по умолчанию (site_1).';
		}
	

		//$result = subd('localhost',2082,$cpaneluser,$cpanelpass,$request);
		//$show = strip_tags($result);
		return $result;
	}
	
	
	public function reg_add_domain_2domain_cpanel($domain, $domain_zone, $id_site, $arg_2dom_reg, $number_farm, $login_cpanel, $password_cpanel, $theme_cpanel){
		
		$result['id_site'] = $id_site;
		$result['domain'] = $domain . $domain_zone;
		$result['result_reg'] = $this->_2domains($command = 'domain/create', $arg_2dom_reg, $domain, $domain_zone, $id_site);	
		
		if($result['result_reg'] == 'SUCCESS'){
		
			time_nanosleep(3, 500000000);
			$result['result_add'] = $this->add_domain_cpanel($domain, $domain_zone, $login_cpanel, $password_cpanel, $theme_cpanel, $number_farm, $id_site);
			
			if($result['result_add'] == 'SUCCESS'){
				$result['result_common'] = '<span style="color:green;font-weight:bold;">SUCCESS</span>';
			}else{
				$result['result_common'] = '<span style="color:red;font-weight:bold;">FAILURE</span>';
			}
		}else{
			$result['result_add'] = 'NO_ADD';
			$result['result_common'] = '<span style="color:red;font-weight:bold;">FAILURE</span>';
		}
		
			

		return $result;
	}
	
	
	
	public function reg_add_domain_2domain_isp($domain, $domain_zone, $id_site, $arg_2dom_reg, $number_farm, $login_panel, $password_panel, $hid_user, $port){
		
		$result['id_site'] = $id_site;
		$result['domain'] = $domain . $domain_zone;
		$result['result_reg'] = $this->_2domains($command = 'domain/create', $arg_2dom_reg, $domain, $domain_zone, $id_site);	
		
		if($result['result_reg'] == 'SUCCESS'){
		
			time_nanosleep(3, 500000000);
			$result['result_add'] = $this->add_domain_isp($domain, $domain_zone, $login_panel, $password_panel, $hid_user, $number_farm, $id_site, $arg_2dom_reg['e_mail'], $port);
			
			if($result['result_add'] == 'SUCCESS'){
				$result['result_common'] = '<span style="color:green;font-weight:bold;">SUCCESS</span>';
			}else{
				$result['result_common'] = '<span style="color:red;font-weight:bold;">FAILURE</span>';
			}
		}else{
			$result['result_add'] = 'NO_ADD';
			$result['result_common'] = '<span style="color:red;font-weight:bold;">FAILURE</span>';
		}
		
			

		return $result;
	}
	
	
	public function mass_reg_add_domain_2domain_cpanel($arg_2dom_reg, $number_farm, $login_cpanel, $password_cpanel, $theme_cpanel, $limit = 0){
		
		$domain_zone = '.ru';
		
		$sites = $this->select_all_info_sites_by_primary_grab_and_default_domain($primary_grab = 1, $default_domain = 'site_1', $limit);
	
		foreach($sites as $key => $value){
			$site_id_and_domain[$key]['id'] = $value['id'];
			$site_id_and_domain[$key]['domain'] = $this->helpers->translit_domain($value['name']);
		}
	
		$not_enough_money_flag = 0;
		foreach($site_id_and_domain as $key => $value){
		
			if($not_enough_money_flag == 0){
				
				$result[$key] = $this->reg_add_domain_2domain_cpanel($value['domain'], $domain_zone, $value['id'], $arg_2dom_reg, $number_farm, $login_cpanel, $password_cpanel, $theme_cpanel);
			}else{
				$result[$key]['id_site'] = $value['id'];
				$result[$key]['domain'] = $value['domain'] . $domain_zone;
				$result[$key]['result_reg'] = 'NOT_ENOUGH_MONEY';				
				$result[$key]['result_add'] = 'NO_ADD';
				$result[$key]['result_common'] = '<span style="color:red;font-weight:bold;">FAILURE</span>';
			}
			
			if($result[$key]['result_reg'] == 'NOT_ENOUGH_MONEY'){
				$not_enough_money_flag = 1;	
				$result[$key]['result_add'] = 'NO_ADD';
				$result[$key]['result_common'] = '<span style="color:red;font-weight:bold;">FAILURE</span>';
			}
			
			
			//если домен уже занят, делаем 100 попыток регистрации добавляя цифру на конце домена с номером регистрации
			//при успешной регистрации завершаем цикл
			if($result[$key]['result_reg'] == 'DOMAIN_ALREADY_EXISTS'){
				for($i = 1 ; $i < 15 ; $i++){	
					
					$result[$key] = $this->reg_add_domain_2domain_cpanel($value['domain'] . $i, $domain_zone, $value['id'], $arg_2dom_reg, $number_farm, $login_cpanel, $password_cpanel, $theme_cpanel);
					
					if($result[$key]['result_reg'] == 'SUCCESS'){
						break;
					}elseif($result[$key]['result_reg'] == 'NOT_ENOUGH_MONEY'){
						$not_enough_money_flag = 1;	
						break;						
					}elseif($result[$key]['result_reg'] == 'UNKNOWN_ERROR'){
						//неизвестная ошибка
						break;						
					}elseif($result[$key]['result_reg'] == 'DOMAIN_ALREADY_EXISTS'){						
						//этот домен тоже занят, переход на следующую итерацию цикла	
					}else{
						//какая то ошибка, в коде будет написано какая
						break;
					}
				}
			}
			
		}
		
		//возвращает массив с результатами регистрации и добавления домена в cpanel
		return $result;		
	}
	
	
	
	public function mass_reg_add_domain_2domain_isp($arg_2dom_reg, $number_farm, $login_panel, $password_panel, $hid_user, $port, $limit = 0){
		
		$domain_zone = '.ru';
		
		$sites = $this->select_all_info_sites_by_primary_grab_and_default_domain($primary_grab = 1, $default_domain = 'site_1', $limit);
	
		foreach($sites as $key => $value){
			$site_id_and_domain[$key]['id'] = $value['id'];
			$site_id_and_domain[$key]['domain'] = $this->helpers->translit_domain($value['name']);
		}
	
		$not_enough_money_flag = 0;
		foreach($site_id_and_domain as $key => $value){
		
			if($not_enough_money_flag == 0){
				
				$result[$key] = $this->reg_add_domain_2domain_isp($value['domain'], $domain_zone, $value['id'], $arg_2dom_reg, $number_farm, $login_panel, $password_panel, $hid_user, $port);
			}else{
				$result[$key]['id_site'] = $value['id'];
				$result[$key]['domain'] = $value['domain'] . $domain_zone;
				$result[$key]['result_reg'] = 'NOT_ENOUGH_MONEY';				
				$result[$key]['result_add'] = 'NO_ADD';
				$result[$key]['result_common'] = '<span style="color:red;font-weight:bold;">FAILURE</span>';
			}
			
			if($result[$key]['result_reg'] == 'NOT_ENOUGH_MONEY'){
				$not_enough_money_flag = 1;	
				$result[$key]['result_add'] = 'NO_ADD';
				$result[$key]['result_common'] = '<span style="color:red;font-weight:bold;">FAILURE</span>';
			}
			
			
			//если домен уже занят, делаем 100 попыток регистрации добавляя цифру на конце домена с номером регистрации
			//при успешной регистрации завершаем цикл
			if($result[$key]['result_reg'] == 'DOMAIN_ALREADY_EXISTS'){
				for($i = 1 ; $i < 15 ; $i++){	
					
					$result[$key] = $this->reg_add_domain_2domain_cpanel($value['domain'] . $i, $domain_zone, $value['id'], $arg_2dom_reg, $number_farm, $login_panel, $password_panel, $hid_user, $port);
					
					if($result[$key]['result_reg'] == 'SUCCESS'){
						break;
					}elseif($result[$key]['result_reg'] == 'NOT_ENOUGH_MONEY'){
						$not_enough_money_flag = 1;	
						break;						
					}elseif($result[$key]['result_reg'] == 'UNKNOWN_ERROR'){
						//неизвестная ошибка
						break;						
					}elseif($result[$key]['result_reg'] == 'DOMAIN_ALREADY_EXISTS'){						
						//этот домен тоже занят, переход на следующую итерацию цикла	
					}else{
						//какая то ошибка, в коде будет написано какая
						break;
					}
				}
			}
			
		}
		
		//возвращает массив с результатами регистрации и добавления домена в cpanel
		return $result;		
	}
	
	
	//вернёт false если не удалось соединиться с api
	//в случает успеха выдаст массив с ответом сервера
	public function _2domains($command = 'domain/create', $arg_2dom_reg = null, $domain = null, $domain_zone = null, $id_site = null){
		
		/*
		###отладка
		$domain = 'stolyarniy';
		$domain_zone = '.ru';		
		###пример аргументов		
		$arg_2dom_reg['username'] = 'drinker_soul@mail.ru'; //LOGIN
        $arg_2dom_reg['password'] = '2473562domains'; //PASSORD
		$arg_2dom_reg['folder_name'] = 'api_reg'; 
		$arg_2dom_reg['post_code'] = '606400';
		$arg_2dom_reg['region'] = 'Нижегородская область';
		$arg_2dom_reg['town'] = 'Балахна';
		$arg_2dom_reg['street_house_room'] = 'ЦКК, д.2, кв.8';		
		$arg_2dom_reg['phone'] = '+71111111111';
        $arg_2dom_reg['e_mail'] = 'fenixthe@mail.ru';				 
        $arg_2dom_reg['person_en'] = 'Vasily D Surodin';
        $arg_2dom_reg['person_r'] = 'Суродин Василий Дмитриевич';	
		$arg_2dom_reg['birth_date'] = '09.01.1987';		
        $arg_2dom_reg['private_person_flag'] = '1';
		$arg_2dom_reg['passport_number'] = '2211863593';
		$arg_2dom_reg['passport_issued_org'] = 'отделением УФМС России по Нижегородской области в Балахнинском районе';
		$arg_2dom_reg['passport_issued_date'] = '14.04.2012';		
        $arg_2dom_reg['country'] = 'RU';
		$arg_2dom_reg['ns0'] = 'ns1.shneider-host.ru';
		$arg_2dom_reg['ns1'] = 'ns2.shneider-host.ru';
		*/
		
		
		$textdm = array();
        $textdm['action'] = $command; // domain/create
        $textdm['username'] = $arg_2dom_reg['username']; //LOGIN
        $textdm['password'] = $arg_2dom_reg['password']; //PASSORD
        $textdm['output_format'] = 'json'; //json
        $textdm['input_format'] = 'json'; //json
        $textdm['period'] = '1'; //1
		$textdm['folder_name'] = $arg_2dom_reg['folder_name'];
        $textdm['sub_user_folder_name'] = $arg_2dom_reg['folder_name'];
        $textdm['__trusted']='1'; //1
        $textdm['ok_if_no_money']='1'; //1      
		
		
		$nl = chr(10);
		
        $contacts = array();
		// $contacts['p_addr']='606400' . $nl . 'Нижегородская область' . $nl . 'Балахна' . $nl . 'ЦКК, д.2, кв.8' . $nl . 'Суродин Василий Дмитриевич';
        $contacts['p_addr'] = $arg_2dom_reg['post_code'] .
			$nl . $arg_2dom_reg['region'] .
			$nl . $arg_2dom_reg['town'] .			
			$nl . $arg_2dom_reg['street_house_room'] .
			$nl . $arg_2dom_reg['person_r']; //$nl . $arg_2dom_reg['town'] . // - убрал 19_02_13-02_21 видимо дублирование
		$contacts['phone'] = $arg_2dom_reg['phone'];
        $contacts['e_mail'] = $arg_2dom_reg['e_mail'];
		$contacts['rp_profile_type']='f';		 
        $contacts['person'] = $arg_2dom_reg['person_en'];
        $contacts['person_r'] = $arg_2dom_reg['person_r'];
        $contacts['private_person_flag'] = $arg_2dom_reg['private_person_flag'];
        //$contacts['passport'] = 'Серия и номер: 2211863593, выдан: отделением УФМС России по Нижегородской области в Балахнинском районе, дата выдачи: 14.04.2012';
		$contacts['passport'] = 'Серия и номер: ' . $arg_2dom_reg['passport_number'] .
			', выдан: ' . $arg_2dom_reg['passport_issued_org'] .
			', дата выдачи: ' . $arg_2dom_reg['passport_issued_date'];
		$contacts['birth_date'] = $arg_2dom_reg['birth_date'];
        $contacts['country'] = $arg_2dom_reg['country'];
        
		
		//$domain = 'nizhnee-belio.ru';
		
		$json = array('contacts' => $contacts, 'domains' => array( array('dname' => $domain . $domain_zone) ), 'nss' => array('ns0' => $arg_2dom_reg['ns0'] , 'ns1' => $arg_2dom_reg['ns1']), 'enduser_ip' => '95.37.97.222');
        $textdm['input_data'] = json_encode($json);
		
		/*
		###отладка
		$textdm['input_data'] = $json;
		return $textdm;
		*/
        
		if( $curl = curl_init() ) {
		
			curl_setopt($curl, CURLOPT_URL, 'http://2domains.ru/reg/api2/');
			curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $textdm);
			$resp = curl_exec($curl);
			curl_close($curl);
			
		}else{
			return false;
		}
		
		$json_array = json_decode($resp);
		
		if($json_array->result == 'error' AND $json_array->error_code == 'NOT_ENOUGH_MONEY'){
			
			return 'NOT_ENOUGH_MONEY';
			
		}elseif($json_array->result == 'success' AND $json_array->answer->domains[0]->error_code == 'DOMAIN_ALREADY_EXISTS'){
			
			return 'DOMAIN_ALREADY_EXISTS';
			
		}elseif($json_array->result == 'error'){
		
			return $json_array->error_code;
			
		}elseif($json_array->result == 'success'){
		
			//обновляем домен в базе
			$this->update_domain_by_id_site($id_site, $domain . $domain_zone);
			return 'SUCCESS';
			
		}else{
			
			return 'UNKNOWN_ERROR';
			
		}
		
		//return $json_array;
	}
	
	
	public function add_mass_sites_mainlink($sites, $login_mainlink, $password_mainlink, $price_array, $count_main_links, $count_other_links, $multiplication, $stop_words){
	
		/*
		$price_array['dol_main_page'] = $_POST['dol_main_page'];
		$price_array['rub_main_page'] = $_POST['rub_main_page'];
		$price_array['dol_lvl_1_page'] = $_POST['dol_lvl_1_page'];
		$price_array['rub_lvl_1_page'] = $_POST['rub_lvl_1_page'];
		$price_array['dol_lvl_2_page'] = $_POST['dol_lvl_2_page'];
		$price_array['rub_lvl_2_page'] = $_POST['rub_lvl_2_page'];
		$price_array['dol_lvl_3_page'] = $_POST['dol_lvl_3_page'];
		$price_array['rub_lvl_3_page'] = $_POST['rub_lvl_3_page'];
		
		$count_main_links = $_POST['count_main_links'];
		$count_other_links = $_POST['count_other_links'];
		$multiplication = $_POST['multiplication'];
		$stop_words = $_POST['stop_words'];
		*/
		
		foreach($sites as $key => $value){
						
			//генерим случайную категорию
			$CategoryID = rand(1 , 90);
			
			$result[$key]['id_site_local'] = $value['id'];
			$result[$key]['id_site_mainlink'] = $this->add_site_mainlink($Url = $value['domain'], $Name = $value['name'], $Description = $value['name'], $CategoryID, $login_mainlink, $password_mainlink, $price_array, $count_main_links, $count_other_links, $multiplication, $stop_words);
			
		}		
		
		return $result;
	}
	
	
	//add 08_02_13-02_39
	public function add_mass_sites_sape($sites, $login_sape, $password_sape, $count_main_links_sape, $count_other_links_sape){
	
		$result = null;
		foreach($sites as $key => $value){
						
			//генерим случайную категорию
			$category_id = rand(1 , 55);
			
			//если выпадает категория английских сайтов меняем её
			if($category_id = 52){
				$rand = rand(1 , 3);				
				$category_id = $category_id + $rand;
			}
			
			$result[$key]['id_site_local'] = $value['id'];
			$id_site_sape = $this->add_site_sape($url = $value['domain'], $category_id, $login_sape, $password_sape, $count_main_links_sape, $count_other_links_sape);
			$result[$key]['id_site_sape'] = $id_site_sape;			

		}		
		
		return $result;
	}
	
	
	//add 10_02_13-23_29
	public function mass_activate_sape_pages_and_price($sites, $login_sape, $password_sape, $price_array, $multiplication_sape){
	
	
		$result = null;
		foreach($sites as $key => $value){						
			
			$result[$key]['id_site_local'] = $value['id'];
			$site_result = $this->activate_sape_pages_and_price($url = $value['domain'], $value['id_sape'], $login_sape, $password_sape, $price_array, $multiplication_sape);
			$result[$key]['pages_activated'] = $site_result['pages_activated'];	
			$result[$key]['id_sape'] = $site_result['id_sape'];				

		}		
		
		return $result;
	}
	
	
	//add 11_02_13-01_44
	public function activate_sape_pages_and_price($url, $id_sape, $login_sape, $password_sape, $price_array, $multiplication_sape){
	
		require_once($_SERVER['DOCUMENT_ROOT'] . '/libs/xmlrpc-3.0.0.beta/xmlrpc.inc');
		
		$path = 'xmlrpc/?v=extended';
		$host = 'api.sape.ru';
		$port = 80;		
		$password_sape = md5($password_sape);
		
		/*
		###отладка
		$login_sape = 'fenixion';
		$password_sape = md5('247356');
		$category_id = 10;
		$url = 'manikurny-pedikurny.ru';
		$price_array['rub_main_page_sape'] = 14.99;
		$price_array['rub_lvl_1_page_sape'] = 2.99;
		$price_array['rub_lvl_2_page_sape'] = 1.99;
		$count_main_links_sape = 3;
		$count_other_links_sape = 1;
		$multiplication_sape = 0.5;
		*/
		
		
		$connect = new xmlrpc_client(
			$path,
			$host,
			$port
		);
		 
		$query = new xmlrpcmsg(
				'sape.login',
				array(
					new xmlrpcval($login_sape, "string"),
					new xmlrpcval($password_sape, "string"),
					new xmlrpcval(true, "boolean") // шлём в md5
				)
		);
		
		
		//соединяемся
		for($i=0;$i<3;$i++){
		
			$response = $connect->send($query);
			if(!$response->faultCode()){
			
				$ret = php_xmlrpc_decode($response->value());
				break;
			}
			sleep(1);
		}  
		
		//пересылаем куки
		foreach ($response->_cookies as $name => $value) {
			$connect->setCookie($name, $value['value']);
		}
		
		//$connect->setdebug(2);				
		
		###работает
		//стоимость по PR расчитывается по формуле PR(предыдущий) * 0.5 = PR(следующий)
		//$price_rub_main_page = 14.99;
		$price_rub_main_page_array = $this->calculate_pr_price($price_array['rub_main_page_sape'], $multiplication_sape);		

		//$price_rub_lvl_1_page = 2.99;
		$price_rub_lvl_1_page_array = $this->calculate_pr_price($price_array['rub_lvl_1_page_sape'], $multiplication_sape);
		
		$price_rub_lvl_2_page_array = $this->calculate_pr_price($price_array['rub_lvl_2_page_sape'], $multiplication_sape);
		
				
		foreach($price_rub_main_page_array as $key => $value){
			$this->update_price_sape($connect, $id_sape, $level = 1, $price = $value, $pr = $key);
		}		
				
		foreach($price_rub_lvl_1_page_array as $key => $value){
			$this->update_price_sape($connect, $id_sape, $level = 2, $price = $value, $pr = $key);
		}
		
		foreach($price_rub_lvl_2_page_array as $key => $value){
			$this->update_price_sape($connect, $id_sape, $level = 3, $price = $value, $pr = $key);
		}		
		
		
		###активируем новые страницы
		$query = new xmlrpcmsg('sape.site_pages_activate',
			array(	
				new xmlrpcval($id_sape, "int")
			)		
		);	
		$site_pages_activate = $connect->send($query);
		$pages_activated = $site_pages_activate->val->me['int'];
			 
			
		$result['id_sape'] = $id_sape;
		$result['pages_activated'] = $pages_activated;		
		
		/*		
		###отладка
		echo '<pre> $result';
		print_r($result);
		echo '</pre>';
		*/
		
		return $result;
	}
	
	
	public function add_site_sape($url, $category_id, $login_sape, $password_sape, $count_main_links_sape = 3, $count_other_links_sape = 1){
	
		require_once($_SERVER['DOCUMENT_ROOT'] . '/libs/xmlrpc-3.0.0.beta/xmlrpc.inc');
		
		$path = 'xmlrpc/?v=extended';
		$host = 'api.sape.ru';
		$port = 80;		
		$password_sape = md5($password_sape);
		
		/*
		###отладка
		$login_sape = 'fenixion';
		$password_sape = md5('247356');
		$category_id = 10;
		$url = 'manikurny-pedikurny.ru';
		$price_array['rub_main_page_sape'] = 14.99;
		$price_array['rub_lvl_1_page_sape'] = 2.99;
		$price_array['rub_lvl_2_page_sape'] = 1.99;
		$count_main_links_sape = 3;
		$count_other_links_sape = 1;
		$multiplication_sape = 0.5;
		*/
		
		
		$connect = new xmlrpc_client(
			$path,
			$host,
			$port
		);
		 
		$query = new xmlrpcmsg(
				'sape.login',
				array(
					new xmlrpcval($login_sape, "string"),
					new xmlrpcval($password_sape, "string"),
					new xmlrpcval(true, "boolean") // шлём в md5
				)
		);
		
		
		//соединяемся
		for($i=0;$i<3;$i++){
		
			$response = $connect->send($query);
			if(!$response->faultCode()){
			
				$ret = php_xmlrpc_decode($response->value());
				break;
			}
			sleep(1);
		}  
		
		//пересылаем куки
		foreach ($response->_cookies as $name => $value) {
			$connect->setCookie($name, $value['value']);
		}
		
		//$connect->setdebug(2);
		
		
		###добавляем сайт		
		$site_url = 'http://' . $url;		
		$query = new xmlrpcmsg('sape.site_add',
					array(
						new xmlrpcval($site_url, "string"),
						new xmlrpcval($category_id, "int")			
					)		
				);	
		$response = $connect->send($query);

		$site_id = $response->val->me['int'];		
				
		
		###органичение на количество ссылок по уровням вложенности
		$query = new xmlrpcmsg('sape.site_set_max_per_page',
					array(
						new xmlrpcval($site_id, "int"),
						new xmlrpcval(
							array(
								"mpp_1" => new xmlrpcval($count_main_links_sape, "int"),
								"mpp_2" => new xmlrpcval($count_other_links_sape, "int"),
								"mpp_3" => new xmlrpcval($count_other_links_sape, "int")
							), "struct"
						)
								
					)		
				);	
		$connect->send($query);		
		
				
		/*
		###работает
		//стоимость по PR расчитывается по формуле PR(предыдущий) * 0.5 = PR(следующий)
		//$price_rub_main_page = 14.99;
		$price_rub_main_page_array = $this->calculate_pr_price($price_array['rub_main_page_sape'], $multiplication_sape);		

		//$price_rub_lvl_1_page = 2.99;
		$price_rub_lvl_1_page_array = $this->calculate_pr_price($price_array['rub_lvl_1_page_sape'], $multiplication_sape);
		
		$price_rub_lvl_2_page_array = $this->calculate_pr_price($price_array['rub_lvl_2_page_sape'], $multiplication_sape);
		
				
		foreach($price_rub_main_page_array as $key => $value){
			$this->update_price_sape($connect, $site_id, $level = 1, $price = $value, $pr = $key);
		}		
				
		foreach($price_rub_lvl_1_page_array as $key => $value){
			$this->update_price_sape($connect, $site_id, $level = 2, $price = $value, $pr = $key);
		}
		
		foreach($price_rub_lvl_2_page_array as $key => $value){
			$this->update_price_sape($connect, $site_id, $level = 3, $price = $value, $pr = $key);
		}		
		*/
		

		###устанавливаем настройки, мы здесь меняем только авторежим на 1, остальное так и было
		$query = new xmlrpcmsg('sape.site_update',
						array(
								new xmlrpcval($site_id, "int"),
								new xmlrpcval( 
									array(
									'auto' => new xmlrpcval(1, "int"),
									'flag_hide_url' => new xmlrpcval(0, "int"),
									'flag_use_unprintable_words_stop_list' => new xmlrpcval(1, "int"),
									'flag_use_adult_words_stop_list' => new xmlrpcval(1, "int"),
									'flag_reject_foreign_words' => new xmlrpcval(0, "int")
									), 'struct'	
								)
							)		
								
		);						
		$response = $connect->send($query);
		
		/*
		###работает
		###активируем новые страницы
		$query = new xmlrpcmsg('sape.site_pages_activate',
			array(	
				new xmlrpcval($site_id, "int")
			)		
		);	
		$site_pages_activate = $connect->send($query);
		$pages_activated = $site_pages_activate->val->me['int'];
			 
			
		$result['site_id'] = $site_id;
		$result['pages_activated'] = $pages_activated;
		*/
		
		/*		
		###отладка
		echo '<pre> $result';
		print_r($result);
		echo '</pre>';
		*/
		//$result['site_id'] = $site_id;
		
		return $site_id;
	}
	
	
	function update_price_sape($connect, $site_id, $level = 1, $price = 14.99, $pr = 0){

		$query = new xmlrpcmsg('sape.pages_update_price',
					array(
							new xmlrpcval($site_id, "int"),
							new xmlrpcval($level, "int"),
							new xmlrpcval($pr, "string"),
							new xmlrpcval($price, "string")
						)		
							
		);	
				
		$response = $connect->send($query);
		/*
		###отладка
		echo '<pre> $response ';
		print_r($response);
		echo '</pre>';
		*/
		return $response->val->me['boolean'];
	}
	
	
	
	public function add_site_mainlink($Url, $Name, $Description, $CategoryID, $login_mainlink, $password_mainlink, $price_array, $count_main_links = 3, $count_other_links = 1, $multiplication = 0.5, $stop_words = null){
	
		require_once($_SERVER['DOCUMENT_ROOT'] . '/libs/nusoap-0.9.5/nusoap.php');
		
		/*
		### параметры
		$Url = 'bazarrec.ru.com';
		$Name = 'Историческая реконструкция';
		$Description = 'Историческая реконструкция';
		$CategoryID = 9;
		$login_mainlink = 'drsoul';
		$password_mainlink = '247356';
		*/
		//$stop_words = 'курсовые,дипломы,варез,справки,phentermine,loans,drug,cheap,gambling,справк,диплом,казино,levitra,warez,досуг,slot,blackjack,insurance,tickets,hotels,фильмдетально,abilify,accutane,acetaminophen,acomplia,alprazolam,ambien,amitriptyline,anti-anxiety,anti-depressants,aripiprazole,ativan,baclofen,benadryl,bupropion,buspar,buspirone,buy,carisoprodol,casino,celebrex,celecoxib,celexa,cialis,citalopram,clonazepam,cocaine,crack,craps,credit,cyclobenzaprine,cymbalta,detox,detoxifying,diazepam,diclofenac';
				
		//$count_main_links = 3;	
		//$count_other_links = 1;	
				
		
		//стоимость по PR расчитывается по формуле PR(предыдущий) * 0.5 = PR(следующий)
		//$price_dol_main_page = 0.32;		
		$price_dol_main_page_array = $this->calculate_pr_price($price_array['dol_main_page'], $multiplication);
		
		//$price_rub_main_page = 9.99;
		$price_rub_main_page_array = $this->calculate_pr_price($price_array['rub_main_page'], $multiplication);
		
		//$price_dol_lvl_1_page = 0.05;
		$price_dol_lvl_1_page_array = $this->calculate_pr_price($price_array['dol_lvl_1_page'], $multiplication);
		
		//$price_rub_lvl_1_page = 1.49;
		$price_rub_lvl_1_page_array = $this->calculate_pr_price($price_array['rub_lvl_1_page'], $multiplication);
		
		//$price_dol_lvl_2_page = 0.03;
		$price_dol_lvl_2_page_array = $this->calculate_pr_price($price_array['dol_lvl_2_page'], $multiplication);
		
		//$price_rub_lvl_2_page = 0.99;
		$price_rub_lvl_2_page_array = $this->calculate_pr_price($price_array['rub_lvl_2_page'], $multiplication);

		//$price_dol_lvl_3_page = 0.02;
		$price_dol_lvl_3_page_array = $this->calculate_pr_price($price_array['dol_lvl_3_page'], $multiplication);
		
		//$price_rub_lvl_3_page = 0.49;
		$price_rub_lvl_3_page_array = $this->calculate_pr_price($price_array['rub_lvl_3_page'], $multiplication);		
		
				
		$login_url = "http://api.mainlink.ru/start.asmx?WSDL";
		$catalogs_url = "http://api.mainlink.ru/catalogs.asmx?WSDL";
		$webmaster_url = "http://api.mainlink.ru/webmaster.asmx?WSDL"; 

		$login = new nusoap_client($login_url, true); 
		$webmaster = new nusoap_client($webmaster_url, true); 
		$catalogs = new nusoap_client($catalogs_url, true); 
		$login->setUseCurl(1); 
		$login->call('sys_LogIn', array('Login' => $login_mainlink, 'Password' => $password_mainlink)); 
		$balance = $login->call('sys_Balance', array()); 
		$cookies = $login->getCookies(); 
		foreach ($cookies as $cookie) 
		{ 
			$webmaster->setCookie($cookie['name'], $cookie['value']); 
			$catalogs->setCookie($cookie['name'], $cookie['value']);
		} 
		
		//$UserID = $login->call('sys_UserId', array()); 

		//$res = $catalogs->call('mlapi_GetCategories', array());

		$res_2 = $webmaster->call('mlapi_AddSite', array(Url => $Url, Name => $Name, Description => $Description, CategoryID => $CategoryID, ScanSite => true, ));
		
		//Найти нужный индекс массива!!!!!!!!!!
		$SiteID = $res_2['mlapi_AddSiteResult'];
		//$SiteID = 799275;

		//количество ссылок
		$webmaster->call('mlapi_UpdateLinksCount', array(SiteID => $SiteID, PR => 0, ULevel => 0, Count => $count_main_links));
		$webmaster->call('mlapi_UpdateLinksCount', array(SiteID => $SiteID, PR => 1, ULevel => 0, Count => $count_main_links)); 
		$webmaster->call('mlapi_UpdateLinksCount', array(SiteID => $SiteID, PR => 2, ULevel => 0, Count => $count_main_links));  
		$webmaster->call('mlapi_UpdateLinksCount', array(SiteID => $SiteID, PR => 3, ULevel => 0, Count => $count_main_links));  
		$webmaster->call('mlapi_UpdateLinksCount', array(SiteID => $SiteID, PR => 4, ULevel => 0, Count => $count_main_links));  
		$webmaster->call('mlapi_UpdateLinksCount', array(SiteID => $SiteID, PR => 5, ULevel => 0, Count => $count_main_links));  

		$webmaster->call('mlapi_UpdateLinksCount', array(SiteID => $SiteID, PR => 0, ULevel => 1, Count => $count_other_links));  
		$webmaster->call('mlapi_UpdateLinksCount', array(SiteID => $SiteID, PR => 1, ULevel => 1, Count => $count_other_links));
		$webmaster->call('mlapi_UpdateLinksCount', array(SiteID => $SiteID, PR => 2, ULevel => 1, Count => $count_other_links));
		$webmaster->call('mlapi_UpdateLinksCount', array(SiteID => $SiteID, PR => 3, ULevel => 1, Count => $count_other_links));
		$webmaster->call('mlapi_UpdateLinksCount', array(SiteID => $SiteID, PR => 4, ULevel => 1, Count => $count_other_links));

		$webmaster->call('mlapi_UpdateLinksCount', array(SiteID => $SiteID, PR => 0, ULevel => 2, Count => $count_other_links));  
		$webmaster->call('mlapi_UpdateLinksCount', array(SiteID => $SiteID, PR => 1, ULevel => 2, Count => $count_other_links));
		$webmaster->call('mlapi_UpdateLinksCount', array(SiteID => $SiteID, PR => 2, ULevel => 2, Count => $count_other_links));
		$webmaster->call('mlapi_UpdateLinksCount', array(SiteID => $SiteID, PR => 3, ULevel => 2, Count => $count_other_links));
		$webmaster->call('mlapi_UpdateLinksCount', array(SiteID => $SiteID, PR => 4, ULevel => 2, Count => $count_other_links));

		$webmaster->call('mlapi_UpdateLinksCount', array(SiteID => $SiteID, PR => 0, ULevel => 3, Count => $count_other_links));  
		$webmaster->call('mlapi_UpdateLinksCount', array(SiteID => $SiteID, PR => 1, ULevel => 3, Count => $count_other_links));
		$webmaster->call('mlapi_UpdateLinksCount', array(SiteID => $SiteID, PR => 2, ULevel => 3, Count => $count_other_links));
		$webmaster->call('mlapi_UpdateLinksCount', array(SiteID => $SiteID, PR => 3, ULevel => 3, Count => $count_other_links));
		$webmaster->call('mlapi_UpdateLinksCount', array(SiteID => $SiteID, PR => 4, ULevel => 3, Count => $count_other_links));

		
		//цена в долларах		
		$webmaster->call('mlapi_UpdateLinksPrice', array(SiteID => $SiteID, PR => 0, ULevel => 0, Price => $price_dol_main_page_array[0] ));
		$webmaster->call('mlapi_UpdateLinksPrice', array(SiteID => $SiteID, PR => 1, ULevel => 0, Price => $price_dol_main_page_array[1] )); 
		$webmaster->call('mlapi_UpdateLinksPrice', array(SiteID => $SiteID, PR => 2, ULevel => 0, Price => $price_dol_main_page_array[2] ));  
		$webmaster->call('mlapi_UpdateLinksPrice', array(SiteID => $SiteID, PR => 3, ULevel => 0, Price => $price_dol_main_page_array[3] ));  
		$webmaster->call('mlapi_UpdateLinksPrice', array(SiteID => $SiteID, PR => 4, ULevel => 0, Price => $price_dol_main_page_array[4] ));  
		$webmaster->call('mlapi_UpdateLinksPrice', array(SiteID => $SiteID, PR => 5, ULevel => 0, Price => $price_dol_main_page_array[5] ));  

		$webmaster->call('mlapi_UpdateLinksPrice', array(SiteID => $SiteID, PR => 0, ULevel => 1, Price => $price_dol_lvl_1_page_array[0] ));  
		$webmaster->call('mlapi_UpdateLinksPrice', array(SiteID => $SiteID, PR => 1, ULevel => 1, Price => $price_dol_lvl_1_page_array[1] ));
		$webmaster->call('mlapi_UpdateLinksPrice', array(SiteID => $SiteID, PR => 2, ULevel => 1, Price => $price_dol_lvl_1_page_array[2] ));
		$webmaster->call('mlapi_UpdateLinksPrice', array(SiteID => $SiteID, PR => 3, ULevel => 1, Price => $price_dol_lvl_1_page_array[3] ));
		$webmaster->call('mlapi_UpdateLinksPrice', array(SiteID => $SiteID, PR => 4, ULevel => 1, Price => $price_dol_lvl_1_page_array[4] ));

		$webmaster->call('mlapi_UpdateLinksPrice', array(SiteID => $SiteID, PR => 0, ULevel => 2, Price => $price_dol_lvl_2_page_array[0] ));  
		$webmaster->call('mlapi_UpdateLinksPrice', array(SiteID => $SiteID, PR => 1, ULevel => 2, Price => $price_dol_lvl_2_page_array[1] ));
		$webmaster->call('mlapi_UpdateLinksPrice', array(SiteID => $SiteID, PR => 2, ULevel => 2, Price => $price_dol_lvl_2_page_array[2] ));
		$webmaster->call('mlapi_UpdateLinksPrice', array(SiteID => $SiteID, PR => 3, ULevel => 2, Price => $price_dol_lvl_2_page_array[3] ));
		$webmaster->call('mlapi_UpdateLinksPrice', array(SiteID => $SiteID, PR => 4, ULevel => 2, Price => $price_dol_lvl_2_page_array[4] ));

		$webmaster->call('mlapi_UpdateLinksPrice', array(SiteID => $SiteID, PR => 0, ULevel => 3, Price => $price_dol_lvl_3_page_array[0] ));  
		$webmaster->call('mlapi_UpdateLinksPrice', array(SiteID => $SiteID, PR => 1, ULevel => 3, Price => $price_dol_lvl_3_page_array[1] ));
		$webmaster->call('mlapi_UpdateLinksPrice', array(SiteID => $SiteID, PR => 2, ULevel => 3, Price => $price_dol_lvl_3_page_array[2] ));
		$webmaster->call('mlapi_UpdateLinksPrice', array(SiteID => $SiteID, PR => 3, ULevel => 3, Price => $price_dol_lvl_3_page_array[3] ));
		$webmaster->call('mlapi_UpdateLinksPrice', array(SiteID => $SiteID, PR => 4, ULevel => 3, Price => $price_dol_lvl_3_page_array[4] ));
		  
		//цена в рублях
		$webmaster->call('mlapi_UpdateLinksPriceRUR', array(SiteID => $SiteID, PR => 0, ULevel => 0, Price => $price_rub_main_page_array[0] ));
		$webmaster->call('mlapi_UpdateLinksPriceRUR', array(SiteID => $SiteID, PR => 1, ULevel => 0, Price => $price_rub_main_page_array[1] )); 
		$webmaster->call('mlapi_UpdateLinksPriceRUR', array(SiteID => $SiteID, PR => 2, ULevel => 0, Price => $price_rub_main_page_array[2] ));  
		$webmaster->call('mlapi_UpdateLinksPriceRUR', array(SiteID => $SiteID, PR => 3, ULevel => 0, Price => $price_rub_main_page_array[3] ));  
		$webmaster->call('mlapi_UpdateLinksPriceRUR', array(SiteID => $SiteID, PR => 4, ULevel => 0, Price => $price_rub_main_page_array[4] ));  
		$webmaster->call('mlapi_UpdateLinksPriceRUR', array(SiteID => $SiteID, PR => 5, ULevel => 0, Price => $price_rub_main_page_array[5] ));  

		$webmaster->call('mlapi_UpdateLinksPriceRUR', array(SiteID => $SiteID, PR => 0, ULevel => 1, Price => $price_rub_lvl_1_page_array[0] ));  
		$webmaster->call('mlapi_UpdateLinksPriceRUR', array(SiteID => $SiteID, PR => 1, ULevel => 1, Price => $price_rub_lvl_1_page_array[1] ));
		$webmaster->call('mlapi_UpdateLinksPriceRUR', array(SiteID => $SiteID, PR => 2, ULevel => 1, Price => $price_rub_lvl_1_page_array[2] ));
		$webmaster->call('mlapi_UpdateLinksPriceRUR', array(SiteID => $SiteID, PR => 3, ULevel => 1, Price => $price_rub_lvl_1_page_array[3] ));
		$webmaster->call('mlapi_UpdateLinksPriceRUR', array(SiteID => $SiteID, PR => 4, ULevel => 1, Price => $price_rub_lvl_1_page_array[4] ));

		$webmaster->call('mlapi_UpdateLinksPriceRUR', array(SiteID => $SiteID, PR => 0, ULevel => 2, Price => $price_rub_lvl_2_page_array[0] ));  
		$webmaster->call('mlapi_UpdateLinksPriceRUR', array(SiteID => $SiteID, PR => 1, ULevel => 2, Price => $price_rub_lvl_2_page_array[1] ));
		$webmaster->call('mlapi_UpdateLinksPriceRUR', array(SiteID => $SiteID, PR => 2, ULevel => 2, Price => $price_rub_lvl_2_page_array[2] ));
		$webmaster->call('mlapi_UpdateLinksPriceRUR', array(SiteID => $SiteID, PR => 3, ULevel => 2, Price => $price_rub_lvl_2_page_array[3] ));
		$webmaster->call('mlapi_UpdateLinksPriceRUR', array(SiteID => $SiteID, PR => 4, ULevel => 2, Price => $price_rub_lvl_2_page_array[4] ));

		$webmaster->call('mlapi_UpdateLinksPriceRUR', array(SiteID => $SiteID, PR => 0, ULevel => 3, Price => $price_rub_lvl_3_page_array[0] ));  
		$webmaster->call('mlapi_UpdateLinksPriceRUR', array(SiteID => $SiteID, PR => 1, ULevel => 3, Price => $price_rub_lvl_3_page_array[1] ));
		$webmaster->call('mlapi_UpdateLinksPriceRUR', array(SiteID => $SiteID, PR => 2, ULevel => 3, Price => $price_rub_lvl_3_page_array[2] ));
		$webmaster->call('mlapi_UpdateLinksPriceRUR', array(SiteID => $SiteID, PR => 3, ULevel => 3, Price => $price_rub_lvl_3_page_array[3] ));
		$webmaster->call('mlapi_UpdateLinksPriceRUR', array(SiteID => $SiteID, PR => 4, ULevel => 3, Price => $price_rub_lvl_3_page_array[4] ));

		
				
		$res_26 = $webmaster->call('mlapi_GetSite', array(SiteID => $SiteID));
		//$StopWords = 'курсовые,дипломы,варез,справки,phentermine,loans,drug,cheap,gambling,справк,диплом,казино,levitra,warez,досуг,slot,blackjack,insurance,tickets,hotels,фильмдетально,abilify,accutane,acetaminophen,acomplia,alprazolam,ambien,amitriptyline,anti-anxiety,anti-depressants,aripiprazole,ativan,baclofen,benadryl,bupropion,buspar,buspirone,buy,carisoprodol,casino,celebrex,celecoxib,celexa,cialis,citalopram,clonazepam,cocaine,crack,craps,credit,cyclobenzaprine,cymbalta,detox,detoxifying,diazepam,diclofenac';

		/*
		$res_26['mlapi_GetSiteResult']['Encoding'] = null;
		$res_26['mlapi_GetSiteResult']['WebServer'] = null;
		$res_26['mlapi_GetSiteResult']['Language'] = null;
		$res_26['mlapi_GetSiteResult']['Hosting'] = null;
		$res_26['mlapi_GetSiteResult']['StopWords'] = null;

		$res_26['mlapi_GetSiteResult']['WordsLength'] = 3;
		$res_26['mlapi_GetSiteResult']['WordsLengthBefore'] = 3;
		$res_26['mlapi_GetSiteResult']['WordsLengthAfter'] = 3;
		*/
		$res_26['mlapi_GetSiteResult']['Dictionaries']['Adult'] = true;
		$res_26['mlapi_GetSiteResult']['Dictionaries']['BadWords'] = true;
		$res_26['mlapi_GetSiteResult']['Dictionaries']['Games'] = true;
		$res_26['mlapi_GetSiteResult']['Dictionaries']['Politics'] = true;


		$res_26['mlapi_GetSiteResult']['StopWords'] = $stop_words;

		$SiteDetails['SiteDetails'] = $res_26['mlapi_GetSiteResult'];


		$res_25 = $webmaster->call('mlapi_UpdateSiteSettings', $SiteDetails ); 

			 
		/*
		###отладка
		echo '<pre>';
		print_r($res_25);
		echo '</pre>';
		*/
		
		return $res_25['mlapi_UpdateSiteSettingsResult'];
	}
	
	
	public function calculate_pr_price($price, $multiplication = 0.5, $precision = 2){
		###индексы это PR
		$price_res[0] = $price;
		$price_res[1] = round($price_res[0] + $price_res[0] * $multiplication, $precision);
		$price_res[2] = round($price_res[1] + $price_res[1] * $multiplication, $precision);
		$price_res[3] = round($price_res[2] + $price_res[2] * $multiplication, $precision);
		$price_res[4] = round($price_res[3] + $price_res[3] * $multiplication, $precision);
		$price_res[5] = round($price_res[4] + $price_res[4] * $multiplication, $precision);
		$price_res[6] = round($price_res[5] + $price_res[5] * $multiplication, $precision);
		
		return $price_res;
	}	

}
?>