<?
// no direct access
defined( '_EXEC' ) or die( 'Restricted access' );

require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/models/sql_model.class.php');

class Automatize extends Sql_model{


	public function curl_secure_connect($url, $login, $password, $params = array() ){
	
		$query = null;
		foreach($params as $key => $value){
			$query .= '&' . $key . '=' . urlencode($value);
		}
		
		$ch = curl_init();		

		curl_setopt($ch, CURLOPT_POST, 1); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 60); 	
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60); 		
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'login=' . urlencode($login) . '&password=' . urlencode($password) . $query );
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		$html = curl_exec($ch);
		curl_close($ch);
		
		return $html;
	}
	

	// 23_02_13-14_44
	public function add_cronjob_reg_domains(){
	
		$flag = 0;
		$file_path = $_SERVER['DOCUMENT_ROOT'] . '/cron/cronjobs/crontab.txt';
		$file = file($file_path); 

		for($i=0; $i<sizeof($file); $i++){		
			
			if(strpos($file[$i], $needle = 'reg_domains', $offset = 0) !== false ){
				$flag = 1;
			}
		}		
		
		if($flag == 0){			
			$line = '*/2    *    *    *    *      cronjobs/reg_domains.php';
			$file = $_SERVER['DOCUMENT_ROOT'] . '/cron/cronjobs/crontab.txt';
			file_put_contents($file,"\n$line", FILE_APPEND);
		}
	}
	
	
	// 23_02_13-14_44
	public function add_cronjob_reg_subdomains(){
	
		$flag = 0;
		$file_path = $_SERVER['DOCUMENT_ROOT'] . '/cron/cronjobs/crontab.txt';
		$file = file($file_path); 

		for($i=0; $i<sizeof($file); $i++){		
			
			if(strpos($file[$i], $needle = 'reg_subdomains', $offset = 0) !== false ){
				$flag = 1;
			}
		}		
		
		if($flag == 0){		
			$line = '*/2    *    *    *    *      cronjobs/reg_subdomains.php';
			$file = $_SERVER['DOCUMENT_ROOT'] . '/cron/cronjobs/crontab.txt';
			file_put_contents($file,"\n$line", FILE_APPEND);
		}
	}
	

	// 05_01_13 не используется пока
	public function get_file_search_queries(){
		
		$filename = $_SERVER['DOCUMENT_ROOT'] . '/words/search_queries_month.txt';
		$filedest = $_SERVER['DOCUMENT_ROOT'] . '/words/search_queries_month_processed.txt';		
		
		$content = file_get_contents($filename, $use_include_path = null, $context = null); //, $offset = -1, $maxlen	

		
		$file_black_list = $_SERVER['DOCUMENT_ROOT'] . '/words/black_list_for_queris.txt';
		$file_black_list_array = file($file_black_list);
		/*
		echo '<pre>';
		print_r($file_black_list_array);
		echo '</pre>';		
		*/
		
		/*
		$file_black_list_array[] = 'картинка';
		$file_black_list_array[] = 'сайт';
		$file_black_list_array[] = 'скачать';
		$file_black_list_array[] = 'отзывы';
		$file_black_list_array[] = 'смотреть';
		$file_black_list_array[] = 'качать';
		$file_black_list_array[] = 'скачать';
		$file_black_list_array[] = 'безплатно';
		$file_black_list_array[] = 'купить';
		$file_black_list_array[] = 'каталог';
		$file_black_list_array[] = 'заказать';
		$file_black_list_array[] = 'через';
		$file_black_list_array[] = 'читать';
		$file_black_list_array[] = 'цена';
		$file_black_list_array[] = 'чей';
		$file_black_list_array[] = 'торрент';
		$file_black_list_array[] = 'игры';
		$file_black_list_array[] = 'онлайн';
		$file_black_list_array[] = 'прайс';
		$file_black_list_array[] = 'цены';
		*/
		
		$pattern = "/[^- а-яА-ЯёЁ0-9\r\n\t]/iu";
		$replacement = "";
		$content = preg_replace($pattern, $replacement, $content);
		$content = nl2br($content, $is_xhtml = false);
		$content_array = explode('<br>', $content);
		$content = null;
		
		foreach($content_array as $key => $value){	
			$tmp_array_str = explode(Chr(9), $value);
			if($tmp_array_str[1] > 0 AND $tmp_array_str[1] < 26){
			
				//проверяем количество слов в запросе, выбираем запросы с количеством слов больше 2х
				$words = explode(' ', trim($tmp_array_str[0]) );
				if(is_array($words) AND count($words) > 0 AND count($words) < 2){					
					
					//проверка чёрным списком
					$flag_b_list_q = 0;
					foreach($file_black_list_array as $key_1 => $value_1){
						
						
						$pattern = "/[^- а-яА-ЯёЁ0-9]/iu";
						$replacement = "";
						$value_1 = preg_replace($pattern, $replacement, $value_1 );				
						
						
						//echo $value_1 . '<br>';
						
						if( mb_strpos($tmp_array_str[0], trim($value_1), $offset = 0, $encoding = 'utf-8') == true ){
						
							$flag_b_list_q = 1;							
						}
						
					}

					if($flag_b_list_q == 0){
						$string_array[$key] = $tmp_array_str;
					}
				}
				
			}
			 
		}
		$content_array = null;
		
		/*
		###отладка
		echo 'Количество элементов: ' . count($string_array);
		echo '<br>';
		echo '<pre>';
		print_r($string_array);
		echo '</pre>';
		*/
		
		//$result = file_put_contents($filedest, $content, $flags = 0, $context = null);		
		//return $result;
	}
	
	//$texts_link берётся из метода класса в парсерах - get_subcats_for_cats
	public function add_sites_blanks($texts_link){
		
		$i = 0;
		foreach($texts_link as $key => $value){
			
			
			foreach($value['texts'] as $key_1 => $value_1){
				$tmp_texts['texts'][$i] = $value_1;
				$tmp_texts['number_cats_sem'][$i] = $key;
				$i++;
				
				
				if($i == 2){
					if($tmp_texts['number_cats_sem'][0] != $tmp_texts['number_cats_sem'][1]){
					
						echo 'Ok! - $tmp_texts[number_cats_sem][0] != $tmp_texts[number_cats_sem][1]<br>';
						
						unset( $tmp_texts['texts'][1] );
						//$this->insert_site_blank($tmp_texts); //сделать этот метод!!!
						//$name_site = mb_strtoupper($tmp_texts['texts'][0], $enc = 'utf-8');
						$name_site = $this->mb_ucfirst_my($tmp_texts['texts'][0], $enc = 'utf-8');
						$search_phrases = $tmp_texts['texts'][0];
						$id_site = $this->insert_site($search_phrases, $name_site, $domain = 'site_1', $id_tags = 27);
						//$this->set_id_sites($id_site);
						
						//$this->select_or_insert_search_phrase($tmp_texts['texts'][0]);
						$this->insert_search_phrases($id_site, $search_phrases);
						
						unset( $tmp_texts['texts'][0] );
					}else{
					
						echo 'No! - $tmp_texts[number_cats_sem][0] == $tmp_texts[number_cats_sem][1]<br>';
						
						//$this->select_or_insert_search_phrase($search_phrase);
						//$this->insert_site_blank($tmp_texts);
						//$name_site = mb_strtoupper($tmp_texts['texts'][0], $enc = 'utf-8');
						$search_phrases = $tmp_texts['texts'][0] . chr(13) . $tmp_texts['texts'][1];
						$name_site = $this->mb_ucfirst_my($tmp_texts['texts'][0], $enc = 'utf-8');
						$id_site = $this->insert_site($search_phrases, $name_site, $domain = 'site_1', $id_tags = 27);
						//$this->set_id_sites($id_site);
						//$this->select_or_insert_search_phrase($tmp_texts['texts'][0]);
						//$this->select_or_insert_search_phrase($tmp_texts['texts'][1]);
						$this->insert_search_phrases($id_site, $search_phrases);
						
						unset( $tmp_texts['texts'][0] );
						unset( $tmp_texts['texts'][1] );
					}
					
					$i = 0;
				}
				
				/*
				if($i >= 2){
					$i = 0;
					$this->insert_site_blank($tmp_texts);
				}
				*/
			}			 
		}
	}
	
	//первая буква в заглавную
	public function mb_ucfirst_my($str, $enc = 'utf-8'){ 
    		return mb_strtoupper(mb_substr($str, 0, 1, $enc), $enc) . mb_substr($str, 1, mb_strlen($str, $enc), $enc); 
    }

	public function chmod_update($chmod = 777){
	
		$sites_primary_grab_1 = $this->select_sites_by_primary_grab($primary_grab = 1);
		
		$paths[] = 'money_house/4e7b5aa94775b09ccb08bad421666f2a5b275a0b';
		$paths[] = 'money_house/1284bf6b955e0d3582972f3b20d80e38';
		$paths[] = 'money_house/mainlinkcxxc_39841/data';
		$paths[] = 'money_house/setlinks_4681f/cache';
		
		$paths[] = 'images/images';
		$paths[] = 'images/video_thumb';
		
		$paths[] = 'templates_c';
		
		
		
		foreach($sites_primary_grab_1 as $key => $value){
			
			foreach($paths as $key_1 => $value_1){
				//$path = $_SERVER['DOCUMENT_ROOT'] . '/site_' . $value['id_sites'] . '/' .  $value_1;
				echo $path . ' !!!!<br>';
				chmod($path, $chmod);
			}
		}

	}
	
	
	public function unpublish_duplicate_cat($id_sites){
	
		$phrases = $this->select_phrases_by_id_site_primary_add_0($id_sites, $limit = 500, $offset = 0);
		
		$separator = null;
		foreach($phrases as $key => $value){
			
			foreach($phrases as $key_1 => $value_1){
			
				###отладка
				//echo $value['search_phrase'] . '-' . $value_1['search_phrase'] . '<br>';

				
				if( mb_strpos( $value['search_phrase'], $value_1['search_phrase'], $offset = 0,  $encoding = 'utf-8') !== false AND $value['id'] != $value_1['id']){
					
					###отладка
					//echo $value['search_phrase'] . ' - ' . $value_1['search_phrase'] . 'Ok!!!<br>';
					
					$ids_cat_duplicates = $separator . $value_1['id'];
					$separator = ',';
					
				}
			}
		}
				
		if( !empty($ids_cat_duplicates) ){
			$this->update_phrase_publish($ids_cat_duplicates, $publish_status = 0);
		}
	}
	

	public function delete_small_content_sites($count_text_censor = 50){
	
		$count_texts_for_sites_primary_grab_1 = $this->select_count_texts_for_sites_primary_grab_1_left_join_text();

		foreach($count_texts_for_sites_primary_grab_1 as $key => $value){
		
			if($value['count'] < $count_text_censor){
				//echo '$value[count]-' . $value['count'] . '------$count_text_censor-' . $count_text_censor . '<br>';
				$ids_sites .= $value['id_site'] . ',';
			}
		}

		//удаляет последнюю запятую
		$ids_sites = preg_replace('/,$/isu', '', $ids_sites);		
				
		$this->delete_sites_small_text($ids_sites);
		
		$ids_sites_array = explode(',', $ids_sites);
		
		//удаляет папки файлов и файл данных для жукладочника из основной папки жукладочника
		foreach($ids_sites_array as $key => $value){
		
			//удаление файла с данными для жукладочника из общей папки жукладочника
			$path = $_SERVER['DOCUMENT_ROOT'] . '/bagbookmark/bagbookmark_' . $value;
			
			if(is_file($path)){
				unlink($path);
			}
			
			//удаление дириктории сайта
			$path = $_SERVER['DOCUMENT_ROOT'] . '/site_' . $value;		
			
			$this->remove_dir($path);
		}
	}	
	
	
	//выводит список сайтов (домены и названия)
	//add 05_11_12 22_58
	public function echo_list_sites(){
	
		$sites = $this->select_all_info_sites_by_primary_grab($primary_grab = 1);
		
		return $sites;
	}	
	
	
	//добавляет рекламный блок на все сайты
	//add 08_11_12 01_04
	public function add_ads($name_ads, $ads_position, $ads_sorting, $code_ads){
		
		$sites = $this->select_all_info_sites_by_primary_grab($primary_grab = 1);
		
		$id_ads = $this->select_or_insert_ads($name_ads, $code_ads);
		
		$ids_sites = '';
		$separator = '';
		foreach($sites as $key => $value){	
			$ids_sites .= $separator . $value['id'];			
			
			$separator = ', ';
		}
		
		$ads_links = $this->select_ads_links($ids_sites, $id_ads);
		
		if( is_array($ads_links) ){
			
			foreach($sites as $key => $value){
			
				foreach($ads_links as $key_1 => $value_1){
				
					//если ссылка на объявление с таким id уже есть для этого сайта то удаляем id сайта из итогового набора ids
					if($value['id'] == $value_1['id_sites']){
						
						unset($sites[$key_1]);
					}
					
				}
				
			}			
		}
		
		
		$last_insert_ids_array = $this->insert_mass_ads_links($sites, $id_ads, $ads_position, $ads_sorting);
		
		
		return $last_insert_ids_array;
	}	
		
	
	//регистрация liveinternet.ru
	//add 01_11_12 23_49
	public function liveinternet_reg(){
	
		$sites = $this->select_all_info_sites_by_primary_grab($primary_grab = 1);

		$liveinternet_result = '';
		
		foreach($sites as $key => $value){		

			if( empty($value['name']) ){
				$value['name'] = 'Домашняя страница';
			}
			
			if( !empty($value['domain']) ){
			
				if( $this->liveinternet('http://' . $value['domain'], $value['name']) ){
				
					$liveinternet_result[$value['domain']] = '<span style="color:green;">ок</span>';
				}else{
				
					$liveinternet_result[$value['domain']] = '<span style="color:red;">НЕ ЗАРЕГЕСТРИРОВАН!</span>';
				}
				
			}
		}	
		
		//вставка счётчиков liveinternet в базу данных		
		$id_ads = $this->select_or_insert_liveinternet('counter_liveinternet.ru');		
		
		$delemiter = '';
		foreach($sites as $key => $value){				
			$ids_sites .= $delemiter . $value['id'];			
			$delemiter = ', ';
		}
		
		$sites_with_ads_liveinternet = $this->select_ads_liveinternet($ids_sites, 'counter_liveinternet.ru');
		
		if( is_array($sites_with_ads_liveinternet) ){
		
			$delemiter = '';
			foreach($sites_with_ads_liveinternet as $key => $value){			
				$ids_sites_with_ads_liveinternet .= $delemiter . $value['id_sites'];
				$delemiter = ', ';				
			}			
			
			$ids_sites_with_ads_liveinternet_array = explode(', ', $ids_sites_with_ads_liveinternet);
			$ids_sites_with_ads_liveinternet_array = array_unique($ids_sites_with_ads_liveinternet_array);			
			$ids_sites_array = explode(', ', $ids_sites);			
			$ids_sites_array = array_unique($ids_sites_array);	
			
			//формирование массива для обновление рекламного статуса у сайтов
			$ids_sites_array_for_ads_status_update = $ids_sites_array;
			
			foreach($ids_sites_with_ads_liveinternet_array as $key => $value){
				//$value['id_sites'];
				
				//если находим id сайта для которого уже есть реклама, то удаляем его из набора сайтов которым будем добавлять рекламу
				if(in_array($value, $ids_sites_array)){
					//unset($ids_sites_array[$value]);
					
					$ids_sites_array = array_flip($ids_sites_array); //Меняем местами ключи и значения
					unset ($ids_sites_array[$value]) ; //Удаляем элемент массива
					$ids_sites_array = array_flip($ids_sites_array);
				}
			}			
			
			$delemiter = '';
			foreach($ids_sites_array as $key => $value){			
				$ids_sites_finish .= $delemiter . $value['id_sites'];
				$delemiter = ', ';				
			}				
			
			//$this->insert_ads_liveinternet($ids_sites_finish);
			$this->insert_ads_links($id_ads, $ids_sites_array);
			
		}else{
			//если сайтов с счётчиком до этого небыло тогда вставляем счётчики для всей выборки сайтов
			//$this->insert_ads_liveinternet($ids_sites);			
			$ids_sites_array = implode(', ', $ids_sites);
			$ids_sites_array = array_unique($ids_sites_array);			
			$this->insert_ads_links($id_ads, $ids_sites_array);
			
			//формирование массива для обновление рекламного статуса у сайтов
			$ids_sites_array_for_ads_status_update = $ids_sites_array;
		}	

		
		//обновление рекламного статуса у сайтов
		$delemiter = '';
		foreach($ids_sites_array_for_ads_status_update as $key => $value){			
			$ids_sites_for_in .= $delemiter . $value;
			$delemiter = ', ';				
		}		
		
		$this->update_sites_ads_status($ids_sites_for_in);		

		return $liveinternet_result;
	}


	//add 01_11_12 23_49
	function liveinternet($url, $name = "Домашняя страница"){
		
		$page = "http://www.liveinternet.ru/add";
		$password = "159753"; // ПАРОЛЬ ДЛЯ ДОСТУПА К СТАТИСТИКЕ
		$email = "fenixthe@mail.ru";  // EMAIL
		$random = rand(10000000, 9999999999);
		$nick = str_replace('http://', '', $url);
		$nick = str_replace('/', '', $nick);
		$counter_code = "document.write(\"<a href='http://www.liveinternet.ru/click' target=_blank><img src='http://counter.yadro.ru/hit?t50.6;r\" + escape(document.referrer) + ((typeof(screen)==\"undefined\")?\"\":\";s\"+screen.width+\"*\"+screen.height+\"*\"+(screen.colorDepth?screen.colorDepth:screen.pixelDepth)) + \";u\" + escape(document.URL) + \";\" + Math.random() + \"' border=0 width=31 height=31 alt='' title='LiveInternet'><\/a>\");\n"; //ЭТО ЕСЛИ НА САЙТ/ДОР ВСТАВЛЯТЬ СРАЗУ
		$fields = "random=$random&rules=agreed&type=site&nick=$nick&url=$url&name=$name&email=$email&password=$password&check=$password&keywords=&aliases=&group=&private=off&subscribe=off&www=&confirmed=";

		$ch = curl_init($page);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

		$response = curl_exec($ch);
		curl_close($ch);

		if(strpos($response,"action=/code") === false){
			return false;
		}else{
			return true;
		}
	}  	
	
	
	function remove_dir($path){
		if(file_exists($path) && is_dir($path)){
			$dirHandle = opendir($path);
			
			while(false!==($file = readdir($dirHandle))){
				if($file!='.' && $file!='..'){
				
					$tmpPath = $path.'/'.$file;
					chmod($tmpPath, 0777);
					
					if(is_dir($tmpPath)){
						$this->remove_dir($tmpPath);
					} else {
						if(!unlink($tmpPath)) echo 'Не удалось удалить файл «'.$path.'»!';
					}
				}				
			}
			closedir($dirHandle);
			 
			// удаляем текущую папку
			if(!rmdir($path)){
				echo 'error', 'Не удалось удалить папку «'.$path.'»!';
			}
			 
		} else {
			echo 'error', 'Папки «'.$path.'» не существует!';
		}
	}	
	

	public function refresh_create_sites_small_content($count_text_censor = 10){
	
		$count_texts_for_sites_primary_grab_1 = $this->select_count_texts_for_sites_primary_grab_1_left_join_text();		 
		 
		foreach($count_texts_for_sites_primary_grab_1 as $key => $value){
		
			if($value['count'] < $count_text_censor){
				//echo '$value[count]-' . $value['count'] . '------$count_text_censor-' . $count_text_censor . '<br>';
				$ids_sites .= $value['id_site'] . ',';
			}
		}
		
		//удаляет последнюю запятую
		$ids_sites = preg_replace('/,$/isu', '', $ids_sites);		
		
		$this->update_mass_site_primary_grab($ids_sites, $primary_grab = 0);
		
		/*
		###отладка
		echo '<pre>';
		echo $ids;
		print_r($count_texts_for_sites_primary_grab_1);
		echo '</pre>';
		*/
	}	
	

	public function create_icon($id_site){
		$images = $this->select_images_by_id_site_for_favicon($id_site);
		
		//$imgContent = file_get_contents($images[0]['file_name']);
		/*
		#отладка
		echo '<pre>';
		print_r($images);
		echo '</pre>';
		*/
		$infile = $_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/images/images/' . $images[0]['file_name'];
		$outfile = $_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/images/favicon/favicon.ico';
		
		$filesize = filesize($infile);
		//echo '<br>' . $filesize;
		
		if($filesize > 0){
			$this->imageresize($outfile, $infile, $neww = 16, $newh = 16, $quality = 80);
		}	
	}
	
	
	function imageresize($outfile,$infile,$neww,$newh,$quality){

		$im=imagecreatefromjpeg($infile);
		$im1=imagecreatetruecolor($neww,$newh);
		imagecopyresampled($im1,$im,0,0,0,0,$neww,$newh,imagesx($im),imagesy($im));

		imagejpeg($im1,$outfile,$quality);
		imagedestroy($im);
		imagedestroy($im1);
    }
	
	//изображения которые не пошли в тексты используются в оформлении сайта
	public function bind_images_on_texts($imgs_on_text_from = 1, $imgs_on_text_to = 3){//$percent_imgs_on_texts = 80, $imgs_on_text_from = 1, $imgs_on_text_to = 3
		$result = $this->select_images_on_texts();
		/*
		#отладка
		echo '$result<pre>';
		print_r($result);
		echo '<pre>';
		*/
		$count_loop = 0;
		$count_loop_local = 0;
		foreach($result as $key => $value){
			
			
			if($value['id_text'] != $temp_id_text){
				$number_images = rand($imgs_on_text_from, $imgs_on_text_to);
				$count_loop_local = 0;
				
				$fin_result[$count_loop]['id_text'] = $value['id_text'];
				
				//записываем наш номер массива чтобы далее писать в него ид изображений
				$temp_count_loop = $count_loop;
			}
			
			$temp_id_text = $value['id_text'];
			
			if($value['id_text'] == $temp_id_text){
				
				//while($count_loop_local < $number_images){
				
					//проверяю использовался ли ранее id этой фото для привязки к тексту
					$flag_old_use_id_image = 0;					
					foreach($fin_result as $key_2 => $value_2){
					
						$array_id_images = $fin_result[$key_2]['id_images'];
						if(is_array($array_id_images)){
							foreach($array_id_images as $key_3 => $value_3){
								if($value_3 == $value['id_images']){
									$flag_old_use_id_image = 1;
								}
							}
						}
						
					}
					
					/*
					$temp_array_id_images = 0;
					foreach($fin_result as $key_2 => $value_2){
					
						if(is_array($value['id_images'])){
							foreach($value['id_images'] as $key_3 => $value_3){
								$temp_array_id_images[] = $value_3;
							}
						}
						$temp_array_id_images[] = $fin_result[$key_2]['id_images'];						
					}
					*/
					if($flag_old_use_id_image == 0){
						if(count($fin_result[$temp_count_loop]['id_images']) < $number_images){
							$fin_result[$temp_count_loop]['id_images'][] = $value['id_images'];
							$count_loop_local++;
						}
					}
					
					/*
					foreach($result as $key_1 => $value_1){
						if($result[$key_1]['id_images'] == $value['id_images']){
							unset($result[$key_1]);
						}
					}
					*/
					
				//}
				
			}
			$this->logs_grab($fin_result, $title = 'bind_images_on_texts_N_' . $count_loop);
			
			$count_loop++;
		}		
		
		/*
		###отладка			
		echo '$fin_result<pre>';
		print_r($fin_result);
		echo '</pre>';
		*/				
		
		$this->update_images_id_text($fin_result);
		
		//return $test_res;//fin_result
	}
	
	
	protected function update_images_id_text($fin_result){
	
		$num_rows = '';
		
		//делаю строку перечисление ids_images для функции sql IN и id_text
		foreach($fin_result as $key => $value){
			$ids_images = '';
		
			$id_texts = $value['id_text'];
			
			//если существуют ид изображений создаём строку перечисление
			if(isset($value['id_images'])){
				
				$count_loop = 0;
				$count_id_images = count($value['id_images']);
				
				while($count_loop < $count_id_images){
					
					$separator = '';
					if($count_loop + 1 < $count_id_images){
						$separator = ', ';
					}
					
					$ids_images .= $value['id_images'][$count_loop] . $separator;
					
					$count_loop++;
				}
			}
			/*
			##отладка			
			echo 'id_texts: ' . $id_texts . '<br>';			
			echo '$ids_images<pre>';
			print_r($ids_images);
			echo '</pre>';
			*/
			
			if(!empty($ids_images)){
				//отправляю сделанное в для обновления строк в баще
				$this->update_images_on_texts($ids_images, $id_texts);
			}

			//$num_rows .= $this->update_images_on_texts($ids_images, $id_texts);
			//$num_rows .= ' | ';
		}
			
		//return $num_rows; //num_rows
	}
	
	
	//создаёт шаблон для сайта (собраны методы из класса Templates)
	public function create_template($id_site){
			
			require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/templates.class.php');
		
			$templates = new Templates;
			//$id_site = 5;
			
			$templates->mkdirs_site($id_site);
			//$url = $_SERVER['DOCUMENT_ROOT'] . '/classes/';
			$templates->copy_dirs_and_files($id_site, 'classes');

			$templates->create_index_php($id_site, $per_page_f = 5, $per_page_t = 10, $num_page_f = 2, $num_page_t = 10, $header_images_count_f = 3, $header_images_count_t = 14, $per_page_gallery_f = 10, $per_page_gallery_t = 36);

			$templates->create_rand_struct_template_level_1($id_site, 'index.html');
			$templates->create_rand_struct_template_level_1($id_site, 'article.html');
			$templates->create_rand_struct_template_level_1($id_site, 'category.html');
			$templates->create_rand_struct_template_level_1($id_site, 'video.html');
			$templates->create_rand_struct_template_level_1($id_site, 'gallery.html', $count_sequence_1 = 2);

			$templates->create_rand_struct_template_level_2($id_site, 'article_block.html', $count_sequence_1 = 2, $count_sequence_2 = 2);
			$templates->create_rand_struct_template_level_2($id_site, 'article_list_category.html', $count_sequence_1 = 2, $count_sequence_2 = 2);
			$templates->create_rand_struct_template_level_2($id_site, 'article_list_main.html', $count_sequence_1 = 2, $count_sequence_2 = 2);
			$templates->create_rand_struct_template_level_2($id_site, 'gallery_block.html', $count_sequence_1 = 2, $count_sequence_2 = 2);
			$templates->create_rand_struct_template_level_2($id_site, 'menu.html', $count_sequence_1 = 2, $count_sequence_2 = 2);
			$templates->create_rand_struct_template_level_2($id_site, 'menu_gallery.html', $count_sequence_1 = 2, $count_sequence_2 = 2);
			$templates->create_rand_struct_template_level_2($id_site, 'meta_header.html', $count_sequence_1 = 2, $count_sequence_2 = 2);
			$templates->create_rand_struct_template_level_2($id_site, 'title_site.html', $count_sequence_1 = 2, $count_sequence_2 = 2);
			$templates->create_rand_struct_template_level_2($id_site, 'video_block.html', $count_sequence_1 = 2, $count_sequence_2 = 2);
			$templates->create_rand_struct_template_level_2($id_site, 'videos_list.html', $count_sequence_1 = 2, $count_sequence_2 = 2);
			$templates->create_rand_struct_template_level_2($id_site, 'header.html', $count_sequence_1 = 2, $count_sequence_2 = 2);
			
			//add 09_11_12 22_57
			$templates->create_rand_struct_template_level_2($id_site, 'links_block_1.html', $count_sequence_1 = 2, $count_sequence_2 = 2);
			$templates->create_rand_struct_template_level_2($id_site, 'links_block_2.html', $count_sequence_1 = 2, $count_sequence_2 = 2);
			$templates->create_rand_struct_template_level_2($id_site, 'links_block_3.html', $count_sequence_1 = 2, $count_sequence_2 = 2);

			$templates->create_css($id_site, 'main.css');
			$templates->copy_files($id_site);
			
			$templates->mkdirs_and_files_for_gallery($id_site);
			$templates->mkdirs_and_files_for_links_change($id_site);
			
			$templates->randomize_css_class_names($id_site);					
			
	}
	
	
	//если $percent = true то лимиты считаются в процентах (передавать без %, целыми числами)
	//и необходимо указать $id_site для которого происходит обновление
	public function updater_status($limit_from = 0, $limit_to = 2, $percent = false, $id_site = 0){
	
		if($percent == false){
			$sites_for_update = $this->select_sites_update_1();
			
			foreach($sites_for_update as $key => $value){
				
				//echo $value['id'];
				$ids_phrases = $this->select_ids_phrases_by_id_site($value['id']);
				//echo '<br><br><br><br><br><br>site - ' . $value['id'] . '<br>';
				//print_r($ids_phrases);
				
				$count_loop = 1;
				$separator = '';
				$IN_ids = '';
				foreach($ids_phrases as $key_1 => $value_1){
					
					if(!empty($IN_ids)){
						$separator = ' ,';
					}					
					
					/*
					if($count_loop == count($ids_phrases)){
						$separator = '';
					}
					*/
					$IN_ids .= $separator . $value_1['id'];
					
					$count_loop++;
				}
				
				//echo '<br>' . $IN_ids;
				
				if(!empty($IN_ids)){
					
					$limit = rand($limit_from, $limit_to);
					$this->update_videos_status($IN_ids, $limit);
					//echo '<br>Обновлено строк videos ' . mysql_affected_rows();
					
					$limit = rand($limit_from, $limit_to);
					$this->update_texts_status($IN_ids, $limit);
					//echo '<br>Обновлено строк texts ' . mysql_affected_rows();
					
					$limit = rand($limit_from, $limit_to);
					$this->update_images_status($IN_ids, $limit);
					//echo '<br>Обновлено строк images ' . mysql_affected_rows();
				
				}				
			}
			
		}else{
		
			$count_texts = $this->select_count_texts_by_id_site_for_updater($id_site);
			$count_images = $this->select_count_images_by_id_site_for_updater($id_site);
			$count_videos = $this->select_count_videos_by_id_site_for_updater($id_site);
			
			$limit_percent = rand($limit_from, $limit_to);
		
			//echo '<br>$limit_percent: ' . round($limit_percent / 100, 2);
			
			$limit_decimal = round($limit_percent / 100, 2);
						
			$limit_texts = round($count_texts['count'] * $limit_decimal);
			//echo '<br>$limit_texts: ' . $limit_texts;
			
			$limit_videos = round($count_videos['count'] * $limit_decimal);
			//echo '<br>$limit_videos: ' . $limit_videos;
			
			$limit_images = round($count_images['count'] * $limit_decimal);
			//echo '<br>$limit_images: ' . $limit_images;
			
			
			/*
			###отладка
			echo '<pre>';
			print_r($count_texts);
			print_r($count_images);
			print_r($count_videos);
			echo '</pre>';
			*/
			
			//$id_site берётся из параметра
			$ids_phrases = $this->select_ids_phrases_by_id_site($id_site);
			
			$count_loop = 1;
			$separator = '';
			foreach($ids_phrases as $key => $value){
				
				if(!empty($IN_ids)){
					$separator = ' ,';
				}						

				$IN_ids .= $separator . $value['id'];
					
				$count_loop++;
			}
				
			
				
			if(!empty($IN_ids)){				
				
				$this->update_texts_status($IN_ids, $limit_texts);
				/*
				echo '<br>Обновлено строк texts ' . mysql_affected_rows();
				echo '<br>$IN_ids: ' . $IN_ids;
				echo '<br>$limit_texts: ' . $limit_texts;
				*/
				
				$this->update_videos_status($IN_ids, $limit_videos);
				/*
				echo '<br>Обновлено строк videos ' . mysql_affected_rows();					
				echo '<br>$IN_ids: ' . $IN_ids;
				echo '<br>$limit_videos: ' . $limit_videos;
				*/
				
				$this->update_images_status($IN_ids, $limit_images);
				/*
				echo '<br>Обновлено строк images ' . mysql_affected_rows();
				echo '<br>$IN_ids: ' . $IN_ids;
				echo '<br>$limit_images: ' . $limit_images;
				*/
			}		

		}
	}
	
	
	//обновлятор контента (для крона)
	public function updater_content(){
	
		$sites_for_update = $this->select_sites();
		//$temp = $this->select_count_sites();
		//$sites_count = $temp['count_sites'];
		$current_site_for_update = $this->select_current_site_for_update();
	
		/*
		###отладка
		print_r($sites_for_update);
		print_r($current_site_for_update);
		*/
	
		/*
		// если вышли за границы существующих сайтов скидываем счётчик в 1
		if($current_site_for_update['value'] > $sites_for_update[0]['count_sites']){
			$current_site_for_update['value'] = 1;
		}
		*/
		//echo '<br>$current_site_for_update[value]: ' . $current_site_for_update['value'];
		$count_and_max_id = $this->select_count_and_max_id_sites();
		//определение количества сайтов
		$count_sites = $count_and_max_id['count_sites'];
		$max_sites = $count_and_max_id['max_sites'];
		
		//echo '<br>$count_sites: ' . $count_sites;
		
		$current_site = $this->search_site_id_in_db($count_sites, $sites_for_update, $current_site_for_update['value']);
		//echo '<br>$current_site - сразу ' . $current_site;
		
		require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/assemble_grab.class.php');
		$assemble_grab = new Assemble_grab;
				
		//echo '<br>$current_site - до ' . $current_site;
		
		//собираем контект для текущего обновляемого сайта
		$assemble_grab->grab_main_loop_second($current_site);
		
		//инкриментируем текущий сайт дляо бновления
		$current_site++;
		//echo '<br>$current_site - после инкримента: ' . $current_site . '<br>';
		//если больше максимального id то приравниваем к 1
		if($current_site > $max_sites){
			$current_site = 1;
		}
		
		//echo '<br>$current_site - после ' . $current_site;
		//обновляем сайта в базе
		$this->update_current_site_for_update($current_site);
		
		return 'Обновление сайта ' . $current_site . ' завершено!';
	}
	
	//выдаёт либо текущий сайт для апдейта, либо следующий если у текущего апдейт отключе (и тд), либо false, если апдейт отключен у всех сайтов
	protected function search_site_id_in_db($count_sites, $sites_for_update, $current_site_for_update, $flag_in_array = 0, $count_loop = 0){
		//$flag_in_array = 0;
		//$count_loop = 0;
		foreach($sites_for_update as $key => $value){
			//echo '<br>$sites_for_update[$key][id_sites]: ' . $sites_for_update[$key]['id_sites'];
			//echo '<br>$key: ' . $key;
			
			//echo '<br>$current_site_for_update: ' . $current_site_for_update;
			
			if($sites_for_update[$key]['id_sites'] == $current_site_for_update){
				$flag_in_array = 1;
				
				//echo '<br>Найдено совпадения сайта для апгрейда!!!' . $current_site_for_update . '<br>';
				break;
			}
		}
		
		if($flag_in_array == 0){
			$current_site_for_update++;
			$count_loop++;
			
			if($count_loop > $count_sites){
				return '101-';
			}
			//echo '<br>!!!' .  $sites_for_update[$key]['id_sites'];
			//echo '<br>!!!' . $count_loop;
			//echo '<br>!!!' . $current_site_for_update;
			
			$current_site_for_update = $this->search_site_id_in_db($count_sites, $sites_for_update, $current_site_for_update, $flag_in_array, $count_loop);
		}
		
		return $current_site_for_update;
	}
}
?>