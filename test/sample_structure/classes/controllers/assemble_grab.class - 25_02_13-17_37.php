<?
// no direct access
defined( '_EXEC' ) or die( 'Restricted access' );

require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/abstract_auth.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/auth_rambler.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/grabs.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/parsers.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/automatize.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/templates.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/models/root_model.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/models/sql_model.class.php');

require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/smarty/Smarty.class.php');

require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/wordstat_yandex.class.php');

class Assemble_grab{
	
	protected $auth_rambler;
	protected $grabs;
	protected $parsers;
	protected $sql_model;
	protected $smarty;
	protected $automatize;
	protected $depth_text_google_grab = 10;
	protected $depth_text_rusarticles_grab = 2;
	
	
	
	public function set_depth_text_google_grab($depth_text_google_grab = 10){
	
		$this->depth_text_google_grab = $depth_text_google_grab;
		
		return true;
	}
	
	
	public function set_depth_text_rusarticles_grab($depth_text_rusarticles_grab = 2){
	
		$this->depth_text_rusarticles_grab = $depth_text_rusarticles_grab;
		
		return true;
	}
	
	
	function Assemble_grab(){
	
		set_time_limit(0);
		
		//ini_set("max_execution_time", "3600");
		
		$this->auth_rambler = new Auth_rambler;
		$this->grabs = new Grabs;
		$this->parsers = new Parsers;
		$this->sql_model = new Sql_model;
		$this->smarty = new Smarty;
		$this->automatize = new Automatize;
		$this->templates = new Templates;	
		$this->wordstat_yandex = new Wordstat_yandex;
	}

	
	public function output_bufer($object, $title = 'Параметр'){
		###вывод буфера
		echo $title . ': ';
		echo '<pre>';
			print_r($object);
		echo '</pre>';		
		flush();
	}
	
	
	//главный цикл для обновлятора
	function grab_main_loop_second($id_site){
	
		
		$phrases_primary_grab_0 = $this->sql_model->select_phrases_primary_grab_0($id_site);		
		
		foreach($phrases_primary_grab_0 as $key => $value){		
		
			$this->sql_model->set_id_sites($id_site);	
			$this->parsers->set_id_sites($id_site);	
			$this->grabs->set_id_sites($id_site);	
			
				
			//сбор поисковых фраз
			//$phrases = $this->adstat_rambler_grab($value['search_phrase']);
			$phrases = $this->wordstat_yandex_grab($value['search_phrase']);			
			
			/*
			echo '<pre>';
			print_r($phrases);
			echo '</pre>';
			*/
			
			//сбор текстов
			$id_texts = $this->texts_grab($phrases);
			
			//echo '<br>сбор тектов прошёл<br>';
			//сбор текстов
			//передаём экземпляр чтобы вместе с ним передалось свойство id_sites
			$ids_images = $this->fotos_grab($phrases, $id_site);			
			
			//echo '<br>сбор фоток прошёл<br>';			
			//сбор видео
			$ids_videos = $this->videos_grab($phrases, $id_site);
			
			//echo '<br>сбор видео прошёл<br>';
				
			$this->automatize->bind_images_on_texts($imgs_on_text_from = 1, $imgs_on_text_to = 3);
			
			//echo '<br>привязка изображений прошла<br>';
		}
	}
	
	
	//главный цикл сбора и вставки контента в базу
	function grab_main_loop($sites_and_phrases_primary_grab_0){
		
		
		//увеличиваем глубину сбора сайтов в гугле
		$this->set_depth_text_google_grab($depth_text_google_grab = 10);
		
		//add 03_12_12-16_48
		//создаём шаблоны в отдельном цикле чтобы не дублировать создание шаблонов в основном цикле (он ниже)
		$sites_primary_grab_0 = $this->sql_model->select_sites_by_primary_grab($primary_grab = 0);	
		
		/*
		foreach($sites_primary_grab_0 as $key => $value){
			
			//создание шаблона сайта
			$this->automatize->create_template($value['id_sites']);	
			
		}
		*/
		
		//$this->output_bufer($sites_and_phrases_primary_grab_0, $title = 'Сайты и первичные фразы');
		
		$temp_id_sites = 0;
		$count_phrases = count($sites_and_phrases_primary_grab_0);
		$phrases_loop = 1;
		foreach($sites_and_phrases_primary_grab_0 as $key => $value){
		
			/*
			if($this->sql_model->get_id_sites() != $value['id_sites']){
			
				//выборка инфы по сайтам
				$info_sites = $this->sql_model->select_info_sites();
				$this->smarty->assign("info_sites", $info_sites);
				$this->smarty->display('sites_panel.html');
				
				
				$this->sql_model->set_id_sites($value['id_sites']);					
			}
			*/		
			//echo "<br>!!!";
			
			$this->sql_model->set_id_sites($value['id_sites']);		
			$this->parsers->set_id_sites($value['id_sites']);
			$this->grabs->set_id_sites($value['id_sites']);
			
			//сбор поисковых фраз
			//$phrases = $this->adstat_rambler_grab($value['search_phrase']);
			$phrases = $this->wordstat_yandex_grab($value['search_phrase'], $depth = 2);
			/*
			echo '<pre>';
			print_r($phrases);
			echo '</pre>';
			*/
				
						
			//сбор текстов
			$id_texts = $this->texts_grab($phrases);			
			
			
			//передаём экземпляр чтобы вместе с ним передалось свойство id_sites
			$ids_images = $this->fotos_grab($phrases, $value['id_sites']);				
			
			//сбор видео
			$ids_videos = $this->videos_grab($phrases, $value['id_sites']);														
						
			//update 30_11_12-08_24
			//проверка чтобы не обновлять статус у сайта после каждого цикла по ключевой фразе, а только после завершения работы по данному сайту
					
			if($temp_id_sites != $value['id_sites'] AND $temp_id_sites != 0){							
				
				$this->automatize->bind_images_on_texts($imgs_on_text_from = 1, $imgs_on_text_to = 3);		
			
				//публикует контент (от % до % процентов)
				$this->automatize->updater_status($limit_from = 70, $limit_to = 75, $percent = true, $value['id_sites']);
				
				//снимает с публикации категории у которых есть дубликаты с дополнительным словом/фразой
				//пример: "внесение удобрений" - "внесение удобрений осенью", "внесение удобрений" - категория будет снята с публикации
				$this->automatize->unpublish_duplicate_cat($id_sites = 1);
				
				//создание иконки (расположено после сбора фоток, чтобы было из чего создавать)
				$this->automatize->create_icon($value['id_sites']);
				
				//записываем в базу что сайт прошёл первичный сбор контента
				$this->sql_model->update_site_primary_grab($temp_id_sites);	
				
			}elseif($count_phrases == $phrases_loop){						
				
				$this->automatize->bind_images_on_texts($imgs_on_text_from = 1, $imgs_on_text_to = 3);		
			
				//публикует контент (от % до % процентов)
				$this->automatize->updater_status($limit_from = 70, $limit_to = 75, $percent = true, $value['id_sites']);
				
				//снимает с публикации категории у которых есть дубликаты с дополнительным словом/фразой
				//пример: "внесение удобрений" - "внесение удобрений осенью", "внесение удобрений" - категория будет снята с публикации
				$this->automatize->unpublish_duplicate_cat($id_sites = 1);
				
				//создание иконки (расположено после сбора фоток, чтобы было из чего создавать)
				$this->automatize->create_icon($value['id_sites']);
				
				$this->sql_model->update_site_primary_grab($value['id_sites']);			
				
			}
			$phrases_loop++;
			$temp_id_sites = $value['id_sites'];

			
			//создание файла с тегами для соц. закладок
			//$this->templates->create_file_for_bagbookmark($value['id_sites']); - больше здесь не требуется			
		}
		return $phrases;
	}

	/*
	$search_phrase = 'салат из капусты';

	$search_phrase = 'оружие';
	$search_phrase = 'варенье из';
	*/
	
	public function adstat_rambler_grab($search_phrase){
		
		//перекодируем т.к. рамблер вордстат в cp1251
		$search_phrase = mb_convert_encoding($search_phrase, 'CP1251', 'UTF8');
		//$search_phrase = 'варенье из кабачков';

		
		//второй параметр - количество страниц парса адстата рамблера
		$html = $this->auth_rambler->browser_adstat_rambler($search_phrase, 1);
		/*	
		echo '<pre>';
		print_r($html);
		echo '</pre>';
		*/
		
		$phrases = $this->parsers->parser_adstat_rambler($html, $search_phrase);
		
		/*
		##отладка
		echo '<pre>';
		print_r($phrases);
		echo '</pre>';
		*/	
		
		return $phrases;
	}
	
	
	public function wordstat_yandex_grab($search_phrase, $depth = 2){
		
		//до 05_12_12 было $depth = 2 в нижеидущем методе
		$html = $this->wordstat_yandex->wordstat_yandex_grab_html($search_phrase, $depth);
		/*
		echo '<pre>';
		print_r($html);
		echo '</pre>';
		*/
		$search_phrases = $this->parsers->wordstat_yandex_parse($html, $search_phrase, $limit_words = 5, $del_phrases_with_dig = false, $full_inclusion = true, $limit_phrases = 150, $strict_key_phrase_del = 1, $strict_word_ending = false);
			
		return $search_phrases;
	}
	

	public function texts_grab($phrases){
	
		foreach($phrases as $key => $value_search_phrase){
													
		//add 19_11_12 11_25
		//служит для того чтобы в названия категорий не попадали пустоты
		if($value_search_phrase == ' ' OR empty($value_search_phrase) ){
			return false;
		}							
		
		//перекодирум в utf8 т.к. данные от рамблер вордстата мы получили в cp1251
		//$value_search_phrase_encode = mb_convert_encoding($value_search_phrase, 'UTF8', 'CP1251');	
		$value_search_phrase_encode = $value_search_phrase;
		

		
		//########сбор статей c google
		//второй параметр задаёт глубину парсинг гугла
		$html = $this->grabs->google_main_grab($value_search_phrase_encode, $this->depth_text_google_grab);
		/*
		echo '<pre><b>value_search_phrase_encode</b>';
		print_r($value_search_phrase_encode);
		echo '</pre>';
		*/
		$results = $this->parsers->google_main_parse($html);
		//print_r($results);
		/*
		echo '<pre><b>results_main_parse</b>';
		print_r($results);
		echo '</pre>';
		*/
		$results = $this->parsers->google_result_censor($results[0], $value_search_phrase_encode);
		/*
		echo '<pre><b>results_result_censor</b>';
		print_r($results);
		echo '</pre>';
		*/
		
		$texts_and_seach_phrase_array = $this->parsers->donor_parser($results);
		/*
		echo '<pre><b>google_donor_parser<b>';
		print_r($texts_and_seach_phrase_array);
		echo '</pre>';
		*/
		
		$texts_and_seach_phrase_array = $this->parsers->full_text_assembler($texts_and_seach_phrase_array);

		/*
		echo '<pre><b>google_full_text_assembler<b>';
		print_r($texts_and_seach_phrase_array);
		echo '</pre>';
		*/
		$id_texts = $this->sql_model->insert_texts_and_search_phrases($texts_and_seach_phrase_array);
		
		/*
		echo '<pre><b>id_texts<b>';
		print_r($id_texts);
		echo '</pre>';
		*/
		
			
		//echo '<b>СЛОВО - </b>' . $value_search_phrase . '!!<br>';
		
		//########сбор статей rusarticles
		$html = $this->grabs->rusarticles_main_grab($value_search_phrase, $this->depth_text_rusarticles_grab);	
		//echo $html . '!!!!';	
		$urls_articles = $this->parsers->rusarticles_urls($html);
		

		
		$texts_and_seach_phrase_array_1 = $this->parsers->mass_text_and_title_article($urls_articles, $value_search_phrase);

		
		$id_texts = $this->sql_model->insert_texts_and_search_phrases($texts_and_seach_phrase_array_1);		
		
		/*
		## отладка		
		echo '<pre>';
		print_r($results);
		echo '</pre>';
		
		echo '<pre>';
		print_r($texts_and_seach_phrase_array);
		echo '</pre>';	
		
		echo '<pre>';
		print_r($id_texts);
		echo '</pre>';
		*/		
		}
		
		return $id_texts;
	}


	public function fotos_grab($phrases, $id_site){
		//echo 'Начался сбор фото<br>';
		//сбор фото
		foreach($phrases as $key => $value_search_phrase){
			//echo 'Cбор фото цикл' . $key . '<br>';
			//перекодирум в utf8 т.к. данные от рамблер вордстата мы получили в cp1251
			//$value_search_phrase_encode = mb_convert_encoding($value_search_phrase, 'UTF8', 'CP1251');	
			$value_search_phrase_encode = $value_search_phrase;
			
			//заберает html с изображениями из гугл
			$html_imgs = $this->grabs->google_img_html_grab($value_search_phrase_encode);
			
			//устанавливаем поисковую фразу для проверки её вхождения в описание изображения
			$this->parsers->set_key_phrase($value_search_phrase_encode);
			//парсить урлы фоток из куска html
			$urls_array = $this->parsers->google_img_parse($html_imgs);
		
			/*	## отладка			
			echo '<pre>';
			print_r($html_imgs);
			echo '</pre>';
			*/
			
			/*
			echo '<pre>$urls_array';
			print_r($urls_array);
			echo '</pre>';
			*/
		
			$id_search_phrase = $this->sql_model->select_or_insert_search_phrase($value_search_phrase_encode);
			//скачивает фотки с серверов гугл
			$ids_images .= $this->grabs->google_img_grab($urls_array, $value_search_phrase_encode, $id_search_phrase, $id_site);			
		}
		
		return $ids_images;
	}	
	

	public function videos_grab($phrases, $id_site){
		
		/*
		###отладка
		echo '<pre>';
		print_r($phrases);
		echo '<pre>';
		*/
		
		//сбор видео
		foreach($phrases as $key => $value_search_phrase){
			//перекодирум в utf8 т.к. данные от рамблер вордстата мы получили в cp1251
			//$value_search_phrase_encode = mb_convert_encoding($value_search_phrase, 'UTF8', 'CP1251');
			$value_search_phrase_encode = $value_search_phrase;
			
			
			//установка ключевого слова для проверки его наличия в описании видео и в названии
			$this->parsers->set_key_phrase($value_search_phrase_encode);			

			$html_videos = $this->grabs->google_video_html_grab($value_search_phrase_encode);
			//echo $html_videos;
			
			$html_video_fragment = $this->parsers->google_video_parse($html_videos);
			//echo $html_video_fragment;
			
			/*
			###отладка
			echo '<pre>html_video_fragment';
			print_r($html_video_fragment);
			echo '<pre>';
			*/
			
			$id_search_phrase = $this->sql_model->select_or_insert_search_phrase($value_search_phrase_encode);
			
			//echo '$id_search_phrase: ' . $id_search_phrase . '<br>';
			
			$ids_videos .= $this->parsers->google_fragments_html_video_parse($html_video_fragment, $id_search_phrase, $id_site);
			
		}
		
		return $ids_videos;
	}
}

?>