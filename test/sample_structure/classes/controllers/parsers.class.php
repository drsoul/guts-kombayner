<?
// no direct access
defined( '_EXEC' ) or die( 'Restricted access' );

require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/grabs.class.php');

class Parsers extends Grabs{	
	
	protected $back_list_words;	
	protected $key_phrase;		
	protected $result_array_1;	
	protected $link_full_text;	
	protected $links_full_text;	
	protected $text_and_links_cat;	
	protected $cats_array_result;	
	protected $count_page_text_and_links_cat;
	
	
	function Parsers(){
		//вызов прадедушкина конструктора
		parent::Root_model(); 
		
		$filename = $_SERVER['DOCUMENT_ROOT'] . '/words/black_list.txt';
		$this->back_list_words = file($filename);
		
		$filename = $_SERVER['DOCUMENT_ROOT'] . '/words/black_list_adstat.txt';
		$this->back_list_adstat_words = file($filename);
		
		$this->count_page_text_and_links_cat = 1;		
	}
	
	
	public function wiktionary_query( $url = '/w/index.php?title=%D0%9A%D0%B0%D1%82%D0%B5%D0%B3%D0%BE%D1%80%D0%B8%D1%8F:%D0%A1%D0%B5%D0%BC%D0%B0%D0%BD%D1%82%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D0%B5_%D0%BA%D0%B0%D1%82%D0%B5%D0%B3%D0%BE%D1%80%D0%B8%D0%B8/ru' ){	
				
		//$url = '/w/index.php?title=%D0%9A%D0%B0%D1%82%D0%B5%D0%B3%D0%BE%D1%80%D0%B8%D1%8F:%D0%A1%D0%B5%D0%BC%D0%B0%D0%BD%D1%82%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D0%B5_%D0%BA%D0%B0%D1%82%D0%B5%D0%B3%D0%BE%D1%80%D0%B8%D0%B8/ru&subcatuntil=%D0%A0%D0%B5%D0%BB%D0%B8%D0%B3%D0%B8%D0%BE%D0%B2%D0%B5%D0%B4%D0%B5%D0%BD%D0%B8%D0%B5%2Fru#mw-subcategories';		
		//echo $url . ' !<br>';
		time_nanosleep(0, 500000000);
		
		$html = $this->curl_connect('http://ru.wiktionary.org' . $url);		
		//echo $html . ' !<br>';
		
		$need_fragment = mb_strstr($html, '<!-- bodycontent -->', $before_needle = false, $enc = 'utf-8' ); //<h2>Подкатегории</h2>
		unset($html);
		$need_fragment = mb_strstr($need_fragment, '<!-- /bodycontent -->', $before_needle = true, $enc = 'utf-8' ); //</div>(предыдущие
		
		preg_match_all($pattern = '|<a .*?</a>|ui', $need_fragment, $matches);
		unset($need_fragment);
		//$count_array = count($this->text_and_links_cat);		
		
		if( mb_strpos($matches[0][0], $needle = 'следующие', $offset = 0, $enc = 'utf-8' ) !== false ){
		
			preg_match($pattern = '|href=".*?"|ui', $matches[0][0], $match);
			$match[0] = str_replace('href=', '', $match[0]);
			$match[0] = str_replace('"', '', $match[0]);
			$next_page = $match[0];
			unset($match);
		}elseif( mb_strpos($matches[0][1], $needle = 'следующие', $offset = 0, $enc = 'utf-8' ) !== false ){
		
			preg_match($pattern = '|href=".*?"|ui', $matches[0][1], $match);
			$match[0] = str_replace('href=', '', $match[0]);
			$match[0] = str_replace('"', '', $match[0]);
			$next_page = $match[0];
			unset($match);
		}
		
		$next_page = str_replace('amp;', '', $next_page);		
		
		//echo $next_page . '!!!!!!!<br>';		
		
		$flag = 1;
		//$next_page = null;
		$finish_flag = 0; 
		foreach($matches[0] as $key => $value){
			
			preg_match($pattern = '|href=".*?"|ui', $value, $match);
			$match[0] = str_replace('href=', '', $match[0]);
			$match[0] = str_replace('"', '', $match[0]);

			
			$this->text_and_links_cat[$this->count_page_text_and_links_cat][$key]['url'] = $match[0];
			unset($match);
			
			preg_match($pattern = '|">.*?</a>|ui', $value, $match_1);
			$match_1[0] = str_replace('">', '', $match_1[0]);
			$match_1[0] = str_replace('</a>', '', $match_1[0]);
			$match_1[0] = str_replace('/ru', '', $match_1[0]);
			/*
			if($flag < 4){
				if( mb_strpos($match_1[0], $needle = 'следующие', $offset = 0, $enc = 'utf-8' ) !== false ){
					$next_page = $match[0];
					echo '!!!!!!!<br>';
				}
				
			}else{
				if( empty($next_page) ){
					$finish_flag = 1; 
				}
			}
			$flag++;
			*/

			$this->text_and_links_cat[$this->count_page_text_and_links_cat][$key]['text'] = $match_1[0];
			unset($match_1, $key, $value);
			//$count_array++;
		}
		unset($matches);
		
		$this->count_page_text_and_links_cat++;
		if( !empty($next_page) ){ //$this->count_page_text_and_links_cat < 10
			
			//echo '$next_page - ' . $next_page . '<br>';
			$this->wiktionary_query($next_page);
		}	
		unset($next_page);
		/*
		if($finish_flag == 0){
			$this->wiktionary_query($next_page);
		}
		*/
		/*
		###отладка
		echo '<pre>';
		print_r($this->text_and_links_cat);
		echo '</pre>';
		*/
		
		//$this->text_and_links_cat = $this->get_subcats_for_cats($this->text_and_links_cat);
		
		
		return $this->text_and_links_cat;
		//echo $need_fragment;

	}

	
	public function get_subcats_for_cats($cats_array, $main_count = 20){
	
		
		
		$link_sem_wiktionary = $this->select_param($param_name = 'link_sem_wiktionary');
		
		/*
		echo '<pre>';
		print_r($link_sem_wiktionary);
		echo '</pre>';
		*/
		
		$link_sem_wiktionary_array = explode('|', $link_sem_wiktionary['value']);
		unset($link_sem_wiktionary);
		
		$counter_loop = 0;
		//foreach($cats_array as $key => $value){
		echo 'OK!!<br>';
		
		for($i_1 = $link_sem_wiktionary_array[0]; $i_1 < count($cats_array) ;  $i_1++){
		//for ($i = 1; $i <= 10; $i++) {
			echo '$i_1 - ' . $i_1 . '<br>';
			//if($key >= $link_sem_wiktionary_array[0]){			
			
				
				//foreach($value as $key_1 => $value_1){
				for($i_2 = $link_sem_wiktionary_array[1]; $i_2 < count($cats_array[$i_1]) ;  $i_2++){
				
					//if( ($key_1 >= $link_sem_wiktionary_array[1] AND $key == $link_sem_wiktionary_array[0]) OR $key > $link_sem_wiktionary_array[0]){
						/*
						if($count > 10){
							return $texts_link;
							//return $this->cats_array_result;
						}
						*/
						
						$html = $this->curl_connect('http://ru.wiktionary.org' . $cats_array[$i_1][$i_2]['url']);	
						//<h2>Подкатегории</h2>
						$need_fragment = mb_strstr($html, '↖ ↑ ↗</a></center>', $before_needle = false, $enc = 'utf-8' ); //<h2>Подкатегории</h2>
						$need_fragment = mb_strstr($need_fragment, '<!-- /printfooter -->', $before_needle = true, $enc = 'utf-8' ); //</div>(предыдущие
						
						###отладка
						//echo $need_fragment . '<br>';
						
						preg_match_all($pattern = '|<a .*?</a>|uis', $need_fragment, $matches);	
						
						//$this->cats_array_result[$counter_loop]['texts'][] = $value_1['text'];
						//$this->cats_array_result[$counter_loop]['url'] = $value_1['url'];
						
						$value_1['text'] = mb_strtolower($value_1['text'], $enc = 'utf-8' );
						$cats_array[$i_1][$i_2]['text'] = mb_strtolower($cats_array[$i_1][$i_2]['text'], $enc = 'utf-8' );
						
						
						
						if( mb_strpos($cats_array[$i_1][$i_2]['text'], $needle = 'следующие', $offset = 0, $enc = 'utf-8')  === false){
							if( mb_strpos($cats_array[$i_1][$i_2]['text'], $needle = 'предыдущие', $offset = 0, $enc = 'utf-8')  === false){
								if( mb_strpos($cats_array[$i_1][$i_2]['text'], $needle = 'homo sapiens', $offset = 0, $enc = 'utf-8')  === false){
									if( mb_strpos($cats_array[$i_1][$i_2]['text'], $needle = 'базовая категория', $offset = 0, $enc = 'utf-8')  === false){
								
									
										$texts_link[$counter_loop]['texts'][] = $cats_array[$i_1][$i_2]['text'];				
										$texts_link[$counter_loop]['url'][] = $cats_array[$i_1][$i_2]['url'];
										
										
										foreach($matches[0] as $key => $value){				
								
											//echo $value . '<br>';
											
											preg_match($pattern = '|">.*?</a>|ui', $value, $match_1);
											/*
											echo '<pre>';
											print_r($match_1[0]);
											echo '</pre>';
											*/
											$match_1[0] = str_replace('">', '', $match_1[0]);
											$match_1[0] = str_replace('</a>', '', $match_1[0]);
											$match_1[0] = str_replace('/ru', '', $match_1[0]);

											//echo $match_1[0] . ' 1111<br>';
											
											//$this->cats_array_result[$counter_loop]['texts'][] = $match_1[0];
											
											$match_1[0] = mb_strtolower($match_1[0], $enc = 'utf-8' );
											
											//проверка чтобы небыло подобного - http:/.wiktionary.org/w/index.php?title=категория:homo_sapiens&oldid=3217040
											if( mb_strpos($match_1[0], $needle = 'http:', $offset = 0, $enc = 'utf-8')  === false){
											

												$texts_link[$counter_loop]['texts'][] = $match_1[0];
							
											}											
										}
									}									
								}
							}
						}
						if( is_array($texts_link[$counter_loop]['texts']) ){
							$texts_link[$counter_loop]['texts'] = array_unique($texts_link[$counter_loop]['texts']);
						}
						/*
						### отладка
						echo '<pre>';
						print_r($matches);
						echo '</pre>';						
						echo '<hr>';
						*/						
						
						$counter_loop++;
						//$count++;
						
						//при достижении нужного количества итераций возвращаем результат
						if($counter_loop >= $main_count){
							/*
							###отладка
							echo '$i_2 - ' . $i_2 . '<br>';
							echo 'count($cats_array[$i_1] - ' . count($cats_array[$i_1]) . '<br>'; 
							echo '<hr>';
							*/
							//записываем новый указатель в базу
							$this->update_param($param_name = 'link_sem_wiktionary', $value = $i_1 . '|' . $i_2);
							return $texts_link;
						}
					
					//}
					//нумерация массива с 0
					if( ($i_2 + 1) == count($cats_array[$i_1])){
						$i_2 = 0;
					}
				}
			
			//}
			//нумерация массива с 1
			if( ($i_1 + 1) == count($cats_array)){
				$i_1 = 1;
			}
			
		}
		
		//return 1;
		//return $texts_link;
		//return $this->cats_array_result;
	}	
		
	
	public function set_key_phrase($key_phrase){	
		$this->key_phrase = $key_phrase;		
	}


	//add 24_12_12 20_46
	public function query_index($sites = false){
		
		if($sites == false){
			$sites = $this->select_all_info_sites_by_primary_grab_and_domain_not_site_1($primary_grab = 1);
		}
		
		foreach($sites as $key => $value){
		
			
			$url = 'http://webmaster.yandex.ru/check.xml?hostname=http%3A%2F%2F' . $value['domain'];
			$html_yandex = $this->curl_connect($url);
			
			$result_yandex = preg_match('|Страницы: [0-9]+|us', $html_yandex, $matches);
			$match_array = explode(' ', $matches[0]);			
			
			if($result_yandex){				
				$domains_index_array[$value['id']]['yandex_index'] = $match_array[1];				
			}else{
				$domains_index_array[$value['id']]['yandex_index'] = 'no';
			}
			
			
			$url = 'http://www.google.ru/search?hl=ru&q=site:' . $value['domain'];
			$html_google = $this->curl_connect($url);
			
			$result_google = preg_match('|Результатов: примерно [&#;0-9]+|us', $html_google, $matches);
			$matches[0] = str_replace('&#160;', '', $matches[0]);
			$match_array = explode('примерно ', $matches[0]);
			
			
			if($result_google){				
				$domains_index_array[$value['id']]['google_index'] = $match_array[1];				
			}else{
				$domains_index_array[$value['id']]['google_index'] = 'no';
			}		

			$domains_index_array[$value['id']]['domain'] = $value['domain'];
			$domains_index_array[$value['id']]['id'] = $value['id'];			
			
			time_nanosleep(0, 500000000); //полсекунды сон	
			
		}
		
		return $domains_index_array;
	}	
	
	
	//add 05_02_13
	public function query_index_by_param($yandex_google){		
		
		$sites = $this->select_all_info_sites_by_primary_grab_and_domain_not_site_1($primary_grab = 1);		
		
		foreach($sites as $key => $value){
		
			if($yandex_google['yandex'] == 1){			
			
				$url = 'http://webmaster.yandex.ru/check.xml?hostname=http%3A%2F%2F' . $value['domain'];
				$html_yandex = $this->curl_connect($url);
				
				$result_yandex = preg_match('|Страницы: [0-9]+|us', $html_yandex, $matches);
				$match_array = explode(' ', $matches[0]);			
				
				if($result_yandex){				
					$domains_index_array[$value['id']]['yandex_index'] = $match_array[1];				
				}else{
					$domains_index_array[$value['id']]['yandex_index'] = 'no';
				}					
			
			}else{
				$domains_index_array[$value['id']]['yandex_index'] = 'nd';
			}
			
			
			if($yandex_google['google'] == 1){
			
				$url = 'http://www.google.ru/search?hl=ru&q=site:' . $value['domain'];
				$html_google = $this->curl_connect($url);
				
				$result_google = preg_match('|Результатов: примерно [&#;0-9]+|us', $html_google, $matches);
				$matches[0] = str_replace('&#160;', '', $matches[0]);
				$match_array = explode('примерно ', $matches[0]);				
				
				if($result_google){				
					$domains_index_array[$value['id']]['google_index'] = $match_array[1];				
				}else{
					$domains_index_array[$value['id']]['google_index'] = 'no';
				}		
				
			}else{
				$domains_index_array[$value['id']]['google_index'] = 'nd';
			}			
			
			$domains_index_array[$value['id']]['domain'] = $value['domain'];
			$domains_index_array[$value['id']]['id'] = $value['id'];
			
			time_nanosleep(0, 500000000); //0.5 сек	
			
		}
		
		return $domains_index_array;
	}	
	
	
	//add 07_11_12 22_58
	public function grab_liveinternet(){
	
		$sites = $this->select_all_info_sites_by_primary_grab($primary_grab = 1);
		foreach($sites as $key => $value){
		
			/*
			### отладка
			$value['domain'] = 'sport-inventar.borrba.ru';
			$value['id'] = 1;
			*/
			
			$url = 'http://www.liveinternet.ru/stat/' . $value['domain'] . '/index.html?graph=table';
			//$url = 'http://www.liveinternet.ru/stat/ballkon.ru.com/index.html?graph=table';
			//varenie-recept.ru mobile-divice.ru.com
			$html = $this->curl_connect($url);
			
			/*
			### отладка
			echo $html . '<br>';
			*/
			
			if( mb_strpos($html, 'Нет данных',$offset = 0, $encoding = 'utf-8') ){
				//add 26_11_12
				unset($html);
				
				$wrapper = '';
				/*
				$wrapper['no_data'] = 'no_data';
				$result_statistic_array[$value['domain']] = $wrapper;
				*/
				$result_statistic_array[$value['id']]['domain'] = $value['domain'];
				$result_statistic_array[$value['id']]['statistic'] = 'no_data';
				$result_statistic_array[$value['id']]['type_statistic'] = 'no_data';
				
				/*
				### отладка
				echo '1ok!';
				*/
				
			}elseif( mb_strpos($html, '[1]',$offset = 0, $encoding = 'utf-8') ){
				
				$html = strstr($html , '</u>');
				$html = strstr($html , '</pre>', true);
				$html = strip_tags($html);
				$html = trim($html);
				$html = nl2br($html, false);
				
				$html_array = explode('<br>', $html);
				
				//add 26_11_12
				unset($html);
				
				$wrapper = '';
				$temp_domain_statistic_array = '';
				foreach($html_array as $key_1 => $value_1){
				
					//заменяем любое количество повторяющиех пробелов на 1
					//пример 7 окт         196      160 - 7 окт 196 160
					$value_1 = preg_replace('|\s+|u', ' ', $value_1);
					
					$value_1 = trim($value_1);
					$temp_domain_statistic_array[] = explode(' ', $value_1);					
					
					//$result_statistic_array[$value['domain']] = $html;
					
				}
				unset($html_array);
				/*
				$wrapper['mass_days'] = $temp_domain_statistic_array;
				$result_statistic_array[$value['domain']] = $wrapper;
				*/
				
				$result_statistic_array[$value['id']]['domain'] = $value['domain'];
				$result_statistic_array[$value['id']]['statistic'] = $temp_domain_statistic_array;
				$result_statistic_array[$value['id']]['type_statistic'] = 'mass_days';
				
				/*
				### отладка
				echo '2ok<pre>';
				print_r($result_statistic_array);
				echo '</pre>';
				*/
			}else{
				/*
				### отладка
				echo '3ok!';
				*/
				
				$html = strstr($html , '<table cellpadding=3 cellspacing=0 border=0 bgcolor=#e8e8e8 width=520');
				$html = strstr($html , '</table>', true);
				
				$html = preg_replace('|<a.*?/a>|us', ' ', $html);
				
				
				$html = preg_replace('|<br>|u', ' ', $html);
				$html = strip_tags($html);
				
				$html = trim($html);
				$html = nl2br($html, false);
				$html_array = explode('<br>', $html);
				
				//add 26_11_12
				unset($html);
				
				$wrapper = '';
				$temp_domain_statistic_array = '';
				foreach($html_array as $key_2 => $value_2){
				
					$value_2 = preg_replace('|\s+|us', ' ', $value_2);
					
					if( !empty($value_2) AND $value_2 != ' ' AND $value_2 != '&nbsp;' ){ // AND $value_2 != '  '
						$temp_domain_statistic_array[] = $value_2;
					}
					
					unset($key_2, $value_2);
				}
				unset($html_array);
				
				//$wrapper['2_days'] = $temp_domain_statistic_array;
				$result_statistic_array[$value['id']]['domain'] = $value['domain'];
				//$result_statistic_array[$value['id']]['statistic'] = $html;
				$result_statistic_array[$value['id']]['statistic'] = $temp_domain_statistic_array;
				$result_statistic_array[$value['id']]['type_statistic'] = '2_days';
				unset($temp_domain_statistic_array);				
			}
			
			unset($key, $value);
		}
		
		$result_statistic_array = $this->forming_statistic_li($result_statistic_array);
		
		return $result_statistic_array;		
	}
	
	
	
	//add 07_11_12 05_41
	public function forming_statistic_li($result_statistic_array){
		
		$b = &$result_statistic_array; // увеличивает счётчик ссылок на массив это приводти к отключению внутрицикленной оптимизации массивов, что уменьшает расходование памяти	
		foreach($result_statistic_array as $key => $value){
		
			$tmp_statistic = '';
			
			if($value['type_statistic'] == '2_days'){
			
				//$day['day'] = $value['statistic'][0];				
				
				$tmp_statistic['day'] = mb_substr($value['statistic'][0], $start = 0, $length = 6, $encoding = 'utf-8');
				///$tmp_statistic['day'][] = mb_substr($value['statistic'][1], $start = 0, $length = 6, $encoding = 'utf-8');				
				
				$tmp_statistic['views'] = $value['statistic'][5];
				//$tmp_statistic['views'][] = $value['statistic'][6];
				$tmp_statistic['visitors'] = $value['statistic'][15];
				//$tmp_statistic['visitors'][] = $value['statistic'][16];
				$tmp_statistic['views_30'] = $value['statistic'][5] + $value['statistic'][6];
				$tmp_statistic['visitors_30'] = $value['statistic'][15] + $value['statistic'][16];				
				
				
			}elseif($value['type_statistic'] == 'mass_days'){
			
				$count_days = count($value['statistic']);
				$last_day = $count_days - 1;
			
				$tmp_statistic['day'] = $value['statistic'][$last_day][0] . ' ' . $value['statistic'][$last_day][1];
				$tmp_statistic['views'] = $value['statistic'][$last_day][2];
				$tmp_statistic['visitors'] = $value['statistic'][$last_day][3];
				$tmp_statistic['views_30'] = '';
				$tmp_statistic['visitors_30'] = '';
				
				$count_loop = 1;
				$b = &$value['statistic']; // увеличивает счётчик ссылок на массив это приводти к отключению внутрицикленной оптимизации массивов, что уменьшает расходование памяти
				foreach($value['statistic'] as $key_1 => $value_1){
					
					//if()
					if($count_loop > 31){
						break;
					}
					$tmp_statistic['views_30'] += $value_1[2];
					$tmp_statistic['visitors_30'] += $value_1[3];
					
					/*
					$tmp_statistic['day'][] = $value_1[0] . ' ' . $value_1[1]; 
					$tmp_statistic['views'][] = $value_1[2];					
					$tmp_statistic['visitors'][] = $value_1[3];
					*/
					
					$count_loop++;
				}
				unset($b); // удаляет ссылку на массив
				
				//разворачиваем массивы чтобы на выводе было сортировка от последней даты
				/*
				$tmp_statistic['day'] = array_reverse($tmp_statistic['day']);
				$tmp_statistic['views'] = array_reverse($tmp_statistic['views']);
				$tmp_statistic['visitors'] = array_reverse($tmp_statistic['visitors']);
				*/
			}else{
				$tmp_statistic = 'no_data';
			}
			
			$result_statistic_array[$key]['statistic'] = $tmp_statistic;
			//unset($tmp_statistic);
		}
		unset($b); // удаляет ссылку на массив
		
		//add 25_11_12
		//sorting array and change structure

				
		unset($temp);	
		$b = &$result_statistic_array; // увеличивает счётчик ссылок на массив это приводти к отключению внутрицикленной оптимизации массивов, что уменьшает расходование памяти	
		foreach($result_statistic_array as $key => $value){
		
			$value_temp = $value;
			unset($value);
			$value_temp['id'] = $key;
			$temp[] = $value_temp;
			//unset($value_temp);
		}
		unset($b); // удаляет ссылку на массив
		$result_statistic_array = $temp;
		$temp = null;
		unset($temp);
		
		
		/*
		//add 26_11_12 09_41
		#### СОРТИРОВКА
		#### ОТКЛЮЧЕНА ИЗ-ЗА ПРЕВЫШЕНИЯ ЛИМИТА ПАМЯТИ
		$count_statistic = count($result_statistic_array);		
		$count_loop = 1;
		
		$b = &$result_statistic_array; // увеличивает счётчик ссылок на массив это приводти к отключению внутрицикленной оптимизации массивов, что уменьшает расходование памяти
		foreach($result_statistic_array as $key => $value){
			$f = 0;
			for($i = 0 ; $count_statistic - $count_loop ; $i++){
				
				if($result_statistic_array[$i]['statistic']['visitors'] > $result_statistic_array[$i+1]['statistic']['visitors']){
					$f = 1;
					$temp = null;
					unset($temp);
					$temp = $result_statistic_array[$i]; 
					$result_statistic_array[$i] = $result_statistic_array[$i+1];
					
					unset($result_statistic_array[$i+1]);
					$result_statistic_array[$i+1] = $temp;
					$temp = null;
					unset($temp);
				}				

			}
				

			if($f == 0){
				break;
			}		
			
			$count_loop++;
			unset($key, $value);
		}
		unset($count_statistic);
		unset($b); // удаляет ссылку на массив
		*/		
		
		return $result_statistic_array;
	}	

	
	//принимает html, ключевую фразу, лимит на количество слов в найденной фразе, параметр удаления строк с цифрами в найденных фразах
	//возвращает массив с обработанными фразами
	public function parser_adstat_rambler($html, $key_phrase, $limit_words = 4, $del_phrases_with_dig = true){
		
		preg_match_all("/<td class=dt_gray align=left>.*<\/td>/", $html, $matches);

		foreach ($matches[0] as $key => $value){
			$matches[0][$key] = strip_tags($matches[0][$key]);
			$matches[0][$key] = trim($matches[0][$key]);			
			
			//preg_match("/$key_phrase .+/", $matches[0][$key], $matches_1);
			if(!preg_match("/$key_phrase .+/", $matches[0][$key], $matches_1)){
				$matches[0][$key] = '';	
			}else{
				$matches[0][$key] = strstr($matches[0][$key], $matches_1[0]);	
			}
					
			
			if(empty($matches[0][$key])){
				unset($matches[0][$key]);
			}
			if($del_phrases_with_dig){
				if(preg_match("/[0-9]/", $matches[0][$key])){
					unset($matches[0][$key]);
				}
			}			
		}

		foreach ($matches[0] as $key => $value){
			foreach ($matches[0] as $key_1 => $value_1){
				if($key != $key_1){
					if($matches[0][$key] == $matches[0][$key_1]){
						unset($matches[0][$key]);			
					}
				}
				
				$temp = explode(' ', $matches[0][$key]);
				if(count($temp) > $limit_words){
					unset($matches[0][$key]);
				}
			}		
		}

		$count_loop = 0;
		foreach ($matches[0] as $key => $value){
			$phrases[$count_loop] = $matches[0][$key];
			$count_loop++;
		}
		
		
		//удаление повторящихся слов (в части не являющейся главной ключевой фразой) в разных ключевых фразах
		$phrases = $this->clean_repeat_fragment_phrases($phrases, $key_phrase);		
		$phrases = $this->adstat_black_list_censor($phrases);

		//add 16_10_12-18_20
		$phrases = $this->individual_category_black_list_censor($phrases, 'utf-8');
		
		//$phrases = $this->censor($phrases);
		
		return $phrases;
	}
	
	
	
	protected function adstat_black_list_censor($phrases, $encode = 'cp1251'){
	
		$encode = strtolower($encode);
		
		foreach($phrases as $key => $value){			
			
			$flag_back_list = 0;
			foreach($this->back_list_adstat_words as $key_1 => $value_1){
			
				$value_1 = trim($value_1);
				/*
				if($encode == 'cp1251'){
					$value_1 = mb_convert_encoding($value_1 , 'CP1251', 'utf-8');
				}
				*/
				
			
				//echo '$value-' . $value . '----- $value_1-' . $value_1 . '<br><br>';
								
				if(mb_stripos(' ' . $value . ' ', ' ' . $value_1 . ' ', 0, $encode) !== false AND !empty($value_1) ){
					
					$flag_back_list = 1;
					break;
				}
				
				unset($key_1, $value_1);
			}
			
			if($flag_back_list == 0){
				$phrases_temp[] =  $value;
			}
			
			unset($key, $value);
		}
		
		$phrases = $phrases_temp;
		unset($phrases_temp);
		
		return $phrases;
	}
	
	
	
	protected function individual_category_black_list_censor($phrases, $encode = 'cp1251'){
	
		$encode = strtolower($encode);		
		$b_list_words = $this->select_b_list_words();
		
		foreach($phrases as $key => $value){
			
			
			$flag_back_list = 0;
			foreach($b_list_words as $key_1 => $value_1){
			
				$value_1 = trim($value_1['words']);
				/*
				if($encode == 'cp1251'){
					$value_1 = mb_convert_encoding($value_1 , 'CP1251', 'utf-8');
				}
				*/								
				
				if(!empty($value_1['words']) ){
				
					if(mb_stripos($value_1, ' ', 0, 'utf-8') !== false){
						$value_temp = $value_1;
					}else{
						$value_temp = $this->cleaner_word_ending($value_1, $encode = 'utf-8');
					}		
					
					echo '$value-' . $value . '----- $value_1-' . $value_1 . '<br><br>';
								
					if(mb_stripos(' ' . $value . ' ', ' ' . $value_temp, 0, $encode) !== false){
						
						$flag_back_list = 1;
						break;
					}		
					unset($value_temp);
				}	
			}
			
			if($flag_back_list == 0){
				$phrases_temp[] =  $value;
			}
			
			unset($key, $value);
		}
		
		$phrases = $phrases_temp;
		unset($phrases_temp);
	
		return $phrases;
	}
	
	
	
	//удаляет дубликаты фраз с разными окончаниями но одинаковыми корнями,
	//при одинаковом количестве слов в фразах и совпадение всех корней слов со всеми корнями слов в другой фразе,
	//порядок слов не важен
	protected function clean_repeat_fragment_phrases($phrases, $key_phrase, $encode = 'cp1251'){
	
		foreach($phrases as $key => $value){
			$phrases_without_main_key_phrase[] = str_replace($key_phrase, '', $value);			
		}
		
		foreach($phrases_without_main_key_phrase as $key_1 => $value_1){
			$phrases_without_main_key_phrase_2_level_array[$key_1] = explode(' ', trim($value_1));
		}
		
		foreach($phrases_without_main_key_phrase_2_level_array as $key_2 => $value_2){
			foreach($value_2 as $key_3 => $value_3){
				$phrases_without_main_key_phrase_2_level_array_new[$key_2][] = $this->cleaner_word_ending($value_3, $encode);
			}			
		}
		
		/*
		echo '<pre>';
		print_r($phrases_without_main_key_phrase_2_level_array_new);
		echo '</pre>';
		*/
		
		foreach($phrases_without_main_key_phrase_2_level_array_new as $key_4 => $value_4){
		
			$flag_global_dublicate = 0;
			foreach($phrases_without_main_key_phrase_2_level_array_new as $key_5 => $value_5){
				if($key_4 != $key_5){
					$flag_duplicate = 0;
					foreach($value_4 as $key_6 => $value_6){
						foreach($value_5 as $key_7 => $value_7){
							if($value_6 == $value_7){
								$flag_duplicate++;
							}
						}
					}
					
					if($flag_duplicate >= count($value_4) AND count($value_4) == count($value_5)){
						$flag_global_dublicate = 1;
					}
				}
			}
			
			if($flag_global_dublicate == 1){
				unset($phrases[$key_4]);
			}			
		}
		
		$phrases = array_merge($phrases);
		
		/*
		###отладка
		echo '<pre>!!!';
		print_r($phrases);
		echo '!!!</pre>';
		*/
		
		return $phrases;
	}
	
	
	
	public function google_main_parse($html){	
		preg_match_all( '/<li class="g">.*<\/li>/isU', $html, $matches);		
		return $matches;
	}
	
	
	public function google_img_parse($html){			

		$html = strstr($html, 'Результатов:'); //было до 15_01_13 -  Фотографии</a> было до 18_12_12 - Фотографии</a><span style="padding:4px">
		//$html = strstr($html, '<div id=hd_1');
		
		$html = strstr($html, 'На главную', true);
		//$html = strstr($html, '</div>', true);
	
		$urls_array = $this->google_img_url_parse($html);		
		
		return $urls_array;
		//return $html;
	}
	
	protected function google_img_url_parse($html){	
	
		preg_match_all( '/src=".*?"/is', $html, $matches);
		preg_match_all( '/><\/a><br(\/|)>.*?<br(\/|)><cite/is', $html, $matches_1);
		//preg_match_all( '/<\/a>.*?<cite/is', $html, $matches_1);
		
		foreach($matches[0] as $key => $value){	
				
			if(stripos($matches_1[0][$key], '<br/>')){
				$alt_array = explode('<br/>', $matches_1[0][$key]);
			}else{
				$alt_array = explode('<br>', $matches_1[0][$key]);
			}		

			/*
			echo '<pre> - $matches_1 - google_img_url_parse';
			print_r($matches_1);
			echo '<pre>';

			echo '<pre> - $alt_array - google_img_url_parse';
			print_r($alt_array);
			echo '<pre>';			
			*/
			//$urls_array[$key]['alt'] = $alt_array;
			$alt = str_ireplace('.JPG', '', str_replace('File:', '', strip_tags($alt_array[2]))); // был 1 индекс, гуглоиды модерят вид выдачи
			
			/*
			echo $matches_1[0][$key] . '!!!<br>';
			echo '<pre>';
			print_r($alt_array);
			echo '<pre>';
			echo '!!!<br><br><br><br>';
			*/
			
			//$result = $alt;
			$result = $this->key_phrase_censor($alt, 0);			
	
			//$result = true;
			
			/*
			echo $result . ' - $result<br><br><br>';
			echo $alt . ' - $alt<br><br><br>';
			echo $value . ' - $value<br><br><br>';
			*/
			 
			if($result !== false){
			
				//13_10_12
				//стоп слова
				$result = $this->black_list_censor($alt);
				if($result !== false){
				
					$alt = $this->alt_clean($alt);			
					$urls_array[$key]['alt'] = $alt;
					
					$value = str_replace('src="',  '', $value);
					//$value = str_replace('"', $value);			
					$urls_array[$key]['urls'] = str_replace('"',  '', $value);
					
				}				
			}			
			
			//$urls_array[$key]['alt'] = strip_tags($matches_1[0][$key]);
		}		
		
		return $urls_array;
	}
	
	
	protected function alt_clean($alt){		
		
		//удаляем домены латиницей (http|http:|http:\/\/| |)([a-z0-9-]{2,20}\.|)([a-z0-9-]{2,20}\.|)
		//$alt = preg_replace('/(http|http:|http:\/\/| |)([a-zA-Z0-9-]{2,20}\.|)([a-zA-Z0-9-]{2,20}\.|)([a-zA-Z0-9-]{2,20}\.[a-zA-Z0-9]{2,4})?(\/[a-zA-Z0-9]{2,20}|)?(\/)?( |\.)/isu', '', $alt);
		
		//удаляем домены латиницей
		$alt = preg_replace('/([-a-zA-Z0-9]{2,20}\.|)[-a-zA-Z0-9]{2,20}\.[-a-zA-Z0-9]{2,4}(| )/isu', '', $alt);
		//удаляем домены киррилицей
		$alt = preg_replace('/([-а-яё0-9]{2,20}\.|)[-а-яё0-9]{2,20}\.[-а-яё0-9]{2,4}(| )/isu', '', $alt);
		
		//удаляет протокол и мусор оставшийся от демена
		$alt = preg_replace('/(http:\/\/|[-a-zA-Z0-9]{2,20}\/[-a-zA-Z0-9]{2,20}\/)/isu', '', $alt);		
		
		//удаляем лишние точки
		$alt = preg_replace('/\.[- ]*\./isu', '.', $alt);
		
		//прижимаем точку к слову
		$alt = preg_replace('/([-a-zA-Z0-9а-яё]) \./isu', '$1.', $alt);
		
		//удаляем все прижатые пробелы больше 1го
		$alt = preg_replace('/ {2,}/isu', ' ', $alt);
		
		//удаляет "Re:"
		$alt = str_ireplace('Re:', '', $alt);
		
		//удаляет "&quot;"
		$alt = str_ireplace('&quot;', ' ', $alt);
		
		//$title = preg_replace('/([а-яА-Я0-9ёЁ.])([- =,:;])$/is', '$1.', $title);				
		//удаляем домены киррилицей
		//$alt = preg_replace('/(http|http:|http:\/\/| |)([-а-яё0-9]{2,20}\.|)([а-яё0-9-]{2,20}\.|)[-а-яё0-9]{2,20}\.[а-яё0-9]{2,4}(\/[а-яё0-9]{2,20}|)(|\/)( |\.)/isu', '', $alt);	
		
		return $alt;
	}
	
	
	public function google_fragments_html_video_parse($html, $id_search_phrase, $id_site){			

		//preg_match_all('/<td.*?<\/td>/is', $html, $images_html);
		
		preg_match_all('/src=".*?"/is', $html, $images_html);
		
		/*
		###отладка
		echo '<pre>$images_html';
		print_r($images_html);
		echo '<pre>';
		*/		
		
		foreach($images_html[0] as $key => $value){
			$value = str_replace('src="',  '', $value);
			//$value = str_replace('"', $value);			
			$urls_imgs_array[$key]['img'] = trim(str_replace('"',  '', $value));
		}	
		
		//preg_match_all('/<td valign="top" style="padding:5px.*?<\/td>/is', $html, $texts_html); - старый вариант (изменили вид)
		
		preg_match_all('/<div class="th".*?<\/td><\/tr>/is', $html, $texts_html);
		
		/*
		###отладка
		echo '<pre>$texts_html';
		print_r($texts_html);
		echo '<pre>';
		*/
		
		foreach($texts_html[0] as $key => $value){		
		
			$result = $this->key_phrase_censor($value, 0);			
			
			if($result !== false){		
				
				preg_match('/<h3 class="r">.*?<\/h3>/isu', $value, $matches_title);				
				$title = strip_tags($matches_title[0]);
				$title = str_ireplace('- YouTube', '', $title);				
				
				preg_match('/<\/cite>.*?<\/td><\/tr>/isu', $value, $matches);				
				$text_elements_array = explode('</span>', $matches[0]);
				/*
				echo '<pre>';
				print_r($text_elements_array);
				echo '</pre>';
				*/
				
				$duration = str_replace('-', '', $text_elements_array[1]);
				$time_publish = $text_elements_array[0];
				$description = $text_elements_array[4];				
				
				preg_match('/href="\/url\?q=.*?"/is', $value, $matches);
				$url_video = $matches[0];
				$url_video = str_replace('href="/url?q=', '', $url_video);
				$url_video = str_replace('"', '', $url_video);				
				$url_video_array = explode('&amp;sa=', $url_video);
				$url_video = $url_video_array[0];				
				$url_video_array = explode('watch%3Fv%3D', $url_video);
				$url_video = $url_video_array[1];
				
				/*
				//href="/url?q=
				preg_match('/href="\/url\?q=.*?"/is', $value, $matches);
				$url_video = $matches[0];
				$url_video = str_replace('href="/url?q=', '', $url_video);
				$url_video = str_replace('"', '', $url_video);
			
				$title = strip_tags($value, '<b><span><cite>');
				//$html = strstr($html, '<div id=hd_1');
				
				preg_match('/<span class="f">.*?<\/span>/is', $title, $matches);
				$time_raw = $matches[0];
				
				
				$time_and_user = $matches[0];
				//$time_and_user = mb_convert_encoding($time_and_user, "cp1251", "UTF-8");
				//добавивший пользователь
				$user_add_raw_array = explode('Добавлено пользователем', $time_and_user);
				$user_add = trim($user_add_raw_array[1]);
				//$user_add = $matches[0];
				//$user_add = mb_convert_encoding($user_add, "UTF-8", "cp1251");
				
				
				//str_replace($title);
				$title = str_replace($time_raw, '|||||' , $title);
				$title_and_description = explode('|||||', $title);
				$title = $title_and_description[0];
				$description = $title_and_description[1];
				
				preg_match('/<cite>.*?<\/cite>/is', $description, $matches);			
				$source = $matches[0];
				
				//вырезаем сайт из описания
				$description = str_replace($source, '' , $description);
				
				if(strpos($time_raw, '-')){
					$time_raw_array = explode('-', $time_raw);
					$duration = $time_raw_array[0];
					$time_publish = $time_raw_array[1];
				}
				*/
				
				$title = strip_tags($title);
				$description = strip_tags($description);
				$duration = strip_tags($duration);
				$time_publish = strip_tags($time_publish);
				
				$description = $this->normalize_video_text($description);				
				
				$title = $this->normalize_video_text($title);
				
				//09_10_12
				//цензоры
				$result = $this->censor_video_text($title);
				if($result == false){
					break;
				}
				$result = $this->censor_video_text($description);
				if($result == false){
					break;
				}
				
				//13_10_12 add
				//стоп слова
				$result = $this->black_list_censor($title);
				if($result == false){
					break;
				}
				$result = $this->black_list_censor($description);
				if($result == false){
					break;
				}
				
				if(mb_stripos($value, 'youtube.com', 0, 'utf-8') OR mb_stripos($value, 'youtube.ru', 0, 'utf-8')){
					$source = 'youtube.com';
				}				
				
				$urls_imgs_array[$key]['video'] = trim($url_video);
				$urls_imgs_array[$key]['title'] = trim($title);	
				$urls_imgs_array[$key]['description'] = trim($description);			
				$urls_imgs_array[$key]['duration'] = trim($duration);
				$urls_imgs_array[$key]['time_publish'] = trim($time_publish);
				$urls_imgs_array[$key]['source'] = $source;
				$urls_imgs_array[$key]['user_add'] = $user_add;
			}
			
			/*
			###отладка
			echo '<pre>$urls_imgs_array';
			print_r($urls_imgs_array);
			echo '<pre>';
			*/
		}
		
		
		$count_loop = 0;
		foreach($urls_imgs_array as $key => $value){
		
			if($urls_imgs_array[$key]['source'] == 'youtube.com' OR $urls_imgs_array[$key]['source'] == 'youtube.ru' OR $urls_imgs_array[$key]['source'] == 'www.youtube.ru' OR $urls_imgs_array[$key]['source'] == 'www.youtube.com'){
				if($count_loop == 5){
					break;
				}
				
				//09_10_12
				//в массиве может присутсвовать изображение но отсутсвовать всё остальное поэтому нужна проверка
				if( !empty($urls_imgs_array[$key]['video']) ){
					$urls_imgs_array_top_5[] = $urls_imgs_array[$key];
				}				
				
				$count_loop++;
			}		
		}
		
		/*
		###отладка
		echo '<pre>$urls_imgs_array_top_5 - до обработки';
		print_r($urls_imgs_array_top_5);
		echo '<pre>';
		*/
		
		//закачиваем миниатуры и записываем их локальные названия в массив данных, заменяя удалённые пути
		$urls_imgs_array_top_5 = $this->google_video_thumb_grab($urls_imgs_array_top_5, $id_site);
		$this->logs_grab($urls_imgs_array_top_5, $title = 'google_video_thumb_grab');
		/*
		###отладка
		echo '<pre>$urls_imgs_array_top_5';
		print_r($urls_imgs_array_top_5);
		echo '<pre>';
		*/
		
		//вставка в базу
		$ids_videos = $this->google_video_grab($urls_imgs_array_top_5, $id_search_phrase);
		$this->logs_grab($ids_videos, $title = 'google_video_grab');
		/*
		###отладка		
		echo '<pre>';
			print_r($ids_videos);
			echo '!!!!';
			print_r($urls_imgs_array_top_5);
			//print_r($texts_html);			
		echo '</pre>';
		*/
		
		//return $urls_array;
		//return $html;		
		//return $urls_imgs_array_top_5;
		
		return $ids_videos;
	}
		
	//09_10_12
	//Цензор против клипов песен и тому подобного
	protected function censor_video_text($text){	
	
		$censor_words[] = 'афиша';
		$censor_words[] = 'концерт';
		$censor_words[] = 'группа';
		$censor_words[] = 'песня';
		$censor_words[] = 'Телешанс';	
		$censor_words[] = 'угадай слово';
		$censor_words[] = 'Составьте слово';
		$censor_words[] = 'Мара ';		
		
		foreach($censor_words as $key => $value){		
			
			if(mb_stripos($text, $value, 0, 'utf-8')){
				return false;
			}
		}
	
		return $text;
	}
	
	
	protected function normalize_video_text($text){	
			
			/*
			//удаление ненужных чисел
			$text = preg_replace('/[0-9]{1,2}:[0-9]{1,2}/i', ' ', $text);
			$text = preg_replace('/[0-9]{2,}/i', ' ', $text);	
			$text = preg_replace('/ [0-9]*\./i', ' ', $text);
			*/
			
			//$text = mb_convert_encoding($text, "cp1251", "UTF-8");
			
			//удаляем домены латиницей
			preg_match('/( |\.)([-a-z0-9]{2,30}\.|)([-a-z0-9]{2,30}\.|)[-a-z0-9]{2,30}\.[a-z0-9]{2,4}( |\.)/isu', $text, $matches);			
			$text = str_replace($matches[0], ' ' , $text);
			
			//удаляем домены киррилицей
			preg_match('/( |\.)([-а-яё0-9]{2,30}\.|)([-а-яё0-9]{2,30}\.|)[-а-яё0-9]{2,30}\.[а-яё0-9]{2,4}( |\.)/isu', $text, $matches);			
			$text = str_replace($matches[0], ' ' , $text);			
			
			//заменяем вставки латинских букв в киррилических словах на русские аналоги
			$text = preg_replace('/B([а-яА-ЯёЁ])/isu', 'В$1', $text);
			$text = preg_replace('/c([а-яА-ЯёЁ])/isu', 'с$1', $text);
			//второй рах это на случай двойной 'c'
			$text = preg_replace('/c([а-яА-ЯёЁ])/isu', 'с$1', $text);			
			
			## этот фрагмент коряво работает, удаляет некоторые русские буквы
			//заменяем кавычки
			$text = preg_replace('/&quot;/isu', '"', $text);			
			//удаляем кодировки специальных символов
			$text = preg_replace('/&[a-z]{2,7};/isu', '', $text);
			
			//удаляем ники
			$text = preg_replace('/([0-9]*[a-z]+[0-9]*)*/isu', '', $text);

			//удаление всего кроме требуемого
			$text = preg_replace('/[^-а-яА-Я0-9ёЁ =.,\\\\;"]/isu', '', $text);
			$text = str_replace('://', '', $text);
			$text = preg_replace('/\( *?-*? *?\)/', '', $text);
			
			//удаление лишних двоеточий
			$text = preg_replace('/([а-яё0-9]) *?\.*? *?: *?\./u', '$1.', $text);
			
			//удаляем мусор из начала строки
			$text = preg_replace('/^[^а-яА-ЯёЁ"]*/isu', '', $text);	

			## 02_06_12-17_48
			//удаление лишних двоеточий
			//$text = preg_replace('/([^а-яА-ЯёЁ \'"])/u', '', $text);			
			
			//удаляем тире вначале и конце текта
			$text = preg_replace('/-\s$/isu', '.', $text);
			
			//нормализуем пробелы у знаков препинания
			$text = preg_replace('/([а-яё0-9]) (,|\.)/iu', '$1$2', $text);
			$text = preg_replace('/(,|\.)([а-яё0-9])/iu', '$1 $2', $text);
			
			//заменяем точки между которыми пробелы на троеточие
			$text = preg_replace('/\. *?\./iu', '...', $text);
			
			//заменяем любое количество точек больше 2х на троеточие
			$text = preg_replace('/\.{2,}/iu', '...', $text);
			
			//проставляем пробелы около тире, кроме цифр
			$text = preg_replace('/([^-0-9])-([^-0-9])/iu', '$1 - $2', $text);		
			
			//удаляем лишние пробелы
			$text = preg_replace('/ {2,}/iu', ' ', $text);			
			
			//удаление пробелов между словом и кавычками
			$text = preg_replace('/" *?([а-яё0-9]*?) *?"/iu', '"$1"', $text);
			
			//первая буква в заглавную
			$text = preg_replace('/^\b(\w)/eu', 'strtoupper("$1")', $text); 
			
			$text = trim($text);
			//проставление точек вконце
			$text = preg_replace('/([а-яё0-9])$/iUu', '$1.', $text); 
			
			//удаление лишних запятых
			$text = preg_replace('/([а-яё0-9]) *?, *?\./iUu', '$1.', $text); 
			
			//удаление мусора между точками и запятыми
			$text = preg_replace('/(\.|,)[^а-яА-ЯёЁ]*(\.|,)/isu', '.', $text);
			
			
			//замена любого количества тире на одно
			$text = preg_replace('/-{2,}/isu', '-', $text);
			
			//нормализация количества тире
			$text = preg_replace('/-{2,}/isu', '-', $text);
			
			//нормализация растояний около тире
			$text = preg_replace('/([^-])- /isu', '$1 - ', $text);

			//прижимание % к цифрам
			$text = preg_replace('/([0-9]) *%/isu', '$1%', $text);				
			
			//удаление кавычек
			$text = preg_replace('/(\'|")/isu', '', $text);
			
			//удаление лишних пробелов
			$text = preg_replace('/ {2,}/isu', ' ', $text);
			
			
			//прижимание точки, точки с запятой к слову
			$text = preg_replace('/([а-яА-ЯёЁ0-9"\'%)]) *(\.|;)/isu', '$1$2', $text);
			
			//удаляем всё кроме требуемого /
			$text = preg_replace('/[^-а-яА-ЯёЁ0-9"\',\. ]/isu', '', $text);
			
			//делает первую букву заглавной
			$text = $this->first_char_bigger($text);
			
			//$text = mb_convert_encoding($text, "UTF-8", "cp1251");
			
						
			return $text;
	}
	
	
	public function google_video_parse($html){			

		$html = strstr($html, '<div id="res">');
		//$html = strstr($html, '<div id=hd_1');
		
		$html = strstr($html, '<div id="foot">', true);
		//$html = strstr($html, '</div>', true);
	
		//$urls_array = $this->google_img_url_parse($html);		
		
		//return $urls_array;
		return $html;
	}	
	
	
	public function google_result_censor($matches, $phrase){
		foreach($matches as $key => $value){		
			//$value = mb_convert_encoding($value, 'CP1251', 'UTF8');
			
			//проверка на наличие искомой фразы в фрагменте текста //отключено по причине не нахождения их в фрагментах гугл
			//if(stripos($value, $phrase)){
				
				//предотвразение ошибки денвера (если не использовать иногда происходит)
				//$value = preg_replace('/[^-a-zA-Z0-9;&:?\.!\/<>"\'=%]/isu', '', $value);
				
				preg_match('/http.*&amp;sa=/isUu', $value, $matches_1);
				
				/*
				echo $value . '<br>';
				print_r($matches_1);
				echo '<br><br><br>';
				*/
				
				$temp_url = str_replace('&amp;sa=', '', $matches_1[0]);
				
				//$temp_url = preg_replace('/&amp;sa=/isu', '', $matches_1[0]);

				
				//$temp_url = substr($matches_1[0], 0, strlen($matches_1[0]) -  8);
				//$temp_url = $matches_1[0];
				//$temp_url = strstr($temp_url, '&amp;sa=', $before_needle = true);
				
				//echo $temp_url . ' !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!<br>';
				
				//$results['url'][] = $matches_1[0];
				$results['url'][] = $temp_url;
				$results['phrase'] = $phrase;
				
				$this->key_phrase = $phrase;
			//}			
		}
		
		/*
		echo '<pre><b>results_result_censor</b>??????????????????????';
		print_r($results);
		echo '</pre>';
		*/
		
		return $results;
	}
	
	
	public function full_text_assembler($texts_and_seach_phrase_array){	
	
		//$count_loop = 0;
		if(is_array($texts_and_seach_phrase_array['texts'])){		
		
			foreach($texts_and_seach_phrase_array['texts'] as $key => $value){
			
				if(!empty($texts_and_seach_phrase_array['texts'][$key]['link'])){
					
					$array_link_and_phrase['url'] = '';
					$array_link_and_phrase['url'][0] = $texts_and_seach_phrase_array['texts'][$key]['link'];					
					
					//echo '!!' . $array_link_and_phrase['url'][0];
					
					$text_array = $this->donor_parser($array_link_and_phrase, 1);
					if(is_array($text_array)){
						$texts_and_seach_phrase_array['texts'][$key]['text'] = $text_array;
					}
					
					//$this->donor_parser($array_link_and_phrase, 1);
					//$count_loop++;
				}
				
				/*
				if($count_loop = 1){
					break;
				}
				*/
			}
		
		}
		
		return $texts_and_seach_phrase_array;
	}
	
	
	protected function strip_scripts_ou_ol_tags($value_1){
		$value_1 = preg_replace("/<script[^>]*>.*<\/script[^>]*>/isU", "", $value_1); 
		$value_1 = preg_replace("/<ul[^>]*>.*<\/ul[^>]*>/isU", "", $value_1); 
		//удаляем списки
		$value_1 = preg_replace("/<ol[^>]*>.*<\/ol[^>]*>/isU", "", $value_1); 
		$value_1 = strip_tags($value_1, '<a><div><p><span><td><br>');
		
		return $value_1;
	}
	
	
	protected function strip_scripts_and_tag_a($value){
	
		$value = preg_replace("/<script[^>]*>.*<\/script[^>]*>/isU", "", $value); 
		$value = preg_replace("/(<|&lt;)a.*?(>|&gt;)/isu", " ", $value);
		$value = preg_replace("/(<|&lt;)\/a(>|&gt;)/isu", " ", $value);		
		
		return $value;
	}
	
	
	//променять до чистки мета тегов страницы
	protected function test_encode_page($text){

		$pattern_1 = '/charset=windows-1251(\'|")/i'; 
		$pattern_2 = '/charset=utf(-|)8(\'|")/i'; 
		if(preg_match($pattern_1, $text, $matches) == 1){
			return 1;
		}elseif(preg_match($pattern_2, $text, $matches) == 1){
			return 2;
		}else{
			return 3;
		}
	}
	
	
	//full_text_flag = 0 - не ходит по страницам с полными текстами, 1 - ходит
	public function donor_parser($array_link_and_phrase, $full_text_flag = 0){
		//setlocale("LC_ALL","ru_RU");
		if($full_text_flag == 0){
		
			$count_loop = 0;
			
			//$html_array = '';
			$html_array_1 = '';
			
			foreach($array_link_and_phrase['url'] as $key => $value){
				$count_loop++;
				//sleep(1);
				//$encode_number = $this->test_encode_page($value_1);
				//$value_1 = $this->strip_scripts_ou_ol_tags($value_1);
				//echo '<br>Занято памяти сейчас (до curl_connect): ' . memory_get_usage() . ' максимально: ' . memory_get_peak_usage() . '<br>';
				$html = $this->file_get_connect($value);
				$this->logs_grab($value, $title = 'file_get_connect');
				
				if($html !== false){
					
					/*
					$len_html = strlen($html);
					
					if($len_html > 650000){
						$html = '';
					}
					*/
					
					//echo '<br>Длина сграбленной страницы' . $len_html . '<br>';
					//echo '<br>Занято памяти сейчас (после curl_connect): ' . memory_get_usage() . ' максимально: ' . memory_get_peak_usage() . '<br>';
					
					$encode_number = $this->test_encode_page($html);
					$this->logs_grab($encode_number, $title = 'encode_number');
					
					$html = $this->strip_scripts_ou_ol_tags($html);
					$this->logs_grab('Ok!', $title = 'strip_scripts_ou_ol_tags');
					
					//echo '<br>Занято памяти сейчас (до mb_convert_encoding): ' . memory_get_usage() . ' максимально: ' . memory_get_peak_usage() . '<br>';
					if($encode_number == 1){
						$html = mb_convert_encoding($html, 'UTF8', 'CP1251');
						$this->logs_grab('Ok!', $title = 'mb_convert_encoding');
					}
					//echo '<br>Занято памяти сейчас (после mb_convert_encoding): ' . memory_get_usage() . ' максимально: ' . memory_get_peak_usage() . '<br>';
					
					$html_array_1[$key]['html'] = $html;
					$html_array_1[$key]['url'] = $value;
					
					//$html_array[] = '<div style="font-size:150%;font-weight:bold;margin-top:40px;margin-bottom:40px;">Сайт номер ' . $count_loop . '</div>' . $this->curl_connect($value);
				}
				//echo $count_loop . '<br>';
			}
		
		}else{
			//echo '!!!' . $array_link_and_phrase['url'][0] . '!!!';
			//$url = 'http://www.liveinternet.ru/users/tatyanf25/post183020085/';
			$value_1 = $this->file_get_connect($array_link_and_phrase['url'][0]);
			$this->logs_grab($array_link_and_phrase['url'][0], $title = 'file_get_connect');
			/*
			$len_html = strlen($value_1);
				
			if($len_html > 650000){
				$value_1 = '';
			}
			*/
			if($value_1 !== false){
			
				$encode_number = $this->test_encode_page($value_1);
				$this->logs_grab($encode_number, $title = 'encode_number');
				//echo '<br>!!!' . $encode_number . '!!!<br>';
				
				$value_1 = $this->strip_scripts_ou_ol_tags($value_1);
				$this->logs_grab('Ok!', $title = 'strip_scripts_ou_ol_tags');
				
				if($encode_number == 1){
					$value_1 = mb_convert_encoding($value_1, 'UTF8', 'CP1251');
					$this->logs_grab('Ok!', $title = 'mb_convert_encoding');
				}
				
				//echo '!!! ' . $encode_number . '!!!';
				
				//echo '!!! ' . $value_1 . '!!!';
				
				//$value_1 = mb_convert_encoding($value_1, 'UTF8', 'CP1251');
				//echo $value_1;
				//$html_array_1[] = $value_1;			
				$html_array_1[$key]['html'] = $value_1;
				$html_array_1[$key]['url'] = $array_link_and_phrase['url'][0];
			}
		}
		/*
		$html = preg_replace("/<script[^>]*>.*<\/script[^>]*>/isU", "", $html); 
		$html = preg_replace("/<ul[^>]*>.*<\/ul[^>]*>/isU", "", $html); 
		//удаляем списки
		$html = preg_replace("/<ol[^>]*>.*<\/ol[^>]*>/isU", "", $html); 
		//удаляем домены
		$html = preg_replace("#[a-z-0-9]{2,30}\.[a-z]{2,3}#iU", "", $html); 
		
		//очистка тегов, разделение по <div
		$html = strip_tags($html , '<div><p><span><td><br>');//<li><ul><ol>	
		*/
		
		
		//$html = preg_replace('/<a\s+.*?href="([^"]+)"[^>]*>([^<]+)<\/a>/is', '\2 (\1)', $html);		

		/*
		echo '<pre>';
		print_r($html_array_1);
		echo '</pre>';
		*/
		
		
		//обнуление носителя текстов, чтобы обзорные фрагменты не залетали на этапе сборки полных текстов
		unset($result_array_1);
		//$this->result_array_1 = '';
		
		$text_link['text'] = '';
		$text_link['link'] = '';
		$value_4['text'] = '';
		$value_4['link'] = '';
		$td_start_array['text'] = '';
		$td_start_array['link'] = '';
		
		/*
		echo '<pre>';
		print_r($html_array_1);
		echo '</pre>';
		*/
		if( is_array($html_array_1) ){
		
			foreach($html_array_1 as $key_10 => $value_10){
			
				if( !empty($value_10['html']) ){
				
					$div_array = explode('<div', $value_10['html']);
					$this->logs_grab('Ok!', $title = 'explode_div');
					
					if( is_array($div_array) ){
					
						foreach($div_array as $key => $value){						
												
							//add 19_11_12-21_04										
							$tmp_result = $this->separate_page_td_blocks($value, $temp, $result_array_1, $value_10);
							$this->logs_grab('Ok!', $title = 'separate_page_td_blocks');
							$temp = $tmp_result['temp'];
							$result_array_1 = $tmp_result['result_array_1'];									
											
						}
					
					}else{
						$tmp_result = $this->separate_page_td_blocks($div_array, $temp, $result_array_1, $value_10);
						$this->logs_grab('Ok!', $title = 'separate_page_td_blocks');
						$temp = $tmp_result['temp'];
						$result_array_1 = $tmp_result['result_array_1'];
					}
					
					/* ### позиция остановки цикла - для отладки
					$count_loop_test++;
					if($count_loop_test == 7){
						break;
					}
					*/
				}
			}
			
		}
		
		if($full_text_flag == 0){
			$result_array_1['key_phrase'] = $this->key_phrase;	
		}
		
		$this->logs_grab('Ok!', $title = 'donor_parser_end');
		
		return $result_array_1;
	}
	
	
	//add 19_11_12-21_04	
	//функция для разделения блоков таблиц страницы
	protected function separate_page_td_blocks($value, $temp, $result_array_1, $value_10){
	
										//add 19_11_12-21_04								
										$td_start_array = explode('<td', $value);
										$this->logs_grab('Ok!', $title = 'explode_td');

										if(is_array($td_start_array)){
											foreach($td_start_array as $key_4 => $value_4){
											
												$value_fin = $this->donor_parser_cleaner($value_4, $full_text_flag);
												$this->logs_grab('Ok!', $title = 'donor_parser_cleaner');
												//$value_fin['text'] = $value_4;
												//$value_fin['link'] = '';
												
												if($temp != $value_fin['text'] AND $value_fin['text'] != false){
												
													$text_link['text'] = $value_fin['text'];
													$text_link['link'] = $value_fin['link'];
													$text_link['title'] = $value_fin['title'];
													$text_link['url'] = $value_10['url'];
													
													$result_array_1['texts'][] = $text_link;												
												}
												
												$temp = $result_array_1['texts'][count($result_array_1['texts']) - 1]['text'];
											}
										}else{
											$value_fin = $this->donor_parser_cleaner($td_start_array, $full_text_flag);
											$this->logs_grab('Ok!', $title = 'donor_parser_cleaner');
											//$value_fin['text'] = $td_start_array;
											//$value_fin['link'] = '';								

											
											if($temp != $value_fin['text'] AND $value_fin['text'] != false){
												
												$text_link['text'] = $value_fin['text'];
												$text_link['link'] = $value_fin['link'];
												$text_link['title'] = $value_fin['title'];
												$text_link['url'] = $value_10['url'];
												
												$result_array_1['texts'][] = $text_link;												
											}
											
											$temp = $result_array_1['texts'][count($result_array_1['texts']) - 1]['text'];
										}

		
		$result_finish['temp'] = $temp;
		$result_finish['result_array_1'] = $result_array_1;
		
		return $result_finish;
	}
	
	
	protected function search_full_link($text){
		
		//если это краткое содержание то ищем ссылку на полный текст
		$link_words_for_full_text = array(
			0 => "Читать далее",
			1 => "подробнее",
			2 => "подробно",
			3 => "more",
			4 => "далее",
			5 => "дальше"
		);
		
		foreach($link_words_for_full_text as $key => $value){
			//найдено вхождение фразы указывающей на основной текс
			//echo '<br>Занято памяти сейчас (до mb_stripos): ' . memory_get_usage() . ' максимально: ' . memory_get_peak_usage() . '<br>';
			if(mb_stripos($text, $value, 0, 'utf-8')){
			//echo '<br>Занято памяти сейчас (после mb_stripos): ' . memory_get_usage() . ' максимально: ' . memory_get_peak_usage() . '<br>';
				//$value = mb_convert_encoding($value, 'CP1251', 'UTF8');
	
				
				//$pattern = '<a href="http:\/\/www\.liveinternet\.ru\/users\/tatyanf25\/post183020085\/">Читать далее\.\.\.<\/a>';
				//$pattern = "/<a.*?a>/is";
				
				//$text = mb_convert_encoding($text, 'CP1251', 'UTF8');

				//$matches[0];
				
				//preg_match('/(http:\/\/.*?)(&quot;|\"|\')(&gt;|>)/', $text, $matches[0]);
				
				//$pattern = "/(http:.*?)(\&quot;|\"|\')(.*?)/";//(\&quot;|\"|\')(\&gt;|>)
				$pattern = '/href=.{1,6}http:.*?(\&quot;|\"|\')/';//(\&quot;|\"|\')(\&gt;|>)
				
				preg_match($pattern, $text, $matches_1);
				//$link_full_text = preg_replace($pattern, '$1', $matches[0]);
				if(!empty($matches_1[0])){
					preg_match("|http:.*|", $matches_1[0], $matches_1);
				}
				
				$matches_1[0] = preg_replace('(\&quot;|\"|\')', '', $matches_1[0]);
				
				//если ещё не обрабатывали эту ссылку работаем по ней
				if(!empty($matches_1[0])){// убрано ввиду неизвестной баги, дубляжа не видно, а условие не пропускает - $matches_1[0] != $this->link_full_text AND 
				
				
					/*
					echo '<br>!!!!!!!!!!!!!!!!!!!!!!!!<br><br><br><br>Ссылка на полный текст UTF-8: <b>' . $matches_1[0] . '<b> ';
					
					echo '<pre>';
					echo $text;
					echo '</pre>';
					*/
				
					$this->link_full_text = $matches_1[0];
					
					if(!empty($matches_1[0])){
					
						$flag_old_link = 0;
						
						if(!empty($this->links_full_text[0])){
							foreach($this->links_full_text as $key_1 => $value_1){
								if($value_1 == $matches_1[0]){
									$flag_old_link = 1;
								}
							}
						}
						
						if($flag_old_link == 0){
							//$array_link_and_phrase['url'][] = $matches_1[0];
							
							//вызывает начало итераций для сбора обработки документа с полным текстом
							//$this->donor_parser($array_link_and_phrase);
							
							$link_full_text = $matches_1[0];
						}
						

					}	
				}
			}
		}
		
		return $link_full_text;
	}
	
	
	protected function domain_clean($text){
	
		$text = preg_replace("#(https?:\/\/)?(www\.)?([-a-z0-9]{2,30}\.){1,3}[a-z]{2,3}#iU", "", $text);
		//$text = preg_replace("#/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/#iU", "", $text);
		
		return $text;
	}
	
	
	//18_11_12-23_57
	protected function ascii_encode($text){	
		
		$text = str_replace('&#33;', '!', $text);
		$text = str_replace('&#34;', '"', $text);
		$text = str_replace('&#63;', '?', $text);	
		$text = str_replace('&#46;', '.', $text);
		$text = str_replace('&#44;', ',', $text);
		$text = str_replace('&#58;', ':', $text);
		$text = str_replace('&#59;', ';', $text);
		$text = str_replace('&#45;', '-', $text);
		$text = str_replace('&#40;', '(', $text);
		$text = str_replace('&#41;', ')', $text);
		$text = str_replace('&#37;', '%', $text);
		$text = str_replace('&#47;', '/', $text);	
		
		return $text;
	}
	
	protected function donor_parser_cleaner($text, $full_text_flag = 0){
		
		$text = trim($text);
	
		/*
		//если это краткое содержание то ищем ссылку на полный текст
		$link_words_for_full_text = array(
			0 => "Читать далее",
			1 => "подробнее",
			2 => "подробно",
			3 => "more",
			4 => "далее",
			5 => "дальше",
		);
		
		foreach($link_words_for_full_text as $key => $value){
			//найдено вхождение фразы указывающей на основной текс
			if(stripos($text, $value)){
				//$value = mb_convert_encoding($value, 'CP1251', 'UTF8');
	
				
				//$pattern = '<a href="http:\/\/www\.liveinternet\.ru\/users\/tatyanf25\/post183020085\/">Читать далее\.\.\.<\/a>';
				//$pattern = "/<a.*?a>/is";
				
				//$text = mb_convert_encoding($text, 'CP1251', 'UTF8');

				//$matches[0];
				
				//preg_match('/(http:\/\/.*?)(&quot;|\"|\')(&gt;|>)/', $text, $matches[0]);
				
				//$pattern = "/(http:.*?)(\&quot;|\"|\')(.*?)/";//(\&quot;|\"|\')(\&gt;|>)
				$pattern = "/href=.{1,6}http:.*?(\&quot;|\"|\')/";//(\&quot;|\"|\')(\&gt;|>)
				
				preg_match($pattern, $text, $matches_1);
				//$link_full_text = preg_replace($pattern, '$1', $matches[0]);
				if(!empty($matches_1[0])){
					preg_match("|http:.*|", $matches_1[0], $matches_1);
				}
				
				$matches_1[0] = preg_replace('(\&quot;|\"|\')', '', $matches_1[0]);
				
				//если ещё не обрабатывали эту ссылку работаем по ней
				if($matches_1[0] != $this->link_full_text AND !empty($matches_1[0])){
				
					echo '<br>!!!!!!!!!!!!!!!!!!!!!!!!<br><br><br><br>Ссылка на полный текст UTF-8: <b>' . $matches_1[0] . '<b> ';
					
					echo '<pre>';
					echo $text;
					echo '</pre>';
				
				
					$this->link_full_text = $matches_1[0];
					
					if(!empty($matches_1[0])){
					
						$flag_old_link = 0;
						foreach($this->links_full_text as $key_1 => $value_1){
							if($value_1 == $matches_1[0]){
								$flag_old_link = 1;
							}
						}
						
						if($flag_old_link == 0){
							$array_link_and_phrase['url'][] = $matches_1[0];
							
							//вызывает начало итераций для сбора обработки документа с полным текстом
							$this->donor_parser($array_link_and_phrase);
							
							$this->links_full_text[] = $matches_1[0];
						}
						

					}	
				}
				
			

			}
		}
		*/
		
		
		if($full_text_flag == 0){
			$link_full_text = $this->search_full_link($text);
			$this->logs_grab('Ok!', $title = 'search_full_link');
		}
		
		//заменяем неразрывный пробел
		$text = str_replace('&nbsp;', ' ', $text);
		//заменяем нижнее подчёркивание
		$text = str_replace('_', ' ', $text);
		
		//заменяет анлийские буквы на русские визуальные аналоги в русских словах
		$text = $this->change_eng_char_on_rus_analog($text);
		$this->logs_grab('Ok!', $title = 'change_eng_char_on_rus_analog');
		
		//удаляем домены
		//$text = preg_replace("#[-a-z0-9]{2,30}\.[a-z]{2,3}#iU", "", $text); 
		$text = $this->domain_clean($text);
		$this->logs_grab('Ok!', $title = 'domain_clean');
		
		//$text = nl2br($text);
		//19_11_12-21_28
		$text = preg_replace('|<\/ ?p>|ui', '<br>', $text);
		
		//21_11_12-08_03
		//заменяет переносы строк на пробелы
		$text = preg_replace('|\s|uis', ' ', $text);
		//$text = nl2br($text);	

		//удаляет содержимое [] чтобы из вики техническая инфа не вставлялась
		$text = preg_replace('|\[.*?\]|uis', ' ', $text);
		
		//удаляет title и alt тегов
		$text = preg_replace('/(title|alt)=(\'|").*?(\'|"|>)/uis', '', $text);
				
		$text = str_replace('<br>', ' ЁбрЁ ', $text);
		$text = str_replace('<br/>', ' Ёбр-Ё ', $text);
		$text = str_replace('<br />', ' Ёбр -Ё ', $text);
		
		//21_11_12-10_34
		//для того чтобы прилипшие слева цифры к слову обозначающему размерность не удалялись в дальнеших фильтрах делаем разрыв
		$text = preg_replace('/([0-9]{1,})([сСмМчЧкК]{1,3})/uis', '$1 $2', $text);
		
		/*
		//21_11_12-08_31
		$text = str_replace('(', ' Ё1бр -Ё ', $text);
		$text = str_replace(')', ' Ё2бр -Ё ', $text);
		*/
		

		
		//$text = str_replace('<p>', ' ЁпЁ ', $text);
		//$text = str_replace('</p>', ' Ё-пЁ ', $text);
		//чтобы небыло слипаний слов
		$text = str_replace('&nbsp;', ' ', $text);		
		$text = str_replace('<', ' ', $text);
		$text = str_replace('>', ' ', $text);
		//$text = str_replace('/', ' ', $text);
		$text = str_replace("\\", ' ', $text);	
		$text = str_replace('*', ' ', $text);	
		
		$text = str_replace('&quot;', '"', $text);
		$text = str_replace('&mdash;', '-', $text);	
		$this->logs_grab('Ok!', $title = 'mass_replace');
	
		$text = $this->ascii_encode($text);				
		$this->logs_grab('Ok!', $title = 'ascii_encode');
		//18_11_12-23_57
		//удаляет кодировки спец вставок, чтобы после зачистки всего лишнего от них не вылазели остающиеся цифры.
		//$text = preg_replace('|&#[0-9]{1,};|ui', '', $text);		
		$text = preg_replace('|&.{3,6};|ui', ' ', $text);
		//$text = str_replace('&.+;', ' ', $text);
		//&#160;		
		
		$text = preg_replace('|[^-А-Яа-яЁё:.,0-9 ?!%()/"]|ui', '', $text);			
		$text = str_replace(' ЁбрЁ ', '<br>', $text);
		$text = str_replace(' Ёбр-Ё ', '<br>', $text);
		$text = str_replace(' Ёбр -Ё ', '<br>', $text);
		//$text = str_replace(' ЁпЁ ', '<p>', $text);
		//$text = str_replace(' Ё-пЁ ', '</p>', $text);

		/*
		//21_11_12-08_31
		$text = str_replace(' Ё1бр -Ё ', '(', $text);
		$text = str_replace(' Ё2бр -Ё ', ')', $text);	
		*/
				
		//удаление лишних проблелов и <br> около <br>
		
		$text = preg_replace('/<br>\s*<br>/', '<br><br>', $text); // - приводит к фатальной ошибке апача
		//$text = preg_replace("/(<br>){3,}/", "<br><br>", $text); // - приводит к фатальной ошибке апача
		$this->logs_grab('Ok!', $title = 'mass_replace_2');
		
		$text = trim($text);
		$text_lench = strlen($text);
		if($text_lench > 650){		
			
			$count_matches = preg_match_all('/<br>/', $text, $matches);
			
			if(empty($count_matches)){
				$count_matches = 1;
			}
			
			if(($text_lench / $count_matches) > 50){
			
				$count_matches_1 = preg_match_all('/,/', $text, $matches_1);
				
				if(empty($count_matches_1)){
					$count_matches_1 = 1;
				}
				
				if(($text_lench / $count_matches_1) > 35){		
				
					$text_array['link'] = $link_full_text;
					
					//использование методов нормализации, цензора и очистки
									
					$text = $this->bad_encode_cleaner($text);	
					$this->logs_grab('Ok!', $title = 'bad_encode_cleaner');
					if( empty($text) ){
						return false;
					}
					
					$text = $this->key_phrase_censor($text, 0);
					$this->logs_grab('Ok!', $title = 'key_phrase_censor');					
					if( empty($text) ){
						return false;
					}
					
					$text = $this->garbage_censor($text);
					$this->logs_grab('Ok!', $title = 'garbage_censor');
					if( empty($text) ){
						return false;
					}
					
					$text = $this->quotes_clean($text);
					$this->logs_grab('Ok!', $title = 'quotes_clean');
					if( empty($text) ){
						return false;
					}
					
					$text = $this->slash_dig_save_other_slash_clean($text);
					$this->logs_grab('Ok!', $title = 'slash_dig_save_other_slash_clean');
					if( empty($text) ){
						return false;
					}
					
					$text = $this->digits_3_plus_dot_clean($text);	
					$this->logs_grab('Ok!', $title = 'digits_3_plus_dot_clean');
					if( empty($text) ){
						return false;
					}
					
					$text = $this->clear_0_and_other($text);
					$this->logs_grab('Ok!', $title = 'clear_0_and_other');
					if( empty($text) ){
						return false;
					}
					
					$text = $this->clear_br_group($text);
					$this->logs_grab('Ok!', $title = 'clear_br_group');
					if( empty($text) ){
						return false;
					}
					
					$text = $this->garbage_dig_and_more_clean($text);
					$this->logs_grab('Ok!', $title = 'garbage_dig_and_more_clean');
					if( empty($text) ){
						return false;
					}
					
					$text = $this->mass_dot_clean($text);
					$this->logs_grab('Ok!', $title = 'mass_dot_clean');
					if( empty($text) ){
						return false;
					}
					
					$text = $this->spec_blog_phrase_clean($text);
					$this->logs_grab('Ok!', $title = 'spec_blog_phrase_clean');
					if( empty($text) ){
						return false;
					}									
												
					$text = $this->garbage_clean($text); //есть закоментированный фрагмент который может вызывать фатальную ошибку апача
					$this->logs_grab('Ok!', $title = 'garbage_clean');
					if( empty($text) ){
						return false;
					}
					
					$text = $this->text_end_clear($text);
					$this->logs_grab('Ok!', $title = 'text_end_clear');
					if( empty($text) ){
						return false;
					}				
					
					$text = $this->text_normalizer($text);
					$this->logs_grab('Ok!', $title = 'text_normalizer');
					if( empty($text) ){
						return false;
					}
					
					//18_11_12-07_04
					$text = $this->censors_count_includes($text);
					$this->logs_grab('Ok!', $title = 'censors_count_includes');
					if( empty($text) ){
						return false;
					}
					
					//09_10_12
					$text = $this->cleaner_normalizer_common($text);
					$this->logs_grab('Ok!', $title = 'cleaner_normalizer_common');
					if( empty($text) ){
						return false;
					}					
					
					$text = $this->first_char_bigger($text);
					$this->logs_grab('Ok!', $title = 'first_char_bigger');
					if( empty($text) ){
						return false;
					}										
					
					$text = $this->black_list_censor($text);
					$this->logs_grab('Ok!', $title = 'black_list_censor');
					if( empty($text) ){
						return false;
					}
					
					//add 16_10_12-21_33
					$text = $this->individual_black_list($text);
					$this->logs_grab('Ok!', $title = 'individual_black_list');
					if( empty($text) ){
						return false;
					}
					
					### отладка
					//echo '<br><br><br><br><br>' . $text . '<br><br><br><br><br>';
					
					$text = $this->mass_space_clean($text);	
					$this->logs_grab('Ok!', $title = 'mass_space_clean');					
					if( empty($text) ){
						return false;
					}					
					
					//18_11_12-07_04
					$text = $this->censor_lench_word($text);
					$this->logs_grab('Ok!', $title = 'censor_lench_word');	
					if( empty($text) ){
						return false;
					}
					
					$text = $this->finish_lench_censor($text, $min_lench = 600);
					$this->logs_grab('Ok!', $title = 'finish_lench_censor');
					if( empty($text) ){
						return false;
					}
					
					//09_10_12
					$text = $this->key_phrase_number_censor($text, $fragmentary_equal_key_phrase = 1, $number_entry_limit = 5);
					$this->logs_grab('Ok!', $title = 'key_phrase_number_censor');
					if( empty($text) ){
						return false;
					}					
					
					//поиск названия текста
					$title_text = $this->title_search($text);
					$this->logs_grab('Ok!', $title = 'title_search');
					//$text = $this->clipped_text_censor($text); - не требуется, удаляет тексты с многоточиями
					
					$text_array['title'] = $title_text;
					$text_array['text'] = $text;
					
					return $text_array;
					
				}else{
					return false;
				}				
			}else{
				return false;
			}
			//$temp_1 = preg_replace('/^[^А-Яа-яЁё0-9]/u', '', $temp_1);
			
			//$temp_1 = trim($temp_1);							
		}else{
			return false;
		}			
		
		//return $text;
	}
	
	
	//цензор на лимит ключевых слов в тексте, если слишком много то текст бракуется
	protected function key_phrase_number_censor($text, $fragmentary_equal_key_phrase = 1, $number_entry_limit = 5){ 
		//$number_entry_limit = 5 - до 5 ключевых слов на 100 символов
	
		$key_phrase = $this->key_phrase;
		//$key_phrase = mb_convert_encoding($key_phrase, 'UTF8', 'CP1251');
		//$key_phrase = str_replace(' ' , '|', $key_phrase);
		$key_phrase_array = explode(" ", $key_phrase);
		
		/*
		echo '<br>!!!!!!';
		print_r($key_phrase_array);
		echo '<br>!!!!!!' ;
		*/
		
		$flag = 0;
		$number_entry = 0;
		$chars_in_text = 0;
		$chars_in_text = mb_strlen($text, 'utf-8');
		
		$number_100 =  $chars_in_text / 100;
		$number_entry_limit_for_all_text = $number_entry_limit * $number_100;
		
		foreach($key_phrase_array as $key => $value){
			
			$value = trim($value);
			/*
			$strlen = mb_strlen($value, 'utf-8');
			
			$end_char = mb_substr($value, $strlen-1, 1, 'utf-8');
			
			$glasnie = 'ауоыиэяюёе';			
			if(mb_strpos($glasnie, $end_char, 0, 'utf-8'){
				
			}
			*/
			
			$key_word_fragment = $this->cleaner_word_ending($value);
	
			$strlen = mb_strlen($key_word_fragment, 'utf-8');
			
			//echo '<br><br><br>!!!! выдала cleaner_word_ending: ' . $value;
			//echo '<br><br><br>!!!!' . $this->key_phrase . '<br><br><br>!!!!';
			//$position_word = false;
			//$position_word = mb_stripos($text, $key_word_fragment, 0, 'utf-8');
			
			$pattern_1 = '/' . $key_word_fragment . '/ius'; 
			$number_entry += preg_match_all($pattern_1, $text, $matches);			
				
			if($strlen > 3){
			
				//echo '<br><br>!!!!! слова: ' . $key_word_fragment . ' позиция: ' . $position_word . ' !!!!!!<br><br>'; 
				
				if($fragmentary_equal_key_phrase == $number){
					if($number_entry >= $number_entry_limit_for_all_text){
						$flag = 1;
						break;						
					}
				}else{
				
					$flag = 1;
					
					if($number_entry < $number_entry_limit_for_all_text){
						$flag = 0;
						break;						
					}
				}

			}

		}
		
		if($flag == 0){
			return $text; 
		}else{
			return false;
		}	
		
	}
	
	
	protected function cleaner_normalizer_common($text){			
		
		//удаляет знак % без цифры
		$text = preg_replace('/(?<=[^0-9])%/uis', '', $text);
		//удаляет всё из начала текста до первой киррилицы
		$text = preg_replace('/^[^а-яА-ЯёЁ]*/uis', '', $text);
		
		//удаляет мусор типа
		//$text = preg_replace('/[-0-9 ,.!?:;]{8,}/uis', '', $text);
		$text = preg_replace('|[-0-9 ,.!?:;/)/(]{8,}|uis', ' ', $text);
		
		//18_11_12 06_46
		//удаляет пустоты прижатые к скобке внутри
		$text = preg_replace('|\( *|uis', '(', $text);
		$text = preg_replace('| *\)|uis', ')', $text);
		
		//удаляет пустые скобки
		$text = preg_replace('|\(\s*\)|uis', '', $text);		
		
		//удаляет квадратные скобки с содержимым
		$text = preg_replace('|\[.*\]|uis', '', $text);
		
		//удаляет многоточия после <br>
		$text = preg_replace('|<br>\.{3,}|uis', '<br>', $text);
		
		//удаляет скобку с одиночным символом и любым количеством пробелов
		$text = preg_replace('|\( *. *\)|uis', '', $text);				
		
		//удаляет пробелы и тире прижатые к правой скобке изнутри
		$text = preg_replace('|[- ,.]*\)|uis', ')', $text);
		//удаляет пробелы и тире прижатые к левой скобке изнутри
		$text = preg_replace('|\([- ,.]*|uis', '(', $text);	
		
		//удаляет прижатые к словам или внешней правой стороне скобок цифры (буквы малые)
		$text = preg_replace('/(\)|[а-яё])[0-9]{1,}/uis', '$1', $text);	

		//удаляет прижатые к внешней левой стороне скобок цифры
		$text = preg_replace('/[0-9]{1,}(\()/uis', '$1', $text);

		//удаляет строку типа Сообщения 1 Зарегистрирован 26 сен
		$text = preg_replace('|Сообщения [0-9]{1,} Зарегистрирован [0-9]{1,} [а-яёЁ]{3,}|uis', '', $text);
		
		//удаляет техническую информацию
		$text = preg_replace('|Перейти на\.\.\.|uis', '', $text);
		
		//удаляет техническую информацию
		$text = preg_replace('|Переходов на сайт [0-9]*|uis', '', $text);
		
		//удаляет техническую информацию
		$text = preg_replace('|Разместить ваш сайт в Каталоге Разместить ваш сайт... Весь Каталог сайтов Весь Каталог|uis', '', $text);
		
		//удаляет техническую информацию
		$text = preg_replace('|Отправил?[0-9]*/ Прочитали[0-9. ]*|uis', '', $text);
				
		//удаляет техническую информацию
		$text = preg_replace('|Рекомендую также следующие статьи|uis', '', $text);

		//удаляет техническую информацию
		$text = preg_replace('|Увеличить\.\.\.|uis', '', $text);		

		//удаляет техническую информацию
		$text = preg_replace('|Читать полностью\.|uis', '', $text);		
		
				
		//нормализует предлоги перед годами
		$text = preg_replace('|([вс])([0-9])|uis', '$1 $2', $text);
		
		//нормализует пробел после скобки
		$text = preg_replace('|\)([-а-яА-ЯёЁ0-9])|uis', ') $1', $text);	
		
		
		//удаляет техническую информацию
		$text = preg_replace('|Стиль\.\.\. стиль|uis', '', $text);		
		
		//удаляет техническую информацию
		$text = preg_replace('|Кол - во просмотров|uis', '', $text);		
		
		//удаляет цифровой мусор в конце
		$text = preg_replace('|<br> [0-9]*\s*\.$|uis', '', $text);	
		
		//удаляет цифровой мусор
		$text = preg_replace('|<br>\s*[0-9]*\.\s*<br>|uis', '', $text);		

		//удаляет мусор типа  /06	
		$text = preg_replace('|\s/06\s|uis', '', $text);	
	
		//исправляет баг, где-то выше пролезает наверн	
		$text = preg_replace('|<br br>|ui', '<br>', $text);				
		
		//удаление многоточий из начала строки
		$text = preg_replace('|<br>\s*\.{1,}\s*([-а-яА-ЯёЁ0-9])|uis', '<br> $1', $text);	
		
		//удаление мусор типа <br> / - 
		$text = preg_replace('|<br>\s*/\s*-|uis', '<br> -', $text);	

		//удаление мусор типа - /<br>
		$text = preg_replace('|[.!?)а-яА-ЯёЁ0-9]\s*/<br>|uis', ' <br>', $text);		
		
		//удаление мусор типа -  Производитель:.., Франция
		$text = preg_replace('|([а-яА-ЯёЁ0-9]):[-., ]*([а-яА-ЯёЁ0-9])|uis', '$1: $2', $text);	
		
		//удаление мусор типа -  :15.  -  1.
		$text = preg_replace('|<br>\s*[:0-9.]*\s*$|uis', '', $text);		 
		
		return $text;
	}
	
	
	//add 18_11_12-06_58
	//различные цензор на частоту вхождение чего либо относительно количества символов в тексте
	protected function censors_count_includes($text){
	
		//проверка частоты левой скобки
		$count_matches = preg_match_all('|\(|', $text, $matches);
				
		if(empty($count_matches)){
			$count_matches = 1;
		}
		
		$text_lench = mb_strlen($text, 'utf-8');	
		
		if(($text_lench / $count_matches) > 110){
			
		}else{
			return false;
		}		
		
		//проверка частоты <br>
		$count_matches = preg_match_all('|<br>|', $text, $matches);
				
		if(empty($count_matches)){
			$count_matches = 1;
		}
		
		$text_lench = mb_strlen($text, 'utf-8');	
		
		if(($text_lench / $count_matches) > 45){
			
		}else{
			return false;
		}		
		
		//проверка частоты многоточий
		$count_matches = preg_match_all('|\.{3,}|', $text, $matches);
				
		if(empty($count_matches)){
			$count_matches = 1;
		}
		
		$text_lench = mb_strlen($text, 'utf-8');	
		
		if(($text_lench / $count_matches) > 45){
			
		}else{
			return false;
		}		
		
		//проверка частоты точек
		$count_matches = preg_match_all('|\. |', $text, $matches);
				
		if(empty($count_matches)){
			$count_matches = 1;
		}
		
		$text_lench = mb_strlen($text, 'utf-8');	
		
		if(($text_lench / $count_matches) < 600){
			
		}else{
			return false;
		}		
		
		//удаление дубля строки из первой позиции
		$text_lines_array = explode('<br>', $text);
		
		if( is_array($text_lines_array) ){
		
			if($text_lines_array[0] == $text_lines_array[1]){
				unset($text_lines_array[0]);
				
				$text = implode('<br>', $text_lines_array);
			}
		}		
		
		return $text;
	}
	
	
	//add 18_11_12-06_58
	//цензор максимальной длины слова
	protected function censor_lench_word($text){
		
		$words_array = explode(' ', $text);
		
		if( is_array($words_array) ){
			
			foreach($words_array as $key => $value){
				
				
				$word_lench = mb_strlen($value, 'utf-8');
				
				if($word_lench > 35){
					return false;
				}
			}
		}		
		
		return $text;
	}
	
	
	//удаляет двойные кавычки
	protected function finish_lench_censor($text, $min_lench = 600){		
		
		$text_len = mb_strlen($text, 'utf-8');		
		
		if($text_len >= $min_lench){
			return $text;
		}else{
			return false;
		}		
	}
	
	//удаляет двойные кавычки
	protected function quotes_clean($text){		
		
		$text = preg_replace('/"/u', ' ', $text);		
		
		return $text;
	}
	
	
	//сохраняет слеши между цифра, удаляя остальные
	protected function slash_dig_save_other_slash_clean($text){		
		
		$text = preg_replace('/([0-9]) *\/ *([0-9])/uis', '$1_|_$2', $text);
		$text = str_replace('/', '', $text);
		$text = str_replace('_|_', '/', $text);
		//$text = preg_replace('/([0-9])_|_([0-9])/uis', '$1/$2', $text);
		
		return $text;
	}
	
	
	//заменяет любое количество пробелов на 1
	protected function mass_space_clean($text){		
		
		$text = preg_replace('/ +/uis', ' ', $text);
		
		return $text;
	}
	
	
	//удаляет  (пробел)0(пробел) и -(пробел)-
	protected function clear_0_and_other($text){
		
		$text = str_replace(' 0 ', '', $text);
		$text = str_replace('- -', '', $text);		
		
		return $text;
	}
	
	//удаляет все точки больше 3х
	//удаляет точки если уже есть !?
	protected function mass_dot_clean($text){
		
		$text = preg_replace('/\.{4,}/uis', '...', $text);
		
		//заменяет скопления запятых на одну
		$text = preg_replace('/,[ ,]*/uis', ', ', $text);
		
		//удаляет точки если уже есть !?
		$text = preg_replace('/([!?])\.*?/uis', '$1', $text);
		
		
		$text = preg_replace('/[. ]{6,}/uis', '...', $text);
		
		return $text;
	}

	
	//удаляет мусор типа - -0 - 0-0 - 0-0 - 0-0 - 0-49,1 55,2 16 - 0 - 0 - 0 - 0 - - -
	protected function garbage_dig_and_more_clean($text){
		
		$text = preg_replace('/[-0-9 ,.]{11,}/uis', ' ', $text);
		
		return $text;
	}
	
	
	//удаляет фразу "Прочитать целиком 4 Сохранить в свой цитатникВ свой цитатник или сообщество" и подобное
	protected function spec_blog_phrase_clean($text){		
		
		$text = preg_replace('/Читать далее/uis', '', $text);
		$text = preg_replace('/читать рецепт/uis', '', $text);
		$text = preg_replace('/Прочитать целиком [0-9" =]*/uis', '', $text);
		$text = preg_replace('/Сохранить в свой цитатник"/uis', '', $text);
		$text = preg_replace('/В свой цитатник или сообщество! ?<br>/uis', '', $text);
		$text = preg_replace('/ЧИТАТЬ ДАЛЕЕ РЕЦЕПТЫ\./uis', '', $text);		
		$text = preg_replace('/все записи автора/uis', '', $text);		
		
		//09_10_12 добавлено
		$text = preg_replace('/Перейти в Дневник\./uis', '', $text);
		$text = preg_replace('/Перейти в Сообщество\./uis', '', $text);
		
		return $text;
	}
	
	
	
	protected function editing_title($title){
		$title = preg_replace('/^[^-а-яёЁА-Я 0-9,]*$/ius', '', $title);
		
		if(is_array($title)){
			$title = trim($title[0]);
		}else{
			$title = trim($title);
		}					
					
		//если первая буква не заглавная то скорей всего выбрана часть предложения после сокращения слова с использованием точки, ищем полное предложение
		$first_char = mb_substr($title, 0, 1, 'utf-8');
		$first_char_big = mb_strtoupper($first_char, 'utf-8');						
					
		if($first_char_big != $first_char){
			$title_plus_dot_space = '. ' . $title;
			$pattern_1 = '/(^|\.|!|\?|;|<br>)[-а-яёЁА-Я 0-9,]*?' . $title_plus_dot_space . '/ius'; 
			preg_match_all($pattern_1, $text, $matches);
						
			if(!empty($matches[0])){
			
				if(is_array($matches[0])){
					$title = $matches[0][0];
				}else{
					$title = $matches[0];
				}
							
				$title = preg_replace('/^[^а-яёЁА-Я 0-9]*/ius', '', $title);
				if(is_array($title)){
					$title = trim($title[0]);
				}			
			}
		}
		
		$title = preg_replace('/^[^а-яёЁА-Я0-9]*/ius', '', $title);
		$title = trim($title);
		
		//очищает начало заголовка от всего кроме букв
		$title = preg_replace('/^[а-яёЁА-Я0-9]*\./ius', '', $title);
		$title = str_replace('<br>', '', $title);	
		
		$title = trim($title);
		
		//удаляем точку с запятой в конце
		$title = preg_replace('/;$/ius', '', $title);
		
		//добавляет точку вконце если её нет и нет других символов окончания предложения		
		$title = preg_replace('/([^.!?]$)/ius', '$1.', $title);
		
		$title_length = mb_strlen($title, 'UTF-8');
		if($title_length > 150){
			$title = mb_substr($title, 0, 150, 'UTF-8');
			$title = trim($title);
			$title = preg_replace('/[.!?;]*$/ius', '', $title);
			$title = $title . '...';
		}
		
		//$title = $title . '.';
		
		$title = $this->first_char_bigger($title);
		
		return $title;
	}
	
	//заменяет анлийские буквы на русские визуальные аналоги в русских словах
	protected function change_eng_char_on_rus_analog($text){
		//замена буквы с
		$text = preg_replace('/([ а-яёЁА-Я])c([ а-яёЁА-Я])/u', '$1с$2', $text);
		//замена буквы С
		$text = preg_replace('/([ а-яёЁА-Я])C([ а-яёЁА-Я])/u', '$1С$2', $text);			
		
		//замена буквы а
		$text = preg_replace('/([ а-яёЁА-Я])a([ а-яёЁА-Я])/u', '$1а$2', $text);
		//замена буквы A
		$text = preg_replace('/([ а-яёЁА-Я])A([ а-яёЁА-Я])/u', '$1А$2', $text);
		
		//замена буквы B
		$text = preg_replace('/([ а-яёЁА-Я])B([ а-яёЁА-Я])/u', '$1В$2', $text);
		
		//замена буквы e
		$text = preg_replace('/([ а-яёЁА-Я])e([ а-яёЁА-Я])/u', '$1е$2', $text);
		//замена буквы E
		$text = preg_replace('/([ а-яёЁА-Я])E([ а-яёЁА-Я])/u', '$1Е$2', $text);
		
		//замена буквы H
		$text = preg_replace('/([ а-яёЁА-Я])H([ а-яёЁА-Я])/u', '$1Н$2', $text);
		
		//замена буквы K
		$text = preg_replace('/([ а-яёЁА-Я])K([ а-яёЁА-Я])/u', '$1К$2', $text);		
		//замена буквы k
		$text = preg_replace('/([ а-яёЁА-Я])k([ а-яёЁА-Я])/u', '$1к$2', $text);
		
		//замена буквы M
		$text = preg_replace('/([ а-яёЁА-Я])M([ а-яёЁА-Я])/u', '$1М$2', $text);
		//замена буквы m
		$text = preg_replace('/([ а-яёЁА-Я])m([ а-яёЁА-Я])/u', '$1м$2', $text);
		
		//замена буквы n
		$text = preg_replace('/([ а-яёЁА-Я])n([ а-яёЁА-Я])/u', '$1п$2', $text);
		
		//замена буквы o
		$text = preg_replace('/([ а-яёЁА-Я])o([ а-яёЁА-Я])/u', '$1о$2', $text);
		//замена буквы O
		$text = preg_replace('/([ а-яёЁА-Я])O([ а-яёЁА-Я])/u', '$1О$2', $text);
		
		//замена буквы р
		$text = preg_replace('/([ а-яёЁА-Я])p([ а-яёЁА-Я])/u', '$1р$2', $text);
		//замена буквы P
		$text = preg_replace('/([ а-яёЁА-Я])P([ а-яёЁА-Я])/u', '$1Р$2', $text);
		
		//замена буквы r
		$text = preg_replace('/([ а-яёЁА-Я])r([ а-яёЁА-Я])/u', '$1г$2', $text);
		
		//замена буквы T
		$text = preg_replace('/([ а-яёЁА-Я])T([ а-яёЁА-Я])/u', '$1Т$2', $text);
		
		//замена буквы u
		$text = preg_replace('/([ а-яёЁА-Я])u([ а-яёЁА-Я])/u', '$1и$2', $text);
		
		//замена буквы x
		$text = preg_replace('/([ а-яёЁА-Я])x([ а-яёЁА-Я])/u', '$1х$2', $text);
		//замена буквы X
		$text = preg_replace('/([ а-яёЁА-Я])X([ а-яёЁА-Я])/u', '$1Х$2', $text);
		
		//замена буквы y
		$text = preg_replace('/([ а-яёЁА-Я])y([ а-яёЁА-Я])/u', '$1у$2', $text);
		//замена буквы Y
		$text = preg_replace('/([ а-яёЁА-Я])Y([ а-яёЁА-Я])/u', '$1У$2', $text);
		
		return $text;
	}
	

	//поиск названия в тексте
	protected function title_search($text){
		$key_phrase = $this->key_phrase;
		//$key_phrase = mb_convert_encoding($key_phrase, 'UTF8', 'CP1251');
		//$key_phrase = str_replace(' ' , '|', $key_phrase);
		$key_phrase_array = explode(" ", $key_phrase);
		
		/*
		echo '<br>!!!!!!';
		print_r($key_phrase_array);
		echo '<br>!!!!!!' ;
		*/
		
		$flag = 0;
		foreach($key_phrase_array as $key => $value){
			
			$value = trim($value);
			
			$key_word_fragment = $this->cleaner_word_ending($value);
	
			$strlen = mb_strlen($key_word_fragment, 'utf-8');			
				
			if($strlen > 3){			
				
				$pattern = '/(^|[-а-яёЁА-Я0-9]{3,}\.|!|\?|;|<br>)[-а-яёЁА-Я 0-9,]*?' . $key_word_fragment . '[-а-яёЁА-Я 0-9]*?([-а-яёЁА-Я0-9]{3,}\.|!|\?|;|<br>|$)/ius'; 
				preg_match_all($pattern, $text, $matches);
				
				
				if(!empty($matches[0])){
									
					if(is_array($matches[0])){
						$title = $matches[0][0];	
					}else{
						$title = $matches[0];
					}
					
					$title = $this->editing_title($title);

					//конец поиска полного предложения
					
					$title = trim($title);
					//$title = preg_replace('/^\s*$/us', '', $title);
					
					if(!empty($title)){
						/*
						echo '|';
						print_r($title);
						echo '|';
						echo '----1<br>';
						*/
						return $title;
					}		
				}

			}

		}
		
		
		//если не нашли предложений с ключевыми словами то в качестве заголовка используем первое предложение
		$pattern = '/(^|[-а-яёЁА-Я0-9]{3,}\.|!|\?|;|<br>)[-а-яёЁА-Я 0-9,]*?([-а-яёЁА-Я0-9]{3,}\.|!|\?|;|<br>|$)/ius'; 
		preg_match($pattern, $text, $matches);
		$title = $matches[0];
						
		$title = $this->editing_title($title);
			
		/*
		echo '|';
		print_r($title);
		echo '|';
		echo '----2<br>';
		*/
		if(!empty($title)){
			return $title;
		}
		
		
		/*
		$pattern = '/(^|[а-яёЁА-Я0-9-]{3,}\.|!|\?|;|<br>)[-а-яёЁА-Я 0-9,]*?([-а-яёЁА-Я0-9]{3,}\.|!|\?|;|<br>|$)/ius'; 
		preg_match($pattern, $text, $matches);
		$title = $matches[0];
		$title = str_replace('<br>', '', $title);
		*/			

		//return 'должно быть false';
		
	}
	
	
	//удаляет все <br> больше 1го рядом
	protected function clear_br_group($text){
	
		$text = preg_replace('/( *<br>){2,}/isu', ' <br>', $text);
		
		return $text;
	}
	
	
	//удаляет тексты где есть 3 и более цифр с заключающей точкой в большом количестве (обычно это перечисления литературы)
	protected function digits_3_plus_dot_clean($text){
	
		$count_matches = preg_match_all('/[0-9]{2,}\./', $text, $matches);
				
		if(empty($count_matches)){
			$count_matches = 1;
		}
		
		$text_lench = mb_strlen($text, 'utf-8');	
		
		if(($text_lench / $count_matches) > 100){
			return $text;
		}
		
		return false;
	}
	
	
	//удаляем четырёхзначные цифры
	protected function digits_4_char_clean($text){

		//удаляет точки, пробелы и тире с конца текста
		$text = preg_replace('/( |>)[0-9]{4,}(<| )/', '$1 $2', $text);
				
		return $text;
	}
	
	
	//очищаем конец текста, ставим одну точку
	protected function text_end_clear($text){

		//удаляет точки, пробелы и тире с конца текста
		$text = preg_replace('/([^а-яА-ЯёЁ0-9?!]*)$/ius', '', $text);
		
		//удаляем <br>`ы и точки вконце текста
		$text = preg_replace('/(\.* *<br> *\.*)*$/ius', '', $text);
		
		//добавляем 1 точку вконце
		$text = trim($text);
		if(!empty($text)){
			//$text = $text . '.';
			$text = preg_replace('/([^.!?]$)/ius', '$1.', $text);
		}	
		
		return $text;
	}
	
	
	//делает первую буквую заглавной
	protected function first_char_bigger($text){

		$text = trim($text);
		$first_char_big = mb_strtoupper(mb_substr($text, 0, 1, 'utf-8'), 'utf-8');
		$text_without_first_char = mb_substr($text, 1, mb_strlen($text, 'utf-8'), 'utf-8');
		 
		$text = $first_char_big . $text_without_first_char;
		
		return $text;
	}
	
	
	//фильтрация неверной кодировки
	protected function bad_encode_cleaner($text){
		if(strpos($text, 'РРР') !== false){
			return false;
		}
		
		return $text;
	}
	
	
	protected function black_list_censor($text){
		//проверка по чёрному списку
		/*
		### debug
		echo '<pre>';
		print_r($this->back_list_words);
		echo '</pre>';
		*/
		
		$count_loop_2 = 0;
		$flag_black_list = 0;
		foreach($this->back_list_words as $key => $value){
		
			$value = trim($value);
			$value = $value . ' ';
			
			if(mb_stripos($text, $value, 0, 'utf-8') !== false){
				$flag_black_list = 1;
				
				//echo '$flag_black_list = 1';
			}
		}				
		
		if($flag_black_list == 1){		
			return false;			
		}			
		
		return $text;
		
		//подготавливаем ключевую фразу для проверки по чёрному списку
		//$phrase = trim($phrase);
		//$phrase = mb_convert_encoding($phrase, 'UTF8', 'CP1251');
	}
	
	
	//add 16_10_12
	protected function individual_black_list($text){
	
			$b_list_words = $this->select_b_list_words();
			
			/* 
			###отладка
			echo '<pre>$b_list_words';
			print_r($b_list_words);
			echo '</pre>';
			*/
			
			$count_loop_2 = 0;
			$flag_black_list = 0;
			foreach($b_list_words as $key => $value){
			
				$value_temp = trim($value['words']);
				
				if( !empty($value['words']) ){
				
					if(mb_stripos($text, ' ', 0, 'utf-8') !== false){
						$value_temp = $value_temp . ' ';
					}else{
						$value_temp = $this->cleaner_word_ending($value_temp, $encode = 'utf-8');
					}				
					
					### debug
					//echo '<br><br> individual_black_list $value_temp-' . $value_temp . ' -------- $value["words"]-' . $value['words'] . '<br><br>';
					
					if(mb_stripos($text, $value_temp, 0, 'utf-8') !== false){					
						
						return false;
					}
				}
			}			
		
			return $text;
	}
	
	
	//цензор мусора
	protected function garbage_censor($text){
	
		$count_matches = preg_match_all('/[0-9]{4,}/', $text, $matches);
				
		if(empty($count_matches)){
			$count_matches = 1;
		}
		
		$text_lench = mb_strlen($text, 'utf-8');	
		
		if(($text_lench / $count_matches) > 100){
					
			return $text; //. ' <br><br><br> длина: ' . $text_lench . ' <br><br><br> количество мусорных вхождений: ' . $count_matches . ' <br><br><br> их отношение /: ' . $text_lench / $count_matches
		}
		
		return false;
	}
	
	
	protected function garbage_clean($text){	
		
		$text = preg_replace('/[0-9]{5,}/u', '', $text);
			
		//удаляем прилипшие цифры к словам
		$text = preg_replace('/([-0-9]{4,})([а-яА-ЯёЁ])/ius', '$2', $text);
			
			
		//удаляем из начала текста всё до первой буквы, цифры
		//$text = preg_replace('/^([^а-яА-ЯёЁ]|[^0-9]{,3})*/ius', '', $text); // может вызывать фатальную ошибку апача
		//$text = preg_replace('/^([^0-9]{,3}|[^а-яА-ЯёЁ])*/ius', '', $text); // может вызывать фатальную ошибку апача
		$text = preg_replace('/^[^а-яА-ЯёЁ0-9]*/ius', '', $text); // безопасный вариант (против ошибки апача)			
			
		//заменяем более одного тире на одно
		$text = preg_replace('/-{2,}/', '-', $text);	
			
		//нормализуем пробелы в которых присутсвуют включения <br>
		$text = preg_replace('/([а-яА-ЯёЁ0-9])\s*(<br>|<br><br>)?\s*(<br>|<br><br>)?\s*(,|\.)/ui', '$1$4', $text);			
		
		//удаляет группы <br>
		$text = preg_replace('/<br><br> *-* *<br><br>/s', '<br><br>', $text);
			
		//удаление всех <br> больше 2х
		$text = preg_replace("/(<br> *){2,}/", "<br><br>", $text); //в другом месте - приводило к фатальной ошибке апача		
		$text = preg_replace('|[-?:.=+ &"]{5,}|usi', " ", $text);		
		$text = preg_replace('/[-.]{4,}/usi', " ", $text);			
		$text = preg_replace('|[0-9/]{5,}|usi', " ", $text);		
		$text = preg_replace('/& *;/sui', " ", $text);	
		
		//удаляет слеши рялом с которыми нет цифр
		$text = preg_replace('|[^0-9] */ *[^0-9]|sui', " ", $text);
		
		//удаляет большие цифры которые не являются годами нашей эры
		$text = preg_replace('|[3456789][0-9]{3,}|sui', " ", $text);
				
		$text = preg_replace('|[-!?;:.=+ &"0-9%]{8,}|usi', " ", $text);		
		
		//удаляет мусор типа ?4%%%8%%8
		//$text = preg_replace('|\?([0-9]?%){3,}|sui', " ", $text);
		
		return $text;
	}
	
	
	//не используется
	protected function word_on_up_reg_to_low_reg($text){
	
		//слова в верхнем регистре изменяем на нижний
		$text_array = explode(' ', $text);
		foreach($text_array as $key => $value){
			
			$value_upper = mb_strtoupper($value, 'utf-8');
			if($value_upper == $value){
				 $text_array_result[] = mb_strtolower($value, 'utf-8');
			}else{
				$text_array_result[] = $value;
			}
		}
		
		$text = implode(' ', $text_array_result);
		
		return $text;
	}
	
	//нормализатор текста
	protected function text_normalizer($text){
	
		//нормализуем пробелы у знаков препинания
		$text = preg_replace('/([а-яА-ЯёЁ0-9)]) +([,.:!?])/uis', '$1$2', $text);
		$text = preg_replace('/([,\.:!?])([а-яА-ЯёЁ(])/uis', '$1 $2', $text);
		
		//удаляем тире вначале и конце текта
		$text = preg_replace('/(^[-]|[-]$)/ui', '', $text);
		//нормализуем тире
		$text = preg_replace('/([а-яА-ЯёЁ])-/ui', '$1 -', $text);
		$text = preg_replace('/-([а-яА-ЯёЁ])/ui', '- $1', $text);
		$text = preg_replace('/([0-9]) *([^а-яА-ЯеЁ()!?"]) *([0-9])/uis', '$1$2$3', $text);
		$text = trim($text);
		
		//проставляем пробелы около тире которое между цифрой и словом
		$text = preg_replace('/([0-9]) *- *([а-яёЁ])/usi', '$1 - $2', $text);
		//прижимает тире между цифрой и буквенным окончанием
		$text = preg_replace('/([0-9]) *- *([а-яё]{1,2} )/us', '$1-$2', $text);
		
		//прижимает кавычки к словам
		$text = preg_replace('/" *([а-яА-ЯёЁ0-9]) *"/uisU', '"$1"', $text);
		
		$text = preg_replace('/ +" +/uis', ' ', $text);
		
		//проставление точек в незакрытых текстах
		//$text = preg_replace('/([а-яА-ЯёЁ0-9]$)/ui', '$1.', $text);
		
		//удаление лишних пробелов
		//$text = preg_replace('/\s+/', ' ',  $text); 
		
		//удаление лишних пробелов в дробных числах
		$text = preg_replace('/([0-9])(,|\.) ([0-9])/uis', '$1$2$3 ',  $text); 
		
		//нормализация сокращений слов
		$text = preg_replace('/([0-9])( *)(г|гр|см|шт|л|ст|кг|км|мм|см|м|мл)(\.?)( |<)/usi', '$1 $3.$5',  $text); 
		
		//отделяет цифры от слов
		$text = preg_replace('/([0-9])([а-яА-ЯёЁ])/usi', '$1 $2',  $text); 
		
		//удаление лишних пробелов
		//$text = preg_replace('/\s+/', ' ',  $text); 
		
		/*
		//удаляем тексты где есть более 2х пробелов подрят
		if(strpos($text, '  ') !== false){
			return false;
		}
		//удаляем тексты где есть более 2х зяпятых подрят
		if(strpos($text, ',,') !== false){
			return false;
		}		
		*/
		
		return $text;
	}
	
	
	//обрезалка окончаний
	protected function cleaner_word_ending($word, $encode = 'utf-8'){	
	
			$encode_lower = mb_strtolower($encode, 'utf-8');
	
			if($encode_lower == 'cp1251'){
				$word = mb_convert_encoding($word, 'utf-8', 'cp1251');						
			}
					
			//обрезка окончаний
			$word = preg_replace('/(их|ов|а)$/iu', '',  $word); 
					
			if($encode_lower == 'cp1251'){
				$word = mb_convert_encoding($word, $encode, 'utf-8');
			}
			
	
			while(true){
				$strlen = mb_strlen($word, $encode);
				
				$end_char = mb_substr($word, $strlen-1, 1, $encode);
				//echo '<br>!!!!<br>' . $end_char . '<br>!!!!<br>';
				
				$glasnie = 'ауоыиэяюёей';	
				
				
				if($encode_lower == 'cp1251'){
					$glasnie = mb_convert_encoding($glasnie , $encode, 'utf-8');
				}								
				
				if(mb_stripos($glasnie, $end_char, 0, $encode)){
				
					$word = mb_substr($word, 0, $strlen-1, $encode);
					
					//echo '<br>!!!! вырезанное: <br>' . $word . '<br>!!!!<br>';
					//$this->cleaner_word_ending($word);
				}else{
										
					return $word;
				}
			}
	}
	
	
	//проверяет наличие в тексте слов из ключевой фразы
	//если второму параметру fragmentary_equal_key_phrase передать 0 будет искать наличие всех значимых (больше 3х символов) ключевых слов (без окончаний) в тексте
	protected function key_phrase_censor($text, $fragmentary_equal_key_phrase = 1){
		$key_phrase = $this->key_phrase;
		//$key_phrase = mb_convert_encoding($key_phrase, 'UTF8', 'CP1251');
		//$key_phrase = str_replace(' ' , '|', $key_phrase);
		$key_phrase_array = explode(" ", $key_phrase);
		
		/*
		echo '<br>!!!!!!';
		print_r($key_phrase_array);
		echo '<br>!!!!!!' ;
		*/
		
		$flag = 0;
		foreach($key_phrase_array as $key => $value){
			
			$value = trim($value);
			/*
			$strlen = mb_strlen($value, 'utf-8');
			
			$end_char = mb_substr($value, $strlen-1, 1, 'utf-8');
			
			$glasnie = 'ауоыиэяюёе';			
			if(mb_strpos($glasnie, $end_char, 0, 'utf-8'){
				
			}
			*/
			
			$key_word_fragment = $this->cleaner_word_ending($value);
	
			$strlen = mb_strlen($key_word_fragment, 'utf-8');
			
			//echo '<br><br><br>!!!! выдала cleaner_word_ending: ' . $value;
			//echo '<br><br><br>!!!!' . $this->key_phrase . '<br><br><br>!!!!';
			$position_word = false;
			$position_word = mb_stripos($text, $key_word_fragment, 0, 'utf-8');			
				
			if($strlen > 3){
			
				//echo '<br><br>!!!!! слова: ' . $key_word_fragment . ' позиция: ' . $position_word . ' !!!!!!<br><br>'; 
				
				if($fragmentary_equal_key_phrase == 1){
					if($position_word == true){
						$flag = 1;
						break;						
					}
				}else{
				
					$flag = 1;
					
					if($position_word === false){
						$flag = 0;
						break;						
					}
				}

			}

		}
		
		if($flag == 1){
			return $text; // . '<br>!!!!!!' . $position_word . '____' . $word . '___' . $this->key_phrase . '___' . serialize (print_r($key_phrase_array))
		}else{
			return false;
		}	
		
	}
	
	
	public function rusarticles_urls($html){
		
		preg_match_all('/<div class="article_row">.*?<\/div>/uis', $html, $matches);
		/*#отладка
		echo '<pre>';
		print_r($matches);
		echo '</pre>';
		*/
		
		/*
		<div class="article_row">
        <div class="title">
                    <h3><a title="Ежевика И Малина" href="http://www.rusarticles.com/sadovodstvo-statya/ezhevika-i-malina-3384849.html">Ежевика И Малина</a></h3>
                  </div>
                <div class="articles_icon"></div>
                <p>Плоды <b>малины</b> содержат, %: сухих веществ - 11­17, сахаров - 3-8, титруемых  ...  - 0,13. Сахара представлены равным количеством глюкозы и фруктозы; <b>из</b> кислот доминирует яблочная и лимонная, имеется винная, салициловая ... </p>
        <div class="article_details">
        <span class="nameby">От:</span>
        <span class="name"><a href="http://www.rusarticles.com/authors/510614" title="Статьи от pavelin1">pavelin1</a></span><span class="separator">l</span>
                  <a href="http://www.rusarticles.com/obustroistvo-byta-statya/" title="Обустройство быта Статей">Обустройство быта</a><span class="separator">&gt;</span>
                <a href="http://www.rusarticles.com/sadovodstvo-statya/" title="Садоводство Статей">Садоводство</a><span class="separator">l</span>
        <span>01/10/2010</span>
                        </div>
		</div>
		*/
		
		foreach($matches[0] as $key => $value){
			
			preg_match('/href=".*?"/uis', $value, $match);
			
			$url = str_replace('href="', '', $match[0]);
			$url = str_replace('"', '', $url);
			
			$url_articles[] = $url;
		}
		
		return $url_articles;
		
	}
	
	//сбор всех текстов и их названий по ссылкам
	public function mass_text_and_title_article($urls_articles, $key_phrase){
	
		$key_phrase = trim($key_phrase);
	
		$this->set_key_phrase($key_phrase);
		
		/*
		###отладка
		echo '<pre>';
		print_r($urls_articles);
		echo '</pre>';
		*/
	
		$html_articles_page_array = $this->rusarticles_content_grab($urls_articles);
	
		/*
		###отладка
		echo '<pre>';
		print_r($html_articles_page_array);
		echo '</pre>';		
		*/
		
		$articles_array['key_phrase'] = $key_phrase;
		
		foreach($html_articles_page_array as $key => $value){
		
			$temp_article = $this->text_and_title_article($value['html'], $value['url'], $key_phrase);
			
			if($temp_article !== false){
				$articles_array['texts'][] = $temp_article;				
			}
			
		}		
		
		return $articles_array;
	}
	
	
	###$del_phrases_with_dig - удаление фраз с цифрами
	###$full_inclusion - удаление фраз без присутсвия всех слов изначальной фразы
	###$limit_words - цензон на количество слов в выборке, если больше то такие фразы удаляются
	public function wordstat_yandex_parse($html, $key_phrase, $limit_words = 5, $del_phrases_with_dig = true, $full_inclusion = true, $limit_phrases = 100, $strict_key_phrase_del = 1, $strict_word_ending = true){
	
		
		/*
		$encoding = 'utf-8';
		
		$needle = 'Что искали со&nbsp;словами';		
		$html = mb_strstr($html, $needle, $before_needle = false, $encoding);
		
		$needle = 'Страницы';		
		$html = mb_strstr($html, $needle, $before_needle = true, $encoding);
		*/
		
		preg_match_all('/Что искали со&nbsp;слов(ами|ом).*?Страницы/uis', $html, $matches);
		
		if(is_array($matches[0])){
		
			foreach($matches[0] as $key => $value){
				$html_edited .= $value;
			}
		}else{
			$html_edited = $matches[0];
		}
		
		/*
		echo '<pre>';
		print_r($html_edited);
		echo '</pre>';
		*/
	
		//preg_match_all('/<td> *?<a href="\?.*?<\/td>/uis', $html, $matches);
		preg_match_all('/<td>\s+<a href="\?.*?<\/td>/uis', $html_edited, $matches_1);
		
		foreach($matches_1[0] as $key_1 => $value_1){
			$phrase = trim(strip_tags($value_1));
			$phrase = html_entity_decode($phrase, $flags = ENT_COMPAT | ENT_HTML401, $encoding = 'UTF-8');
			
			$search_phrases_count = count($search_phrases);
			
			if($search_phrases_count >= $limit_phrases){
				break;
			}
			
			$temp_phrase = str_replace('+', '', $phrase);
			
			//если найденная фраза в точности соответсвует первоначальной фразе - удаляем её (можно изменить в параметрах)
			if($strict_key_phrase_del == 1){
				if($temp_phrase != $key_phrase){
					$search_phrases[] = $temp_phrase;
				}
			}else{
				$search_phrases[] = $temp_phrase;
			}

			
		}
		/*
		echo '<pre>';
		print_r($search_phrases);
		echo '</pre>';
		*/
				
				
		if($full_inclusion == true){
			$key_phrase_array = explode(' ', $key_phrase);
			$count_key_phrase_array = count($key_phrase_array);		
		}		
		
		//проверки
		foreach ($search_phrases as $key => $value){
				
				
			//проверка присутсвия всех слов первичной фразе в анализируемой фразе
			if($full_inclusion == true){
				
				if($strict_word_ending == false){
					
					$encoding = 'utf-8';
					$count_loop = 0;
					
					
					if(!is_array($key_phrase_array)){
					
						$count_key_phrase_array = 1;
						if(mb_stripos(' ' . $value, ' ' . $this->cleaner_word_ending($key_phrase_array, $encoding), $offset = 0, $encoding) !== false){
							$count_loop++;
						}
						
					}else{
					
						foreach($key_phrase_array as $key_1 => $value_1){
							//. ' ' - используется для того чтобы можно было искать слова целиком и не путать их с фрагментами слов
							if(mb_stripos(' ' . $value, ' ' . $this->cleaner_word_ending($value_1, $encoding), $offset = 0, $encoding) !== false){
								$count_loop++;
							}
							 
							//echo 'где - ' . $value . '________что - ' . $value_1 . '________сколько - ' . $count_loop . '________сколько нужно - ' . $count_key_phrase_array . '<br>';
						}						
					}					
					
					if($count_loop < $count_key_phrase_array){
						unset($search_phrases[$key]);
						
						//echo 'удаление фразы по цензору - отсутсвие всех слов<br>';
					}					
					
				}else{				
				
					$count_loop = 0;
					
					if(!is_array($key_phrase_array)){
					
						$count_key_phrase_array = 1;
						if(mb_stripos(' ' . $value . ' ', ' ' . $key_phrase_array . ' ', $offset = 0, $encoding = 'utf-8') !== false){
							$count_loop++;
						}
						
					}else{
					
						foreach($key_phrase_array as $key_1 => $value_1){
							//. ' ' - используется для того чтобы можно было искать слова целиком и не путать их с фрагментами слов
							if(mb_stripos(' ' . $value . ' ', ' ' . $value_1 . ' ', $offset = 0, $encoding = 'utf-8') !== false){
								$count_loop++;
							}
							 
							//echo 'где - ' . $value . '________что - ' . $value_1 . '________сколько - ' . $count_loop . '________сколько нужно - ' . $count_key_phrase_array . '<br>';
						}
						
					}							
					
					if($count_loop < $count_key_phrase_array){
						unset($search_phrases[$key]);
						
						//echo 'удаление фразы по цензору - отсутсвие всех слов<br>';
					}					
				}
			}				
				
			if(empty($value)){
				unset($search_phrases[$key]);
			}
				
			//удаляет фразы с цифрами
			if($del_phrases_with_dig){
				if(preg_match("/[0-9]/", $value)){
					unset($search_phrases[$key]);
				}
			}
				
			//удаляет фразы с большим количеством слов чем требуемое
			$temp = explode(' ', $value);
			if(count($temp) > $limit_words){
				unset($search_phrases[$key]);
			}
					
		}		
		
		//удаление повторящихся слов (в части не являющейся главной ключевой фразой) в разных ключевых фразах
		$search_phrases = $this->clean_repeat_fragment_phrases($search_phrases, $key_phrase, $encode = 'utf-8');			
		
		$search_phrases = $this->adstat_black_list_censor($search_phrases, 'utf-8');		
		//add 16_10_12-18_20
		$search_phrases = $this->individual_category_black_list_censor($search_phrases, 'utf-8');		
		
		$search_phrases = array_merge($search_phrases);
		
		/*
		echo '<pre>';
		print_r($search_phrases);
		echo '</pre>';
		*/
		
		return $search_phrases;
	}
	
	
	//add 18_11_12-23_37
	public function censors_and_other_for_article($article_text_temp){
	
		//add 18_11_12-23_37
		//заменяет перевод строки на <br>
		//сделано из-за того что в некоторых статьях используются <p> которые удаляются при обработке.
		//$article_text_temp = nl2br($article_text_temp, $is_xhtml = false);
		//заменил строчку выше на preg_replace('|</p>|usi', "<br>", $article_text_temp); в теле родительской функции - text_and_title_article
		
		//удаляет мусор
		$article_text_temp = preg_replace('|/m|usi', "", $article_text_temp);
		$article_text_temp = preg_replace('|m/ng/|usi', "", $article_text_temp);
		$article_text_temp = preg_replace('|\[.*?\]|usi', "", $article_text_temp);
		$article_text_temp = preg_replace('|\$IMAGE[0-9]*\$|usi', "", $article_text_temp);
		$article_text_temp = preg_replace('|\/\. inc=[a-z]*|usi', "", $article_text_temp);		
		
		//нормализует пробел после знаков препинания
		$article_text_temp = preg_replace('|[.,!?:;]([а-яА-ЯёЁ0-9a-zA-Z])|usi', ". $1", $article_text_temp);		
		
		return $article_text_temp;
	}
	
	public function text_and_title_article($html_article, $url){			
		
		preg_match('/<index>.*?<\/index>/uis', $html_article, $match);

		//add 18_11_12-23_37
		//замена тега завершения абзаца на тег перевода строки
		$article_text_temp = preg_replace('|<\/p>|usi', "<br>", $match[0]);
		
		//add 18_11_12-23_37
		$article_text_temp = strip_tags($article_text_temp , '<br>');
		$article_text_temp = $this->strip_scripts_and_tag_a($article_text_temp);
		
		$article_text_temp = $this->key_phrase_censor($article_text_temp, 0);
		
		//add 16_10_12-03_08
		//общий и индивидуальный чёрный список
		$article_text_temp = $this->individual_black_list($article_text_temp);
		
		$article_text_temp = $this->domain_clean($article_text_temp);
		
		//если в статье нет ключевых слов то возвращаем false
		if($article_text_temp === false){
			return false;
		}
		
		//add 18_11_12-23_37
		$article_text_temp = $this->censors_and_other_for_article($article_text_temp);
		
		$article['text'] = $article_text_temp;		
		
		preg_match('/<h1 itemprop="name">.*?<\/h1>/uis', $html_article, $match);
		$article_title_temp = strip_tags($match[0]);
		$article_title_temp = $this->strip_scripts_and_tag_a($article_title_temp);
		
		$article['title'] = $article_title_temp;
		
		$article['url'] = $url;

		
		$this->set_key_phrase($key_phrase);
		$text = $this->key_phrase_censor($text, 0);
		
		return $article;
	}
	
	
	protected function clipped_text_censor($text){
		if(strpos($text, '..') !== false){
			return false;
		}
		
		return $text;
	}
		
}
?>