<?
// no direct access
defined( '_EXEC' ) or die( 'Restricted access' );

//require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/abstract_auth.class.php');

class Abstract_auth{

	protected $postdata = array();
	protected $cookie_file_name = '';
	protected $user_agent = '';
	protected $url_auth = '';
	protected $bad_login_pass = '';	
	protected $path_cookie_file_name = '';	
	protected $browser_referer = '';
	protected $query = '';
	
	
	function Abstract_auth(){
		
		//$this->browser_referer = 'http://www.google.ru/';
		$d_s = '/';
		$document_root = str_replace('/', DIRECTORY_SEPARATOR, $_SERVER['DOCUMENT_ROOT']);
		//$this->path_cookie_file_name = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . $this->cookie_file_name . '.txt';
		$this->path_cookie_file_name = $document_root . DIRECTORY_SEPARATOR . 'cookie' . DIRECTORY_SEPARATOR . $this->cookie_file_name . '.txt';
		
		/* ###отладка
		echo $this->path_cookie_file_name . '!!!!<br>';		
		echo $this->postdata . '!!!!<br>';
		echo $this->cookie_file_name . '!!!!<br>';
		echo $this->user_agent . '!!!!<br>';
		echo $this->url_auth . '!!!!<br>';
		echo $this->bad_login_pass . '!!!!<br>';
		echo $this->path_cookie_file_name . '!!!!<br>';	
		echo $this->browser_referer . '!!!!<br>';
		echo $this->query . '!!!!<br>';
		*/
	}
	
	
	public function geturl($url, $postdata=0, $header=0){
		$poststr="";
		if ($postdata){
			while (list($name,$value)=each($postdata)){
				if (strlen($poststr)>0){
					$poststr.="&";
				}
				$poststr.=$name."=".urlencode($value);
			}
		}
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);

		if ($header){
			curl_setopt($ch, CURLOPT_HEADER, 1);
			curl_setopt($ch, CURLOPT_NOBODY, 0);
		}else{
			curl_setopt($ch, CURLOPT_HEADER, 0);
		}

		if ($postdata){
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $poststr);
		}

		//$refurl = $url;
		$refurl = parse_url($url);
		$refurl = $refurl[scheme]."://".$refurl[host];

		curl_setopt($ch, CURLOPT_REFERER, $refurl);
		
		curl_setopt($ch, CURLOPT_COOKIEJAR, $this->path_cookie_file_name);
		//curl_setopt($ch, CURLOPT_COOKIEFILE, $this->path_cookie_file_name);
		
		###########################!!!!!!!!!!!!!!!!!!!!!отключил из-за настроек сервера (open_dir), не может с ней работать выдаёт warning
		//curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

		curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent);
		curl_setopt($ch, CURLOPT_COOKIESESSION, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			
		$res = curl_exec($ch);
		$error = curl_errno($ch);
		curl_close($ch);
		

		
		$ret->res = $res;
		$ret->error = $error;
		
		if(!empty($ret->error)){
			return $ret->error;
		}else{
			/*
			if (!strstr($ret->res, $bad_login_pass)) {
				return "Ошибка авторизации, проверьте логин и пароль!";				
			}else{
			*/
				return $ret->res;
				//echo $ret->res ;
			//}
		}		
	}


	public function browser() {
		sleep(2);		
		
		$this->curl_connect();		
		
		return $html; //Возвращаем ответ
	}
	
	protected function curl_connect(){
		//полсекунды задержка
		time_nanosleep(0, 500000000);
		$ch = curl_init();
		
		
		###новое 
		//curl_setopt($ch, CURLOPT_COOKIE, "fuid01=4b6f4b7105a7928a.C8X8OvpjCKF8gVmP5FhSUxrVFwhbpsFjSeO-J2VErfH1G0txRhJOE40xlGtzAd1o9UfSbTYhqrYO3kZWEnZbib9iGYWTDubuSarhD2pWCl_a8aBRu_Hy5ZV2T7giSZeO");
		###->новое 
		
		curl_setopt($ch, CURLOPT_REFERER, $this->browser_referer);
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent);
		//curl_setopt($ch, CURLOPT_COOKIEJAR, $this->path_cookie_file_name);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $this->path_cookie_file_name);
		curl_setopt($ch, CURLOPT_URL, $this->query);
		$html = curl_exec($ch);
		curl_close($ch);
		
		return $html;
	}
}
?>