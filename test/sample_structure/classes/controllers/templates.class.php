<?
// no direct access
defined( '_EXEC' ) or die( 'Restricted access' );

require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/models/sql_model.class.php');

class Templates extends Sql_model{

	protected $random_sequence_1;
	protected $random_sequence_2;
	protected $pre_phrase_finished;
	
	
	public function replace_keys_links_exchange($settings){	

		//чтение файла
		$file_name_src = $_SERVER['DOCUMENT_ROOT'] . '/site/index.php';
		$lines = file($file_name_src);
		$content = implode('', $lines);				

		//sape
		$pattern = "/define\('SAPE_USER', '[_a-zA-Z0-9]{0,70}'\);/";		
		$replace = "define('SAPE_USER', '" . $settings['key_sape'] . "');";	
		$content = preg_replace($pattern, $replace, $content);	
		
		//mainlink
		$pattern = "/define\('MAINLINK_USER', '[_a-zA-Z0-9]{0,70}'\);/";		
		$replace = "define('MAINLINK_USER', '" . $settings['key_mainlink'] . "');";	
		$content = preg_replace($pattern, $replace, $content);	
		
		//mainlink
		$pattern = "/define\('LINKFEED_USER', '[_a-zA-Z0-9]{0,70}'\);/";		
		$replace = "define('LINKFEED_USER', '" . $settings['key_linkfeed'] . "');";	
		$content = preg_replace($pattern, $replace, $content);	
			
		//mainlink
		$pattern = "/define\('SETLINKS_USER', '[_a-zA-Z0-9]{0,70}'\);/";		
		$replace = "define('SETLINKS_USER', '" . $settings['key_setlinks'] . "');";	
		$content = preg_replace($pattern, $replace, $content);				
			
		//запись файла
		$file_name_des = $_SERVER['DOCUMENT_ROOT'] . '/site/index.php';
		$w=fopen($file_name_des,'w');
		$res = fwrite($w,$content);
		fclose($w);	

		return $res;
	}
	
	
	public function randomize_css_class_names($id_site){
		
		$css_files[] = 'main.css';
		$css_files[] = 'typography.css';
		
		foreach($css_files as $key => $file_name){
		
			$content_array[] = $this->get_css_file_content($file_name, $id_site);
		}
		
		foreach($content_array as $key => $content){
		
			$pattern = '/\.[a-zA-Z0-9_-]*( |{)/ius';
			preg_match_all($pattern, $content, $matches);
			
			$matches_array[] = $matches[0];
		}
		
		/*
		#отладка
		echo '<pre>';
		print_r($matches_array);
		echo '</pre>';
		*/
		
		//удаление повторяющихся классов
		foreach($matches_array as $key => $matches){
			
			//очистка скобок, точек и пробелов
			foreach($matches as $key_1 => $value_1){
				$matches[$key_1] = preg_replace('/(\.| |{)/ius', '', $value_1);
			}
			
			$matches_array[$key] = $matches;
		}
		
		/*
		echo '<pre>';
		print_r($matches_array);
		echo '</pre>';
		*/
		
		//объединение массивов
		$matches_global = array();
		foreach($matches_array as $key => $matches){			
			
			$matches_global = array_merge($matches_global, $matches);
		}
		
		//выборка уникальных элементов
		$matches_global = array_unique($matches_global);
		
		
		/*
		echo '<pre>';
		print_r($matches_global);
		echo '</pre>';
		*/
		
		foreach($matches_global as $key => $value){
			$rnd_css_name = $this->gen_rnd_css_class_name();
			
			$origin_css_name_and_rnd_name = '';
			$origin_css_name_and_rnd_name[] = $value;
			$origin_css_name_and_rnd_name[] = $rnd_css_name;
			$rnd_css_name_array[] = $rnd_css_name;
			 
			$origin_css_name_and_rnd_name_array[] = $origin_css_name_and_rnd_name;
		}
		
		$rnd_css_name_array = array_unique($rnd_css_name_array);
		
		//запись последнего ключа в переменную
		end($rnd_css_name_array);
		$end_key_array_rnd_css_name = key($rnd_css_name_array);
		
		for($i = 0; $i < $end_key_array_rnd_css_name; $i++){
		
			if(empty($rnd_css_name_array[$i])){
				$origin_css_name_and_rnd_name_array[$i][1] = '';
			}			
		}
		
		/*
		echo '<pre>';
		print_r($origin_css_name_and_rnd_name_array);
		echo '</pre>';
		*/
		
		$this->replace_css_class_names_on_template_files($id_site, $origin_css_name_and_rnd_name_array);
		$this->replace_css_class_names_on_css_files($id_site, $origin_css_name_and_rnd_name_array);
	}
	
	
	protected function gen_rnd_css_class_name($length_from = 8, $length_to = 12){
	
		$length = rand($length_from, $length_to);
		$chars = 'abcdefghijklmnopqrstuvwxwz';
		$numChars = strlen($chars);
		$string = '';
		
		for ($i = 0; $i < $length; $i++) {
			$string .= substr($chars, rand(1, $numChars) - 1, 1);
		}
		
		return $string;
	}
	
	
	protected function replace_css_class_names_on_css_files($id_site, $origin_css_name_and_rnd_name_array){
	
		$dir = $_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/css/';
		if(is_dir($dir)) {
			$files = scandir($dir);
			array_shift($files); // удаляем из массива '.'
			array_shift($files); // удаляем из массива '..'
		}
		
		foreach($files as $key => $file_name){
		
			//чтение файла
			$file_name_src = $_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/css/' . $file_name;
			$lines = file($file_name_src);
			$content = implode('', $lines);	
			
			foreach($origin_css_name_and_rnd_name_array as $key_1 => $value_1){
			
				if(!empty($value_1[1])){
					$replace = '${1}' . $value_1[1] . '$2';					
				}else{
					$replace = '${1}' . $value_1[0] . '$2';
				}				
				$pattern = '/(\.)' . $value_1[0] . '([ {])/ius';				
				$content = preg_replace($pattern, $replace, $content);	
			}
			
			//запись файла
			$file_name_des = $_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/css/' . $file_name;
			$w=fopen($file_name_des,'w');
			fwrite($w,$content);
			fclose($w);
			
		}
	}
	
	
	protected function replace_css_class_names_on_template_files($id_site, $origin_css_name_and_rnd_name_array){
	
		$dir = $_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/templates/';
		if(is_dir($dir)) {
        
			$files = scandir($dir);
			array_shift($files); // удаляем из массива '.'
			array_shift($files); // удаляем из массива '..'
			
			/*
			echo '<pre>';
			print_r($files);
			echo '</pre>';
			*/
		}
		
		foreach($files as $key => $file_name){
		
			//чтение файла
			$file_name_src = $_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/templates/' . $file_name;
			$lines = file($file_name_src);
			$content = implode('', $lines);	
			
			foreach($origin_css_name_and_rnd_name_array as $key_1 => $value_1){
			
				if(!empty($value_1[1])){
					$replace = '${1}' . $value_1[1] . '$2';					
				}else{
					$replace = '${1}' . $value_1[0] . '$2';
				}
				
				$pattern = '/(class=["\'])' . $value_1[0] . '(["\' ])/ius';				
				$content = preg_replace($pattern, $replace, $content);
				
				$pattern = '/(class=["\'][a-zA-Z0-9_-]* *?)' . $value_1[0] . '(["\' ])/ius';				
				$content = preg_replace($pattern, $replace, $content);
			}
			
			//запись файла
			$file_name_des = $_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/templates/' . $file_name;
			$w=fopen($file_name_des,'w');
			fwrite($w,$content);
			fclose($w);			
		}
		
		/*		
		return $content;
		*/
	}
	
	protected function get_css_file_content($file_name, $id_site){
	
		$file_name_src = $_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/css/' . $file_name;
		$lines = file($file_name_src);
		$content = implode('', $lines);	
		
		return $content;
	}
	
	
	public function create_index_php($id_site, $per_page_f = 5, $per_page_t = 10, $num_page_f = 2, $num_page_t = 10, $header_images_count_f = 3, $header_images_count_t = 14, $per_page_gallery_f = 10, $per_page_gallery_t = 36){
	
		$file_name_src = $_SERVER['DOCUMENT_ROOT'] . '/site/index.php';
		//$file = file_get_contents($filename);
		
		     /*           
		$file_handle = fopen($filename, "r");
		
		$lines = '';
		while (!feof($file_handle)) {
		   $lines .= '<br>' . fgets($file_handle);
		   
		}
		fclose($file_handle);
		*/
		
		$lines = file($file_name_src);
		$content = implode('', $lines);	
		
		
		//устанавливаем переменную отвечающую за определение сайта как тестового в 0
		$replace_pattern = '${1}0';
		$content = preg_replace('/(\$this_test_site = )[0-9]*/', $replace_pattern, $content);		
		
		//устанавливаем идентификатор сайта
		$replace_pattern = '${1}' . $id_site;
		$content = preg_replace('/(\$id_site = )[0-9]*/', $replace_pattern, $content);
		
		//устанавливаем количество статей на страницу
		$replace_pattern = '${1}' . rand($per_page_f, $per_page_t);
		$content = preg_replace('/(\$per_page = )[0-9]*/', $replace_pattern, $content);
		
		//устанавливаем количество ссылок в пагинации
		$replace_pattern = '${1}' . rand($num_page_f, $num_page_t);
		$content = preg_replace('/(\$num_page = )[0-9]*/', $replace_pattern, $content);
		
		//устанавливаем количество изображений в шапке
		while(true){
			$count_images_rand = rand($header_images_count_f, $header_images_count_t);
			if($count_images_rand < 8 OR $count_images_rand > 10){
				break;
			}
		}
		$replace_pattern = '${1}' . $count_images_rand;
		$content = preg_replace('/(\$header_images_count = )[0-9]*/', $replace_pattern, $content);
		
		//устанавливаем количество изображений на странице галереи
		$replace_pattern = '${1}' . rand($per_page_gallery_f, $per_page_gallery_t);		
		$content = preg_replace('/(\$per_page_gallery = )[0-9]*/', $replace_pattern, $content);
							
		//указываем путь к новому файлу
		$file_name_des = $_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/index.php';
		$w=fopen($file_name_des,'w');

		fwrite($w,$content);

		fclose($w);
		
	
	}
	
	
	public function create_css($id_site){
	
		$site = $this->select_site($id_site);
		################
		//генерация main
		$file_name_src = $_SERVER['DOCUMENT_ROOT'] . '/site/css/main.css';
		$lines = file($file_name_src);
		$content = implode('', $lines);
		
		
		$colors = $this->gen_color_scheme();
				
		//выбираем стили блоков для правки
		$pattern = '/div\.box_[0-9]\{.*?\}/ius';
		preg_match_all($pattern, $content, $matches);
		//print_r($matches);
		
		//рандомные значения для теней
		$rand_1 = rand(0,6);
		$rand_2 = rand(0,6);
		$rand_3 = rand(0,6);
		$rand_4 = rand(0,6);
		
		//генерация толщины границы
		$border_thickness = rand(0,6);
		
		//типы рамок
		$border_type[] = 'dotted';
		$border_type[] = 'dashed';
		$border_type[] = 'solid';
		$border_type[] = 'double';
		$border_type[] = 'grouve';
		$border_type[] = 'ridge';
		//$border_type[] = 'inset';
		//$border_type[] = 'outset';
		$number_border_type = rand(0, count($border_type) - 1);
		
		//рандомизация float
		$float[] = 'left';
		$float[] = 'right';
		$number_float = rand(0, count($float) - 1);
		
		//рандомизация выравнивания 
		$align[] = 'left';
		$align[] = 'right';
		$align[] = 'center';
		
		
		//массив названий ключей для обращения через номера используется чуть ниже
		$names_key[] = 'add_color_1';
		$names_key[] = 'rand_color';
		$names_key[] = 'add_color_2';
		$names_key[] = 'add_complementary_color';
		
		
		//рандомизация отступа до названия сайта
		$margin_top_state[] = '0px';
		$margin_top_state[] = '-35px';		
		
		foreach($matches[0] as $key => $value){
		
			//echo $key . '<br>';
			//echo $value . '<br>';
			
			$matches_modif[$key] = $value;			
			
			//echo $colors['angle']['main'][$names_key[$key]] . '!!!';
			
			//подбираем более тёмный цвет для тени и более светлый для рамки
			if($colors['angle']['main'][$names_key[$key]] > 120 AND $colors['angle']['main'][$names_key[$key]] < 300){
				$color_for_shadow = $this->escape_meta_char($colors['second'][$key]);
				$color_for_border = $this->escape_meta_char($colors['third'][$key]);
			}else{
				$color_for_shadow = $this->escape_meta_char($colors['third'][$key]);
				$color_for_border = $this->escape_meta_char($colors['second'][$key]);
			}
			
			$pattern = '/(background-color:)#[a-f0-9]{3,6}/ius';
			$replace = '${1}' . $this->escape_meta_char($colors['main'][$key]);			
			$matches_modif[$key] = preg_replace($pattern, $replace, $matches_modif[$key]);
			
			//цвет тени
			$pattern = '/(box-shadow:.*?)#[a-f0-9]{3,6}/ius';
			//box-shadow:        3px 3px  4px 1px #003eff;
			$replace = '${1}' . $color_for_shadow;			
			$matches_modif[$key] = preg_replace($pattern, $replace, $matches_modif[$key]);
			
			$pattern = '/(-moz-box-shadow:.*?)#[a-f0-9]{3,6}/ius';
			//-moz-box-shadow:   3px 3px  4px 1px #003eff;
			$replace = '${1}' . $color_for_shadow;			
			$matches_modif[$key] = preg_replace($pattern, $replace, $matches_modif[$key]);
			
			$pattern = '/(-webkit-box-shadow:.*?)#[a-f0-9]{3,6}/ius';
			//-webkit-box-shadow: 3px 3px 4px 1px #003eff;
			$replace = '${1}' . $color_for_shadow;			
			$matches_modif[$key] = preg_replace($pattern, $replace, $matches_modif[$key]);
			
			//цвет рамки
			$pattern = '/(border:.*?)#[a-f0-9]{3,6}/ius';
			//border: 1px #ff1300 solid;
			$replace = '${1}' . $color_for_border;			
			$matches_modif[$key] = preg_replace($pattern, $replace, $matches_modif[$key]);
						
			//толщина рамки
			$pattern = '/(border: *?)[0-9]{1,2}(px)/iu';
			//border: 1px #ff1300 solid;
			$replace = '${1}' . $border_thickness . '$2';			
			$matches_modif[$key] = preg_replace($pattern, $replace, $matches_modif[$key]);
			
			//тип рамки
			$pattern = '/(border: *?[0-9]{1,2}px *?#[a-f0-9]{3,6} *?)[a-z]*?(;)/iu';
			//border: 1px #ff1300 solid;
			$replace = '${1}' . $border_type[$number_border_type] . '$2';			
			$matches_modif[$key] = preg_replace($pattern, $replace, $matches_modif[$key]);
			
			
			//размеры тени			
			//box-shadow:        3px 3px  4px 1px #003eff;
			$replace = '${1}' . $rand_1 . 'px ' . $rand_2 . 'px ' . $rand_3 . 'px ' . $rand_4 . 'px $2';	
			
			$pattern = '/(box-shadow:).*?(#)/ius';					
			$matches_modif[$key] = preg_replace($pattern, $replace, $matches_modif[$key]);
			
			$pattern = '/(-moz-box-shadow:).*?(#)/ius';					
			$matches_modif[$key] = preg_replace($pattern, $replace, $matches_modif[$key]);	

			$pattern = '/(-webkit-box-shadow:).*?(#)/ius';					
			$matches_modif[$key] = preg_replace($pattern, $replace, $matches_modif[$key]);	
			
			/*
			$matches_modif[$key]  = $value;
			//$count_loop = 0;
			foreach($matches_1[0] as $key_1 => $value_1){
				
				$replace = '${1}' . $this->escape_meta_char($colors['main'][$key]);
				$matches_modif[$key] = preg_replace($value_1, $replace, $matches_modif[$key]);
				
			}
			*/
		}		
		
		foreach($matches[0] as $key => $value){			
			$content = str_replace($value, $matches_modif[$key], $content);
		}
		
		/*
		#отладка
		echo '<pre>!!!';
		print_r($matches_modif);
		echo '!!!</pre>';
		
		echo '<pre>!!!';
		print_r($matches[0]);
		echo '!!!</pre>';
		*/
		
		
		//обработка float
		$pattern_wrap = '/(img.image_text{).*?(})/ius';	
		preg_match($pattern_wrap, $content, $matches_2);		
		
		$pattern = '/(float:)[a-z]*?(;)/ius';	
		$replace = '${1}' . $float[$number_float] . '$2';			
		$value_modif = preg_replace($pattern, $replace, $matches_2[0]);			
		
		$content = preg_replace($pattern_wrap, $value_modif, $content);	
		
		//меняем положение для того чтобы второе изображение в строке было противоположно первому
		if($number_float == 1){
			$number_float = 0;
		}else{
			$number_float = 1;
		}
		
		$pattern_wrap = '/(img.image_text_1{).*?(})/ius';	
		preg_match($pattern_wrap, $content, $matches_2);		
		
		$pattern = '/(float:)[a-z]*?(;)/ius';	
		$replace = '${1}' . $float[$number_float] . '$2';			
		$value_modif = preg_replace($pattern, $replace, $matches_2[0]);			
		
		$content = preg_replace($pattern_wrap, $value_modif, $content);		
		
		$pattern_wrap = '/(img.video_thumb{).*?(})/ius';	
		preg_match($pattern_wrap, $content, $matches_2);		
		
		$pattern = '/(float:)[a-z]*?(;)/ius';	
		$replace = '${1}' . $float[$number_float] . '$2';			
		$value_modif = preg_replace($pattern, $replace, $matches_2[0]);			
			
		$content = preg_replace($pattern_wrap, $value_modif, $content);
		
		//обработка align
		$number_align = rand(0, count($align) - 1);
		$pattern_wrap = '/(div.text_more{).*?(})/ius';	
		preg_match($pattern_wrap, $content, $matches_2);				
		$pattern = '/(text-align:)[a-z]*?(;)/ius';	
		$replace = '${1}' . $align[$number_align] . '$2';		
		$value_modif = preg_replace($pattern, $replace, $matches_2[0]);			
		$content = preg_replace($pattern_wrap, $value_modif, $content);
		
		
		$number_align = rand(0, count($align) - 1);
		$pattern_wrap = '/(div.name_site{).*?(})/ius';	
		preg_match($pattern_wrap, $content, $matches_2);				
		$pattern = '/(text-align:)[a-z]*?(;)/ius';	
		$replace = '${1}' . $align[$number_align] . '$2';		
		$value_modif = preg_replace($pattern, $replace, $matches_2[0]);			
		$content = preg_replace($pattern_wrap, $value_modif, $content);
		
		
		$number_align = rand(0, count($align) - 1);
		$pattern_wrap = '/(div.gallery_link{).*?(})/ius';	
		preg_match($pattern_wrap, $content, $matches_2);				
		$pattern = '/(text-align:)[a-z]*?(;)/ius';	
		$replace = '${1}' . $align[$number_align] . '$2';		
		$value_modif = preg_replace($pattern, $replace, $matches_2[0]);			
		$content = preg_replace($pattern_wrap, $value_modif, $content);		
		
		
		//add 16_10_12-18_05
		$number_align = rand(0, count($align) - 1);
		$pattern_wrap = '/(div.date_publish{).*?(})/ius';	
		preg_match($pattern_wrap, $content, $matches_2);				
		$pattern = '/(text-align:)[a-z]*?(;)/ius';	
		$replace = '${1}' . $align[$number_align] . '$2';		
		$value_modif = preg_replace($pattern, $replace, $matches_2[0]);			
		$content = preg_replace($pattern_wrap, $value_modif, $content);		
		

		//add 22_10_12-01_30
		$number_margin_top_state = rand(0, count($margin_top_state) - 1);
		$pattern_wrap = '/(div.main_wrapper{).*?(})/ius';	
		preg_match($pattern_wrap, $content, $matches_2);				
		$pattern = '/(margin-top:)[-a-z0-9]*?(;)/ius';	
		$replace = '${1}' . $margin_top_state[$number_margin_top_state] . '$2';		
		$value_modif = preg_replace($pattern, $replace, $matches_2[0]);			
		$content = preg_replace($pattern_wrap, $value_modif, $content);

		
		//body
		//background-color:
		$color_background[] = '#000000';
		$color_background[] = '#111111';
		$color_background[] = '#222222';
		$color_background[] = '#333333';
		$color_background[] = '#444444';
		$color_background[] = '#555555';
		$color_background[] = '#666666';
		$color_background[] = '#777777';
		$color_background[] = '#888888';
		$color_background[] = '#999999';
		$color_background[] = '#aaaaaa';
		$color_background[] = '#bbbbbb';
		$color_background[] = '#cccccc';
		$color_background[] = '#dddddd';
		$color_background[] = '#eeeeee';
		$color_background[] = '#ffffff';		
		
		$number_color_background = rand(0, count($color_background) - 1);
		
		$middle = round(count($color_background) / 2);
		if($number_color_background <= $middle){
			$text_on_body_background = '#ffffff';
		}else{
			$text_on_body_background = '#000000';
		}
		
		$pattern_wrap = '/body\{.*?\}/ius';	
		preg_match($pattern_wrap, $content, $matches_2);				
		$pattern = '/(background-color:) *?#[a-z0-9]{3,6}? *?(;)/ius';	
		$replace = '${1}' . $color_background[$number_color_background] . '$2';		
		$value_modif = preg_replace($pattern, $replace, $matches_2[0]);			
		$content = preg_replace($pattern_wrap, $value_modif, $content);		
		
		
		//рандомизация углов
		$angles_type = rand(1,14);
		$angle_radius = rand(10,30);
		
		if($angles_type == 1){
			$replace = '${1}' . $angle_radius . 'px 0 ' . $angle_radius . 'px 0$2';
		}elseif($angles_type == 2){
			$replace = '${1}0 ' . $angle_radius . 'px 0 ' . $angle_radius . 'px$2';
		}elseif($angles_type == 3){
			$replace = '${1}' . $angle_radius . 'px ' . $angle_radius . 'px ' . $angle_radius . 'px ' . $angle_radius . 'px$2';
		}elseif($angles_type == 4){
			$replace = '${1}' . $angle_radius . 'px 0 0 0$2';
		}elseif($angles_type == 5){
			$replace = '${1}0 ' . $angle_radius . 'px 0 0$2';
		}elseif($angles_type == 6){
			$replace = '${1}0 0 ' . $angle_radius . 'px 0$2';
		}elseif($angles_type == 7){
			$replace = '${1}0 0 0 ' . $angle_radius . 'px$2';
		}elseif($angles_type == 8){
			$replace = '${1}' . $angle_radius . 'px ' . $angle_radius . 'px 0 0$2';
		}elseif($angles_type == 9){
			$replace = '${1}0 0 ' . $angle_radius . 'px ' . $angle_radius . 'px$2';
		}elseif($angles_type == 10){
			$replace = '${1}0 ' . $angle_radius . 'px ' . $angle_radius . 'px ' . $angle_radius . 'px$2';
		}elseif($angles_type == 11){
			$replace = '${1}' . $angle_radius . 'px 0 ' . $angle_radius . 'px ' . $angle_radius . 'px$2';
		}elseif($angles_type == 12){
			$replace = '${1}' . $angle_radius . 'px ' . $angle_radius . 'px 0 ' . $angle_radius . 'px$2';
		}elseif($angles_type == 13){
			$replace = '${1}' . $angle_radius . 'px ' . $angle_radius . 'px ' . $angle_radius . 'px 0$2';
		}elseif($angles_type == 14){
			$replace = '${1}0 0 0 0$2';
		}				
		
		$content = preg_replace('/(border-radius:).*?(;)/', $replace, $content);
		$content = preg_replace('/(-webkit-border-radius:).*?(;)/', $replace, $content);
		$content = preg_replace('/(-moz-border-radius:).*?(;)/', $replace, $content);
		$content = preg_replace('/(-khtml-border-radius:).*?(;)/', $replace, $content);		
		
		
		//записываем изменнённый файл
		$file_name_des = $_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/css/main.css';
		$w=fopen($file_name_des,'w');
		fwrite($w,$content);
		fclose($w);		
		
		
		######################		
		//генерация typography
		$file_name_src = $_SERVER['DOCUMENT_ROOT'] . '/site/css/typography.css';
		$lines = file($file_name_src);
		$content = implode('', $lines);
			
		
		/*
		$colors['angle']['main']['add_color_1'] = $add_color_angle_1;
		$colors['angle']['main']['rand_color'] = $rand_angle;
		$colors['angle']['main']['add_color_2'] = $add_color_angle_2;
		$colors['angle']['main']['$add_complementary_color'] = $add_complementary_color_angle;
		*/
		
		$content = $this->create_typography_color($color_angle = $colors['angle']['main']['add_color_1'], $content, 'div.box_1');
		$content = $this->create_typography_color($color_angle = $colors['angle']['main']['add_color_1'], $content, 'div.box_1 a');
		
		$content = $this->create_typography_color($color_angle = $colors['angle']['main']['rand_color'], $content, 'div.box_2');
		$content = $this->create_typography_color($color_angle = $colors['angle']['main']['rand_color'], $content, 'div.box_2 a');
		
		$content = $this->create_typography_color($color_angle = $colors['angle']['main']['add_color_2'], $content, 'div.box_3');
		$content = $this->create_typography_color($color_angle = $colors['angle']['main']['add_color_2'], $content, 'div.box_3 a');
		
		$content = $this->create_typography_color($color_angle = $colors['angle']['main']['add_complementary_color'], $content, 'div.box_4');
		$content = $this->create_typography_color($color_angle = $colors['angle']['main']['add_complementary_color'], $content, 'div.box_4 a');
		
		
		$text_decor[] = 'underline';
		$text_decor[] = 'none';
		$text_decor_number = rand(0, count($text_decor) - 1);		
		
		$pattern_wrap = '/(div.name_site a{).*?(})/ius';	
		preg_match($pattern_wrap, $content, $matches_2);				
		$pattern = '/(text-decoration:)[a-z]*?(;)/ius';	
		$replace = '${1}' . $text_decor[$text_decor_number] . '$2';		
		$value_modif = preg_replace($pattern, $replace, $matches_2[0]);			
		$content = preg_replace($pattern_wrap, $value_modif, $content);	
		
		$font_size = rand(14, 16);
		$pattern_wrap = '/(body{).*?(})/ius';	
		preg_match($pattern_wrap, $content, $matches_2);				
		$pattern = '/(font-size:) *?[0-9]*?(px *?;)/ius';	
		$replace = '${1}' . $font_size . '$2';		
		$value_modif = preg_replace($pattern, $replace, $matches_2[0]);			
		$content = preg_replace($pattern_wrap, $value_modif, $content);	
		
		//font-family
		/*
		$font_family[] = 'Times, serif';
		$font_family[] = 'Arial, sans-serif';
		$font_family[] = 'cursive';
		$font_family[] = 'fantasy';
		$font_family[] = 'Courier, monospace';
		*/		
		$font_family[] = 'Arial';
		$font_family[] = 'Arial Black';
		$font_family[] = 'Arial Narrow';
		$font_family[] = 'Book Antiqua';
		$font_family[] = 'Century Gothic';
		$font_family[] = 'Comic Sans MS';
		$font_family[] = 'Courier New';
		$font_family[] = 'Franklin Gothic Medium';
		$font_family[] = 'Georgia';
		//$font_family[] = 'Impact';
		$font_family[] = 'Lucida Console';
		$font_family[] = 'Lucida Sans Unicode';
		$font_family[] = 'Microsoft Sans Serif';
		$font_family[] = 'Palatino Linotype';
		$font_family[] = 'Sylfaen';
		$font_family[] = 'Tahoma';
		$font_family[] = 'Times New Roman';
		$font_family[] = 'Trebuchet MS';
		$font_family[] = 'Verdana';

		
		$font_family_number = rand(0, count($font_family) - 1);
		$pattern_wrap = '/(body{).*?(})/ius';	
		preg_match($pattern_wrap, $content, $matches_2);				
		$pattern = '/(font-family:) *?[a-z]*?( *?;)/ius';	
		$replace = '${1}' . $font_family[$font_family_number] . '$2';		
		$value_modif = preg_replace($pattern, $replace, $matches_2[0]);			
		$content = preg_replace($pattern_wrap, $value_modif, $content);	
		
		
		//list-style-type
		$list_st_type[] = 'circle';
		$list_st_type[] = 'disc';
		$list_st_type[] = 'square';
		
		$list_st_type_number = rand(0, count($list_st_type) - 1);
		$pattern_wrap = '/(ul.menu_ul{).*?(})/ius';	
		preg_match($pattern_wrap, $content, $matches_2);				
		$pattern = '/(list-style-type:) *?[a-z]*?( *?;)/ius';	
		$replace = '${1}' . $list_st_type[$list_st_type_number] . '$2';		
		$value_modif = preg_replace($pattern, $replace, $matches_2[0]);			
		$content = preg_replace($pattern_wrap, $value_modif, $content);	
		
		
		//span.text_on_body_background h1
		$pattern_wrap = '/span.text_on_body_background h1\{.*?\}/ius';	
		preg_match($pattern_wrap, $content, $matches_2);				
		$pattern = '/(color:) *?#[a-z0-9]{3,6}? *?(;)/ius';	
		//$text_on_body_background - устанавливается в выше в сегменте обработки main.css исходя из цветах background-color
		$replace = '${1}' . $text_on_body_background . '$2';		
		$value_modif = preg_replace($pattern, $replace, $matches_2[0]);			
		$content = preg_replace($pattern_wrap, $value_modif, $content);
		
		
		//div.pagination
		$pattern_wrap = '/div.pagination\{.*?\}/ius';	
		preg_match($pattern_wrap, $content, $matches_2);				
		$pattern = '/(color:) *?#[a-z0-9]{3,6}? *?(;)/ius';	
		//$text_on_body_background - устанавливается в выше в сегменте обработки main.css исходя из цветах background-color
		$replace = '${1}' . $text_on_body_background . '$2';		
		$value_modif = preg_replace($pattern, $replace, $matches_2[0]);			
		$content = preg_replace($pattern_wrap, $value_modif, $content);
		
		
		//div.pagination a
		$pattern_wrap = '/div.pagination a\{.*?\}/ius';	
		preg_match($pattern_wrap, $content, $matches_2);				
		$pattern = '/(color:) *?#[a-z0-9]{3,6}? *?(;)/ius';	
		//$text_on_body_background - устанавливается в выше в сегменте обработки main.css исходя из цветах background-color
		$replace = '${1}' . $text_on_body_background . '$2';		
		$value_modif = preg_replace($pattern, $replace, $matches_2[0]);			
		$content = preg_replace($pattern_wrap, $value_modif, $content);
		
		
		//div.gallery_title
		$pattern_wrap = '/div.gallery_title\{.*?\}/ius';	
		preg_match($pattern_wrap, $content, $matches_2);				
		$pattern = '/(color:) *?#[a-z0-9]{3,6}? *?(;)/ius';	
		//$text_on_body_background - устанавливается в выше в сегменте обработки main.css исходя из цветах background-color
		$replace = '${1}' . $text_on_body_background . '$2';		
		$value_modif = preg_replace($pattern, $replace, $matches_2[0]);			
		$content = preg_replace($pattern_wrap, $value_modif, $content);			
		
		
		//записываем изменнённый файл
		$file_name_des = $_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/css/typography.css';
		$w=fopen($file_name_des,'w');
		fwrite($w,$content);
		fclose($w);
		
	}
	
	
	protected function create_typography_color($color_angle, $content, $style_name){
	
		$style_name = $this->escape_meta_char($style_name);
		
		if($color_angle > 220 OR $color_angle < 40){		
			$new_color = 'ffffff';				
		}else{
			$new_color = '000000';
		}
		
		$pattern = '/(' . $style_name . '\{.*?color:#)[a-f-0-9]{3,6}(.*?\})/ius';
		$content = preg_replace($pattern, '${1}' . $new_color . '$2', $content);
		
		return $content;
	}
	
	
	public function create_robots_txt($id_site){
	
		$site = $this->select_site($id_site);			
		
		$file_name_src = $_SERVER['DOCUMENT_ROOT'] . '/site/robots.txt';
		
		$lines = file($file_name_src);
		$content = implode('', $lines);		
		
		//записывает главное зеркало сайта
		$replace_pattern = '${1} ' . $site['domain'];
		$content = preg_replace('/(Host:)/', $replace_pattern, $content);		
		
		$file_name_des = $_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/robots.txt';
		$w=fopen($file_name_des,'w');

		fwrite($w,$content);

		fclose($w);
	}
	
	
	public function gen_color_scheme(){
	
		//основной цвет
		$rand_angle = rand(0, 360);
		//отклонение вторичных цветов
		$angle_add_color = 20;
		//отклонение дополниетльного цвета 1
		$gen_angle = rand(15,40);
		$angle_add_color_1 = $gen_angle;
		//отклонение дополниетльного цвета 2
		$angle_add_color_2 = $gen_angle;
		//отклонение противоположноного цвета
		$angle_complementary_color = 180;
		
		$add_color_angle_1 = $rand_angle - $angle_add_color;
		if($add_color_angle_1 < 0){
			$add_color_angle_1 = 360 + $add_color_angle_1;
		}
		$add_color_angle_1_1 = $rand_angle - $angle_add_color + $angle_add_color_1;
		if($add_color_angle_1_1 < 0){
			$add_color_angle_1_1 = 360 + $add_color_angle_1_1;
		}elseif($add_color_angle_1_1 > 360){
			$add_color_angle_1_1 = $add_color_angle_1_1 - 360;
		}
		$add_color_angle_1_2 = $rand_angle - $angle_add_color - $angle_add_color_2;
		if($add_color_angle_1_2 < 0){
			$add_color_angle_1_2 = 360 + $add_color_angle_1_2;
		}elseif($add_color_angle_1_2 > 360){
			$add_color_angle_1_2 = $add_color_angle_1_2 - 360;
		}
		
		$rand_angle_1 = $rand_angle + $angle_add_color_1;
		if($rand_angle_1 > 360){
			$rand_angle_1 = $rand_angle_1 - 360;
		}
		$rand_angle_2 = $rand_angle - $angle_add_color_2;
		if($rand_angle_2 < 0){
			$rand_angle_2 = 360 + $rand_angle_2;
		}
		
		
		$add_color_angle_2 = $rand_angle + $angle_add_color;
		if($add_color_angle_2 > 360){
			$add_color_angle_2 = $add_color_angle_2 - 360;
		}
		$add_color_angle_2_1 = $rand_angle - $angle_add_color + $angle_add_color_1;
		if($add_color_angle_2_1 < 0){
			$add_color_angle_2_1 = 360 + $add_color_angle_1_1;
		}
		$add_color_angle_2_2 = $rand_angle - $angle_add_color - $angle_add_color_2;
		if($add_color_angle_2_2 < 0){
			$add_color_angle_2_2 = 360 + $add_color_angle_2_2;
		}elseif($add_color_angle_2_2 > 360){
			$add_color_angle_2_2 = $add_color_angle_2_2 - 360;
		}		
		
		$add_complementary_color_angle = $rand_angle + $angle_complementary_color;
		if($add_complementary_color_angle > 360){
			$add_complementary_color_angle = $add_complementary_color_angle - 360;
		}
		$add_complementary_color_angle_1 = $rand_angle + $angle_complementary_color + $angle_add_color_1;
		if($add_complementary_color_angle_1 > 360){
			$add_complementary_color_angle_1 = $add_complementary_color_angle_1 - 360;
		}
		$add_complementary_color_angle_2 = $rand_angle + $angle_complementary_color - $angle_add_color_2;
		if($add_complementary_color_angle_2 > 360){
			$add_complementary_color_angle_2 = $add_complementary_color_angle_2 - 360;
		}elseif($add_complementary_color_angle_2 < 0){
			$add_complementary_color_angle_2 = 360 + $add_complementary_color_angle_2;
		}
		
		$add_color_1 = $this->gen_color_by_angle($add_color_angle_1);		
		$add_color_1_1 = $this->gen_color_by_angle($add_color_angle_1_1);
		$add_color_1_2 = $this->gen_color_by_angle($add_color_angle_1_2);		
		
		
		$rand_color = $this->gen_color_by_angle($rand_angle);		
		$rand_color_1 = $this->gen_color_by_angle($rand_angle_1);
		$rand_color_2 = $this->gen_color_by_angle($rand_angle_2);
		
		$add_color_2 = $this->gen_color_by_angle($add_color_angle_2);		
		$add_color_2_1 = $this->gen_color_by_angle($add_color_angle_2_1);
		$add_color_2_2 = $this->gen_color_by_angle($add_color_angle_2_2);
		
		$add_complementary_color = $this->gen_color_by_angle($add_complementary_color_angle);	
		$add_complementary_color_1 = $this->gen_color_by_angle($add_complementary_color_angle_1);		
		$add_complementary_color_2 = $this->gen_color_by_angle($add_complementary_color_angle_2);	

		/*
		echo '<div style="width:200px;height:50px; background-color:rgb(' . $add_color_1 . ')"></div><br>';	
		echo '<div style="width:200px;height:50px; background-color:rgb(' . $add_color_1_1 . ')"></div><br>';	
		echo '<div style="width:200px;height:50px; background-color:rgb(' . $rand_color . ')"></div><br>';	
		echo '<div style="width:200px;height:50px; background-color:rgb(' . $rand_color_1 . ')"></div><br>';
		echo '<div style="width:200px;height:50px; background-color:rgb(' . $add_color_2 . ')"></div><br>';	
		echo '<div style="width:200px;height:50px; background-color:rgb(' . $add_color_2_1 . ')"></div><br>';
		echo '<div style="width:200px;height:50px; background-color:rgb(' . $add_complementary_color . ')"></div><br>';
		echo '<div style="width:200px;height:50px; background-color:rgb(' . $add_complementary_color_1 . ')"></div><br>';
		*/
		/*
		$colors['add_color_1'] = $add_color_1;
		$colors['add_color_1_1'] = $add_color_1_1;
		$colors['rand_color'] = $rand_color;
		$colors['rand_color_1'] = $rand_color_1;
		$colors['add_color_2'] = $add_color_2;
		$colors['add_color_2_1'] = $add_color_2_1;
		$colors['add_complementary_color'] = $add_complementary_color;
		$colors['add_complementary_color_1'] = $add_complementary_color_1;
		*/
		$colors['main'][] = $add_color_1;
		$colors['second'][] = $add_color_1_1;
		$colors['third'][] = $add_color_1_2;
		
		$colors['main'][] = $rand_color;
		$colors['second'][] = $rand_color_1;
		$colors['third'][] = $rand_color_2;
		
		$colors['main'][] = $add_color_2;
		$colors['second'][] = $add_color_2_1;
		$colors['third'][] = $add_color_2_2;
		
		$colors['main'][] = $add_complementary_color;
		$colors['second'][] = $add_complementary_color_1;
		$colors['third'][] = $add_complementary_color_2;
		
		//перевод в hex с решёткой
		foreach($colors['main'] as $key => $value){
			$value_array = explode(',', $value);
			$colors['main'][$key] = $this->rgb2html($value_array[0], $value_array[1], $value_array[2]);
			
			/*
			#отладка
			echo $value;
			echo '<pre>';
			print_r($value_array);
			echo '</pre>';
			*/
		}
		foreach($colors['second'] as $key => $value){
			$value_array = explode(',', $value);
			$colors['second'][$key] = $this->rgb2html($value_array[0], $value_array[1], $value_array[2]);
		}
		foreach($colors['third'] as $key => $value){
			$value_array = explode(',', $value);
			$colors['third'][$key] = $this->rgb2html($value_array[0], $value_array[1], $value_array[2]);
		}
		
		/*
		#отладка
		echo $rand_angle;
		echo '<br>';
		echo $rand_color;
		echo '<br>';
		echo $colors['main'][0];		
		echo '<div style="width:200px; height:100px; background-color:' . $colors['main'][0] . '"></div>';
		echo '<br>';
		echo '<br>';
		echo $add_complementary_color_angle;
		echo '<br>';
		echo $add_complementary_color;
		echo '<br>';
		echo $colors['main'][3];		
		echo '<div style="width:200px; height:100px; background-color:' . $colors['main'][3] . '"></div>';
		*/
		
		$colors['angle']['main']['add_color_1'] = $add_color_angle_1;
		$colors['angle']['main']['rand_color'] = $rand_angle;
		$colors['angle']['main']['add_color_2'] = $add_color_angle_2;
		$colors['angle']['main']['add_complementary_color'] = $add_complementary_color_angle;
		
		$colors['angle']['second']['add_color_1'] = $add_color_angle_1_1;
		$colors['angle']['second']['rand_color'] = $rand_angle_1;
		$colors['angle']['second']['add_color_2'] = $add_color_angle_2_1;
		$colors['angle']['second']['add_complementary_color'] = $add_complementary_color_angle_1;
		
		$colors['angle']['third']['add_color_1'] = $add_color_angle_1_2;
		$colors['angle']['third']['rand_color'] = $rand_angle_2;
		$colors['angle']['third']['add_color_2'] = $add_color_angle_2_2;
		$colors['angle']['third']['add_complementary_color'] = $add_complementary_color_angle_2;
		
		return $colors;
	}
	
	
	protected function rgb2html($r, $g=-1, $b=-1){
	
		if (is_array($r) && sizeof($r) == 3)
			list($r, $g, $b) = $r;

		$r = intval($r); $g = intval($g);
		$b = intval($b);

		$r = dechex($r<0?0:($r>255?255:$r));
		$g = dechex($g<0?0:($g>255?255:$g));
		$b = dechex($b<0?0:($b>255?255:$b));

		$color = (strlen($r) < 2?'0':'').$r;
		$color .= (strlen($g) < 2?'0':'').$g;
		$color .= (strlen($b) < 2?'0':'').$b;
		return '#'.$color;
	}
	
	
	protected function gen_color_by_angle($angle){	
		
		
		if($angle >= $from = 0 AND $angle < $to = 120){
			
			//угол от начала сектора
			$angle_by_start_sector = $angle - $from;
			
			//из градуса конца вычитаем градус начала сектора
			$count = $to - $from;		
			$color = round(255 * ($angle_by_start_sector / $count));
			return '255,' . $color . ',0';
			//0-120 --- 100 0 0 - 100 100 0 up
			
		}elseif($angle >= $from = 120 AND $angle < $to = 180){
		
			//угол от начала сектора
			$angle_by_start_sector = $angle - $from;
			
			$count = $to - $from;
			$color = round(255 * ($angle_by_start_sector / $count));
			//идёт уменьшение цвета поэтому вычаем из 255 цвет который был высчитан
			$color = 255 - $color;
			return $color . ',255,0';
			//120-180 --- 100 100 0 - 0 100 0 down
		}elseif($angle >= $from = 180 AND $angle < $to = 210){
		
			//угол от начала сектора
			$angle_by_start_sector = $angle - $from;
			
			$count = $to - $from;
			$color = round(255 * ($angle_by_start_sector / $count));
			return '0,255,' . $color;
			//180-210 --- 0 100 0 - 0 100 100 up
			
		}elseif($angle >= $from = 210 AND $angle < $to = 255){

			//угол от начала сектора
			$angle_by_start_sector = $angle - $from;
		
			$count = $to - $from;
			$color = round(255 * ($angle_by_start_sector / $count));
			//идёт уменьшение цвета поэтому вычаем из 255 цвет который был высчитан
			$color = 255 - $color;
			return '0,' . $color . ',255';
			//210-255 --- 0 100 100 - 0 0 100 down
			
		}elseif($angle >= $from = 255 AND $angle < $to = 315){
		
			//угол от начала сектора
			$angle_by_start_sector = $angle - $from;
		
			$count = $to - $from;
			$color = round(255 * ($angle_by_start_sector / $count));
			return $color . ',0,255';
			//255-315 --- 0 0 100 - 100 0 100 up
			
		}elseif($angle >= $from = 315 AND $angle <= $to = 360){
		
			//угол от начала сектора
			$angle_by_start_sector = $angle - $from;
		
			$count = $to - $from;
			$color = round(255 * ($angle_by_start_sector / $count));
			//идёт уменьшение цвета поэтому вычаем из 255 цвет который был высчитан
			$color = 255 - $color;
			return '255,0,' . $color;
			//315-360 --- 100 0 100 - 100 0 0 down
			
		}
	}
	
	public function gen_random_sequence($count_sequence = 3, $sequence_number = 1){
	
		//создаём изначальную последовательность
		$count_loop = 0;
		while($count_sequence != count($sequence)){
			$sequence[] = $count_loop;
			$count_loop++;
		}
	
		$count_loop = 0;
		while(true){
			$rand = rand(1,2);
			
			if($rand == 1){
				//проверяем не заносили ли мы уже этот номер в итоговый массив
				if(is_array($random_sequence)){
				
					$flag_use_file = 0;
					foreach($random_sequence as $value){
						if($sequence[$count_loop] == $value){
							$flag_use_file = 1;
						}
					}
					
					if($flag_use_file == 0){
						$random_sequence[] = $sequence[$count_loop];
					}
				}else{
					//если ещё ничего не заносили заносим без проверок
					$random_sequence[] = $sequence[$count_loop];
				}				
			}
			
			$count_loop++;
			
			if($count_loop == $count_sequence){
				$count_loop = 0;
			}
			
			//если захватили все файлы выходим из цикла
			if(count($random_sequence) == $count_sequence){
				break;
			}
		}
		
		/*
		##отладка
		echo '<pre>';
		print_r($sequence);
		print_r($random_sequence);		
		echo '</pre>';
		*/
		
		$name_metod = 'random_sequence_' . $sequence_number;
		$this->$name_metod = $random_sequence;
		return $random_sequence;
	}
	
	
	protected function meta_text_on_body_randomize($phrase, $id_site){
	
		//0 яцейка первого уровня - массив с заголовками 
		$prepare_phrases[0][1] = 'Подробнее';
		$prepare_phrases[0][2] = 'Категория';
		$prepare_phrases[0][3] = 'Смотреть видео';
		$prepare_phrases[0][4] = 'Галерея';		
		$prepare_phrases[0][5] = 'Читать статьи';	
		$prepare_phrases[0][6] = 'К сожалению такой страницы нет на сайте. Ошибка 404.';
		$prepare_phrases[0][7] = 'Меню';
		$prepare_phrases[0][8] = 'Меню галереи';
		$prepare_phrases[0][9] = 'Описание';
		$prepare_phrases[0][10] = 'Длительность';
		$prepare_phrases[0][11] = 'Дата публикации';
		$prepare_phrases[0][12] = 'Название отсутствует';
		$prepare_phrases[0][13] = 'Название видео';	
		$prepare_phrases[0][14] = 'Описание отсутствует';	
		$prepare_phrases[0][15] = 'Этот раздел галереи пуст';
		$prepare_phrases[0][16] = 'Спонсоры';
		$prepare_phrases[0][17] = '-';		
	
		
		//каждый последующий сектор первого уровня массив со словами к соответвующей ячейке заголовка
		$prepare_phrases[1][] = 'Подробнее';
		$prepare_phrases[1][] = 'Читать дальше';
		$prepare_phrases[1][] = 'Читать далее';
		$prepare_phrases[1][] = 'Полная статья';
		$prepare_phrases[1][] = 'Дальше';
		$prepare_phrases[1][] = 'Далее';
		$prepare_phrases[1][] = 'Полностью';
		$prepare_phrases[1][] = 'Читать всё';
		$prepare_phrases[1][] = 'Читать полностью';
		$prepare_phrases[1][] = 'Полный текст';
		$prepare_phrases[1][] = 'К полной статье';
		$prepare_phrases[1][] = 'Полный текст';
		$prepare_phrases[1][] = 'Весь текст';
		$prepare_phrases[1][] = 'Вся статья';
		$prepare_phrases[1][] = 'К полному тексту';
		$prepare_phrases[1][] = 'Прочесть целиком';
		$prepare_phrases[1][] = 'Прочесть всё';
		$prepare_phrases[1][] = 'Прочесть полностью';
		$prepare_phrases[1][] = 'Прочесть всю статью';
		$prepare_phrases[1][] = 'Прочесть весь текст';
		$prepare_phrases[1][] = 'Больше';
		$prepare_phrases[1][] = 'Читать больше';
		
		$prepare_phrases[2][] = 'Категория';
		$prepare_phrases[2][] = 'Рублика';
		$prepare_phrases[2][] = 'Раздел';
		$prepare_phrases[2][] = 'Сектор';
		
		$prepare_phrases[3][] = 'Смотреть видео';
		$prepare_phrases[3][] = 'Видео';
		$prepare_phrases[3][] = 'Глянуть видео';
		$prepare_phrases[3][] = 'Посмотреть видео';
		$prepare_phrases[3][] = 'Видео ролики';
		$prepare_phrases[3][] = 'Видео записи';
		$prepare_phrases[3][] = 'Видео материалы';
		$prepare_phrases[3][] = 'Видео контент';
		$prepare_phrases[3][] = 'Смотреть ролики';
		$prepare_phrases[3][] = 'Глянуть ролики';
		
		$prepare_phrases[4][] = 'Галерея';
		$prepare_phrases[4][] = 'Фотографии';
		$prepare_phrases[4][] = 'Фото';
		$prepare_phrases[4][] = 'Изображения';
		$prepare_phrases[4][] = 'Картинки';
		
		$prepare_phrases[5][] = 'Читать статьи';
		$prepare_phrases[5][] = 'Статьи';
		$prepare_phrases[5][] = 'Почитать';	
		$prepare_phrases[5][] = 'Почитать статьи';	
		$prepare_phrases[5][] = 'Читать материалы';
		$prepare_phrases[5][] = 'Почитать материалы';
		$prepare_phrases[5][] = 'Перейти к статьям';
		$prepare_phrases[5][] = 'К статьям';

		$prepare_phrases[6][] = 'К сожалению такой страницы нет на сайте. Ошибка 404.';
		$prepare_phrases[6][] = 'К сожалению такой страницы нет на сайте.';
		$prepare_phrases[6][] = 'Такой страницы нет на сайте. Ошибка 404.';		
		$prepare_phrases[6][] = 'Такой страницы нет на сайте.';	
		$prepare_phrases[6][] = 'Нет такой страницы на сайте.';	
		$prepare_phrases[6][] = 'На сайте нет такой страницы.';
		$prepare_phrases[6][] = 'Запрошенная вами страница отсутствует на сайте.';
		$prepare_phrases[6][] = 'Страница которую вы пытались найти не существует.';
		$prepare_phrases[6][] = 'Здесь нет этой страницы.';
		$prepare_phrases[6][] = 'На сайте нет этой страницы.';
		$prepare_phrases[6][] = 'Сожалеем, но этой страницы нет на сайте.';
		$prepare_phrases[6][] = 'Упс, такой страницы нет.';
		$prepare_phrases[6][] = 'Упс, такой страницы нет на сайте.';
		$prepare_phrases[6][] = 'Странно, но этой страницы нет на сайте.';
		
		$prepare_phrases[7][] = 'Меню';
		$prepare_phrases[7][] = 'Разделы';
		$prepare_phrases[7][] = 'Категории';
		$prepare_phrases[7][] = 'Категории сайта';
		$prepare_phrases[7][] = 'Разделы сайта';
		$prepare_phrases[7][] = 'Список категорий';
		$prepare_phrases[7][] = 'Список разделов';
		
		$prepare_phrases[8][] = 'Меню галереи';
		$prepare_phrases[8][] = 'Разделы галереи';
		$prepare_phrases[8][] = 'Категории галереи';
		$prepare_phrases[8][] = 'Разделы фото';
		$prepare_phrases[8][] = 'Меню фото';
		$prepare_phrases[8][] = 'Категории фото';
		$prepare_phrases[8][] = 'Разделы фотографий';
		$prepare_phrases[8][] = 'Категории фотографий';
		$prepare_phrases[8][] = 'Меню фотографий';
		$prepare_phrases[8][] = 'Меню изображений';
		$prepare_phrases[8][] = 'Разделы изображений';
		$prepare_phrases[8][] = 'Категории изображений';
		$prepare_phrases[8][] = 'Меню картинок';
		$prepare_phrases[8][] = 'Категории картинок';
		$prepare_phrases[8][] = 'Разделы картинок';
		
		$prepare_phrases[9][] = 'Описание';
		$prepare_phrases[9][] = 'Пояснение';
		$prepare_phrases[9][] = 'О видео';
		$prepare_phrases[9][] = 'Объяснение';
		
		$prepare_phrases[10][] = 'Длительность';
		$prepare_phrases[10][] = 'Продолжительность';
		$prepare_phrases[10][] = 'Время клипа';
		$prepare_phrases[10][] = 'Время записи';
		
		$prepare_phrases[11][] = 'Дата публикации';
		$prepare_phrases[11][] = 'Публикация';
		$prepare_phrases[11][] = 'Опубликовано';
		$prepare_phrases[11][] = 'Дата опубликования';
		$prepare_phrases[11][] = 'Появилось';
		$prepare_phrases[11][] = 'Было опубликовано';
		
		$prepare_phrases[12][] = 'Название отсутствует';
		$prepare_phrases[12][] = 'Названия нет';
		$prepare_phrases[12][] = 'Нет названия';
		$prepare_phrases[12][] = 'Без названия';
		$prepare_phrases[12][] = 'Видео без названия';
		$prepare_phrases[12][] = 'Не названо';	
		$prepare_phrases[12][] = 'Без заголовка';	
		$prepare_phrases[12][] = 'Нет заголовка';		
		
		$prepare_phrases[13][] = 'Название видео';
		$prepare_phrases[13][] = 'Название';
		$prepare_phrases[13][] = 'Заголовок';
		$prepare_phrases[13][] = 'Заголовок видео';
		$prepare_phrases[13][] = 'Видео названо';
		
		$prepare_phrases[14][] = 'Описание отсутствует';
		$prepare_phrases[14][] = 'Описания нет';
		$prepare_phrases[14][] = 'Нет описания';
		$prepare_phrases[14][] = 'Не описано';
		$prepare_phrases[14][] = 'Без описания';
		$prepare_phrases[14][] = 'Без пояснения';
		$prepare_phrases[14][] = 'Нет пояснения';
		$prepare_phrases[14][] = 'Пояснения нет';
		$prepare_phrases[14][] = 'Без объяснения';
		$prepare_phrases[14][] = 'Нет объяснения';
		$prepare_phrases[14][] = 'Объяснения нет';
		
		$prepare_phrases[15][] = 'Этот раздел галереи пуст';
		$prepare_phrases[15][] = 'Этот раздел изображений пуст';
		$prepare_phrases[15][] = 'Этот раздел фотографий пуст';
		$prepare_phrases[15][] = 'Этот раздел фото пуст';
		$prepare_phrases[15][] = 'Этот раздел галереи отсутствует';
		$prepare_phrases[15][] = 'Этот раздел изображений отсутствует';
		$prepare_phrases[15][] = 'Этот раздел фотографий отсутствует';
		$prepare_phrases[15][] = 'Этот раздел фото отсутствует';
		$prepare_phrases[15][] = 'Эта категория галереи пуста';	
		$prepare_phrases[15][] = 'Эта категория изображений пуста';	
		$prepare_phrases[15][] = 'Эта категория фотографий пуста';	
		$prepare_phrases[15][] = 'Эта категория фото пуста';
		$prepare_phrases[15][] = 'В этом разделе нет изображений';
		$prepare_phrases[15][] = 'В этом разделе нет фотографий';
		$prepare_phrases[15][] = 'В этом разделе нет фото';
		$prepare_phrases[15][] = 'В этом разделе нет картинок';
		$prepare_phrases[15][] = 'В этой категории нет фото';
		$prepare_phrases[15][] = 'В этой категории нет фотографий';
		$prepare_phrases[15][] = 'В этой категории нет изображений';
		$prepare_phrases[15][] = 'В этой категории нет картинок';
		$prepare_phrases[15][] = 'Тут пока нет фото';
		$prepare_phrases[15][] = 'Тут пока нет изображений';
		$prepare_phrases[15][] = 'Тут пока нет фотографий';
		$prepare_phrases[15][] = 'Тут пока нет картинок';
		$prepare_phrases[15][] = 'Сюда пока не добавлено изображений';
		$prepare_phrases[15][] = 'Сюда пока не добавлено фото';
		$prepare_phrases[15][] = 'Сюда пока не добавлено фотографий';
		$prepare_phrases[15][] = 'Сюда пока не добавлено картинок';
		
		$prepare_phrases[16][] = 'Спонсоры';
		$prepare_phrases[16][] = 'Реклама';
		$prepare_phrases[16][] = 'Оплачено';
		$prepare_phrases[16][] = 'Оплаченные ссылки';
		$prepare_phrases[16][] = 'Ссылки от спонсоров';
		$prepare_phrases[16][] = 'Рекламные ссылки';
		$prepare_phrases[16][] = 'Оплаченные ссылки';
		$prepare_phrases[16][] = 'Рекламное место';
		$prepare_phrases[16][] = 'Рекламный блок';
		$prepare_phrases[16][] = 'Блок рекламы';
		$prepare_phrases[16][] = 'Блок спонсоров';
		$prepare_phrases[16][] = 'Спонсорский блок';
		$prepare_phrases[16][] = 'Оплаченный блок';
		$prepare_phrases[16][] = 'Выкупленный блок';
		$prepare_phrases[16][] = 'На правах рекламы';
		$prepare_phrases[16][] = 'Размещено спонсорами';
		$prepare_phrases[16][] = 'Платное размещение';
		$prepare_phrases[16][] = 'Оплаченное размещение';
		$prepare_phrases[16][] = 'Выкупленное размещение';
		$prepare_phrases[16][] = 'Рекламируем';
		$prepare_phrases[16][] = 'Рекламируется';
		$prepare_phrases[16][] = 'Ссылки на спонсоров';
		$prepare_phrases[16][] = 'Место куплено';
		$prepare_phrases[16][] = 'Место выкуплено';
		$prepare_phrases[16][] = 'Место рекламы';
		$prepare_phrases[16][] = 'Немного рекламы';
		$prepare_phrases[16][] = 'Место для рекламы';
		$prepare_phrases[16][] = 'Место для спонсоров';
		$prepare_phrases[16][] = 'Спонсорское место';
		$prepare_phrases[16][] = 'Размещение куплено';
		
		$prepare_phrases[17][] = '-';
		$prepare_phrases[17][] = '/';		
		$prepare_phrases[17][] = '|';
		$prepare_phrases[17][] = ':';
		
		
		foreach($prepare_phrases[0] as $key => $value){
			//echo "__" . $key . "<br>";
			
			
			//очищаем фразу от меток
			$phrase = str_replace('_mark-t_', '', $phrase);
			//echo $value . '==' . $phrase;
			
			if($value == $phrase){
				
				//update 30_11_12-08_57
				//логика была изменена. Теперь подготовленные фразы рандомятся один раз для одного сайта, а не для каждого файла шаблона на сайте				
				if( empty($this->pre_phrase_finished[$id_site][$key]) ){
					$rand_number_phrase = rand(0, count($prepare_phrases[$key]) - 1);	
					$this->pre_phrase_finished[$id_site][$key] = $rand_number_phrase;
				}
				
				$randomize_phrase = $prepare_phrases[$key][$this->pre_phrase_finished[$id_site][$key]];
				//$randomize_phrase = $prepare_phrases[$key][rand(0, count($prepare_phrases[$key]) - 1)];
				
				//echo count($prepare_phrases[$key]) . "__" . $key . "<br>";
				
				return $randomize_phrase;
			}
		}		
	}
	
	
	//ищет текстовые метки и заменяет фразы на рандомные из заготовленного массива
	protected function sercher_mark_t($content, $id_site){
	
		$pattern = '/_mark-t_.*?_mark-t_/iu';
		
		preg_match_all($pattern, $content, $matches);
		
		foreach($matches[0] as $key => $value){		
		
			$randomize_phrase = $this->meta_text_on_body_randomize($value, $id_site);
		
			$value_escape = $this->escape_meta_char($value);			
			
			$pattern = '/' . $value . '/u';
			$replace_body = $randomize_phrase . '#replaced#';	
			//echo $randomize_phrase . '_________что_' . $pattern . '_________на что_' . $replace_body . '<br>';
			//die;
			
			$content = preg_replace($pattern, $replace_body, $content);
			
		}
		
		return $content;		
	}
	
	
	public function create_rand_struct_template_level_2($id_site, $file_name, $count_sequence_1 = '', $count_sequence_2 = ''){
	
		$site = $this->select_site($id_site);
		$file_name_src = $_SERVER['DOCUMENT_ROOT'] . '/site/templates/' . $file_name;
		$lines = file($file_name_src);
		$content = implode('', $lines);
		
		//для рандомизации позиций колонок в центральном блоке
		$pattern = '/_mark_.*?_mark_/ius';
		
		//для рандомизации позиции шапки с изображениями
		$pattern_1 = '/_mark-1_.*?_mark-1_/ius';		
		
		preg_match_all($pattern, $content, $matches);
		preg_match_all($pattern_1, $content, $matches_1);
		
		
		if(empty($count_sequence_1) AND empty($count_sequence_2)){
		
			//$this->random_sequence = $this->gen_random_sequence($count_sequence = 3);
			if(empty($this->random_sequence_1)){
				$this->gen_random_sequence($count_sequence_1 = 3, $sequence_number = 1);
			}
			if(empty($this->random_sequence_2)){
				$this->gen_random_sequence($count_sequence_2 = 2, $sequence_number = 2);
			}
			
		}elseif(!empty($count_sequence_1) AND empty($count_sequence_2)){
		
			$this->gen_random_sequence($count_sequence_1, $sequence_number = 1);
			
			if(empty($this->random_sequence_2)){
				$this->gen_random_sequence($count_sequence_2 = 2, $sequence_number = 2);
			}
			
		}elseif(empty($count_sequence_1) AND !empty($count_sequence_2)){
		
			if(empty($this->random_sequence_1)){
				$this->gen_random_sequence($count_sequence_1 = 3, $sequence_number = 1);
			}
			
			$this->gen_random_sequence($count_sequence_2, $sequence_number = 2);
			
		}else{
			$this->gen_random_sequence($count_sequence_1, $sequence_number = 1);
			$this->gen_random_sequence($count_sequence_2, $sequence_number = 2);
		}
		
		/*
		echo '<pre>';
		print_r($matches[0]);
		echo '</pre>';
		*/

		//меняем порядок названий файлов на сгенерированный
		foreach($matches[0] as $key => $value){
		
			//echo $value . ' - Что заменяем до обработки<br>';	

					
			//экранируем мета символы в строке
			$value_escape = $this->escape_meta_char($value);
			
			$pattern = '/' . $value_escape . '[^#]/us';
			
			$replace_body = $matches[0][$this->random_sequence_1[$key]] . '#replaced#';
			
			//echo $replace_body . ' - На что заменяем до обработки<br>';
			
			//$replace_body = $this->escape_meta_char($replace_body);
			
			//echo $replace_body . ' - На что заменяем после обработки<br><br><br>';
			
			$content = preg_replace($pattern, $replace_body, $content);
			
			//echo $value . '___' .$replace . '<br>';
		}
		
		/*
		echo '<pre>';
		print_r($matches_1[0]);
		echo '</pre>';
		*/
		
		//меняем порядок названий файлов на сгенерированный в возможных позициях header
		foreach($matches_1[0] as $key => $value){		
		
			//удаляем метки из шаблона
			//$search = "_mark-1_";
			//$value = str_replace($search, '', $value);
		
			//экранируем мета символы в строке
			$value_escape = $this->escape_meta_char($value);
			
			$pattern = '/' . $value_escape . '([^#]|$)/us';			
			$replace_body = $matches_1[0][$this->random_sequence_2[$key]] . '#replaced#';
			
			$content = preg_replace($pattern, $replace_body, $content);
			
			//echo '___$value: ' . $value . '<br>___$pattern: ' . $pattern . '<br>___$replace_body: ' .$replace_body . '<br><br><br>';
		}
		
		//ищет и заменяет текстовые метки
		$content = $this->sercher_mark_t($content, $id_site);
	
		
		$content = str_replace('_mark-1_', '', $content);
		$content = str_replace('_mark_', '', $content);		
		$content = str_replace('_replaced_', '', $content);	
		$content = str_replace('#replaced#', '', $content);	
		$content = str_replace('_mb_position_', '', $content);	
		
		
		//записываем изменнённый файл
		$file_name_des = $_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/templates/' . $file_name;
		$w=fopen($file_name_des,'w');

		fwrite($w,$content);

		fclose($w);		
	}
	
	
	protected function escape_meta_char($replace_body){
	
			//экранируем метасимволы			
			$replace_body = str_replace('$', '\$', $replace_body);
			$replace_body = str_replace('/', '\/', $replace_body);
			$replace_body = str_replace('.', '\.', $replace_body);
			$replace_body = str_replace('^', '\^', $replace_body);
			$replace_body = str_replace('[', '\[', $replace_body);
			$replace_body = str_replace(']', '\]', $replace_body);
			$replace_body = str_replace(')', '\)', $replace_body);
			$replace_body = str_replace('(', '\(', $replace_body);
			$replace_body = str_replace('|', '\|', $replace_body);
			$replace_body = str_replace('"', '\"', $replace_body);
			$replace_body = str_replace("'", "\'", $replace_body);
			$replace_body = str_replace("?", "\?", $replace_body);
			$replace_body = str_replace("{", "\{", $replace_body);		
			$replace_body = str_replace("}", "\}", $replace_body);	
			$replace_body = str_replace("=", "\=", $replace_body);				
			
			return $replace_body;
	}
	
	
	public function create_rand_struct_template_level_1($id_site, $file_name, $count_sequence_1 = '', $count_sequence_2 = ''){
	
		$site = $this->select_site($id_site);			
		
		$file_name_src = $_SERVER['DOCUMENT_ROOT'] . '/site/templates/' . $file_name;
		
		$lines = file($file_name_src);
		$content = implode('', $lines);
	
		//для рандомизации позиций колонок в центральном блоке
		$pattern = '/_mark_.*?_mark_/ius';
		
		//для рандомизации позиции шапки с изображениями
		$pattern_1 = '/_mark-1_.*?_mark-1_/ius';
		
		//очищаем названия файлов от мусора
		preg_match_all($pattern, $content, $matches);
		preg_match_all($pattern_1, $content, $matches_1);
		
		//очистка колонок
		foreach($matches[0] as $key => $value){
			$search = "_mark_{include file=";
			$temp_value = str_replace($search, '', $value);
			
			$search = "}_mark_";
			$templates_files[] = str_replace($search, '', $temp_value);
		}
		
		//очистка header
		foreach($matches_1[0] as $key => $value){
			
			$search = "_mark-1_{include file=";
			$temp_value = str_replace($search, '', $value);			
			
			$search = "}_mark-1_";
			$templates_files_1[] = str_replace($search, '', $temp_value);
		}
						
		if(empty($count_sequence_1) AND empty($count_sequence_2)){
		
			//$this->random_sequence = $this->gen_random_sequence($count_sequence = 3);
			if(empty($this->random_sequence_1)){
				$this->gen_random_sequence($count_sequence_1 = 3, $sequence_number = 1);
			}
			if(empty($this->random_sequence_2)){
				$this->gen_random_sequence($count_sequence_2 = 2, $sequence_number = 2);
			}
			
		}elseif(!empty($count_sequence_1) AND empty($count_sequence_2)){
		
			$this->gen_random_sequence($count_sequence_1, $sequence_number = 1);
			
			if(empty($this->random_sequence_2)){
				$this->gen_random_sequence($count_sequence_2 = 2, $sequence_number = 2);
			}
			
		}elseif(empty($count_sequence_1) AND !empty($count_sequence_2)){
		
			if(empty($this->random_sequence_1)){
				$this->gen_random_sequence($count_sequence_1 = 3, $sequence_number = 1);
			}
			
			$this->gen_random_sequence($count_sequence_2, $sequence_number = 2);
			
		}else{
			$this->gen_random_sequence($count_sequence_1, $sequence_number = 1);
			$this->gen_random_sequence($count_sequence_2, $sequence_number = 2);
		}

		
		//меняем порядок названий файлов на сгенерированный
		foreach($templates_files as $key => $value){
		
			$pattern = '/' . $value . '[^_]/us';
			$replace = $templates_files[$this->random_sequence_1[$key]] . '_replaced_}';
			$content = preg_replace($pattern, $replace, $content);
			
			//echo $value . '___' .$replace . '<br>';
		}
		//echo $content . '<br>';
		
		//меняем порядок названий файлов на сгенерированный в возможных позициях header
		foreach($templates_files_1 as $key => $value){
		
			$pattern = '/' . $value . '[^_]/us';
			$replace = $templates_files_1[$this->random_sequence_2[$key]] . '_replaced_}';
			$content = preg_replace($pattern, $replace, $content);
			
			//echo $value . '___' .$replace . '<br>';
		}
		//echo $content . '<br>';
		
		//удаляем метки	
		
		$content = str_replace('_mark-1_', '', $content);
		$content = str_replace('_mark_', '', $content);		
		$content = str_replace('_replaced_', '', $content);	
		$content = str_replace("{include file='mb_position.html'}", '', $content);			
		
		//записываем изменнённый файл
		$file_name_des = $_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/templates/' . $file_name;
		$w=fopen($file_name_des,'w');

		fwrite($w,$content);

		fclose($w);
		
		/*
		##отладка
		echo '<pre>';
		print_r($templates_files);
		print_r($result_order_templates_files);		
		echo '</pre>';
		*/
	}
	
	
	public function copy_files($id_site){
		
		$file_names_templates[] = 'ads_block_1.html';
		$file_names_templates[] = 'ads_block_2.html';
		$file_names_templates[] = 'ads_block_3.html';
		/*
		//fix 09_11_12 22_58
		$file_names_templates[] = 'links_block_1.html';
		$file_names_templates[] = 'links_block_2.html';
		$file_names_templates[] = 'links_block_3.html';
		*/
		$file_names_templates[] = 'mb_position.html';
		$file_names_templates[] = 'pagination.html';
		$file_names_templates[] = 'meta_footer.html';
		
		$file_names_lvl_1[] = 'robots.txt';

		
		foreach($file_names_templates as $key => $value){
			$file_name_src = $_SERVER['DOCUMENT_ROOT'] . '/site/templates/' . $value;		
			$lines = file($file_name_src);
			$content = implode('', $lines);
			
			$file_name_des = $_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/templates/' . $value;
			$w=fopen($file_name_des,'w');
			fwrite($w,$content);
			fclose($w);
		}
		
		foreach($file_names_lvl_1 as $key => $value){
			$file_name_src = $_SERVER['DOCUMENT_ROOT'] . '/site/' . $value;		
			$lines = file($file_name_src);
			$content = implode('', $lines);
			
			$file_name_des = $_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/' . $value;
			$w=fopen($file_name_des,'w');
			fwrite($w,$content);
			fclose($w);
		}
	}
	
	
	public function mkdirs_site($id_site){
	
		$result = '';
		//основная
		$pathname = $_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/';
		
		if(!is_dir($pathname)){
		
			if (!mkdir($pathname, $mode = 0755)){
				die('Не удалось создать основную директорию...');
			}			
			
		}else{
			$result .= 'Основная директория: "' . $pathname . '" уже существует.<br>';
		}
		
		$dir_array[] = 'css';
		//$dir_array[] = 'js';
		$dir_array[] = 'plugins';
		$dir_array[] = 'templates';
		$dir_array[] = 'templates_c';	
		
		
		foreach($dir_array as $key => $value){
			//основная
			$pathname = $_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/' . $value . '/';
			
			if(!is_dir($pathname)){
			
				if(!mkdir($pathname, $mode = 0755)){
					die("Не удалось создать директорию $value...");
				}			
				
			}else{
				$result .=  'Директория: "' . $pathname . '" уже существует.<br>';
			}
		}
		
		return $result;
	}
	
	
	//копирование файлов и папок бирж ссылок
	public function mkdirs_and_files_for_links_change($id_site){

		$result = '';
		//папка бирж ссылок
		$pathname = $_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/money_house/';
		
		if(!is_dir($pathname)){
		
			if (!mkdir($pathname, $mode = 0755)){
				die('Не удалось создать основную директорию...');
			}			
			
		}else{
			$result .= 'Основная директория: "' . $pathname . '" уже существует.<br>';
		}
		
		$dir_array[] = '4e7b5aa94775b09ccb08bad421666f2a5b275a0b';
		$dir_array[] = '1284bf6b955e0d3582972f3b20d80e38';
		$dir_array[] = 'mainlinkcxxc_39841';
		$dir_array[] = 'mainlinkcxxc_39841/data';
		$dir_array[] = 'setlinks_4681f';
		$dir_array[] = 'setlinks_4681f/cache';		
		
		$files_array[] = '4e7b5aa94775b09ccb08bad421666f2a5b275a0b/linkfeed.php';
		$files_array[] = '1284bf6b955e0d3582972f3b20d80e38/sape.php';
		
		$files_array[] = 'mainlinkcxxc_39841/mainlink.php';
		//$files_array[] = 'mainlinkcxxc_39841/3EAFE7A979C801DE51730FA748B5F9AE.sec';
		$files_array[] = 'mainlinkcxxc_39841/.htaccess';
		$files_array[] = 'mainlinkcxxc_39841/data/.htaccess';
		
		$files_array[] = 'setlinks_4681f/slarticles.php';
		$files_array[] = 'setlinks_4681f/slclient.php';
		$files_array[] = 'setlinks_4681f/slconfig.php';
		$files_array[] = 'setlinks_4681f/slsimple.php';
		$files_array[] = 'setlinks_4681f/slsimple_f.php';
		$files_array[] = 'setlinks_4681f/slsmarty.php';
		$files_array[] = 'setlinks_4681f/slssi.php';
		
				
		
		foreach($dir_array as $key => $value){
			//основная
			$pathname = $_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/money_house/' . $value . '/';
			
			if(!is_dir($pathname)){
			
				if(!mkdir($pathname, $mode = 0777)){
					die("Не удалось создать директорию $value...");
				}			
				
			}else{
				$result .= 'Директория: "' . $pathname . '" уже существует.<br>';
			}
		}
		
		
		foreach($files_array as $key => $value){
			$file_name_src = $_SERVER['DOCUMENT_ROOT'] . '/site/money_house/' . $value;		
			$lines = file($file_name_src);
			$content = implode('', $lines);
			
			$file_name_des = $_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/money_house/' . $value;
			$w=fopen($file_name_des,'w');
			fwrite($w,$content);
			fclose($w);
		}
		
		return $result;
	}	
	
	
	//создание директорий для галереи
	public function mkdirs_and_files_for_gallery($id_site){

		$result = '';
		//папка бирж ссылок
		$pathname = $_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/images/';
		
		if(!is_dir($pathname)){
		
			if (!mkdir($pathname, $mode = 0755)){
				die('Не удалось создать основную директорию...');
			}			
			
		}else{
			$result .= 'Основная директория: "' . $pathname . '" уже существует.<br>';
		}
		
		$dir_array[] = 'favicon';
		$dir_array[] = 'images';
		$dir_array[] = 'video_thumb';		
		
		$files_array[] = 'favicon/index.html';
		$files_array[] = 'images/index.html';
		$files_array[] = 'images/index.html';
		$files_array[] = 'index.html';
						
		
		foreach($dir_array as $key => $value){
			//основная
			$pathname = $_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/images/' . $value . '/';
			
			if(!is_dir($pathname)){
			
				if(!mkdir($pathname, $mode = 0777)){
					die("Не удалось создать директорию $value...");
				}			
				
			}else{
				$result .= 'Директория: "' . $pathname . '" уже существует.<br>';
			}
		}
		
		
		//копирует файлы
		foreach($files_array as $key => $value){
			$file_name_src = $_SERVER['DOCUMENT_ROOT'] . '/site/images/' . $value;		
			$lines = file($file_name_src);
			$content = implode('', $lines);
			
			$file_name_des = $_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/images/' . $value;
			$w=fopen($file_name_des,'w');
			fwrite($w,$content);
			fclose($w);
		}
		
		return $result;
	}
	
	
	//add 02_11_12 22_17
	public function update_files_by_sample(){
		
		$sites = $this->select_all_info_sites();
		
		//обновление classes
		foreach($sites as $key => $value){
			
			$result_update_files[$value["domain"]]['classes']['errors'] = $this->copy_dirs_and_files($value['id'], 'classes');
			
			if( !empty($result_update_files[$value["domain"]]['classes']['errors']) ){
				$result_update_files[$value["domain"]]['classes']['legend'] = 'Если нет ошибок чтения/записи файлов, но есть ошибки создания директорий, то всё ОК, просто директории уже были.';
			}
		}		
		
		//обновление site/money_house
		foreach($sites as $key => $value){
			
			$result_update_files[$value["domain"]]['site/money_house']['errors'] = $this->copy_dirs_and_files($value['id'], 'money_house', '/site');
			
			if( !empty($result_update_files[$value["domain"]]['site/money_house']['errors']) ){
			
				$result_update_files[$value["domain"]]['site/money_house']['legend'] = 'Если нет ошибок чтения/записи файлов, но есть ошибки создания директорий, то всё ОК, просто директории уже были.';
			}			
		}
		
		
		//обновление index.php сайтов
		$content_sample = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/site/index.php');
		
		if($content_sample == false){
			$result_update_files[$value["domain"]]['site/index.php'] .= '<span style="color:red;">Ошибка чтения ' . $_SERVER['DOCUMENT_ROOT'] . '/site/index.php проверте права на чтение</span>';
		}
		
		foreach($sites as $key => $value){
		
			$content_uniq = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/site_' . $value["id"] . '/index.php');
		
			if($content_uniq == false){
				//return 'Ошибка чтения DOCUMENT_ROOT/site_' . $value["id"] . '/index.php проверте права на чтение';
				$result_update_files[$value["domain"]]['site/index.php'] .= '<span style="color:red;">Ошибка чтения DOCUMENT_ROOT/site_' . $value["id"] . '/index.php проверте права на чтение</span>';
			}
		
			//устанавливаем переменную отвечающую за определение сайта как тестового в 0
			$replace_pattern = '${1}0';
			$content_finish = preg_replace('/(\$this_test_site = )[0-9]*/', $replace_pattern, $content_sample);		
			
			//устанавливаем идентификатор сайта
			$replace_pattern = '${1}' . $value["id"];
			$content_finish = preg_replace('/(\$id_site = )[0-9]*/', $replace_pattern, $content_finish);
			
			//находим количество статей на страницу из уникализированного файла сайта		
			preg_match('/(\$per_page = )[0-9]*/', $content_uniq, &$matches);
			$per_page = $matches[0];
			
			//устанавливаем количество статей на страницу в контенте взятом из образца	
			//$replace_pattern = '${1}' . $per_page;
			$replace_pattern = $per_page;
			$content_finish = preg_replace('/(\$per_page = )[0-9]*/', $replace_pattern, $content_finish);
			
			
			//находим количество ссылок в пагинации из уникализированного файла сайта		
			preg_match('/(\$num_page = )[0-9]*/', $content_uniq, &$matches);
			$num_page = $matches[0];
			
			//устанавливаем количество ссылок в пагинации в контенте взятом из образца
			$replace_pattern = $num_page;
			//$replace_pattern = '${1}' . $num_page;
			$content_finish = preg_replace('/(\$num_page = )[0-9]*/', $replace_pattern, $content_finish);
			
			
			//находим количество изображений в шапке из уникализированного файла сайта		
			preg_match('/(\$header_images_count = )[0-9]*/', $content_uniq, &$matches);
			$count_images = $matches[0];
			
			//устанавливаем количество изображений в шапке в контенте взятом из образца
			//$replace_pattern = '${1}' . $count_images;
			$replace_pattern = $count_images;
			$content_finish = preg_replace('/(\$header_images_count = )[0-9]*/', $replace_pattern, $content_finish);
			
			
			//находим количество изображений в шапке из уникализированного файла сайта		
			preg_match('/(\$per_page_gallery = )[0-9]*/', $content_uniq, &$matches);
			$per_page_gallery = $matches[0];			
			
			
			//устанавливаем количество изображений на странице галереи в контенте взятом из образца
			//$replace_pattern = '${1}' . $per_page_gallery;	
			$replace_pattern = $per_page_gallery;				
			$content_finish = preg_replace('/(\$per_page_gallery = )[0-9]*/', $replace_pattern, $content_finish);
			
			
			
			
			////////////////////////////////////////////
			/////////////////////////////// - ПОЛЕЗНО!!!
			////////////////////////////////////////////
			/*
			//temp debug 04_11_12 17_42
			//ПОВТОРНАЯ РАНДОМИЗАЦИЯ ОСНОВНЫХ УСТАНОВОК САЙТА
			//устанавливаем количество статей на страницу
			$replace_pattern = '${1}' . rand(5, 10);
			$content_finish = preg_replace('|(\$per_page = )[0-9]*|', $replace_pattern, $content_finish);	

			//устанавливаем количество ссылок в пагинации
			$replace_pattern = '${1}' . rand(2, 10);
			$content_finish = preg_replace('|(\$num_page = )[0-9]*|', $replace_pattern, $content_finish);	


			//устанавливаем количество изображений в шапке
			while(true){
				$count_images_rand = rand(3, 14);
				if($count_images_rand < 8 OR $count_images_rand > 10){
					break;
				}
			}
			$replace_pattern = '${1}' . $count_images_rand;
			$content_finish = preg_replace('|(\$header_images_count = )[0-9]*|', $replace_pattern, $content_finish);	

			//устанавливаем количество изображений на странице галереи
			$replace_pattern = '${1}' . rand(10, 36);		
			$content_finish = preg_replace('|(\$per_page_gallery = )[0-9]*|', $replace_pattern, $content_finish);			
			//end temp debug 04_11_12 17_42		
			*/
			
			

			
								
			//указываем путь к новому файлу
			$file_name_des = $_SERVER['DOCUMENT_ROOT'] . '/site_' . $value["id"] . '/index.php';
			$w=fopen($file_name_des,'w');
			
			/*
			if( $w != fopen($file_name_des,'w') ){
				return 'Ошибка открытия файла DOCUMENT_ROOT/site_' . $value["id"] . '/index.php проверте права на чтение и запись';
			}
			*/

			//fwrite($w, $content_finish);

			if( !fwrite($w, $content_finish) ){
				//return 'Ошибка записи DOCUMENT_ROOT/site_' . $value["id"] . '/index.php проверте права на запись';
				$result_update_files[$value["domain"]]['site/index.php'] .= '<span style="color:red;">Ошибка записи DOCUMENT_ROOT/site_' . $value["id"] . '/index.php проверте права на запись</span>';
			}
			
			fclose($w);	
		
			if( empty($result_update_files[$value["domain"]]['site/index.php']) ){
				$result_update_files[$value["domain"]]['site/index.php'] = '<span style="color:green;">Ok</span>';
			}
			
		}
		
		return $result_update_files;
	}
	
	
	//update 02_11_12 23_13
	//$sample_root_folder_for_path_big_1lvl - любой путь в образце глубже чем в 1 папку, пример site/money_house - $url = site, $sample_root_folder_for_path_big_1lvl = /money_house
	public function copy_dirs_and_files($id_site, $url, $sample_root_folder_for_path_big_1lvl = ''){			
			
			//Проверяем, является ли директорией 
			if (is_dir($_SERVER['DOCUMENT_ROOT'] . $sample_root_folder_for_path_big_1lvl . '/' . $url)) { 
			
				//Проверяем, была ли открыта директория 
				if ($dir = opendir($_SERVER['DOCUMENT_ROOT'] . $sample_root_folder_for_path_big_1lvl . '/' . $url)) { 
					//Сканируем директорию 
					while ($file = readdir($dir)){ 
						//Убираем лишние элементы 
						if ($file != "." && $file != "..") { 

							//Если папка, то записываем в массив $folders 
							if(is_dir($_SERVER['DOCUMENT_ROOT'] . $sample_root_folder_for_path_big_1lvl . '/' . $url . '/' . $file)) { 
								$folders[] = $url . '/' . $file; 
							}else{//Если файл, то пишем в массив $files 
								$files[] = $url . '/' . $file;
							} 
						} 
					} 
				}
				
				//Закрываем директорию 
				closedir($dir); 
			} 			
			
			//если это первая дирриктория то создаём её, вложенные дирриктории будут создаваться в цикле
			if(!strpos($url, '/')){
				//создание материнской директории
				//echo $_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/' . $url . '<br><br><br>';
				$file_name_des = $_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/' . $url;
				//mkdir($file_name_des, 0755);
				if( !mkdir($file_name_des, 0755) ){
					$result_errors[] = '<span style="color:red;">Ошибка создания основной директории (' . $_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/' . $url . '), проверьте права на папку в которой она создаётся.</span>';
				}
			} 
			
			foreach($files as $key => $file){
			
				//echo $_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/' . $file . '<br><br><br>';
				$content = file_get_contents($_SERVER['DOCUMENT_ROOT'] . $sample_root_folder_for_path_big_1lvl . '/' . $file); 
				
				if( $content == false){
					//return 'Ошибка чтения файла - ' . $_SERVER['DOCUMENT_ROOT'] . '/' . $file . '. Проверьте права на чтение.';
					$result_errors[] = '<span style="color:red;">Ошибка чтения файла - ' . $_SERVER['DOCUMENT_ROOT'] . '/' . $file . '. Проверьте права на чтение.</span>';
				}
				
				$file_name_des = $_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/' . $file;
				//file_put_contents($file_name_des, $content); 
				
				if( !file_put_contents($file_name_des, $content)){
					//return 'Ошибка записи файла - ' . $_SERVER['DOCUMENT_ROOT'] . '/' . $file . '. Проверьте права на запись.';
					$result_errors[] = '<span style="color:red;">Ошибка записи файла - ' . $_SERVER['DOCUMENT_ROOT'] . '/' . $file . '. Проверьте права на запись.</span>';
				}
			}
			
			foreach($folders as $key => $folder){
				
				//echo $_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/' . $folder . '<br><br><br>';
				$file_name_des = $_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/' . $folder;
				//mkdir($file_name_des, 0755); 
				if( !mkdir($file_name_des, 0755)){
					//return 'Ошибка создания директории, проверьте права на папку в которой она создаётся.';
					$result_errors[] = '<span style="color:red;">Ошибка создания директории (' . $_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/' . $folder . '), проверьте права на папку в которой она создаётся.</span>';
				}				
				//рекурсия для вложенных папок
				//echo $url . '/' . $folder . '<br><br><br>';
				$this->copy_dirs_and_files($id_site, $folder, $sample_root_folder_for_path_big_1lvl);
			}
			
			
			if( count($result_errors) == 0 OR (count($result_errors) == 1 AND empty($result_errors[0]) )){
				return true;
			}else{
				return $result_errors;
			}			
	} 
	
	
	//создание файла с данными для жукладочника
	public function create_files_for_bagbookmark(){
	
		$ids_sites = $this->select_sites_primary_grab_1();
		
		foreach($ids_sites as $key => $value){
	
			$phrases = $this->select_phrases_and_site_name_by_id_site($value['id_sites'], 250);
			
			$count_phrases = count($phrases);
			
			$file_content = $phrases[0]['name_site'] . ': ';
			$title_soc = $phrases[0]['name_site'] . ': ';
			
			$part_file_content = ($count_phrases / 3);
			
			$flag_1_3 = 0;
			$flag_2_3 = 0;
			$flag_3_3 = 0;
						
						
			$count_loop = 0;
			foreach($phrases as $key_1 => $value_1){	

				if($count_loop == 0){
					$title_soc .= '{' . $value_1['search_phrase'];
				}
				
				$count_loop++;
				if($count_loop >= count($phrases)){
					$title_soc .= $value_1['search_phrase'] . '}';
				}else{
					$title_soc .= '|' . $value_1['search_phrase'];
				}

				
				
				if($key_1 <= $part_file_content){				
					
					if($flag_1_3 == 1){
						$file_content .= '|' . $value_1['search_phrase'];
					}else{
						$file_content .= '{' . $value_1['search_phrase'];
					}
					
					$flag_1_3 = 1;
					
				}elseif($key_1 <= $part_file_content * 2){
							
					if($flag_2_3 == 1){
						$file_content .= '|' . $value_1['search_phrase'];
					}else{
						$file_content .= '}, {' . $value_1['search_phrase'];
					}
					
					$flag_2_3 = 1;
					
				}elseif($key_1 > $part_file_content * 2){
				
					if($key_1 == $count_phrases - 1){
						$file_content .=  $value_1['search_phrase'] . '}.';					
						break;
					}
				
					if($flag_3_3 == 1){
						$file_content .= '|' . $value_1['search_phrase'];
					}else{
						$file_content .= '}, {' . $value_1['search_phrase'];
					}
					
					$flag_3_3 = 1;				
					
				}
				
			}
			
			
			
			$tag_text = mb_strtolower($phrases[0]['name_site'], 'utf-8');

			$count_loop_1 = 0;
			foreach($phrases as $key_2 => $value_2){
				$rand = rand(0,1);
				if($rand == 1){
					$tag_text .= ', ' . $value_2['search_phrase'];
					$count_loop_1++;
				}
				
				if($count_loop_1 >= 3){
					break;
				}
			}
			
			
			/*
			echo '<pre>';
			print_r($phrases);
			echo $file_content;
			echo '</pre>';
			*/			
			
			
			if(true){ //$phrases[0]['id_tags'] == 27  //<Descriptors />
				$cat_tag = '<Descriptors />
    <CategoryType>Random</CategoryType>';
			}elseif($phrases[0]['id_tags'] == 28){
				$cat_tag = '	<Descriptors />
			<CategoryType>ByTag</CategoryType>';
			}else{
				//доделать - и в создании общего файла тоже
				$file_name_tags = $_SERVER['DOCUMENT_ROOT'] . '/words/bookmark/persons.txt'; //' . $phrases[0]['name_tag'] . '
				$tags_array = file($file_name_tags); 
				
				$cat_tag = '	<Descriptors>
					';

				foreach($tags_array as $key_1 => $value_1){
					$cat_tag .= '	<string>' . trim($value_1) . '</string>
					';
				}
				
				$cat_tag .= '	</Descriptors>
						<CategoryType>Union</CategoryType>';
			}
			
			/*
			$phrases[0]['domain'] = $this->convert_encode($phrases[0]['domain']);
			$tag_text = $this->convert_encode($tag_text);
			$file_content = $this->convert_encode($file_content);
			$title_soc = $this->convert_encode($title_soc);
			$cat_tag = $this->convert_encode($cat_tag);
			*/
			
			
			$xml_bookmark ='<?xml version="1.0"?>
<Content xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <Url>http://' . $phrases[0]['domain'] . '</Url>
  <TagText>' . $tag_text . '</TagText>
  <Category>
    ' . $cat_tag . '
  </Category>
  <Text>' . $file_content . '</Text>
  <Title>' . $title_soc . '</Title>
</Content>';
			
			
			//запись данных в файл		
			$file_name_des = $_SERVER['DOCUMENT_ROOT'] . '/site_' . $value['id_sites'] . '/bagbookmark_' . $value['id_sites'] . '.xml';
			file_put_contents($file_name_des, $xml_bookmark); 
			
			$file_name_des = $_SERVER['DOCUMENT_ROOT'] . '/bagbookmark/bagbookmark_' . $value['id_sites'] . '.xml';
			file_put_contents($file_name_des, $xml_bookmark); 
		
		}
	}
	
	
	//создание общего файла с данными для соц. закладок
	public function create_file_for_bagbookmark_common(){
	
		$ids_sites = $this->select_sites_primary_grab_1();
		
		foreach($ids_sites as $key_1 => $value){			
		
			$phrases = $this->select_phrases_and_site_name_by_id_site($value['id_sites'], 250);
			
			$count_phrases = count($phrases);
			
			$file_content = $phrases[0]['name_site'] . ': ';
			$title_soc = $phrases[0]['name_site'] . ': ';
			
			$part_file_content = ($count_phrases / 3);
			
			$flag_1_3 = 0;
			$flag_2_3 = 0;
			$flag_3_3 = 0;
			
			$count_loop = 0;
			foreach($phrases as $key => $value){
			
				if($count_loop == 0){
					$title_soc .= '{' . $value['search_phrase'];
				}
				
				$count_loop++;
				if($count_loop >= count($phrases)){
					$title_soc .= $value['search_phrase'] . '}';
				}else{
					$title_soc .= '|' . $value['search_phrase'];
				}				
				
				if($key <= $part_file_content){				
					
					if($flag_1_3 == 1){
						$file_content .= '|' . $value['search_phrase'];
					}else{
						$file_content .= '{' . $value['search_phrase'];
					}
					
					$flag_1_3 = 1;
					
				}elseif($key <= $part_file_content * 2){
							
					if($flag_2_3 == 1){
						$file_content .= '|' . $value['search_phrase'];
					}else{
						$file_content .= '}, {' . $value['search_phrase'];
					}
					
					$flag_2_3 = 1;
					
				}elseif($key > $part_file_content * 2){
				
					if($key == $count_phrases - 1){
						$file_content .=  $value['search_phrase'] . '}.';					
						break;
					}
				
					if($flag_3_3 == 1){
						$file_content .= '|' . $value['search_phrase'];
					}else{
						$file_content .= '}, {' . $value['search_phrase'];
					}
					
					$flag_3_3 = 1;				
					
				}
				
			}			
			
			/*
			$tag_text = mb_strtolower($phrases[0]['name_site'], 'utf-8');		
			foreach($phrases as $key => $value){
				$rand = rand(0,1);
				if($rand == 1){
					$tag_text .= ', ' . $value['search_phrase'];
				}
			}
			*/
			
			/*
			echo '<pre>';
			print_r($phrases);
			echo $file_content;
			echo '</pre>';
			*/
			if(true){ //$phrases[0]['id_tags'] == 27
				$cat_tag = 'Random';
			}elseif($phrases[0]['id_tags'] == 28){
				$cat_tag = 'ByTag';
			}else{
				
				$file_name_tags = $_SERVER['DOCUMENT_ROOT'] . '/words/bookmark/persons.txt'; //' . $phrases[0]['name_tag'] . '
				$tags_array = file($file_name_tags); 				

				foreach($tags_array as $key => $value){
					if(!empty($cat_tag) AND !empty($value)){
						$sep = ', ';
					}else{
						$sep = '';
					}
					$cat_tag .= $sep . trim($value);
				}
				
				$cat_tag .= $sep . 'Union';
			}			

			if(!empty($file_content) AND !empty($phrases[0]['domain'])){
			
				$phrases[0]['domain'] = $this->convert_encode($phrases[0]['domain']);
				$title_soc = $this->convert_encode($title_soc);
				$file_content = $this->convert_encode($file_content);
				$cat_tag = $this->convert_encode($cat_tag);
				
				$list[$key_1][] = $phrases[0]['domain'];
				$list[$key_1][] = $title_soc;
				$list[$key_1][] = $file_content;
				$list[$key_1][] = $cat_tag;			
			
				//$csv_content .= $phrases[0]['domain'] . ';"' . $file_content . '";"' . $file_content . '";"' . $cat_tag . '"\n';
				//$csv_content .= $phrases[0]['domain'] . ';' . $title_soc . ';' . $file_content . ';' . $cat_tag . '\n';
			}
		}
		
		$file_name_des = $_SERVER['DOCUMENT_ROOT'] . '/bagbookmark/b_common.csv';
		
		//запись данных в файл	
		$fp = fopen($file_name_des, 'w');
		foreach ($list as $fields) {
			fputcsv($fp, $fields, ';');
		}
		fclose($fp);		
		
		//создание индивидуальных файлов
		$this->create_files_for_bagbookmark();
		
		/*
		//запись данных в файл	
		file_put_contents($file_name_des, $csv_content); 
		*/
	}
	
	
	protected function convert_encode($str){
	
		$str = mb_convert_encoding($str, 'cp1251', 'utf-8');
		
		return $str;	
	}	
		
}
?>