<?
// no direct access
defined( '_EXEC' ) or die( 'Restricted access' );

require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/models/root_model.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/models/sql_model.class.php');

class Grabs extends Sql_model{

	protected $user_agent = 'Opera/10.00 (Windows NT 5.1; U; ru) Presto/2.2.0';
	protected $browser_referer = 'http://www.google.ru/';
	
	
	protected function generate_rnd_string($length = 15){
	  $chars = 'abcdefghijklmnopqrstuvwxwzABCDEFGHIJKLMNOPQRSTUVWXWZ123456789';
	  $numChars = strlen($chars);
	  $string = '';
	  for ($i = 0; $i < $length; $i++) {
		$string .= substr($chars, rand(1, $numChars) - 1, 1);
	  }
	  return $string;
	}
	
	
	//второй параметр задаёт глубину парсинг гугла
	public function google_main_grab($phrase, $count_result = 10){
		$this->browser_referer = 'http://www.google.ru/';
		
		//$phrase = mb_convert_encoding($phrase, "UTF-8", "CP1251");

		$query = urlencode($phrase);		
		
		$count_loop = 0;
		
		while($count_loop < $count_result){	
			sleep(1);
			if($count_loop != 0){
				$start = "&start=$count_loop";
			}
			
			// &tbs=li:1&sa - точное совпадение
			$url = "http://www.google.ru/search?aq=f&sourceid=opera&ie=UTF-8&q=$query$start&tbs=li:1&sa";
			
			
			$html .= $this->curl_connect($url);
			unset($url, $start);
			//$html .= $temp_html;
			
			/*
			$lench_temp_html = strlen($temp_html);
			
			echo '<br>' . $lench_temp_html . '<br>';
			echo '<br>Занято памяти сейчас: ' . memory_get_usage() . ' максимально: ' . memory_get_peak_usage() . '<br>';
			
			if($lench_temp_html < 100000){
				$html .= $temp_html;
			}else{
				$temp_html = '';
			}			
			$lench_temp_html = 0;
			*/
			
			$count_loop = $count_loop + 10;
		}
		
		unset($count_loop);
		
		
		return $html;
	}	
	
	
	public function google_img_grab($urls_array, $search_phrase, $id_search_phrase, $id_site){
	
		/*
		require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/parsers.class.php');
		$sql_model = new Sql_model;	
		
		$search_phrase_result = $sql_model->select_search_phrase_by_phrase($search_phrase);
		$id_search_phrase = $search_phrase_result['id'];
		
		if(empty($id_search_phrase)){
			$id_search_phrase = $sql_model->insert_search_phrase($search_phrase);
		}
		*/
		
		//вызываем у переданного экземпляра метод
		//$id_search_phrase = $grabs->select_or_insert_search_phrase($search_phrase);
	
		foreach($urls_array as $value){
			
		
			$file_name = $this->generate_rnd_string(15);
			
			$isert_id = $this->insert_img($id_search_phrase, $file_name . '.jpg', $value['alt'], $value['urls']);
			$this->logs_grab($isert_id, $title = 'insert_img id');
			
			$ids_images .= 	$isert_id;
			//echo '$isert_id-' . $isert_id . '!!!!!<br>';
			
			if((int)$isert_id > 0){
			
				//echo '$isert_id-' . $isert_id . '!!!!!ВСТАВКА В БАЗУ<br>';
				
				$imgContent = file_get_contents($value['urls']);				
				file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/images/images/' . $file_name . '.jpg', $imgContent);
			}
		}
		
		return $ids_images;
	}
	
	
	public function google_video_thumb_grab($urls_imgs_array_top_5, $id_site){
		
		//if(is_array($urls_imgs_array_top_5)){
		
			foreach($urls_imgs_array_top_5 as $key => $value){
				$imgContent = file_get_contents($value['img']);
				$file_name = $this->generate_rnd_string(15);
				file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/images/video_thumb/' . $file_name . '.jpg', $imgContent);
				
				$urls_imgs_array_top_5[$key]['img'] = $file_name . '.jpg';
			}
		
			return $urls_imgs_array_top_5;
		//}
	}
	
	
	public function google_video_grab($urls_imgs_array_top_5, $id_search_phrase){
		
		//if(is_array($urls_imgs_array_top_5)){
		
			foreach($urls_imgs_array_top_5 as $key => $value){			
				$ids_videos[] = $this->insert_video($id_search_phrase, $value['img'], $value['video'], $value['title'], $value['description'], $value['duration'], $value['time_publish'], $value['source'], $value['user_add']);
			}
			
			return $ids_videos;
		//}
	}
	
	
	public function google_img_html_grab($phrase){
		$this->browser_referer = 'http://www.google.ru/';
		
		//$phrase = mb_convert_encoding($phrase, "UTF-8", "CP1251");
		//$phrase = str_replace(' ', '+', $phrase);
		$phrase = urlencode($phrase);
		
		//$count_loop = 0;					
		//while($count_loop < $count_result){	
			//sleep(1);
			
			// &tbs=li:1&sa - точное совпадение
			//$url = "http://www.google.ru/search?aq=f&sourceid=opera&ie=UTF-8&q=$query$start&tbs=li:1&sa";
			//$url = "http://www.google.ru/search?q=" . $phrase . "&hl=ru&newwindow=1&prmd=imvns&source=lnms&tbm=isch&ei=oguUT4-TN8bysgbi_6zNBA&sa=X&oi=mode_link&ct=mode&cd=2&ved=0CCUQ_AUoAQ&biw=1066&bih=747&sei=pQuUT87XJYXltQbltqifBA#q=%D1%85%D0%BE%D0%BB%D0%BE%D0%B4%D0%BD%D0%BE%D0%B5+%D0%BE%D1%80%D1%83%D0%B6%D0%B8%D0%B5&hl=ru&newwindow=1&tbm=isch&prmd=imvns&source=lnt&tbs=isz:l&sa=X&ei=pQuUT_eiM4XmtQbR9aDPBA&ved=0CAwQpwUoAQ&bav=on.2,or.r_gc.r_pw.r_qf.,cf.osb&fp=f160ac09ed30fecc&biw=1066&bih=747";
			//$url = "http://www.google.ru/search?q=" . $phrase . "&hl=ru&newwindow=1&prmd=imvns&source=lnms&tbm=isch&oi=mode_link&ct=mode&cd=2&biw=1066&bih=747";
			//$url = "http://www.google.ru/search?q=" . $phrase . "&hl=ru&newwindow=1&prmd=imvns&source=lnms&tbm=isch&ei=oguUT4-TN8bysgbi_6zNBA&sa=X&oi=mode_link&ct=mode&cd=2&ved=0CCUQ_AUoAQ&biw=1066&bih=747&sei=pQuUT87XJYXltQbltqifBA#q=%D1%85%D0%BE%D0%BB%D0%BE%D0%B4%D0%BD%D0%BE%D0%B5+%D0%BE%D1%80%D1%83%D0%B6%D0%B8%D0%B5&hl=ru&newwindow=1&tbs=isz:l&tbm=isch&prmd=imvns&source=lnt&sa=X&ei=cAyUT8fkIcPetAbK46DfBA&ved=0CA0QpwUoAQ&bav=on.2,or.r_gc.r_pw.r_qf.,cf.osb&fp=603862a403244fee&biw=1066&bih=747";
			//$url = "http://www.google.ru/search?q=" . $phrase . "&hl=ru&newwindow=1&prmd=imvns&source=lnms&tbm=isch&ei=oguUT4-TN8bysgbi_6zNBA&sa=X&oi=mode_link&ct=mode&cd=2&ved=0CCUQ_AUoAQ&biw=1066&bih=747&sei=pQuUT87XJYXltQbltqifBA#q=%D1%85%D0%BE%D0%BB%D0%BE%D0%B4%D0%BD%D0%BE%D0%B5+%D0%BE%D1%80%D1%83%D0%B6%D0%B8%D0%B5&hl=ru&newwindow=1&tbs=iszw:1024,iszh:1024,isz:l&tbm=isch&prmd=imvns&source=lnt&sa=X&ei=ugyUT8_ELIrVtAbV0Om7BA&ved=0CA0QpwUoAQ&fp=1&biw=1066&bih=747&bav=on.2,or.r_gc.r_pw.r_qf.,cf.osb&cad=b";
			//$url = "http://www.google.ru/search?q=" . $phrase . $start . "&hl=ru&tbs=iszw:1024,iszh:1024,isz:l&tbm=isch&prmd=imvns&source=lnt&sa=X&biw=1066&bih=747&hl=ru&newwindow=1&safe=active&tbs=iszw:1024,iszh:1024,isz:lt,islt:xga&tbm=isch&prmd=imvns&source=lnt&sa=X&bav=on.2,or.r_gc.r_pw.r_qf.,cf.osb&biw=1066&bih=747";
			$url = "http://www.google.ru/search?as_st=y&tbm=isch&hl=ru&as_q=" . $phrase . "&as_epq=&as_oq=&as_eq=&cr=&as_sitesearch=&safe=active&orq=&tbs=isz:lt,islt:xga,itp:photo,ift:jpg&biw=1066&bih=747"; // &as_sitesearch=http://ru.wikipedia.org/
			$html = $this->curl_connect($url);	

			//$count_loop = $count_loop + 10;			
		//}
				
		return $html;
	}
	
	
	public function google_video_html_grab($phrase){
		$this->browser_referer = 'http://www.google.ru/';
		
		//$phrase = mb_convert_encoding($phrase, "UTF-8", "CP1251");
		$phrase = $phrase . ' youtube';
		$query = urlencode($phrase);	
		
			$url = "http://www.google.ru/search?q=" . $query . "&hl=ru&newwindow=1&safe=active&tbas=0&biw=1066&bih=747&tbm=vid&prmd=imvnse&source=lnms&sa=X&oi=mode_link&ct=mode&cd=4";
			
			$html .= $this->curl_connect($url);
		
		return $html;
	}	
	
	
	//сбор статей с rusarticles
	public function rusarticles_main_grab($phrase, $depth = 1){
		$this->browser_referer = 'http://www.rusarticles.com/';
		//$this->browser_referer = 'http://www.google.ru/';
		
		//$phrase = mb_convert_encoding($phrase, "UTF-8", "CP1251");
		//$phrase = $phrase . ' youtube';
		$query = urlencode($phrase);		
	
		$url = "http://www.rusarticles.com/find-articles.php?q=" . $query;
		$html .= $this->curl_connect($url);
			
		if($depth > 1){
			$count_loop = 2;
			while($count_loop <= $depth){
			
				$url = "http://www.rusarticles.com/find-articles.php?q=" . $query . "&page=" . $count_loop;
				$html .= $this->curl_connect($url);
					
				$count_loop++;
			}
		}			
		
		return $html;
	}
	
	
	//сбор статей с rusarticles
	public function rusarticles_content_grab($urls_articles){
		$this->browser_referer = 'http://www.rusarticles.com/';
		//$this->browser_referer = 'http://www.google.ru/';
		
		//$phrase = mb_convert_encoding($phrase, "UTF-8", "CP1251");
		//$phrase = $phrase . ' youtube';
		//$query = urlencode($phrase);		
	
		//$url = "http://www.rusarticles.com/find-articles.php?q=" . $query;
		
		foreach($urls_articles as $key => $value){
			$html_articles_page_array[$key]['html'] = $this->curl_connect($value);
			$html_articles_page_array[$key]['url'] = $value;
		}
		
		return $html_articles_page_array;
	}
		
	
	public function curl_connect($url){
		$ch = curl_init();
		//curl_setopt($ch, CURLOPT_BUFFERSIZE, 500000); 
		curl_setopt($ch, CURLOPT_REFERER, $this->browser_referer);
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent);
		//curl_setopt($ch, CURLOPT_COOKIEJAR, $this->path_cookie_file_name);
		//curl_setopt($ch, CURLOPT_COOKIEFILE, $this->path_cookie_file_name);
		curl_setopt($ch, CURLOPT_URL, $url);
		$html = curl_exec($ch);
		curl_close($ch);
		
		return $html;
	}
	
	
	public function file_get_connect($url){
		$url = urldecode($url);
		$html = file_get_contents($url, false, null, -1, 500000);
		//echo $url . '<br><br><br><br><br>';
		//echo $html;
		//die;
		return $html;
		//return '';
	}
}
?>