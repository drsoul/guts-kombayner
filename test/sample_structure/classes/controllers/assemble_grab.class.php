<?
// no direct access
defined( '_EXEC' ) or die( 'Restricted access' );

require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/wordstat_yandex.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/abstract_auth.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/auth_rambler.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/grabs.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/parsers.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/automatize.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/templates.class.php');

require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/models/root_model.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/models/sql_model.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/models/dbconfig.class.php');

require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/smarty/Smarty.class.php');

class Assemble_grab{
	
	protected $auth_rambler;
	protected $grabs;
	protected $parsers;
	protected $sql_model;
	protected $smarty;
	protected $automatize;
	protected $depth_text_google_grab = 10;
	protected $depth_text_rusarticles_grab = 2;
	
	
	
	public function set_depth_text_google_grab($depth_text_google_grab = 10){
	
		$this->depth_text_google_grab = $depth_text_google_grab;
		
		return true;
	}
	
	
	public function set_depth_text_rusarticles_grab($depth_text_rusarticles_grab = 2){
	
		$this->depth_text_rusarticles_grab = $depth_text_rusarticles_grab;
		
		return true;
	}
	
	
	function Assemble_grab(){
	
		set_time_limit(0);
		
		//ini_set("max_execution_time", "3600");
		
		$this->auth_rambler = new Auth_rambler;
		$this->grabs = new Grabs;
		$this->parsers = new Parsers;
		$this->sql_model = new Sql_model;
		$this->smarty = new Smarty;
		$this->automatize = new Automatize;
		$this->templates = new Templates;	
		$this->wordstat_yandex = new Wordstat_yandex;
		$this->config = New Dbconfig();
	}

	
	
	//главный цикл для обновлятора
	function grab_main_loop_second($id_site){

		$phrases_primary_grab_0 = $this->sql_model->select_phrases_primary_grab_0($id_site);		
		
		foreach($phrases_primary_grab_0 as $key => $value){		
		
			$this->sql_model->set_id_sites($id_site);	
			$this->parsers->set_id_sites($id_site);	
			$this->grabs->set_id_sites($id_site);	
			
				
			//сбор поисковых фраз
			//$phrases = $this->adstat_rambler_grab($value['search_phrase']);
			$phrases = $this->wordstat_yandex_grab($value['search_phrase']);			
			$this->sql_model->logs_grab($id_site, $title = 'Сайт');
			$this->sql_model->logs_grab($phrases, $title = 'Найденные фразы');
			/*
			echo '<pre>';
			print_r($phrases);
			echo '</pre>';
			*/
			
			//сбор текстов
			$this->texts_grab($phrases, $id_site);
			$this->sql_model->logs_grab('Ok!', $title = 'Тексты');
			
			//echo '<br>сбор тектов прошёл<br>';
			//сбор текстов
			//передаём экземпляр чтобы вместе с ним передалось свойство id_sites
			$this->fotos_grab($phrases, $id_site);			
			$this->sql_model->logs_grab('Ok!', $title = 'Фотографии');
			//echo '<br>сбор фоток прошёл<br>';			
			//сбор видео
			$ids_videos = $this->videos_grab($phrases, $id_site);
			$this->sql_model->logs_grab($ids_videos, $title = 'Видео');
			
			//echo '<br>сбор видео прошёл<br>';
				
			$this->automatize->bind_images_on_texts($imgs_on_text_from = 1, $imgs_on_text_to = 3);
			$this->sql_model->logs_grab('Ok!', $title = 'bind_images_on_texts');
			//echo '<br>привязка изображений прошла<br>';
		}
	}
	
	
	//главный цикл сбора и вставки контента в базу
	function grab_main_loop($sites_and_phrases_primary_grab_0){	

		//ob_start();
		error_reporting(E_ERROR | E_PARSE);
		
		//увеличиваем глубину сбора сайтов в гугле
		$this->set_depth_text_google_grab($depth_text_google_grab = 10);
		
		//add 03_12_12-16_48
		//создаём шаблоны в отдельном цикле чтобы не дублировать создание шаблонов в основном цикле (он ниже)
		$sites_primary_grab_0 = $this->sql_model->select_sites_by_primary_grab($primary_grab = 0);			
		
		$this->sql_model->logs_grab($sites_and_phrases_primary_grab_0, $title = 'Сайты и первичные фразы');			
		
		foreach($sites_primary_grab_0 as $key => $value){
			
			//создание шаблона сайта
			$this->automatize->create_template($value['id_sites']);	
			$this->sql_model->logs_grab($value['id_sites'], $title = 'Создан шаблон для сайта');
		}		
		
				
		$temp_id_sites = 0;
		$count_phrases = count($sites_and_phrases_primary_grab_0);
		$phrases_loop = 1;
		foreach($sites_and_phrases_primary_grab_0 as $key => $value){
						
			$this->sql_model->set_id_sites($value['id_sites']);		
			$this->parsers->set_id_sites($value['id_sites']);
			$this->grabs->set_id_sites($value['id_sites']);
			
			//сбор поисковых фраз
			//$phrases = $this->adstat_rambler_grab($value['search_phrase']);
			$phrases = $this->wordstat_yandex_grab($value['search_phrase'], $depth = 2);
						
			$this->sql_model->logs_grab($value['id_sites'], $title = 'Сайт');
			$this->sql_model->logs_grab($phrases, $title = 'Найденные фразы');
			/*
			echo '<pre>';
			print_r($phrases);
			echo '</pre>';
			*/					
			
			//сбор текстов
			$id_texts = $this->texts_grab($phrases, $value['id_sites']);			
			$this->sql_model->logs_grab('Ok!', $title = 'Тексты');
			
			
			//передаём экземпляр чтобы вместе с ним передалось свойство id_sites
			$ids_images = $this->fotos_grab($phrases, $value['id_sites']);	
			$this->sql_model->logs_grab($ids_images, $title = 'Фотографии');
			
			//сбор видео
			$ids_videos = $this->videos_grab($phrases, $value['id_sites']);														
			$this->sql_model->logs_grab($ids_videos, $title = 'Видео');			
						
			unset($phrases);			
						
			//update 30_11_12-08_24
			//проверка чтобы не обновлять статус у сайта после каждого цикла по ключевой фразе, а только после завершения работы по данному сайту
					
			$this->action_after_grab($temp_id_sites, $value['id_sites'], $count_phrases, $phrases_loop);
			
			$phrases_loop++;
			$temp_id_sites = $value['id_sites'];

			
			//создание файла с тегами для соц. закладок
			//$this->templates->create_file_for_bagbookmark($value['id_sites']); - больше здесь не требуется			
		}	
		
	}

	
	protected function action_after_grab($temp_id_sites, $id_sites, $count_phrases, $phrases_loop){
	
			if($temp_id_sites != $id_sites AND $temp_id_sites != 0){				
				
				$url =  'http://' . $_SERVER['HTTP_HOST'] . '/admin/their_requests.php?command=action_after_grab_1';		
				
				$params['id_sites'] = $id_sites;
				$params['temp_id_sites'] = $temp_id_sites;				
				$login = $this->config->user['name'];
				$password = md5($this->config->user['pass']);
				//$params['domain'] = 'adm.ekstrim.borrba.ru';		
				
				$html = $this->automatize->curl_secure_connect($url, $login, $password, $params);
				unset($html, $login, $password, $params, $url);				
				
			}elseif($count_phrases == $phrases_loop){

				$url =  'http://' . $_SERVER['HTTP_HOST'] . '/admin/their_requests.php?command=action_after_grab_2';		
				
				$params['id_sites'] = $id_sites;
				$login = $this->config->user['name'];
				$password = md5($this->config->user['pass']);
				//$params['domain'] = 'adm.ekstrim.borrba.ru';		
				
				$html = $this->automatize->curl_secure_connect($url, $login, $password, $params);
				unset($html, $login, $password, $params, $url);			
				
			}
	}
	
	
	public function adstat_rambler_grab($search_phrase){
		
		//перекодируем т.к. рамблер вордстат в cp1251
		$search_phrase = mb_convert_encoding($search_phrase, 'CP1251', 'UTF8');
		//$search_phrase = 'варенье из кабачков';

		
		//второй параметр - количество страниц парса адстата рамблера
		$html = $this->auth_rambler->browser_adstat_rambler($search_phrase, 1);
		/*	
		echo '<pre>';
		print_r($html);
		echo '</pre>';
		*/
		
		$phrases = $this->parsers->parser_adstat_rambler($html, $search_phrase);
		
		/*
		##отладка
		echo '<pre>';
		print_r($phrases);
		echo '</pre>';
		*/	
		
		return $phrases;
	}
	
	
	public function wordstat_yandex_grab($search_phrase, $depth = 2){
		
		//до 05_12_12 было $depth = 2 в нижеидущем методе
		$html = $this->wordstat_yandex->wordstat_yandex_grab_html($search_phrase, $depth);
		/*
		echo '<pre>';
		print_r($html);
		echo '</pre>';
		*/
		$search_phrases = $this->parsers->wordstat_yandex_parse($html, $search_phrase, $limit_words = 5, $del_phrases_with_dig = false, $full_inclusion = true, $limit_phrases = 150, $strict_key_phrase_del = 1, $strict_word_ending = false);
			
		return $search_phrases;
	}
	

	public function texts_grab($phrases, $id_site){	

	
		foreach($phrases as $key => $value_search_phrase){
													
			//add 19_11_12 11_25
			//служит для того чтобы в названия категорий не попадали пустоты
			if($value_search_phrase == ' ' OR empty($value_search_phrase) ){
				break;
			}else{
				$url =  'http://' . $_SERVER['HTTP_HOST'] . '/admin/their_requests.php?command=text_grab';		
				
				$params['value_search_phrase'] = $value_search_phrase;
				$params['depth_text_google_grab'] = $this->depth_text_google_grab;
				$params['depth_text_rusarticles_grab'] = $this->depth_text_rusarticles_grab;
				$params['id_site'] = $id_site;
				
				$login = $this->config->user['name'];
				$password = md5($this->config->user['pass']);
				//$params['domain'] = 'adm.ekstrim.borrba.ru';		
				
				$html = $this->automatize->curl_secure_connect($url, $login, $password, $params);
				unset($html, $login, $password, $params, $url);
			}		
		}		
	}


	public function fotos_grab($phrases, $id_site){
		//echo 'Начался сбор фото<br>';
		//сбор фото
		foreach($phrases as $key => $value_search_phrase){	
		
			if($value_search_phrase == ' ' OR empty($value_search_phrase) ){
				break;
			}else{
				$url =  'http://' . $_SERVER['HTTP_HOST'] . '/admin/their_requests.php?command=fotos_grab';					
				$params['value_search_phrase'] = $value_search_phrase;			
				$params['id_site'] = $id_site;
				$login = $this->config->user['name'];
				$password = md5($this->config->user['pass']);					
				
				$html = $this->automatize->curl_secure_connect($url, $login, $password, $params);
				unset($html, $login, $password, $params, $url);	
			}					
		}			
	}	
	

	public function videos_grab($phrases, $id_site){

		//сбор видео
		foreach($phrases as $key => $value_search_phrase){	
		
			if($value_search_phrase == ' ' OR empty($value_search_phrase) ){
				break;
			}else{
				$url =  'http://' . $_SERVER['HTTP_HOST'] . '/admin/their_requests.php?command=videos_grab';					
				$params['value_search_phrase'] = $value_search_phrase;			
				$params['id_site'] = $id_site;
				$login = $this->config->user['name'];
				$password = md5($this->config->user['pass']);					
				
				$html = $this->automatize->curl_secure_connect($url, $login, $password, $params);
				unset($html, $login, $password, $params, $url);		
			}			
		}		
	}
}

?>