<?php /* Smarty version Smarty-3.1.8, created on 2013-03-09 14:56:35
         compiled from "X:\home\unique_site_gen_21\www\admin\template\templates\create_domains_cron_panel.html" */ ?>
<?php /*%%SmartyHeaderCode:32431513b23736fffe5-60513067%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'aa1c72dd03693654b8f8fae46f3860ff4ff49906' => 
    array (
      0 => 'X:\\home\\unique_site_gen_21\\www\\admin\\template\\templates\\create_domains_cron_panel.html',
      1 => 1361706075,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '32431513b23736fffe5-60513067',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_513b237372f452_19863827',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_513b237372f452_19863827')) {function content_513b237372f452_19863827($_smarty_tpl) {?><div class="distance"></div>

<div id="create_domains_cron_panel" class="box_2 create_domains_cron_panel">

	<h1>(Cron) Создание доменов 2domains.ru и размещение их в Cpanel.</h1>
	
	ВНИМАНИЕ! Работает только когда данный скрипт размещён на сервере где установлена Cpanel.<br>
	Требуется активация api 2domains.ru для вашего акаунта у них. Подайте запрос в их тех. поддержку.<br><br>
	Добавьте файл планировщика в cron (этот файл в кроне должен быть в единственном экземпляре):<br>
	команда: wget -o /dev/null http://<?php echo $_SERVER['SERVER_NAME'];?>
/cron/pseudo-cron.php > /dev/null 2>&1 <br>
	время: * * * * *
	<div class="distance"></div>
	<div class="distance"></div>
	
	Регистрирует ru домены. Имя формируется методом транслитерации названия сайта, если домен уже существует в конец добавляется цифра.
	
	<div>
		<span onclick="fill_fields_cd_from_file_cron()" class="link_imitate">Заполнит поля из файла</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Файл находиться: root/words/data_for_domain_reg_add.txt
	</div>
	<div class="distance"></div>
	
	<div>
		<input id="username_cd_cron" type="text" class="input_text" value="mail@mail.ru"> Ваш логин в 2domains.ru
	</div>	
	<div class="distance"></div>
	
	<div>
		<input id="password_cd_cron" type="password" class="input_text" value=""> Ваш пароль в 2domains.ru
	</div>	
	<div class="distance"></div>

	<div>
		<input id="folder_name_cd_cron" type="text" class="input_text" value="folder_name"> Название папки с доменами в 2domains.ru
	</div>	
	<div class="distance"></div>	
	
	<div>
		<input id="post_code_cd_cron" type="text" class="input_text" value="111111"> Ваш почтовый индекс.
	</div>	
	<div class="distance"></div>	
	
	<div>
		<input id="region_cd_cron" type="text" class="input_text" value="Московская область"> Ваш регион.
	</div>	
	<div class="distance"></div>
		
	<div>
		<input id="town_cd_cron" type="text" class="input_text" value="Москва"> Ваш город.
	</div>	
	<div class="distance"></div>
			
	<div>
		<input id="street_house_room_cd_cron" type="text" class="input_text" value="ул. Невского, д.24, кв.12"> Улица, дом, квартира.
	</div>	
	<div class="distance"></div>
	
	<div>
		<input id="phone_cd_cron" type="text" class="input_text" value="+71111111111"> Ваш телефон.
	</div>	
	<div class="distance"></div>
	
	<div>
		<input id="e_mail_cd_cron" type="text" class="input_text" value="mail@mail.ru"> Контактная электропочта.
	</div>	
	<div class="distance"></div>
	
	<div>
		<input id="person_en_cd_cron" type="text" class="input_text" value="Ivan I Ivanov"> Имя, первая буква отчества, фамилия на английском.
	</div>	
	<div class="distance"></div>
	
	<div>
		<input id="person_r_cd_cron" type="text" class="input_text" value="Иванов Иван Иванович"> Фамилия, имя, отчество.
	</div>	
	<div class="distance"></div>	
	
	<div>
		<input id="birth_date_cd_cron" type="text" class="input_text" value="15.03.1985"> Дата рождения с ведущими нулями, через точку, без пробелов (дд.мм.гггг).
	</div>	
	<div class="distance"></div>
	
	<div>
		<input id="private_person_flag_cd_cron" type="checkbox" class="input_text" value="1" CHECKED> Активировать для домена Private Person.
	</div>	
	<div class="distance"></div>	
	
	<div>
		<input id="passport_number_cd_cron" type="text" class="input_text" value="1111111111"> Серия и номер паспорта без пробелов.
	</div>	
	<div class="distance"></div>
	
	<div>
		<input id="passport_issued_org_cd_cron" type="text" class="input_text" value="отделением УФМС России по Московской области в Ленинском районе"> Орган выдавший паспорт.
	</div>	
	<div class="distance"></div>
	
	<div>
		<input id="passport_issued_date_cd_cron" type="text" class="input_text" value="03.05.2010"> Дата выдачи паспорта с ведущими нулями, через точку, без пробелов (дд.мм.гггг).
	</div>	
	<div class="distance"></div>
	
	<div>
		<input id="country_cd_cron" type="text" class="input_text" value="RU"> Код вашей страны, 2 большие английские буквы.
	</div>	
	<div class="distance"></div>
	
	<div>
		<input id="ns0_cd_cron" type="text" class="input_text" value="ns1.reg.ru"> NS сервер 1, для делегирования домена.
	</div>	
	<div class="distance"></div>

	<div>
		<input id="ns1_cd_cron" type="text" class="input_text" value="ns2.reg.ru"> NS сервер 2, для делегирования домена.
	</div>	
	<div class="distance"></div>	
	
	
	<div>
		<input id="number_farm_cd_cron" type="text" class="input_text" value="1"> Номер фермы сайтов. Пример номеров разных ферм: 1, 2, 3, 4 и т.д.<br>
		Должна быть уникальная цифра для акаунта Cpanel, чтобы избежать одинаковых директорий для разных доменов.<br>
		В разных акаунтах Cpanel можно использовать одинаковые номера ферм.<br>
		Шаблон пути: root/domains/sites_[Номер фермы сайтов]/site_[id сайта]
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="theme_cpanel_cd_cron" type="text" class="input_text" value="x3"> Тема в Cpanel. По умолчанию x3
	</div>
	<div class="distance"></div>	
	
	
	<div>
		<input id="login_cpanel_cd_cron" type="text" class="input_text"> Логин в Cpanel.
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="password_cpanel_cd_cron" type="password" class="input_text"> Пароль в Cpanel.
	</div>
	<div class="distance"></div>
	

	<div>
		<span onclick="create_domains_cron()" class="link_imitate">Создать домены</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span id="create_domains_cron_indicator" class="indicator"></span>
	</div>

</div><?php }} ?>