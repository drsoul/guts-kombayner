<?php /* Smarty version Smarty-3.1.8, created on 2013-03-09 14:56:35
         compiled from "X:\home\unique_site_gen_21\www\admin\template\templates\create_subdomains_panel.html" */ ?>
<?php /*%%SmartyHeaderCode:3472513b23732f4c60-21300126%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6582a1a77be0e3b400f73f0469e1af10825db77d' => 
    array (
      0 => 'X:\\home\\unique_site_gen_21\\www\\admin\\template\\templates\\create_subdomains_panel.html',
      1 => 1361208393,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3472513b23732f4c60-21300126',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_513b23733253e1_69889120',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_513b23733253e1_69889120')) {function content_513b23733253e1_69889120($_smarty_tpl) {?><div class="distance"></div>

<div id="create_subdomains_panel" class="box_2 create_subdomains_panel">

	<h1>Создание поддоменов Cpanel.</h1>
	ВНИМАНИЕ! Работает только когда данный скрипт размещён на сервере где установлена Cpanel.
	<div class="distance"></div>
	
	<div>
		Домен/ы верхнего уровня (каждый с новой строки, без http:// и завершающего - /).<br> Если введено больше 1го домена то домены будут чередоваться по кругу при создании субдоменов:<br>
		<textarea id="up_domains" class="textarea"></textarea>
	</div>	
	<div class="distance"></div>
	
	
	<div>
		<input id="number_farm" type="text" class="input_text" value="1"> Номер фермы сайтов. Пример номеров разных ферм: 1, 2, 3, 4 и т.д.<br>
		Должна быть уникальная цифра для акаунта Cpanel, чтобы избежать одинаковых директорий для разных доменов.<br>
		В разных акаунтах Cpanel можно использовать одинаковые номера ферм.<br>
		Шаблон пути: корень/domains/sites_[Номер фермы сайтов]/site_[id сайта]
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="theme_cpanel" type="text" class="input_text" value="x3"> Тема в Cpanel. По умолчанию x3
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="login_cpanel" type="text" class="input_text"> Логин в Cpanel.
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="password_cpanel" type="password" class="input_text"> Пароль в Cpanel.
	</div>
	<div class="distance"></div>
	

	<div>
		<span onclick="create_subdomains()" class="link_imitate" >Создать поддомены</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span id="create_subdomains_indicator" class="indicator"></span>
	</div>

</div><?php }} ?>