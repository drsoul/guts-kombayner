<?php /* Smarty version Smarty-3.1.8, created on 2013-02-19 04:19:53
         compiled from "X:\home\unique_site_gen_19\www\admin\template\templates\create_domains_panel.html" */ ?>
<?php /*%%SmartyHeaderCode:220655122d3391ff817-94575735%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f3cfe52d6d5d9021a85028ad413c0c910a875916' => 
    array (
      0 => 'X:\\home\\unique_site_gen_19\\www\\admin\\template\\templates\\create_domains_panel.html',
      1 => 1361227250,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '220655122d3391ff817-94575735',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_5122d339234288_33781515',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5122d339234288_33781515')) {function content_5122d339234288_33781515($_smarty_tpl) {?><div class="distance"></div>

<div id="create_domains_panel" class="box_2 create_domains_panel">

	<h1>Создание доменов 2domains.ru и размещение их в Cpanel.</h1>
	ВНИМАНИЕ! Работает только когда данный скрипт размещён на сервере где установлена Cpanel.<br>
	Требуется активация api 2domains.ru для вашего акаунта у них. Подайте запрос в их тех. поддержку.
	<div class="distance"></div>
	<div class="distance"></div>
	
	Регистрирует ru домены. Имя формируется методом транслитерации названия сайта, если домен уже существует в конец добавляется цифра.
	
	<div>
		<span onclick="fill_fields_cd_from_file()" class="link_imitate">Заполнит поля из файла</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Файл находиться: root/words/data_for_domain_reg_add.txt
	</div>
	<div class="distance"></div>
	
	<div>
		<input id="username_cd" type="text" class="input_text" value="mail@mail.ru"> Ваш логин в 2domains.ru
	</div>	
	<div class="distance"></div>
	
	<div>
		<input id="password_cd" type="password" class="input_text" value=""> Ваш пароль в 2domains.ru
	</div>	
	<div class="distance"></div>

	<div>
		<input id="folder_name_cd" type="text" class="input_text" value="folder_name"> Название папки с доменами в 2domains.ru
	</div>	
	<div class="distance"></div>	
	
	<div>
		<input id="post_code_cd" type="text" class="input_text" value="111111"> Ваш почтовый индекс.
	</div>	
	<div class="distance"></div>	
	
	<div>
		<input id="region_cd" type="text" class="input_text" value="Московская область"> Ваш регион.
	</div>	
	<div class="distance"></div>
		
	<div>
		<input id="town_cd" type="text" class="input_text" value="Москва"> Ваш город.
	</div>	
	<div class="distance"></div>
			
	<div>
		<input id="street_house_room_cd" type="text" class="input_text" value="ул. Невского, д.24, кв.12"> Улица, дом, квартира.
	</div>	
	<div class="distance"></div>
	
	<div>
		<input id="phone_cd" type="text" class="input_text" value="+71111111111"> Ваш телефон.
	</div>	
	<div class="distance"></div>
	
	<div>
		<input id="e_mail_cd" type="text" class="input_text" value="mail@mail.ru"> Контактная электропочта.
	</div>	
	<div class="distance"></div>
	
	<div>
		<input id="person_en_cd" type="text" class="input_text" value="Ivan I Ivanov"> Имя, первая буква отчества, фамилия на английском.
	</div>	
	<div class="distance"></div>
	
	<div>
		<input id="person_r_cd" type="text" class="input_text" value="Иванов Иван Иванович"> Фамилия, имя, отчество.
	</div>	
	<div class="distance"></div>	
	
	<div>
		<input id="birth_date_cd" type="text" class="input_text" value="15.03.1985"> Дата рождения с ведущими нулями, через точку, без пробелов (дд.мм.гггг).
	</div>	
	<div class="distance"></div>
	
	<div>
		<input id="private_person_flag_cd" type="checkbox" class="input_text" value="1" CHECKED> Активировать для домена Private Person.
	</div>	
	<div class="distance"></div>	
	
	<div>
		<input id="passport_number_cd" type="text" class="input_text" value="1111111111"> Серия и номер паспорта без пробелов.
	</div>	
	<div class="distance"></div>
	
	<div>
		<input id="passport_issued_org_cd" type="text" class="input_text" value="отделением УФМС России по Московской области в Ленинском районе"> Орган выдавший паспорт.
	</div>	
	<div class="distance"></div>
	
	<div>
		<input id="passport_issued_date_cd" type="text" class="input_text" value="03.05.2010"> Дата выдачи паспорта с ведущими нулями, через точку, без пробелов (дд.мм.гггг).
	</div>	
	<div class="distance"></div>
	
	<div>
		<input id="country_cd" type="text" class="input_text" value="RU"> Код вашей страны, 2 большие английские буквы.
	</div>	
	<div class="distance"></div>
	
	<div>
		<input id="ns0_cd" type="text" class="input_text" value="ns1.reg.ru"> NS сервер 1, для делегирования домена.
	</div>	
	<div class="distance"></div>

	<div>
		<input id="ns1_cd" type="text" class="input_text" value="ns2.reg.ru"> NS сервер 2, для делегирования домена.
	</div>	
	<div class="distance"></div>	
	
	
	<div>
		<input id="number_farm_cd" type="text" class="input_text" value="1"> Номер фермы сайтов. Пример номеров разных ферм: 1, 2, 3, 4 и т.д.<br>
		Должна быть уникальная цифра для акаунта Cpanel, чтобы избежать одинаковых директорий для разных доменов.<br>
		В разных акаунтах Cpanel можно использовать одинаковые номера ферм.<br>
		Шаблон пути: root/domains/sites_[Номер фермы сайтов]/site_[id сайта]
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="theme_cpanel_cd" type="text" class="input_text" value="x3"> Тема в Cpanel. По умолчанию x3
	</div>
	<div class="distance"></div>	
	
	
	<div>
		<input id="login_cpanel_cd" type="text" class="input_text"> Логин в Cpanel.
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="password_cpanel_cd" type="password" class="input_text"> Пароль в Cpanel.
	</div>
	<div class="distance"></div>
	

	<div>
		<span onclick="create_domains()" class="link_imitate">Создать домены</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span id="create_domains_indicator" class="indicator"></span>
	</div>

</div><?php }} ?>