<?php /* Smarty version Smarty-3.1.8, created on 2013-02-24 14:41:30
         compiled from "X:\home\unique_site_gen_19\www\admin\template\templates\create_subdomains_cron_panel.html" */ ?>
<?php /*%%SmartyHeaderCode:8162512506fc6b2491-06282459%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2c91676869a71d2ca33021dff9a0ea188450501e' => 
    array (
      0 => 'X:\\home\\unique_site_gen_19\\www\\admin\\template\\templates\\create_subdomains_cron_panel.html',
      1 => 1361706087,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '8162512506fc6b2491-06282459',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_512506fc6b7bc7_47610215',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_512506fc6b7bc7_47610215')) {function content_512506fc6b7bc7_47610215($_smarty_tpl) {?><div class="distance"></div>

<div id="create_subdomains_cron_panel" class="box_2 create_subdomains_cron_panel">

	<h1>(Cron) Создание поддоменов Cpanel.</h1>
	
	ВНИМАНИЕ! Работает только когда данный скрипт размещён на сервере где установлена Cpanel.<br><br>
	Добавьте файл планировщика в cron (этот файл в кроне должен быть в единственном экземпляре):<br>
	команда: wget -o /dev/null http://<?php echo $_SERVER['SERVER_NAME'];?>
/cron/pseudo-cron.php > /dev/null 2>&1 <br>
	время: * * * * *
	<div class="distance"></div>
	
	<div>
		Домен/ы верхнего уровня (каждый с новой строки, без http:// и завершающего - /).<br> Если введено больше 1го домена то домены будут чередоваться по кругу при создании субдоменов:<br>
		<textarea id="up_domains_cron" class="textarea"></textarea>
	</div>	
	<div class="distance"></div>
	
	
	<div>
		<input id="number_farm_cron" type="text" class="input_text" value="1"> Номер фермы сайтов. Пример номеров разных ферм: 1, 2, 3, 4 и т.д.<br>
		Должна быть уникальная цифра для акаунта Cpanel, чтобы избежать одинаковых директорий для разных доменов.<br>
		В разных акаунтах Cpanel можно использовать одинаковые номера ферм.<br>
		Шаблон пути: корень/domains/sites_[Номер фермы сайтов]/site_[id сайта]
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="theme_cpanel_cron" type="text" class="input_text" value="x3"> Тема в Cpanel. По умолчанию x3
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="login_cpanel_cron" type="text" class="input_text"> Логин в Cpanel.
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="password_cpanel_cron" type="password" class="input_text"> Пароль в Cpanel.
	</div>
	<div class="distance"></div>
	

	<div>
		<span onclick="create_subdomains_cron()" class="link_imitate" >Создать поддомены</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span id="create_subdomains_cron_indicator" class="indicator"></span>
	</div>

</div><?php }} ?>