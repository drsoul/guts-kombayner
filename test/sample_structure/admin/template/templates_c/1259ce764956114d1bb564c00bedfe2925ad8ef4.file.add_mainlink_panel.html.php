<?php /* Smarty version Smarty-3.1.8, created on 2013-02-26 16:35:11
         compiled from "X:\home\unique_site_gen_20\www\admin\template\templates\add_mainlink_panel.html" */ ?>
<?php /*%%SmartyHeaderCode:13285512cba0f27c6f7-22241849%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1259ce764956114d1bb564c00bedfe2925ad8ef4' => 
    array (
      0 => 'X:\\home\\unique_site_gen_20\\www\\admin\\template\\templates\\add_mainlink_panel.html',
      1 => 1361208183,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13285512cba0f27c6f7-22241849',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_512cba0f282de6_86425391',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_512cba0f282de6_86425391')) {function content_512cba0f282de6_86425391($_smarty_tpl) {?><div class="distance"></div>

<div id="add_mainlink_panel" class="box_2 add_mainlink_panel">

	<h1>Добавление сайтов в mainlink.ru.</h1>
	Добавляются сайты у которых в индексе Яндекса больше 30 страниц.
	<div class="distance"></div>
	
	
	<div>
		<input id="login_mainlink" type="text" class="input_text" value=""> Логин mainlink.ru.
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="password_mainlink" type="password" class="input_text" value=""> Пароль mainlink.ru.
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="count_main_links" type="text" class="input_text" value="3"> Количество ссылок на главной (рекомендуем не увеличивать).
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="count_other_links" type="text" class="input_text" value="1"> Количество ссылок на прочих страницах (рекомендуем не увеличивать).
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="multiplication" type="text" class="input_text" value="0.5"> Мультипликатор для формирования цены по PR. Формула: PR(предыдущий) * Мультипликатор = PR(следующий).
	</div>
	<div class="distance"></div>	
	
	
	<div>
		<input id="dol_main_page" type="text" class="input_text" value="0.32"> Цена ссылки на главной странице в долларах (отделитель дробной части - точка, точность 2 знака после точки).
	</div>
	<div class="distance"></div>
		
	
	<div>
		<input id="rub_main_page" type="text" class="input_text" value="9.99"> Цена ссылки на главной странице в рублях (отделитель дробной части - точка, точность 2 знака после точки).
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="dol_lvl_1_page" type="text" class="input_text" value="0.05"> Цена ссылки на странице в 1 клике от главной в долларах (отделитель дробной части - точка, точность 2 знака после точки).
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="rub_lvl_1_page" type="text" class="input_text" value="1.49"> Цена ссылки на странице в 1 клике от главной в рублях (отделитель дробной части - точка, точность 2 знака после точки).
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="dol_lvl_2_page" type="text" class="input_text" value="0.03"> Цена ссылки на странице в 2х кликах от главной в долларах (отделитель дробной части - точка, точность 2 знака после точки).
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="rub_lvl_2_page" type="text" class="input_text" value="0.99"> Цена ссылки на странице в 2х кликах от главной в рублях (отделитель дробной части - точка, точность 2 знака после точки).
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="dol_lvl_3_page" type="text" class="input_text" value="0.02"> Цена ссылки на странице в 3х кликах от главной в долларах (отделитель дробной части - точка, точность 2 знака после точки).
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="rub_lvl_3_page" type="text" class="input_text" value="0.49"> Цена ссылки на странице в 3х кликах от главной в рублях (отделитель дробной части - точка, точность 2 знака после точки).
	</div>
	<div class="distance"></div>
	
	
	<div>		
		Чёрный список слов для блокирования ссылок (сплошной строкой, без пробелов и переносов строк, через запятую).		
		<textarea id="stop_words" class="textarea">курсовые,дипломы,варез,справки,phentermine,loans,drug,cheap,gambling,справк,диплом,казино,levitra,warez,досуг,slot,blackjack,insurance,tickets,hotels,фильмдетально,abilify,accutane,acetaminophen,acomplia,alprazolam,ambien,amitriptyline,anti-anxiety,anti-depressants,aripiprazole,ativan,baclofen,benadryl,bupropion,buspar,buspirone,buy,carisoprodol,casino,celebrex,celecoxib,celexa,cialis,citalopram,clonazepam,cocaine,crack,craps,credit,cyclobenzaprine,cymbalta,detox,detoxifying,diazepam,diclofenac</textarea>
	</div>	
	<div class="distance"></div>
		

	<div>
		<span onclick="activate_mainlink()" class="link_imitate">Добавить сайты</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span id="add_mainlink_indicator" class="indicator"></span>
	</div>

</div><?php }} ?>