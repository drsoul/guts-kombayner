<?php /* Smarty version Smarty-3.1.8, created on 2013-03-09 14:56:35
         compiled from "X:\home\unique_site_gen_21\www\admin\template\templates\create_subdomains_isp_panel.html" */ ?>
<?php /*%%SmartyHeaderCode:21061513b23739f7006-28028275%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '13097226176bc18556c79c8e42d39eb9df2ad25e' => 
    array (
      0 => 'X:\\home\\unique_site_gen_21\\www\\admin\\template\\templates\\create_subdomains_isp_panel.html',
      1 => 1362492476,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '21061513b23739f7006-28028275',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_513b2373a1a259_15208638',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_513b2373a1a259_15208638')) {function content_513b2373a1a259_15208638($_smarty_tpl) {?><div class="distance"></div>

<div id="create_subdomains_isp_panel" class="box_2 create_subdomains_isp_panel">

	<h1>Создание поддоменов в панели ISPmanager.</h1>
	ВНИМАНИЕ! Работает только когда данный скрипт размещён на сервере где установлен ISPmanager.
	<div class="distance"></div>
	
	<div>
		Домен/ы верхнего уровня (каждый с новой строки, без http:// и завершающего - /).<br> Если введено больше 1го домена то домены будут чередоваться по кругу при создании субдоменов:<br>
		<textarea id="up_domains_isp_sub" class="textarea"></textarea>
	</div>	
	<div class="distance"></div>
	
	
	<div>
		<input id="number_farm_isp_sub" type="text" class="input_text" value="1"> Номер фермы сайтов. Пример номеров разных ферм: 1, 2, 3, 4 и т.д.<br>
		Должна быть уникальная цифра для ISPmanager, чтобы избежать одинаковых директорий для разных доменов.<br>		
		Шаблон пути: корень/domains/sites_[Номер фермы сайтов]/site_[id сайта]
	</div>
	<div class="distance"></div>

	<div>
		<input id="hid_email_isp_sub" type="text" class="input_text" value="mail@mail.ru"> Почта владельца субдомена для связи.
	</div>
	<div class="distance"></div>

	<div>
		<input id="port_isp_sub" type="text" class="input_text" value="1500"> Порт на котором у вас находится ISPmanager, без двоеточия - только цифры. Можно использовать пустое значение, если вы выходите на админку без порта (указывая только ip).
	</div>
	<div class="distance"></div>	
	
	<div>
		<input id="hid_user_cd_isp_sub" type="text" class="input_text" value=""> Владелец в ISPmanager. Создайте пользователя в ISPmanager, если у вас его ещё нет (включите ему PHP как модуль, домены, трафик, БД, пользователи БД, диск - по максимуму).
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="login_panel_isp_sub" type="text" class="input_text"> Логин root в ISPmanager.
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="password_panel_isp_sub" type="password" class="input_text"> Пароль root`а в ISPmanager.
	</div>
	<div class="distance"></div>
	

	<div>
		<span onclick="create_subdomains_isp()" class="link_imitate" >Создать поддомены</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span id="create_subdomains_isp_indicator" class="indicator"></span>
	</div>

</div><?php }} ?>