function set_value_input(id_input, value){
	$('#'+id_input).val(value);	
	
	if(id_input == 'logs_grab'){		
		if(value == 0){		
			$('#logs_grab_0').attr('checked', 'checked');
		}else{
			$('#logs_grab_1').attr('checked', 'checked');
		}
	}		
}

function toogle_panel(name_panel){
	$('#'+name_panel).toggle('slow', function() {
		// Animation complete.
	}); 
}

function active_message_and_fade(){
	//вкл. затемнение	
	$('#message_box_wrapper').append('<div class="bg_layer"></div>');	
	$('.bg_layer').show().animate({
		opacity: 0.5
	});
}

function deactive_message_and_fade(){
	//выкл. затемнения
	$('#message_box_wrapper').html('');
}

function message_box(message){
		
	$('#message_box_wrapper').html('<div class="box_2 message_box"><strong>'+message+'</strong></div>');		
}

function message_box_big(message){	

	$('#message_box_wrapper').html('<div class="box_2 message_box_big"><strong>'+message+'</strong></div>');		
}

function toggle_hint(hint_text_id){	
	$('#'+hint_text_id).toggle('slow', function() { });	
}

function toggle_hint_fast(hint_text_id){	
	$('#'+hint_text_id).toggle(0, function() { });	
}

function open_add_site_panel(){
	$('#add_site_panel').toggle('slow', function() {
    // Animation complete.
  });
}

                  
function open_add_ads_panel(){
	$('#add_ads_panel').toggle('slow', function() {
    // Animation complete.
  });
}


function open_add_sites_blanks_panel(){
	$('#add_sites_blanks_panel').toggle('slow', function() {
    // Animation complete.
  });
}


function open_create_subdomain_panel(){
	$('#create_subdomains_panel').toggle('slow', function() {
    // Animation complete.
  });
}

function open_create_subdomain_isp_panel(){
	$('#create_subdomains_isp_panel').toggle('slow', function() {
    // Animation complete.
  });
}

function open_create_subdomain_cron_panel(){
	$('#create_subdomains_cron_panel').toggle('slow', function() {
    // Animation complete.
  });
}


function open_create_domain_panel(){
	$('#create_domains_panel').toggle('slow', function() {
    // Animation complete.
  });
}

function open_create_domain_isp_panel(){
	$('#create_domains_isp_panel').toggle('slow', function() {
    // Animation complete.
  });
}


function open_create_domain_cron_panel(){
	$('#create_domains_cron_panel').toggle('slow', function() {
    // Animation complete.
  });
}


function open_add_mainlink_panel(){
	$('#add_mainlink_panel').toggle('slow', function() {
    // Animation complete.
  });
}


function open_add_sape_panel(){
	$('#add_sape_panel').toggle('slow', function() {
    // Animation complete.
  });
}


function open_activate_sape_pages_and_price_panel(){
	$('#activate_sape_pages_and_price_panel').toggle('slow', function() {
    // Animation complete.
  });
}

function open_settings_panel(){	
	
	update_settings_panel();
	toogle_panel('settings_panel');	
}


function settings_save(){

	var key_sape = $('#key_sape').val();
	var key_mainlink = $('#key_mainlink').val();	
	var key_setlinks = $('#key_setlinks').val();
	var key_linkfeed = $('#key_linkfeed').val();
	var logs_grab = $('input:radio[name="logs_grab"]:checked').val();	
	
	
	var itog_location = create_url_for_request();
	
	//update условие 19_11_12-11_52
	if(key_sape != '' && key_mainlink != '' && key_setlinks != '' && key_linkfeed != ''){
	
		message_box('Происходит сохранение настроек.');
		active_message_and_fade();
		
		//alert(name_site+'  |  '+search_phrases+'  |  '+itog_location); 

		$.ajax({
			type: "POST",
			url: itog_location,
			data: {command_settings_save : 1, key_sape : key_sape, key_mainlink : key_mainlink, key_setlinks : key_setlinks, key_linkfeed : key_linkfeed, logs_grab : logs_grab},
			success: function(msg){								

				deactive_message_and_fade();
				
				//отладка
				//win1 = window.open("", "Отладка", "toolbar=0,width=450,height=600,scrollbars=YES"); 
				//win1.document.write('<html><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Отладка</title><pre>'+msg+'</pre></html>'); 
				//win1.document.close() 
				//win1.focus();	
				//конец отладки
				if(msg == 1){
					$('#settings_indicator').animate({
						opacity: 1,
					}, 50).html('Настройки сохранены!').animate({
						opacity: 0,
					}, 4000);	
				}else{
					$('#settings_indicator').animate({
						opacity: 1,
					}, 50).html('Ошибка записи файла шаблона ROOT/site/index.php! Проверьте права на запись.').animate({
						opacity: 0,
					}, 4000);
				}
			}	
		});

	}else{

		$('#settings_indicator').animate({
			opacity: 1,
		}, 50).html('<span style="color:red;">Не все поля заполнены. Настройки не сохранены!</span>').animate({
			opacity: 0,
		}, 4000);		
	}
	
	
}	


function fill_fields_cd_from_file(){	

	var itog_location = create_url_for_request();
	
	message_box('Происходит заполнение полей регистрации и добаления доменов данными из файла.');
	active_message_and_fade();
	
	$.ajax({
		type: "POST",

		url: itog_location,

		data: {command_fill_fields_cd_from_file : 1},

		success: function(msg){	
			
			json = JSON.parse(msg);

			//отладка
			//alert(json);
			
			for (var key in json) {		
				//отладка
				//alert('key-'+json[key]['index']+'---value-'+json[key]['value']);
				$('#'+json[key]['index']).val(json[key]['value']);			  
			}
			
			
			deactive_message_and_fade();
		}	

	});
}


function fill_fields_cd_from_file_cron(){	

	var itog_location = create_url_for_request();
	
	message_box('Происходит заполнение полей регистрации и добаления доменов данными из файла.');
	active_message_and_fade();
	
	$.ajax({
		type: "POST",

		url: itog_location,

		data: {command_fill_fields_cd_from_file : 1},

		success: function(msg){	
			
			json = JSON.parse(msg);

			//отладка
			//alert(json);
			
			for (var key in json) {		
				//отладка
				//alert('key-'+json[key]['index']+'---value-'+json[key]['value']);
				$('#'+json[key]['index']+'_cron').val(json[key]['value']);			  
			}
			
			
			deactive_message_and_fade();
		}	

	});
}


function add_site(){

	var name_site = $('#name_site').val();
	var search_phrases = $('#search_phrases').val();	
	var b_list_words = $('#b_list_words').val();
	var domain = $('#domain').val();
	var id_tag = $('#id_tag').val();	

	var itog_location = create_url_for_request();
	
	//update условие 19_11_12-11_52
	if(search_phrases != '' && name_site != '' && domain != ''){	

	
		message_box('Происходит добавление сайта.');
		active_message_and_fade();
		
		//alert(name_site+'  |  '+search_phrases+'  |  '+itog_location); 

		$.ajax({

			type: "POST",

			url: itog_location,

			data: {command_add_site : 1, name_site : name_site, search_phrases : search_phrases, domain : domain, id_tag : id_tag, b_list_words : b_list_words},

			success: function(msg){	

				$('#name_site').val('');
				$('#search_phrases').val('');				
				$('#b_list_words').val('');
				$('#domain').val('site_1');
				$('#id_tag').val('27');
				//alert(msg);				

				deactive_message_and_fade();			

				$('#add_site_indicator').animate({
					opacity: 1,
				}, 50).html('Сайт добавлен!').animate({
					opacity: 0,
				}, 4000);	
				
				update_sites_panel();				
			}	
		});		

	}else{

		$('#add_site_indicator').animate({
			opacity: 1,
		}, 50).html('<span style="color:red;">Не все поля заполнены. Сайт НЕ добавлен!</span>').animate({
			opacity: 0,
		}, 4000);	
	}
}			


//08_11_12 00_55
function add_ads(){

	var name_ads = $('#name_ads').val();
	var ads_position = $('#ads_position').val();	
	var ads_sorting = $('#ads_sorting').val();
	var code_ads = $('#code_ads').val();	
	var itog_location = create_url_for_request();	

	if(code_ads != '' && ads_sorting != '' && ads_position != '' && name_ads != ''){	

		message_box('Происходит вставка рекламного блока в базу данных для всех сайтов.');			
		active_message_and_fade();		
		//alert(name_site+'  |  '+search_phrases+'  |  '+itog_location); 

		$.ajax({
			type: "POST",
			url: itog_location,
			data: {command_add_ads : 1, name_ads : name_ads, ads_position : ads_position, ads_sorting : ads_sorting, code_ads : code_ads},
			success: function(msg){	
				$('#name_ads').val('');
				//$('#ads_position').val('');				
				$('#ads_sorting').val('');
				$('#code_ads').val('');				
				
				deactive_message_and_fade();
				//alert(msg);				
				
				win1 = window.open("", "Отчёт о вставке рекламного блока", "toolbar=0,width=450,height=600,scrollbars=YES"); 
				win1.document.write('<html><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Отчёт о вставке рекламного блока в базу данных</title>В таблицу ads_links вставлены строки с ads_links_id_min по ads_links_id_max, количеством count_ads_links:<br><pre>'+msg+'</pre></html>'); 
				win1.document.close() 
				win1.focus(); 					

				$('#add_ads_indicator').animate({
					opacity: 1,
				}, 50).html('Отправлено, смотрите отчёт во всплывающем окне (разблокируйте его в браузере)!').animate({
					opacity: 0,
				}, 4000);
			}	
		});		

	}else{
		$('#add_ads_indicator').animate({
			opacity: 1,
		}, 50).html('<span style="color:red;">Не все поля заполнены!</span>').animate({
			opacity: 0,
		}, 4000);		
	}
}


//07_01_13 04_05
function add_sites_blanks(){

	var count_sites_blacks = $('#count_sites_blacks').val();
	var itog_location = create_url_for_request();	

	if(count_sites_blacks != ''){		
	
		message_box('Происходит создание заготовок для сайтов.');
		active_message_and_fade();
		//alert(name_site+'  |  '+search_phrases+'  |  '+itog_location); 

		$.ajax({
			type: "POST",
			url: itog_location,
			data: {command_add_sites_blacks : 1, count_sites_blacks : count_sites_blacks},
			success: function(msg){	

				update_sites_panel();				
				deactive_message_and_fade();

				$('#add_sites_blanks_indicator').animate({
					opacity: 1,
				}, 50).html('Заготовки созданы!').animate({
					opacity: 0,
				}, 4000);		
				//update_sites_panel();
			}	
		});		

	}else{
		$('#add_sites_blanks_indicator').animate({
			opacity: 1,
		}, 50).html('<span style="color:red;">Не все поля заполнены!</span>').animate({
			opacity: 0,
		}, 4000);		
	}
}


//12_01_13 23_59
function create_subdomains(){

	//переключает видимость кнопки
	//toggle_hint_fast($(element).attr("id"));		
	var up_domains = $('#up_domains').val();	
	var number_farm = $('#number_farm').val();	
	var theme_cpanel = $('#theme_cpanel').val();	
	var login_cpanel = $('#login_cpanel').val();	
	var password_cpanel = $('#password_cpanel').val();
	
	var itog_location = create_url_for_request();	

	if(up_domains != '' && number_farm != '' && theme_cpanel != '' && login_cpanel != '' && password_cpanel != ''){	

		message_box('Происходит создание поддоменов.');
		active_message_and_fade();		
		//alert(name_site+'  |  '+search_phrases+'  |  '+itog_location); 

		$.ajax({
			type: "POST",
			url: itog_location,
			data: {command_create_subdomains : 1, up_domains : up_domains, number_farm : number_farm, theme_cpanel : theme_cpanel, login_cpanel : login_cpanel, password_cpanel : password_cpanel},
			success: function(msg){										
				
				deactive_message_and_fade();				
				
				win1 = window.open("", "Отчёт о создании поддоменов", "toolbar=0,width=450,height=600,scrollbars=YES"); 
				win1.document.write('<html><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Отчёт о создании поддоменов</title><br><pre>'+msg+'</pre></html>'); 
				win1.document.close() 
				win1.focus(); 

				$('#create_subdomains_indicator').animate({
					opacity: 1,
				}, 50).html('Субдомены созданы!').animate({
					opacity: 0,
				}, 4000);						
		
				//переключает видимость кнопки
				//toggle_hint_fast($(element).attr("id"));
			}	
		});		

	}else{
		$('#create_subdomains_indicator').animate({
			opacity: 1,
		}, 50).html('<span style="color:red;">Не все поля заполнены!</span>').animate({
			opacity: 0,
		}, 4000);	

		//переключает видимость кнопки
		//toggle_hint_fast($(element).attr("id"));
	}
}



//04_03_13 00_48
function create_subdomains_isp(){

	//переключает видимость кнопки
	//toggle_hint_fast($(element).attr("id"));		
	var up_domains = $('#up_domains_isp_sub').val();	
	var number_farm = $('#number_farm_isp_sub').val();	
	var hid_user = $('#hid_user_cd_isp_sub').val();	
	var login_panel = $('#login_panel_isp_sub').val();	
	var password_panel = $('#password_panel_isp_sub').val();
	var hid_email = $('#hid_email_isp_sub').val();
	var port = $('#port_isp_sub').val();
	
	var itog_location = create_url_for_request();	

	//отладка
	//alert(up_domains+'  |  '+number_farm+'  |  '+hid_user+'  |  '+login_panel+'  |  '+password_panel);
	
	if(up_domains != '' && number_farm != '' && hid_user != '' && login_panel != '' && password_panel != '' && hid_email != ''){	

		message_box('Происходит создание поддоменов.');
		active_message_and_fade();		
		//alert(name_site+'  |  '+search_phrases+'  |  '+itog_location); 

		$.ajax({
			type: "POST",
			url: itog_location,
			data: {command_create_subdomains_isp : 1, up_domains : up_domains, number_farm : number_farm, hid_user : hid_user, login_panel : login_panel, password_panel : password_panel, hid_email : hid_email, port : port},
			success: function(msg){										
				
				deactive_message_and_fade();				
				
				win1 = window.open("", "Отчёт о создании поддоменов", "toolbar=0,width=450,height=600,scrollbars=YES"); 
				win1.document.write('<html><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Отчёт о создании поддоменов</title><br><pre>'+msg+'</pre></html>'); 
				win1.document.close() 
				win1.focus(); 

				$('#create_subdomains_isp_indicator').animate({
					opacity: 1,
				}, 50).html('Субдомены созданы!').animate({
					opacity: 0,
				}, 4000);						
		
				//переключает видимость кнопки
				//toggle_hint_fast($(element).attr("id"));
			}	
		});		

	}else{
		$('#create_subdomains_isp_indicator').animate({
			opacity: 1,
		}, 50).html('<span style="color:red;">Не все поля заполнены!</span>').animate({
			opacity: 0,
		}, 4000);	

		//переключает видимость кнопки
		//toggle_hint_fast($(element).attr("id"));
	}
}


//24_02_13 14_52
function create_subdomains_cron(){

	//переключает видимость кнопки
	//toggle_hint_fast($(element).attr("id"));		
	var up_domains = $('#up_domains_cron').val();	
	var number_farm = $('#number_farm_cron').val();	
	var theme_cpanel = $('#theme_cpanel_cron').val();	
	var login_cpanel = $('#login_cpanel_cron').val();	
	var password_cpanel = $('#password_cpanel_cron').val();
	
	var itog_location = create_url_for_request();	

	if(up_domains != '' && number_farm != '' && theme_cpanel != '' && login_cpanel != '' && password_cpanel != ''){	

		message_box('Происходит добавление задания на создание поддоменов.');
		active_message_and_fade();		
		//alert(name_site+'  |  '+search_phrases+'  |  '+itog_location); 

		$.ajax({
			type: "POST",
			url: itog_location,
			data: {command_create_subdomains_cron : 1, up_domains : up_domains, number_farm : number_farm, theme_cpanel : theme_cpanel, login_cpanel : login_cpanel, password_cpanel : password_cpanel},
			success: function(msg){										
				
				deactive_message_and_fade();				
				
				/*
				win1 = window.open("", "Отчёт о создании поддоменов", "toolbar=0,width=450,height=600,scrollbars=YES"); 
				win1.document.write('<html><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Отчёт о создании поддоменов</title><br><pre>'+msg+'</pre></html>'); 
				win1.document.close() 
				win1.focus(); 
				*/

				$('#create_subdomains_cron_indicator').animate({
					opacity: 1,
				}, 50).html('Задание на создание поддоменов добавлено!').animate({
					opacity: 0,
				}, 4000);						
		
				//переключает видимость кнопки
				//toggle_hint_fast($(element).attr("id"));
			}	
		});		

	}else{
		$('#create_subdomains_cron_indicator').animate({
			opacity: 1,
		}, 50).html('<span style="color:red;">Не все поля заполнены!</span>').animate({
			opacity: 0,
		}, 4000);	

		//переключает видимость кнопки
		//toggle_hint_fast($(element).attr("id"));
	}
}


//18_01_13 21_30
function create_domains(){
	
	var username = $('#username_cd').val();
	var password = $('#password_cd').val();
	var folder_name = $('#folder_name_cd').val();
	var post_code = $('#post_code_cd').val();
	var region = $('#region_cd').val();
	var town = $('#town_cd').val();
	var street_house_room = $('#street_house_room_cd').val();
	var phone = $('#phone_cd').val();
	var e_mail = $('#e_mail_cd').val();
	var person_en = $('#person_en_cd').val();
	var person_r = $('#person_r_cd').val();
	var birth_date = $('#birth_date_cd').val();
	var private_person_flag = $('#private_person_flag_cd').val();	
	var passport_number = $('#passport_number_cd').val();
	var passport_issued_org = $('#passport_issued_org_cd').val();
	var passport_issued_date = $('#passport_issued_date_cd').val();
	var country = $('#country_cd').val();
	var ns0 = $('#ns0_cd').val();
	var ns1 = $('#ns1_cd').val();	
	
	var number_farm = $('#number_farm_cd').val();	
	var theme_cpanel = $('#theme_cpanel_cd').val();	
	var login_cpanel = $('#login_cpanel_cd').val();	
	var password_cpanel = $('#password_cpanel_cd').val();	

	var itog_location = create_url_for_request();
	//отладка
	//alert('number_farm-'+number_farm+'|||theme_cpanel-'+theme_cpanel+'|||login_cpanel-'+login_cpanel+'|||password_cpanel-'+password_cpanel+'|||username-'+username+'|||password-'+password+'|||folder_name-'+folder_name+'|||post_code-'+post_code+'|||region-'+region+'|||town-'+town+'|||street_house_room-'+street_house_room+'|||phone-'+phone+'|||e_mail-'+e_mail+'|||person_en-'+person_en+'|||person_r-'+person_r+'|||birth_date-'+birth_date+'|||private_person_flag-'+private_person_flag+'|||passport_number-'+passport_number+'|||passport_issued_org-'+passport_issued_org+'|||passport_issued_date-'+passport_issued_date+'|||country-'+country+'|||ns0-'+ns0+'|||ns1-'+ns1);
	
	if(number_farm != '' && theme_cpanel != '' && login_cpanel != '' && password_cpanel != '' && username != '' && password != '' && folder_name != '' && post_code != '' && region != '' && town != '' && street_house_room != '' && phone != '' && e_mail != '' && person_en != '' && person_r != '' && birth_date != '' && passport_number != '' && passport_issued_org != '' && passport_issued_date != '' && country != '' && ns0 != '' && ns1 != ''){	
		 
		message_box('Происходит создание доменов.');
		active_message_and_fade();
		
		//alert(name_site+'  |  '+search_phrases+'  |  '+itog_location); 

		$.ajax({
			type: "POST",
			url: itog_location,
			data: {command_create_domains : 1, number_farm : number_farm, theme_cpanel : theme_cpanel, login_cpanel : login_cpanel, password_cpanel : password_cpanel,
				username : username, password : password, folder_name : folder_name, post_code : post_code,
				region : region, town : town, street_house_room : street_house_room, phone : phone,
				e_mail : e_mail, person_en : person_en, person_r : person_r, birth_date : birth_date,
				passport_number : passport_number, passport_issued_org : passport_issued_org,
				passport_issued_date : passport_issued_date, country : country, ns0 : ns0, ns1 : ns1, private_person_flag : private_person_flag},
			success: function(msg){						
							
				deactive_message_and_fade();				
				win1 = window.open("", "Отчёт о создании доменов", "toolbar=0,width=450,height=600,scrollbars=YES"); 
				win1.document.write('<html><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Отчёт о создании доменов</title><br><pre>'+msg+'</pre></html>'); 
				win1.document.close() 
				win1.focus(); 

				$('#create_domains_indicator').animate({
					opacity: 1,
				}, 50).html('Домены созданы!').animate({
					opacity: 0,
				}, 4000);		
				//update_sites_panel();
			}	
		});		

	}else{
		$('#create_domains_indicator').animate({
			opacity: 1,
		}, 50).html('<span style="color:red;">Не все поля заполнены!</span>').animate({
			opacity: 0,
		}, 4000);		
	}
	
}


//18_01_13 21_30
function create_domains_isp(){
	
	var username = $('#username_cd_isp').val();
	var password = $('#password_cd_isp').val();
	var folder_name = $('#folder_name_cd_isp').val();
	var post_code = $('#post_code_cd_isp').val();
	var region = $('#region_cd_isp').val();
	var town = $('#town_cd_isp').val();
	var street_house_room = $('#street_house_room_cd_isp').val();
	var phone = $('#phone_cd_isp').val();
	var e_mail = $('#e_mail_cd_isp').val();
	var person_en = $('#person_en_cd_isp').val();
	var person_r = $('#person_r_cd_isp').val();
	var birth_date = $('#birth_date_cd_isp').val();
	var private_person_flag = $('#private_person_flag_cd_isp').val();	
	var passport_number = $('#passport_number_cd_isp').val();
	var passport_issued_org = $('#passport_issued_org_cd_isp').val();
	var passport_issued_date = $('#passport_issued_date_cd_isp').val();
	var country = $('#country_cd_isp').val();
	var ns0 = $('#ns0_cd_isp').val();
	var ns1 = $('#ns1_cd_isp').val();	
	
	var number_farm = $('#number_farm_cd_isp').val();	
	var hid_user = $('#hid_user_cd_isp').val();	
	var login_panel = $('#login_panel_cd_isp').val();	
	var password_panel = $('#password_panel_cd_isp').val();	
	
	var port = $('#port_isp_sub').val();

	var itog_location = create_url_for_request();
	//отладка
	//alert('number_farm-'+number_farm+'|||theme_cpanel-'+theme_cpanel+'|||login_cpanel-'+login_cpanel+'|||password_cpanel-'+password_cpanel+'|||username-'+username+'|||password-'+password+'|||folder_name-'+folder_name+'|||post_code-'+post_code+'|||region-'+region+'|||town-'+town+'|||street_house_room-'+street_house_room+'|||phone-'+phone+'|||e_mail-'+e_mail+'|||person_en-'+person_en+'|||person_r-'+person_r+'|||birth_date-'+birth_date+'|||private_person_flag-'+private_person_flag+'|||passport_number-'+passport_number+'|||passport_issued_org-'+passport_issued_org+'|||passport_issued_date-'+passport_issued_date+'|||country-'+country+'|||ns0-'+ns0+'|||ns1-'+ns1);
	
	if(number_farm != '' && hid_user != '' && login_panel != '' && password_panel != '' && username != '' && password != '' && folder_name != '' && post_code != '' && region != '' && town != '' && street_house_room != '' && phone != '' && e_mail != '' && person_en != '' && person_r != '' && birth_date != '' && passport_number != '' && passport_issued_org != '' && passport_issued_date != '' && country != '' && ns0 != '' && ns1 != ''){	
		 
		message_box('Происходит создание доменов.');
		active_message_and_fade();
		
		//alert(name_site+'  |  '+search_phrases+'  |  '+itog_location); 

		$.ajax({
			type: "POST",
			url: itog_location,
			data: {command_create_domains_isp : 1, number_farm : number_farm, hid_user : hid_user, login_panel : login_panel, password_panel : password_panel,
				username : username, password : password, folder_name : folder_name, post_code : post_code,
				region : region, town : town, street_house_room : street_house_room, phone : phone,
				e_mail : e_mail, person_en : person_en, person_r : person_r, birth_date : birth_date,
				passport_number : passport_number, passport_issued_org : passport_issued_org,
				passport_issued_date : passport_issued_date, country : country, ns0 : ns0, ns1 : ns1, private_person_flag : private_person_flag, port : port},
			success: function(msg){						
							
				deactive_message_and_fade();				
				win1 = window.open("", "Отчёт о создании доменов", "toolbar=0,width=450,height=600,scrollbars=YES"); 
				win1.document.write('<html><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Отчёт о создании доменов</title><br><pre>'+msg+'</pre></html>'); 
				win1.document.close() 
				win1.focus(); 

				$('#create_domains_isp_indicator').animate({
					opacity: 1,
				}, 50).html('Домены созданы!').animate({
					opacity: 0,
				}, 4000);		
				//update_sites_panel();
			}	
		});		

	}else{
		$('#create_domains_isp_indicator').animate({
			opacity: 1,
		}, 50).html('<span style="color:red;">Не все поля заполнены!</span>').animate({
			opacity: 0,
		}, 4000);		
	}
	
}


//23_02_13 13_59
function create_domains_cron(){
	
	var username = $('#username_cd_cron').val();
	var password = $('#password_cd_cron').val();
	var folder_name = $('#folder_name_cd_cron').val();
	var post_code = $('#post_code_cd_cron').val();
	var region = $('#region_cd_cron').val();
	var town = $('#town_cd_cron').val();
	var street_house_room = $('#street_house_room_cd_cron').val();
	var phone = $('#phone_cd_cron').val();
	var e_mail = $('#e_mail_cd_cron').val();
	var person_en = $('#person_en_cd_cron').val();
	var person_r = $('#person_r_cd_cron').val();
	var birth_date = $('#birth_date_cd_cron').val();
	var private_person_flag = $('#private_person_flag_cd_cron').val();	
	var passport_number = $('#passport_number_cd_cron').val();
	var passport_issued_org = $('#passport_issued_org_cd_cron').val();
	var passport_issued_date = $('#passport_issued_date_cd_cron').val();
	var country = $('#country_cd_cron').val();
	var ns0 = $('#ns0_cd_cron').val();
	var ns1 = $('#ns1_cd_cron').val();	
	
	var number_farm = $('#number_farm_cd_cron').val();	
	var theme_cpanel = $('#theme_cpanel_cd_cron').val();	
	var login_cpanel = $('#login_cpanel_cd_cron').val();	
	var password_cpanel = $('#password_cpanel_cd_cron').val();	

	var itog_location = create_url_for_request();
	//отладка
	//alert('number_farm-'+number_farm+'|||theme_cpanel-'+theme_cpanel+'|||login_cpanel-'+login_cpanel+'|||password_cpanel-'+password_cpanel+'|||username-'+username+'|||password-'+password+'|||folder_name-'+folder_name+'|||post_code-'+post_code+'|||region-'+region+'|||town-'+town+'|||street_house_room-'+street_house_room+'|||phone-'+phone+'|||e_mail-'+e_mail+'|||person_en-'+person_en+'|||person_r-'+person_r+'|||birth_date-'+birth_date+'|||private_person_flag-'+private_person_flag+'|||passport_number-'+passport_number+'|||passport_issued_org-'+passport_issued_org+'|||passport_issued_date-'+passport_issued_date+'|||country-'+country+'|||ns0-'+ns0+'|||ns1-'+ns1);
	
	if(number_farm != '' && theme_cpanel != '' && login_cpanel != '' && password_cpanel != '' && username != '' && password != '' && folder_name != '' && post_code != '' && region != '' && town != '' && street_house_room != '' && phone != '' && e_mail != '' && person_en != '' && person_r != '' && birth_date != '' && passport_number != '' && passport_issued_org != '' && passport_issued_date != '' && country != '' && ns0 != '' && ns1 != ''){	
		 
		message_box('Происходит добавление задания по созданию доменов в cron.');
		active_message_and_fade();
		
		//alert(name_site+'  |  '+search_phrases+'  |  '+itog_location); 

		$.ajax({
			type: "POST",
			url: itog_location,
			data: {command_create_domains_cron : 1, number_farm : number_farm, theme_cpanel : theme_cpanel, login_cpanel : login_cpanel, password_cpanel : password_cpanel,
				username : username, password : password, folder_name : folder_name, post_code : post_code,
				region : region, town : town, street_house_room : street_house_room, phone : phone,
				e_mail : e_mail, person_en : person_en, person_r : person_r, birth_date : birth_date,
				passport_number : passport_number, passport_issued_org : passport_issued_org,
				passport_issued_date : passport_issued_date, country : country, ns0 : ns0, ns1 : ns1, private_person_flag : private_person_flag},
			success: function(msg){						
							
				deactive_message_and_fade();
				
				/*
				win1 = window.open("", "Отчёт о добавлении задания по созданию доменов", "toolbar=0,width=450,height=600,scrollbars=YES"); 
				win1.document.write('<html><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Отчёт о добавлении задания по созданию доменов</title><br><pre>'+msg+'</pre></html>'); 
				win1.document.close() 
				win1.focus(); 
				*/

				$('#create_domains_cron_indicator').animate({
					opacity: 1,
				}, 50).html('Задание добавлено!').animate({
					opacity: 0,
				}, 4000);		
				//update_sites_panel();
			}	
		});		

	}else{
		$('#create_domains_cron_indicator').animate({
			opacity: 1,
		}, 50).html('<span style="color:red;">Не все поля заполнены!</span>').animate({
			opacity: 0,
		}, 4000);		
	}
	
}


//обновление панели сайтов
function update_sites_panel(){	

	var itog_location = create_url_for_request();	
	
	$.ajax({
		type: "POST",
		url: itog_location,
		data: {command_update_sites_panel : 1},
		success: function(msg){				
			$('#sites_panel_wrapper').html(msg);
		}	
	});
}


//обновление панели настроек
function update_settings_panel(){	
	
	$('.settings_input').val('загружается...');
	$('#settings_sabmit').hide();

	var itog_location = create_url_for_request();	
	
	$.ajax({
		type: "POST",
		url: itog_location,
		data: {command_update_settings_panel : 1},
		success: function(msg){				
			//alert(msg);
			obj = JSON.parse(msg);
			//alert(obj);
			//console.log(obj);
			
			for (var key in obj) {
			  var val = obj[key];
			  //console.log(val.name);
			  set_value_input(val.name, val.value);
			}
			
			$('#settings_sabmit').show();
			/*
			$('#work_panel_wrapper').html(msg);
			toogle_panel('settings_panel');
			*/
		}	
	});
}


//add 11_11_12 21_34
function update_content_publish(){	

	//подтверждение
	if (confirm("Уверены?")){

		var itog_location = create_url_for_request();
		
		message_box('Происходит обновление публикации контента на всех сайтах.');
		active_message_and_fade();
		
		$.ajax({
			type: "POST",
			url: itog_location,
			data: {command_update_content_publish : 1},
			success: function(msg){				
				update_sites_panel();			
				deactive_message_and_fade();				
			},				
			complete: function(msg){
				alert('Готово!');
			}
		});	
	}
}


//создание общего файла для соц. закладок
function social_bookmark(){	

	//подтверждение
	if (confirm("Уверены?")){

		var itog_location = create_url_for_request();	

		message_box('Происходит создание общего файла для социальных закладок.</strong><br> Это займёт некоторое время.');
		active_message_and_fade();
		
		$.ajax({
			type: "POST",
			url: itog_location,
			data: {command_social_bookmark : 1},
			success: function(msg){	
				deactive_message_and_fade();		

				//обновление панели сайтов
				$('#sites_panel_wrapper').html(msg);			

				alert('Готово!');
			}	
		});
	}
}


//первичный сбор контента
function primary_grab(){	

	//подтверждение
	if (confirm("Уверены?")){
	
		var itog_location = create_url_for_request();	
		
		message_box('Происходит сбор и обработка контента. Это может занять длительное время, около часа на 1 сайт.');
		active_message_and_fade();

		
		//$("#message_box_wrapper").html('<div class="box_2 message_box_big"></div>');
		//$(".message_box_big").load(itog_location, {command_primary_grab: 1});
		
		query_logs_grab();
		
		$.ajax({
			type: "POST",
			url: itog_location,				
			data: {command_primary_grab : 1},
			success: function(msg){					
				
				
				clearInterval(window.interval_update_logs_view);
				
				//выводим msg чтобы если будут ошибки показать их, иначе msg будет пустой
				message_box_big(msg+'<div style="margin-top:50px;padding-top:20px;font-weight:bold;">Готово!</div>');
				//alert('Готово!');
				//document.write(msg);
			},
			complete: function(msg){
				//deactive_message_and_fade();
				//update_sites_panel();
			},
			error: function(msg){				
				clearInterval(window.interval_update_logs_view);
				//выводим msg чтобы если будут ошибки показать их, иначе msg будет пустой
				message_box_big(msg+'<div style="margin-top:50px;padding-top:20px;font-weight:bold;">Ошибка!</div>');
			}			
		});	
	}
}

//пока не используется (нужно довести до ума, не работает)
function abort_ajax(var_ajax){
	var_ajax.abort();
}


function query_logs_grab(){

	var ret_mas = 0;
	var itog_location = create_url_for_request();
	$.ajax({
		type: "POST",
		url: itog_location,				
		data: {command_query_logs_grab : 1},
		success: function(logs_grab){
			
			if(logs_grab == 1){
				//первый вывод
				update_logs_view();
				active_message_and_fade();
				//вывод логов в цикле
				window.interval_update_logs_view = setInterval(update_logs_view, 20000);
			}
		}	
	});			
}


function update_logs_view(){

	var itog_location = create_url_for_request();
	$.ajax({
		type: "POST",
		url: itog_location,				
		data: {command_update_logs_view : 1},
		success: function(msg){				
			message_box_big(msg);
			active_message_and_fade();
		}		
	});	
}


//первичный сбор контента
function refresh_small_content_sites(){

	//подтверждение
	if (confirm("Уверены?")){
		
		var itog_location = create_url_for_request();	

		message_box('Происходит сбор и обработка контента (вторичная, глубокая).<br> Это может занять длительный период времени, при стандартных настройках около 30 минут на 1 сайт.');
		active_message_and_fade();		

		$.ajax({
			type: "POST",
			url: itog_location,
			data: {command_refresh_small_content_sites : 1},
			success: function(msg){	
				deactive_message_and_fade();	
				
				//обновление панели сайтов
				$('#sites_panel_wrapper').html(msg);			

				alert('Готово!');
				//document.write(msg);
			}	
		});
	}
}


//первичный сбор контента
function delete_small_content_sites(){
	
	//подтверждение
	if (confirm("Уверены?")){

		var itog_location = create_url_for_request();	

		message_box('Происходит удаление сайтов у которых мало текстов.<br> Это может занять некоторый период времени.');
		active_message_and_fade();

		$.ajax({
			type: "POST",
			url: itog_location,
			data: {command_delete_small_content_sites : 1},
			success: function(msg){	
				deactive_message_and_fade();			

				//обновление панели сайтов
				$('#sites_panel_wrapper').html(msg);			

				alert('Готово!');
				//document.write(msg);
			}	
		});
	}
}


//назначение прав доступа требуемым папкам
function chmod_update(){	

	//подтверждение
	if (confirm("Уверены?")){

		var itog_location = create_url_for_request();	

		message_box('Происходит назначение прав доступа на папки изображений, бирж ссылок и т.д.<br> Это может занять некоторый период времени.');
		active_message_and_fade();

		$.ajax({
			type: "POST",
			url: itog_location,
			data: {command_chmod_update : 1},
			success: function(msg){	
				deactive_message_and_fade();			

				//обновление панели сайтов
				$('#sites_panel_wrapper').html(msg);				

				alert('Готово!');
				//document.write(msg);			
			}	
		});
	}
}


//add_09_11_12_23_51
function update_all_design(){

	//подтверждение
	if (confirm("Уверены?")){

		var itog_location = create_url_for_request();	

		message_box('Происходит обновление дизайнов у всех сайтов. Это может занять до 7 секунд на один сайт.');
		active_message_and_fade();

		$.ajax({
			type: "POST",
			url: itog_location,
			data: {command_update_all_design : 1},
			success: function(msg){	
				deactive_message_and_fade();
				
				win1 = window.open("", "Обновление дизайнов у всех сайтов", "toolbar=0,width=450,height=600,scrollbars=YES"); 
				win1.document.write('<html><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Обновление дизайнов у всех сайтов</title><pre>'+msg+'</pre></html>'); 
				win1.document.close() 
				win1.focus();
			}	
		});
	}
}


//add_01_11_12_23_44
function liveinternet_reg(){

	//подтверждение
	if (confirm("Уверены?")){
		
		var itog_location = create_url_for_request();	

		message_box('Происходит регистрация сайтов в системе статистики liveinternet.ru. Может занять до 2 секунд на один сайт. По окончании работы появится окно с отчётом, ваш браузер скорей всего заблокирует его, вам нужно будет это исправить.');
		active_message_and_fade();

		$.ajax({
			type: "POST",
			url: itog_location,
			data: {command_liveinternet_reg : 1},
			success: function(msg){	
				deactive_message_and_fade();
				
				win1 = window.open("", "liveinternet.ru", "toolbar=0,width=450,height=600,scrollbars=YES"); 
				win1.document.write('<html><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Отчёт регистрации сайтов liveinternet.ru</title><pre>'+msg+'</pre></html>'); 
				win1.document.close() 
				win1.focus(); 
			}	
		});
	}
}


//add_07_11_12_01_19
function grab_liveinternet(){

	//подтверждение
	if (confirm("Уверены?")){

		var itog_location = create_url_for_request();	

		message_box('Происходит сбор статистики liveinternet.ru.');
		active_message_and_fade();

		$.ajax({
			type: "POST",
			url: itog_location,
			data: {command_grab_liveinternet : 1},
			success: function(msg){	
				deactive_message_and_fade();
				$('#statistic_block').html(msg);
				
				/*
				win1 = window.open("", "liveinternet.ru", "toolbar=0,width=450,height=600,scrollbars=YES"); 
				win1.document.write('<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Отчёт регистрации сайтов liveinternet.ru</title></head><body><pre>'+msg+'</pre></body></html>'); 
				win1.document.close() 
				win1.focus(); 
				*/
			}	
		});
	}
}


//add_22_01_13-16_02
function activate_mainlink(){
	
	var login_mainlink = $('#login_mainlink').val();	
	var password_mainlink = $('#password_mainlink').val();	
	var count_main_links = $('#count_main_links').val();	
	var count_other_links = $('#count_other_links').val();	
	var multiplication = $('#multiplication').val();	
	var stop_words = $('#stop_words').val();		
	
	var dol_main_page = $('#dol_main_page').val();
	var rub_main_page = $('#rub_main_page').val();
	var dol_lvl_1_page = $('#dol_lvl_1_page').val();
	var rub_lvl_1_page = $('#rub_lvl_1_page').val();
	var dol_lvl_2_page = $('#dol_lvl_2_page').val();
	var rub_lvl_2_page = $('#rub_lvl_2_page').val();
	var dol_lvl_3_page = $('#dol_lvl_3_page').val();
	var rub_lvl_3_page = $('#rub_lvl_3_page').val();	

	var itog_location = create_url_for_request();	

	if(login_mainlink != '' && password_mainlink != '' && count_main_links != '' && count_other_links != '' && multiplication != '' && stop_words != '' && dol_main_page != '' && rub_main_page != '' && dol_lvl_1_page != '' && rub_lvl_1_page != '' && dol_lvl_2_page != '' && rub_lvl_2_page != '' && dol_lvl_3_page != '' && rub_lvl_3_page != ''){		

		message_box('Происходит активация и добавление сайтов в Mainlink.');
		active_message_and_fade();

		$.ajax({
			type: "POST",
			url: itog_location,
			data: {command_activate_mainlink : 1 , login_mainlink : login_mainlink, password_mainlink : password_mainlink , count_main_links : count_main_links , count_other_links : count_other_links, multiplication : multiplication, stop_words : stop_words, dol_main_page : dol_main_page, rub_main_page : rub_main_page, dol_lvl_1_page : dol_lvl_1_page, rub_lvl_1_page : rub_lvl_1_page, dol_lvl_2_page : dol_lvl_2_page, rub_lvl_2_page : rub_lvl_2_page, dol_lvl_3_page : dol_lvl_3_page, rub_lvl_3_page : rub_lvl_3_page},
			success: function(msg){					
				deactive_message_and_fade();

				//$('#statistic_block').html(msg);			
				
				win1 = window.open("", "mainlink.ru", "toolbar=0,width=450,height=600,scrollbars=YES"); 
				win1.document.write('<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Отчёт добавления сайтов в mainlink.ru</title></head><body><pre>'+msg+'</pre></body></html>'); 
				win1.document.close();
				win1.focus(); 			
			}	
		});
	
	}else{
		$('#add_mainlink_indicator').animate({
			opacity: 1,
		}, 50).html('<span style="color:red;">Не все поля заполнены!</span>').animate({
			opacity: 0,
		}, 4000);		
	}
}



//add_08_02_13-02_20
function activate_and_add_sape(){
	
	var login_sape = $('#login_sape').val();	
	var password_sape = $('#password_sape').val();	
	var count_main_links_sape = $('#count_main_links_sape').val();	
	var count_other_links_sape = $('#count_other_links_sape').val();	

	var itog_location = create_url_for_request();	

	if(login_sape != '' && password_sape != '' && count_main_links_sape != '' && count_other_links_sape != ''){		

		message_box('Происходит активация и добавление сайтов в Sape.');
		active_message_and_fade();

		$.ajax({
			type: "POST",
			url: itog_location,
			data: {command_activate_add_sape : 1 , login_sape : login_sape, password_sape : password_sape , count_main_links_sape : count_main_links_sape , count_other_links_sape : count_other_links_sape},
			success: function(msg){					
				deactive_message_and_fade();				
				
				win1 = window.open("", "sape.ru", "toolbar=0,width=450,height=600,scrollbars=YES"); 
				win1.document.write('<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Отчёт добавления сайтов в sape.ru</title></head><body><pre>'+msg+'</pre></body></html>'); 
				win1.document.close();
				win1.focus(); 	
			}	
		});
	
	}else{
		$('#add_sape_indicator').animate({
			opacity: 1,
		}, 50).html('<span style="color:red;">Не все поля заполнены!</span>').animate({
			opacity: 0,
		}, 4000);		
	}
}


//add_08_02_13-02_20
function activate_sape_pages_and_price(){	
	
	var login_sape = $('#login_sape_activ_pages').val();	
	var password_sape = $('#password_sape_activ_pages').val();		
	var multiplication_sape = $('#multiplication_sape').val();		
	var rub_main_page_sape = $('#rub_main_page_sape').val();	
	var rub_lvl_1_page_sape = $('#rub_lvl_1_page_sape').val();	
	var rub_lvl_2_page_sape = $('#rub_lvl_2_page_sape').val();	

	var itog_location = create_url_for_request();	

	if(login_sape != '' && password_sape != '' && multiplication_sape != '' && rub_main_page_sape != '' && rub_lvl_1_page_sape != '' && rub_lvl_2_page_sape != ''){		

		message_box('Происходит активация и добавление сайтов в Sape.');
		active_message_and_fade();

		$.ajax({
			type: "POST",
			url: itog_location,
			data: {command_activate_sape_pages_and_price : 1 , login_sape : login_sape, password_sape : password_sape, multiplication_sape : multiplication_sape, rub_main_page_sape : rub_main_page_sape, rub_lvl_1_page_sape : rub_lvl_1_page_sape, rub_lvl_2_page_sape : rub_lvl_2_page_sape},
			success: function(msg){					
				deactive_message_and_fade();				
				
				win1 = window.open("", "sape.ru", "toolbar=0,width=450,height=600,scrollbars=YES"); 
				win1.document.write('<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Отчёт активации страниц сайтов сайтов в sape.ru</title></head><body><pre>'+msg+'</pre></body></html>'); 
				win1.document.close();
				win1.focus(); 	
			}	
		});
	
	}else{
		$('#activate_sape_pages_and_price_indicator').animate({
			opacity: 1,
		}, 50).html('<span style="color:red;">Не все поля заполнены!</span>').animate({
			opacity: 0,
		}, 4000);		
	}
}


//add_25_12_12-20_22
function activate_sape(){

	//подтверждение
	if (confirm("Уверены?")){

		var itog_location = create_url_for_request();	

		message_box('Происходит активация Sape.');
		active_message_and_fade();		

		$.ajax({
			type: "POST",
			url: itog_location,
			data: {command_activate_sape : 1},
			success: function(msg){	
				deactive_message_and_fade();
				$('#statistic_block').html(msg);		
				
				/*
				win1 = window.open("", "liveinternet.ru", "toolbar=0,width=450,height=600,scrollbars=YES"); 
				win1.document.write('<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Отчёт регистрации сайтов liveinternet.ru</title></head><body><pre>'+msg+'</pre></body></html>'); 
				win1.document.close() 
				win1.focus(); 
				*/
			}	
		});
	}
}


//add_24_12_12-20_22
function query_index(){

	//подтверждение
	if (confirm("Уверены?")){
		
		var itog_location = create_url_for_request();	

		message_box('Сбор данных о индексации сайтов.');
		active_message_and_fade();		

		$.ajax({
			type: "POST",
			url: itog_location,
			data: {command_query_index : 1},
			success: function(msg){	
				deactive_message_and_fade();
				
				$('#statistic_block').html(msg);
				
				/*
				win1 = window.open("", "liveinternet.ru", "toolbar=0,width=450,height=600,scrollbars=YES"); 
				win1.document.write('<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Отчёт регистрации сайтов liveinternet.ru</title></head><body><pre>'+msg+'</pre></body></html>'); 
				win1.document.close() 
				win1.focus(); 
				*/
			}	
		});
	}
}


//add_05_11_12_22_55
function echo_list_sites(){
	
	//подтверждение
	if (confirm("Уверены?")){
	
		var itog_location = create_url_for_request();	

		message_box('Происходит формирование списка сайтов.');
		active_message_and_fade();		

		$.ajax({
			type: "POST",
			url: itog_location,
			data: {command_echo_list_sites : 1},
			success: function(msg){	
				deactive_message_and_fade();
				
				win1 = window.open("", "liveinternet.ru", "toolbar=0,width=450,height=600,scrollbars=YES"); 
				win1.document.write('<html><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Список сайтов</title><pre>'+msg+'</pre></html>'); 
				win1.document.close() 
				win1.focus(); 
			}	
		});	
	}
}


//add_02_11_12_22_08
function update_files_by_sample(){

	//подтверждение
	if (confirm("Уверены?")){
		
		var itog_location = create_url_for_request();	

		message_box('Происходит обновление сайтов по образцу. Может занять до 5 секунд на 1 сайт. По окончании работы появится окно с отчётом, ваш браузер скорей всего заблокирует его, вам нужно будет это исправить.');
		active_message_and_fade();		

		$.ajax({
			type: "POST",
			url: itog_location,
			data: {command_update_files_by_sample : 1},
			success: function(msg){	
				deactive_message_and_fade();
				
				win1 = window.open("", "files_update_by_sample", "toolbar=0,width=1000,height=800,scrollbars=YES"); 
				win1.document.write('<html><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title>Отчёт обновления файлов сайтов по образцу</title><pre>'+msg+'</pre></html>'); 
				win1.document.close() 
				win1.focus(); 
			}	
		});
	}
}


//создаём url для запроса данных к ajax_controller.php нашего модуля
function create_url_for_request(){

	var location_array = document.location.href.split("?");		
	var location_array_start_fragments = location_array[0].split("index.php");
	var itog_location = location_array_start_fragments[0]+"template/ajax_controller.php"+"?"+location_array[1];	

	return itog_location;
}