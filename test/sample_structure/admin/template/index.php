<?php
// no direct access
defined( '_EXEC' ) or die( 'Restricted access' );

header("Content-Type: text/html; charset=utf-8");

require_once(PATH_BASE . DS . '..' . DS . 'classes' . DS . 'smarty' . DS . 'Smarty.class.php');
require_once(PATH_BASE . DS . '..' . DS . 'classes' . DS . 'models' . DS . 'sql_model.class.php');
		
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/models/dbconfig.class.php');

$smarty = new Smarty;
$sql = new Sql_model;
$dbconfig = new Dbconfig;


$smarty->debugging = false;
$smarty->caching = false;
$smarty->cache_lifetime = 120;


$smarty->template_dir = PATH_BASE . DS . 'template' . DS . 'templates' . DS;
$smarty->compile_dir = PATH_BASE . DS . 'template' . DS . 'templates_c' . DS;
$smarty->config_dir = PATH_BASE . DS . 'template' . DS . 'configs' . DS;
$smarty->cache_dir = PATH_BASE . DS . 'template' . DS . 'cache' . DS;


/*
//id сайта
$id_site = 1;
$smarty->assign("id_site", $id_site);

//домен
$domain = 'http://unique_site_gen';
$smarty->assign("domain", $domain);

//название сайта
$name_site = 'Оружие';
$smarty->assign("name_site", $name_site);
*/

//login
if(!empty($_POST['login'])){
	if($_POST['login'] == $dbconfig->user['name'] AND $_POST['pass'] == $dbconfig->user['pass']){
		
		session_start();
		$_SESSION['login'] = md5('wedqdqwfqwerqwe123131');
	}
}

//logout
if(!empty($_POST['logout'])){
	if($_POST['logout'] == 1){
		unset($_SESSION['login']);
	}
}

//проверка сессии
if($_SESSION['login'] == md5('wedqdqwfqwerqwe123131')){
	//выборка инфы по сайтам
	$info_sites = $sql->select_info_sites();
	$tags = $sql->select_tags();
	
	$smarty->assign("tags", $tags);
	$smarty->assign("info_sites", $info_sites);
		
	$smarty->display('index.html');
	
}else{
	$smarty->display('login.html');
}
?>