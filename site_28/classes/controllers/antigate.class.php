<?
// no direct access
defined( '_EXEC' ) or die( 'Restricted access' );
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/models/sql_model.class.php');
 
class Antigate{

	protected $filename = 'antigate.jpg'; //filename captcha	
	protected $apikey = ''; //account's API key
    protected $is_verbose = true; // false(commenting OFF),  true(commenting ON)
    protected $domain = 'antigate.com';
    protected $rtimeout = 5; //delay between captcha status checks
    protected $mtimeout = 180; //captcha recognition timeout
    protected $is_phrase = 0; //0 OR 1 - captcha has 2 or more words
    protected $is_regsense = 0; // 0 OR 1 - captcha is case sensetive
    protected $is_numeric = 0; //0 OR 1 - captcha has digits only
    protected $min_len = 0; //0 is no limit, an integer sets minimum text length
    protected $max_len = 0; //0 is no limit, an integer sets maximum text length
    protected $is_russian = 0;	
	protected $sql_model;
	//0 OR 1 - with flag = 1 captcha will be given to a Russian-speaking worker
	
	protected $path = ''; //file path to captcha
	//protected $path = "E:/WebServers1/home/unique_site_gen_43/www/tmp/antigate.jpg"; //file path to captcha
	
	function Antigate(){
		$this->path = $_SERVER['DOCUMENT_ROOT'] . "/tmp/" . $this->filename;
		//$this->path = 'http://u.captcha.yandex.net/image?key=30YLXhlyyxCH0wi91jxIuA4zkQEgB573';
		
		$this->sql_model = new Sql_model;
		
		$params = $this->sql_model->select_params();		
		foreach($params as $key => $value){
			if($value['name'] == 'key_antigate'){
				$this->apikey = $value['value'];
			}
		}
	}
	
	public function get_path_img(){
		return $this->path;
	}
	
	//��������� ����� ����������, ���� ���������� ������ ��������� (�� 0, 0 - ����� ����������� ��� ��������� ��������) �� ����� ������������ ��������� �� ���������
	public function setup($filename, $apikey, $is_verbose, $domain,
		$rtimeout, $mtimeout, $is_phrase, $is_regsense, $is_numeric,
		$min_len, $max_len, $is_russian){
		
		if( !empty($filename) OR $filename === 0){
			$this->filename = $filename;
		}
		/* //������ ����������� ������������� apikey ����� ��������, �� ��� �������� ��������, ���� �� ����������� ��������� ������
		if( !empty($apikey) AND $apikey !== 0){ //�������� apikey � ������������, �� �� ������ ������ ��������� ����������� �������� ��� ����� ��������
			$this->apikey = $apikey;
		}	
		*/
		if( !empty($is_verbose) OR $is_verbose === 0){
			$this->is_verbose = $is_verbose;
		}
		if( !empty($domain) OR $domain === 0){
			$this->domain = $domain;
		}
		if( !empty($rtimeout) OR $rtimeout === 0){
			$this->rtimeout = $rtimeout;
		}
		if( !empty($mtimeout) OR $mtimeout === 0){
			$this->mtimeout = $mtimeout;
		}
		if( !empty($is_phrase) OR $is_phrase === 0){
			$this->is_phrase = $is_phrase;
		}
		if( !empty($is_regsense) OR $is_regsense === 0){
			$this->is_regsense = $is_regsense;
		}		
		if( !empty($is_numeric) OR $is_numeric === 0){
			$this->is_numeric = $is_numeric;
		}	
		if( !empty($min_len) OR $min_len === 0){
			$this->min_len = $min_len;
		}		
		if( !empty($max_len) OR $max_len === 0){
			$this->max_len = $max_len;
		}		
		if( !empty($is_russian) OR $is_russian === 0){
			$this->is_russian = $is_russian;
		}				
		
		$this->Antigate();
	}

	//�����������
	//���������� �����
	public function recognize(){
	
		if (!file_exists($this->path)){
			if ($this->is_verbose) echo "file $this->path not found\n";
			return false;
		}
		$postdata = array(
			'method'    => 'post', 
			'key'       => $this->apikey, 
			'file'      => '@'.$this->path, //������ ���� � �����
			'phrase'	=> $this->is_phrase,
			'regsense'	=> $this->is_regsense,
			'numeric'	=> $this->is_numeric,
			'min_len'	=> $this->min_len,
			'max_len'	=> $this->max_len			
		);
		
		while(true){
		
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,             "http://$this->domain/in.php");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,     1);
			curl_setopt($ch, CURLOPT_TIMEOUT,             60);
			curl_setopt($ch, CURLOPT_POST,                 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,         $postdata);
			$result = curl_exec($ch);
			
			if (curl_errno($ch)){
				if ($this->is_verbose) echo "CURL returned error: ".curl_error($ch)."\n";
				return false;
			}
			
			curl_close($ch);
			
			//���� ��� ������ ������� � ������ �����
			if($result != 'ERROR_NO_SLOT_AVAILABLE'){
				break;
			}
			
			sleep(2);
		}
		

		
		if (strpos($result, "ERROR")!== false){
			if ($this->is_verbose) echo "server returned error: $result\n";
			return false;
		}else{
		
			$ex = explode("|", $result);
			$captcha_id = $ex[1];
			if ($is_verbose) echo "captcha sent, got captcha ID $captcha_id\n";
			$waittime = 0;
			if ($this->is_verbose) echo "waiting for $this->rtimeout seconds\n";
			sleep($this->rtimeout);
			while(true){
				$result = file_get_contents("http://$this->domain/res.php?key=".$this->apikey.'&action=get&id='.$captcha_id);
				if (strpos($result, 'ERROR')!==false){
					if ($this->is_verbose) echo "server returned error: $result\n";
					return false;
				}
				
				if($result=="CAPCHA_NOT_READY" OR $result=="ERROR_NO_SLOT_AVAILABLE" ){
					if ($this->is_verbose) echo "captcha is not ready yet\n";
					$waittime += $this->rtimeout;
					if ($waittime>$this->mtimeout){
						if ($this->is_verbose) echo "timelimit ($this->mtimeout) hit\n";
						break;
					}
					if ($this->is_verbose) echo "waiting for $rtimeout seconds\n";
					sleep($this->rtimeout);
				}else{
					$ex = explode('|', $result);
					if (trim($ex[0])=='OK') return trim($ex[1]);
				}
			}
			
			return false;
		}
	}
}
?>