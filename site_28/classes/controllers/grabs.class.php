<?
// no direct access
defined( '_EXEC' ) or die( 'Restricted access' );

require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/models/root_model.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/models/sql_model.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/google_captcha.class.php');

class Grabs extends Sql_model{

	protected $user_agent = 'Opera/10.00 (Windows NT 5.1; U; ru) Presto/2.2.0';
	protected $browser_referer = 'http://www.google.ru/';
	public $google_captcha;
	
	function __construct(){
		$this->google_captcha = new Google_captcha;
	}
	
	
	
	public function authorization_liveinternet($url, $password){
		$password = trim($password);
		$poststr = 'url=' . $url . '&' . 'password=' . $password . '&' . 'ok=>';
		###отладка
		//echo '$poststr = ' . $poststr . '<br>';
		
		$cookie_file_name ='liveinternet';
		$user_agent ='Opera/10.00 (Windows NT 5.1; U; ru) Presto/2.2.0';		
		$user_agent ='Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20100101 Firefox/12.0.';
		//$user_agent ='Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)';
		//$user_agent ='Opera/9.80 (Windows NT 6.1; WOW64; U; en) Version/12.00';
		//$query ='http://www.liveinternet.ru/stat/' . $url . '/queries.html?period=month;total=yes&per_page=100&ok=+OK+';
		$query ='http://www.liveinternet.ru/stat/';
		$browser_referer = 'http://google.com/';
		$path_cookie_file_name = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'cookie' . DIRECTORY_SEPARATOR . $cookie_file_name . '.txt';
			
		$ch = curl_init();			
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $poststr);		
		curl_setopt($ch, CURLOPT_REFERER, $browser_referer);		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		//curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
		curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $path_cookie_file_name);
		//curl_setopt($ch, CURLOPT_COOKIEFILE, $path_cookie_file_name);
		curl_setopt($ch, CURLOPT_URL, $query);
		$html = curl_exec($ch);
		curl_close($ch);
			
		//return $html;
	}
		
	
	//пример параметров 
	//$link_params = 'queries.html?period=month;total=yes&per_page=100&ok=+OK+';
	//24hours.html?ready=23176;ready=16862;popup=on&day_0=1&day_1=1&day_2=1&day_3=1&day_4=1&day_5=1&day_6=1&day_7=1&day_8=1&day_9=1&day_10=1&day_11=1&day_12=1&day_13=1&day_14=1&day_15=1&day_16=1&day_17=1&day_18=1&day_19=1&day_20=1&day_21=1&day_22=1&day_23=1&day_24=1&day_25=1&day_26=1&day_27=1&day_28=1&day_29=1&gosearchword-url=1&systemcode=1&search-word=скоростной%20велосипед
	public function get_html_liveinternet($url, $link_params){
		
		$query ='http://www.liveinternet.ru/stat/' . $url . '/' . $link_params;
		$user_agent ='Opera/10.00 (Windows NT 5.1; U; ru) Presto/2.2.0';
		$user_agent ='Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20100101 Firefox/12.0.';
		//$user_agent ='Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)';
		//$user_agent ='Opera/9.80 (Windows NT 6.1; WOW64; U; en) Version/12.00';
		$browser_referer = 'http://www.liveinternet.ru/';
		$cookie_file_name ='liveinternet';
		$path_cookie_file_name = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'cookie' . DIRECTORY_SEPARATOR . $cookie_file_name . '.txt';
		
		
		$ch = curl_init();			
		//curl_setopt($ch, CURLOPT_POST, 1);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, $poststr);		
		curl_setopt($ch, CURLOPT_REFERER, $browser_referer);		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
		//curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		//curl_setopt($ch, CURLOPT_COOKIEJAR, $path_cookie_file_name);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $path_cookie_file_name);
		curl_setopt($ch, CURLOPT_URL, $query);
		$html = curl_exec($ch);
		curl_close($ch);
		
		return $html;
	}
				
	
	protected function generate_rnd_string($length = 15){
		$chars = 'abcdefghijklmnopqrstuvwxwzABCDEFGHIJKLMNOPQRSTUVWXWZ123456789';
		$numChars = strlen($chars);
		$string = '';
		for ($i = 0; $i < $length; $i++) {
			$string .= substr($chars, rand(1, $numChars) - 1, 1);
		}
		return $string;
	}
	
	
	//второй параметр задаёт глубину парсинг гугла
	public function google_main_grab($phrase, $count_result = 10, $lang = 'ru'){
		$this->browser_referer = 'http://www.google.ru/';
		
		
		
		//$phrase = mb_convert_encoding($phrase, "UTF-8", "CP1251");
		
		//ищет только в HTML
		//add 09.09.13
		//$phrase = $phrase . ' filetype:html';
		
		$query = urlencode($phrase);		
		
		$count_loop = 0;
		
		while($count_loop < $count_result){	
			sleep(1);
			if($count_loop != 0){
				$start = "&start=$count_loop";
			}
			
			if($lang == 'ru'){
				// &tbs=li:1&sa - точное совпадение
				$url = "http://www.google.ru/search?aq=f&sourceid=opera&ie=UTF-8&q=$query$start&tbs=li:1&sa";
			}elseif($lang == 'ua'){
				$url = "http://www.google.ru/search?aq=f&sourceid=opera&ie=UTF-8&q=$query$start&tbs=li:1&sa&lr=lang_uk&as_sitesearch=.ua";
			}elseif($lang == 'by'){
				$url = "http://www.google.ru/search?aq=f&sourceid=opera&ie=UTF-8&q=$query$start&tbs=li:1&sa&lr=lang_be&as_sitesearch=.by";
			}
			
			//для украиского поиска
			//$url = "http://www.google.ru/search?q=$query$start&oq=$query$start&aqs=chrome.0.57j0l3.353j0&sourceid=chrome&ie=UTF-8";
			//lr=lang_uk
			//lr=lang_be
			//as_sitesearch=.ua
			
			$tmp_html = $this->curl_connect($url);
			$tmp_html = $this->google_captcha->work($tmp_html, $url);
			$html .= $tmp_html;			
			
			unset($url, $start, $tmp_html);
			//$html .= $temp_html;
			
			/*
			$lench_temp_html = strlen($temp_html);
			
			echo '<br>' . $lench_temp_html . '<br>';
			echo '<br>Занято памяти сейчас: ' . memory_get_usage() . ' максимально: ' . memory_get_peak_usage() . '<br>';
			
			if($lench_temp_html < 100000){
				$html .= $temp_html;
			}else{
				$temp_html = '';
			}			
			$lench_temp_html = 0;
			*/
			
			$count_loop = $count_loop + 10;
		}
		
		unset($count_loop);
		
		/*
		//отладка
		echo '<pre><b>html</b>';
		print_r($html);
		echo '</pre>';
		die;
		*/
		
		return $html;
	}	
	
	
	public function google_img_grab($urls_array, $search_phrase, $id_search_phrase, $id_site){
	
		/*
		require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/parsers.class.php');
		$sql_model = new Sql_model;	
		
		$search_phrase_result = $sql_model->select_search_phrase_by_phrase($search_phrase);
		$id_search_phrase = $search_phrase_result['id'];
		
		if(empty($id_search_phrase)){
			$id_search_phrase = $sql_model->insert_search_phrase($search_phrase);
		}
		*/
		
		//вызываем у переданного экземпляра метод
		//$id_search_phrase = $grabs->select_or_insert_search_phrase($search_phrase);
	
		foreach($urls_array as $value){
			
		
			$file_name = $this->generate_rnd_string(15);
			
			$isert_id = $this->insert_img($id_search_phrase, $file_name . '.jpg', $value['alt'], $value['urls']);
			$this->logs_grab($isert_id, $title = 'insert_img id');
			
			$ids_images .= 	$isert_id;
			//echo '$isert_id-' . $isert_id . '!!!!!<br>';
			
			if((int)$isert_id > 0){
			
				//echo '$isert_id-' . $isert_id . '!!!!!ВСТАВКА В БАЗУ<br>';
				
				$imgContent = file_get_contents($value['urls']);				
				file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/images/images/' . $file_name . '.jpg', $imgContent);
			}
		}
		
		return $ids_images;
	}
	
	
	public function google_video_thumb_grab($urls_imgs_array_top_5, $id_site){
		
		//if(is_array($urls_imgs_array_top_5)){
		
			foreach($urls_imgs_array_top_5 as $key => $value){
				$imgContent = file_get_contents($value['img']);
				$file_name = $this->generate_rnd_string(15);
				file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/site_' . $id_site . '/images/video_thumb/' . $file_name . '.jpg', $imgContent);
				
				$urls_imgs_array_top_5[$key]['img'] = $file_name . '.jpg';
			}
		
			return $urls_imgs_array_top_5;
		//}
	}
	
	
	public function google_video_grab($urls_imgs_array_top_5, $id_search_phrase){
		
		//if(is_array($urls_imgs_array_top_5)){
		
			foreach($urls_imgs_array_top_5 as $key => $value){			
				$ids_videos[] = $this->insert_video($id_search_phrase, $value['img'], $value['video'], $value['title'], $value['description'], $value['duration'], $value['time_publish'], $value['source'], $value['user_add']);
			}
			
			return $ids_videos;
		//}
	}
	
	
	public function google_img_html_grab($phrase){
		$this->browser_referer = 'http://www.google.ru/';
		
		//$phrase = mb_convert_encoding($phrase, "UTF-8", "CP1251");
		//$phrase = str_replace(' ', '+', $phrase);
		$phrase = urlencode($phrase);
		
		//$count_loop = 0;					
		//while($count_loop < $count_result){	
			//sleep(1);
			
			// &tbs=li:1&sa - точное совпадение
			//$url = "http://www.google.ru/search?aq=f&sourceid=opera&ie=UTF-8&q=$query$start&tbs=li:1&sa";
			//$url = "http://www.google.ru/search?q=" . $phrase . "&hl=ru&newwindow=1&prmd=imvns&source=lnms&tbm=isch&ei=oguUT4-TN8bysgbi_6zNBA&sa=X&oi=mode_link&ct=mode&cd=2&ved=0CCUQ_AUoAQ&biw=1066&bih=747&sei=pQuUT87XJYXltQbltqifBA#q=%D1%85%D0%BE%D0%BB%D0%BE%D0%B4%D0%BD%D0%BE%D0%B5+%D0%BE%D1%80%D1%83%D0%B6%D0%B8%D0%B5&hl=ru&newwindow=1&tbm=isch&prmd=imvns&source=lnt&tbs=isz:l&sa=X&ei=pQuUT_eiM4XmtQbR9aDPBA&ved=0CAwQpwUoAQ&bav=on.2,or.r_gc.r_pw.r_qf.,cf.osb&fp=f160ac09ed30fecc&biw=1066&bih=747";
			//$url = "http://www.google.ru/search?q=" . $phrase . "&hl=ru&newwindow=1&prmd=imvns&source=lnms&tbm=isch&oi=mode_link&ct=mode&cd=2&biw=1066&bih=747";
			//$url = "http://www.google.ru/search?q=" . $phrase . "&hl=ru&newwindow=1&prmd=imvns&source=lnms&tbm=isch&ei=oguUT4-TN8bysgbi_6zNBA&sa=X&oi=mode_link&ct=mode&cd=2&ved=0CCUQ_AUoAQ&biw=1066&bih=747&sei=pQuUT87XJYXltQbltqifBA#q=%D1%85%D0%BE%D0%BB%D0%BE%D0%B4%D0%BD%D0%BE%D0%B5+%D0%BE%D1%80%D1%83%D0%B6%D0%B8%D0%B5&hl=ru&newwindow=1&tbs=isz:l&tbm=isch&prmd=imvns&source=lnt&sa=X&ei=cAyUT8fkIcPetAbK46DfBA&ved=0CA0QpwUoAQ&bav=on.2,or.r_gc.r_pw.r_qf.,cf.osb&fp=603862a403244fee&biw=1066&bih=747";
			//$url = "http://www.google.ru/search?q=" . $phrase . "&hl=ru&newwindow=1&prmd=imvns&source=lnms&tbm=isch&ei=oguUT4-TN8bysgbi_6zNBA&sa=X&oi=mode_link&ct=mode&cd=2&ved=0CCUQ_AUoAQ&biw=1066&bih=747&sei=pQuUT87XJYXltQbltqifBA#q=%D1%85%D0%BE%D0%BB%D0%BE%D0%B4%D0%BD%D0%BE%D0%B5+%D0%BE%D1%80%D1%83%D0%B6%D0%B8%D0%B5&hl=ru&newwindow=1&tbs=iszw:1024,iszh:1024,isz:l&tbm=isch&prmd=imvns&source=lnt&sa=X&ei=ugyUT8_ELIrVtAbV0Om7BA&ved=0CA0QpwUoAQ&fp=1&biw=1066&bih=747&bav=on.2,or.r_gc.r_pw.r_qf.,cf.osb&cad=b";
			//$url = "http://www.google.ru/search?q=" . $phrase . $start . "&hl=ru&tbs=iszw:1024,iszh:1024,isz:l&tbm=isch&prmd=imvns&source=lnt&sa=X&biw=1066&bih=747&hl=ru&newwindow=1&safe=active&tbs=iszw:1024,iszh:1024,isz:lt,islt:xga&tbm=isch&prmd=imvns&source=lnt&sa=X&bav=on.2,or.r_gc.r_pw.r_qf.,cf.osb&biw=1066&bih=747";
			$url = "http://www.google.ru/search?as_st=y&tbm=isch&hl=ru&as_q=" . $phrase . "&as_epq=&as_oq=&as_eq=&cr=&as_sitesearch=&safe=active&orq=&tbs=isz:lt,islt:xga,itp:photo,ift:jpg&biw=1066&bih=747"; // &as_sitesearch=http://ru.wikipedia.org/
				
			$html = $this->curl_connect($url);
						
			$html = $this->google_captcha->work($html, $url);
					
			
			


			//$count_loop = $count_loop + 10;			
		//}
				
		return $html;
	}
	
	
	public function google_video_html_grab($phrase){
		$this->browser_referer = 'http://www.google.ru/';
		
		//$phrase = mb_convert_encoding($phrase, "UTF-8", "CP1251");
		$phrase = $phrase . ' youtube';
		$query = urlencode($phrase);	
		
		$url = "http://www.google.ru/search?q=" . $query . "&hl=ru&newwindow=1&safe=active&tbas=0&biw=1066&bih=747&tbm=vid&prmd=imvnse&source=lnms&sa=X&oi=mode_link&ct=mode&cd=4";
		
		$tmp_html = $this->curl_connect($url);
		$tmp_html = $this->google_captcha->work($tmp_html, $url);
		$html .= $tmp_html;			
		
		
		return $html;
	}	
	
	
	//сбор статей с rusarticles
	public function rusarticles_main_grab($phrase, $depth = 1){
		$this->browser_referer = 'http://www.rusarticles.com/';
		//$this->browser_referer = 'http://www.google.ru/';
		
		//$phrase = mb_convert_encoding($phrase, "UTF-8", "CP1251");
		//$phrase = $phrase . ' youtube';
		$query = urlencode($phrase);		
	
		$url = "http://www.rusarticles.com/find-articles.php?q=" . $query;
		$html .= $this->curl_connect($url);
			
		if($depth > 1){
			$count_loop = 2;
			while($count_loop <= $depth){
			
				$url = "http://www.rusarticles.com/find-articles.php?q=" . $query . "&page=" . $count_loop;
				$html .= $this->curl_connect($url);
					
				$count_loop++;
			}
		}			
		
		return $html;
	}
	
	
	//сбор статей с rusarticles
	public function rusarticles_content_grab($urls_articles){
		$this->browser_referer = 'http://www.rusarticles.com/';
		//$this->browser_referer = 'http://www.google.ru/';
		
		//$phrase = mb_convert_encoding($phrase, "UTF-8", "CP1251");
		//$phrase = $phrase . ' youtube';
		//$query = urlencode($phrase);		
	
		//$url = "http://www.rusarticles.com/find-articles.php?q=" . $query;
		
		foreach($urls_articles as $key => $value){
			$html_articles_page_array[$key]['html'] = $this->curl_connect($value);
			$html_articles_page_array[$key]['url'] = $value;
		}
		
		return $html_articles_page_array;
	}
		
	
	public function curl_connect($url, $proxy = 0){

		//прокси не доделано
		if($proxy == 1){
		
			$proxy[] = '94.228.205.33:8080';
			$proxy[] = '178.219.41.140:3128';			
			$proxy[] = '80.84.117.233:3128';
			$proxy[] = '46.50.175.146:3128';
			$proxy[] = '77.94.48.5:80';
			$proxy[] = '86.111.144.194:20000';
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,$url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch, CURLOPT_USERAGENT, $this->browser_referer);
			curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 60);
			curl_setopt ($ch, CURLOPT_TIMEOUT, 60);	
			curl_setopt($ch, CURLOPT_PROXY, $proxy[rand(0, count($proxy) - 1 )] );
			$html=curl_exec($ch);
			curl_close($ch);
			
		}else{
		
			$ch = curl_init();
			//curl_setopt($ch, CURLOPT_BUFFERSIZE, 500000); 
			curl_setopt($ch, CURLOPT_REFERER, $this->browser_referer);
			
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent);
			curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 60);
			curl_setopt ($ch, CURLOPT_TIMEOUT, 60);		
			//curl_setopt($ch, CURLOPT_COOKIEJAR, $this->path_cookie_file_name);
			//curl_setopt($ch, CURLOPT_COOKIEFILE, $this->path_cookie_file_name);
			curl_setopt($ch, CURLOPT_URL, $url);
			$html = curl_exec($ch);
			curl_close($ch);
		}
		
		return $html;
	}
	
	
	public function file_get_connect($url){
		$url = urldecode($url);
		$html = file_get_contents($url, false, null, -1, 500000);
		//echo $url . '<br><br><br><br><br>';
		//echo $html;
		//die;
		return $html;
		//return '';
	}
}
?>