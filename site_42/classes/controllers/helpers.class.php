<?
// no direct access
defined( '_EXEC' ) or die( 'Restricted access' );

class Helpers{	
	
	public function pagination($total, $per_page, $num_links, $start_row, $url='', $char_separator_params = 1, $nofollow_link = 2, $link_param = 'p'){
	
		if($char_separator_params == 1){
			$sep_params = '&';
		}else{
			$sep_params = '?';
		}
		
		if($nofollow_link == 1){
			$nofollow = 'rel="nofollow"';
		}else{
			$nofollow = '';
		}
	
		//Получаем общее число страниц
		$num_pages = ceil($total/$per_page);

		if ($num_pages == 1) return '';

		//Получаем количество элементов на страницы
		$cur_page = $start_row;

		//Если количество элементов на страницы больше чем общее число элементов
		// то текущая страница будет равна последней
		if ($cur_page > $total){
			$cur_page = ($num_pages - 1) * $per_page;
		}

		//Получаем номер текущей страницы
		$cur_page = floor(($cur_page/$per_page) + 1);

		//Получаем номер стартовой страницы выводимой в пейджинге
		$start = (($cur_page - $num_links) > 0) ? $cur_page - $num_links : 0;
		//Получаем номер последней страницы выводимой в пейджинге
		$end   = (($cur_page + $num_links) < $num_pages) ? $cur_page + $num_links : $num_pages;

		//$output = '<span class="ways">';
		//$output .= '</span>';

		//Формируем ссылку на первую страницу
		if  ($cur_page > ($num_links + 1)){
			$output .= '<a ' . $nofollow . ' href="'.$url.'" title="Первая">Первая</a>';
		}		
		
		//Формируем ссылку на предыдущую страницу
		if  ($cur_page != 1){
			$i = $start_row - $per_page;
			if ($i <= 0) $i = 0;
			$output .= ' <i>←</i><a ' . $nofollow . ' href="'.$url . $sep_params . $link_param .'='.$i.'">Предыдущая</a> &nbsp; ';
		}
		else{
			$output .=' <span><i>←</i>Предыдущая</span> &nbsp; ';
		}		
		
		//$output .= '<span class="divider">|</span>';

		// Формируем список страниц с учетом стартовой и последней страницы   >
			for ($loop = $start; $loop <= $end; $loop++){
			$i = ($loop * $per_page) - $per_page;

			if ($i >= 0)
			{
				if ($cur_page == $loop)
				{
				   //Текущая страница
				   $output .= '<span>'.$loop.'</span> '; 
				}
				else
				{

				   $n = ($i == 0) ? 0 : $i;

				   $output .= '<a ' . $nofollow . ' href="'.$url . $sep_params . $link_param .'='.$n.'">'.$loop.'</a> ';
				}
			}
		}
		
		
		//Формируем ссылку на следующую страницу
		if ($cur_page < $num_pages){
			$output .= ' &nbsp; <a ' . $nofollow . ' href="'.$url . $sep_params . $link_param .'='.($cur_page * $per_page).'">Следующая</a><i>→</i> ';
		}
		else{
			$output .=' &nbsp; <span>Следующая<i>→</i></span> ';
		}

		//Формируем ссылку на последнюю страницу
		if (($cur_page + $num_links) < $num_pages){
			$i = (($num_pages * $per_page) - $per_page);
			$output .= '<a ' . $nofollow . ' href="'.$url . $sep_params . $link_param .'='.$i.'" title="Последняя">Последняя</a>';
		}
		
		if(strpos($output, 'href')){
			return '<div><strong>Страницы:</strong> '.$output.'</div>';
		}
		
		return '';
	}
	
	
	public function get_int($int_raw){
		$int = (!empty($int_raw))? intval($int_raw): 0;
		
		return $int;
	}
	
	
	public function get_big($int_1, $int_2){
	
		if($int_1 >= $int_2){
			$total = $int_1;
		}else{
			$total = $int_2;
		}
		
		return $total;
	}
	
	public function first_char_big($phrase, $encode = 'utf-8'){
		//Делаем первую букву фразы заглавной
		$first = mb_substr($phrase, 0, 1, $encode);
		$last = mb_substr($phrase, 1, mb_strlen($phrase, $encode), $encode);
		mb_strlen($phrase, $encode);
		$first =  mb_strtoupper($first, $encode);
				
		return $first.$last;
	}
	
	
	function translit_domain($str){
	
		$lat_letters = array(
			'а' => 'a',
			'А' => 'a',
			'б' => 'b',
			'Б' => 'b',
			'в' => 'v',
			'В' => 'v',
			'г' => 'g',
			'Г' => 'g',
			'д' => 'd',
			'Д' => 'd',
			'е' => 'e',
			'Е' => 'e',
			'ё' => 'e',
			'Ё' => 'e',
			'ж' => 'zh',
			'Ж' => 'zh',
			'з' => 'z',
			'З' => 'z',
			'и' => 'i',
			'И' => 'i',
			'й' => 'y',
			'Й' => 'y',
			'к' => 'k',
			'К' => 'k',
			'л' => 'l',
			'Л' => 'l',
			'м' => 'm',
			'М' => 'm',
			'н' => 'n',
			'Н' => 'n',
			'о' => 'o',
			'О' => 'o',
			'п' => 'p',
			'П' => 'p',
			'р' => 'r',
			'Р' => 'r',
			'с' => 's',
			'С' => 's',
			'т' => 't',
			'Т' => 't',
			'у' => 'u',
			'У' => 'u',
			'ф' => 'ph',
			'Ф' => 'ph',
			'х' => 'h',
			'Х' => 'h',
			'ц' => 'c',
			'Ц' => 'c',
			'ч' => 'ch',
			'Ч' => 'ch',
			'ш' => 'sh',
			'Ш' => 'sh',
			'щ' => 'sh',
			'Щ' => 'sh',
			'ъ' => '',
			'Ъ' => '',
			'ы' => 'i',
			'Ы' => 'i',
			'ь' => '',
			'Ь' => '',
			'э' => 'e',
			'Э' => 'e',
			'ю' => 'u',
			'Ю' => 'u',
			'я' => 'ya',
			'Я' => 'ya',
			' ' => '-',
			'-' => '-',
			',' => '',
			'.' => '',
			'!' => '',
			'?' => '',
			':' => '',
			';' => '',
			'(' => '',
			')' => '',
			'{' => '',
			'}' => '',
		);
		
		
		
		$str_translit = strtr(trim($str), $lat_letters);
	 
		/*
		$str_translit = null;
		for ($i = 0; $i <= strlen($str); $i++){	  
			$str_translit .= $lat_letters[$str[$i]];				
		}
		*/
	  
		return $str_translit;
	} 
	
	
	public function gen_random_str($length = 12){
	
		$chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789';
		$numChars = strlen($chars);
		$string = '';
		for ($i = 0; $i < $length; $i++) {
			$string .= substr($chars, rand(1, $numChars) - 1, 1);
		}
		return $string;
	}
		
}
?>