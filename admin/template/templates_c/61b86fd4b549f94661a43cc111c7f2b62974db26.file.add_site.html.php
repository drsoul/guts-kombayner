<?php /* Smarty version Smarty-3.1.8, created on 2013-11-08 04:51:55
         compiled from "X:\home\unique_site_gen_46\www\admin\template\templates\add_site.html" */ ?>
<?php /*%%SmartyHeaderCode:14025527c35ab110a94-82283052%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '61b86fd4b549f94661a43cc111c7f2b62974db26' => 
    array (
      0 => 'X:\\home\\unique_site_gen_46\\www\\admin\\template\\templates\\add_site.html',
      1 => 1378133535,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14025527c35ab110a94-82283052',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'tags' => 0,
    'value' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_527c35ab1b7434_93558090',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_527c35ab1b7434_93558090')) {function content_527c35ab1b7434_93558090($_smarty_tpl) {?><div class="distance"></div>

<div id="add_site_panel" class="box_2 add_site_panel">

	<h1>Добавление сайта.</h1>
	<div class="distance"></div>
	
	
	<div>
		<input id="name_site" type="text" class="input_text"> название сайта 
	</div>
	<div class="distance"></div>
	

	<div>
		 <input id="domain" type="text" class="input_text" value="site_1"> домен (без http:// и заключительного /)
	</div>
	<div class="distance"></div>	
	

	<div>
		Ключевые фразы (каждая фраза с новой строки, желательно не менее 2х слов в фразе для придания тематичности сайту), достаточно нескольких фраз, система найдёт подобные и в итоге фраз станет много больше (при автоматическом способе сбора категорий).<br> При ручном способе необходимо добавить все категории которые будут на сайте вручную:<br>
		<textarea id="search_phrases" class="textarea"></textarea>
	</div>	
	<div class="distance"></div>	
	
	
	<div>
		Индивидуальный список стоп-слов для сайта (статьи, фото и видео в которых найдены эти фразы/слова не будут добавляться в базу), вхождение по корню с суффиксом для отдельных слов (покрываются все словоформы) и точное вхождение для фраз (используйте словоформы в фразах для увеличения покрытия).<br>Каждое слово/фразу с новой строки:<br>
		<textarea id="b_list_words" class="textarea"></textarea>
	</div>	
	<div class="distance"></div>		


	Тэги для соц. закладок:<br>
	<select id="id_tag">
		<?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['value']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['tags']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value){
$_smarty_tpl->tpl_vars['value']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['value']->key;
?>
			<?php if ($_smarty_tpl->tpl_vars['value']->value['name']=='Случайная категория'||$_smarty_tpl->tpl_vars['value']->value['name']=='Определение по тегам'){?>
				<option value="<?php echo $_smarty_tpl->tpl_vars['value']->value['id'];?>
"><<?php echo $_smarty_tpl->tpl_vars['value']->value['name'];?>
></option>
			<?php }?>
		<?php } ?>		

		<?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['value']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['tags']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value){
$_smarty_tpl->tpl_vars['value']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['value']->key;
?>
			<?php if ($_smarty_tpl->tpl_vars['value']->value['name']!='Случайная категория'&&$_smarty_tpl->tpl_vars['value']->value['name']!='Определение по тегам'){?>
				<option value="<?php echo $_smarty_tpl->tpl_vars['value']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value['name'];?>
</option>
			<?php }?>
		<?php } ?>
	</select>	
	<div class="distance"></div>
	

	<div>
		<span onclick="add_site()" class="link_imitate">Добавить сайт</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span id="add_site_indicator" class="indicator"></span>
	</div>

</div><?php }} ?>