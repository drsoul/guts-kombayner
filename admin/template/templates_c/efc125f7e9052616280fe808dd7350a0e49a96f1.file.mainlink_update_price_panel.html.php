<?php /* Smarty version Smarty-3.1.8, created on 2013-11-11 04:04:39
         compiled from "X:\home\unique_site_gen_47\www\admin\template\templates\mainlink_update_price_panel.html" */ ?>
<?php /*%%SmartyHeaderCode:872452801f1718aac9-35205933%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'efc125f7e9052616280fe808dd7350a0e49a96f1' => 
    array (
      0 => 'X:\\home\\unique_site_gen_47\\www\\admin\\template\\templates\\mainlink_update_price_panel.html',
      1 => 1375905945,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '872452801f1718aac9-35205933',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_52801f17192c21_44403765',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52801f17192c21_44403765')) {function content_52801f17192c21_44403765($_smarty_tpl) {?><div id="mainlink_update_price_panel" class="box_2 mainlink_update_price_panel">

	<h1>Установка цен на добавленные сайты в mainlink.ru.</h1>	
	<div class="distance"></div>
	
	
	<div>
		<input id="login_mainlink_up_price" type="text" class="input_text" value=""> Логин mainlink.ru.
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="password_mainlink_up_price" type="password" class="input_text" value=""> Пароль mainlink.ru.
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="count_main_links_up_price" type="text" class="input_text" value="3"> Количество ссылок на главной (рекомендуем не увеличивать).
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="count_other_links_up_price" type="text" class="input_text" value="1"> Количество ссылок на прочих страницах (рекомендуем не увеличивать).
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="multiplication_up_price" type="text" class="input_text" value="0.5"> Мультипликатор для формирования цены по PR. Формула: PR(предыдущий) * Мультипликатор = PR(следующий).
	</div>
	<div class="distance"></div>	
	
	
	<div>
		<input id="dol_main_page_up_price" type="text" class="input_text" value="0.32"> Цена ссылки на главной странице в долларах (отделитель дробной части - точка, точность 2 знака после точки).
	</div>
	<div class="distance"></div>
		
	
	<div>
		<input id="rub_main_page_up_price" type="text" class="input_text" value="9.99"> Цена ссылки на главной странице в рублях (отделитель дробной части - точка, точность 2 знака после точки).
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="dol_lvl_1_page_up_price" type="text" class="input_text" value="0.05"> Цена ссылки на странице в 1 клике от главной в долларах (отделитель дробной части - точка, точность 2 знака после точки).
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="rub_lvl_1_page_up_price" type="text" class="input_text" value="1.49"> Цена ссылки на странице в 1 клике от главной в рублях (отделитель дробной части - точка, точность 2 знака после точки).
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="dol_lvl_2_page_up_price" type="text" class="input_text" value="0.03"> Цена ссылки на странице в 2х кликах от главной в долларах (отделитель дробной части - точка, точность 2 знака после точки).
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="rub_lvl_2_page_up_price" type="text" class="input_text" value="0.99"> Цена ссылки на странице в 2х кликах от главной в рублях (отделитель дробной части - точка, точность 2 знака после точки).
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="dol_lvl_3_page_up_price" type="text" class="input_text" value="0.02"> Цена ссылки на странице в 3х кликах от главной в долларах (отделитель дробной части - точка, точность 2 знака после точки).
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="rub_lvl_3_page_up_price" type="text" class="input_text" value="0.49"> Цена ссылки на странице в 3х кликах от главной в рублях (отделитель дробной части - точка, точность 2 знака после точки).
	</div>
	<div class="distance"></div>
		
		

	<div>
		<span onclick="update_price_mainlink()" class="link_imitate">Установить цены для всех добавленных сайтов сайты</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span id="add_mainlink_indicator" class="indicator"></span>
	</div>

</div><?php }} ?>