<?php /* Smarty version Smarty-3.1.8, created on 2013-11-11 04:04:39
         compiled from "X:\home\unique_site_gen_47\www\admin\template\templates\create_subdomains_panel.html" */ ?>
<?php /*%%SmartyHeaderCode:819752801f170435d9-62412077%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '90d8d94a74fdbf00e1b26b73635385986169a7d1' => 
    array (
      0 => 'X:\\home\\unique_site_gen_47\\www\\admin\\template\\templates\\create_subdomains_panel.html',
      1 => 1375905938,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '819752801f170435d9-62412077',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_52801f170530e5_84110343',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52801f170530e5_84110343')) {function content_52801f170530e5_84110343($_smarty_tpl) {?><div id="create_subdomains_panel" class="box_2 create_subdomains_panel">

	<h1>Создание поддоменов Cpanel.</h1>
	ВНИМАНИЕ! Работает только когда данный скрипт размещён на сервере где установлена Cpanel.
	<div class="distance"></div>
	
	<div>
		Домен/ы верхнего уровня (каждый с новой строки, без http:// и завершающего - /).<br> Если введено больше 1го домена то домены будут чередоваться по кругу при создании субдоменов:<br>
		<textarea id="up_domains" class="textarea"></textarea>
	</div>	
	<div class="distance"></div>
	
	
	<div>
		<input id="number_farm" type="text" class="input_text" value="1"> Номер фермы сайтов. Пример номеров разных ферм: 1, 2, 3, 4 и т.д.<br>
		Должна быть уникальная цифра для акаунта Cpanel, чтобы избежать одинаковых директорий для разных доменов.<br>
		В разных акаунтах Cpanel можно использовать одинаковые номера ферм.<br>
		Шаблон пути: корень/domains/sites_[Номер фермы сайтов]/site_[id сайта]
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="theme_cpanel" type="text" class="input_text" value="x3"> Тема в Cpanel. По умолчанию x3
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="login_cpanel" type="text" class="input_text"> Логин в Cpanel.
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="password_cpanel" type="password" class="input_text"> Пароль в Cpanel.
	</div>
	<div class="distance"></div>
	

	<div>
		<span onclick="create_subdomains()" class="link_imitate" >Создать поддомены</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span id="create_subdomains_indicator" class="indicator"></span>
	</div>

</div><?php }} ?>