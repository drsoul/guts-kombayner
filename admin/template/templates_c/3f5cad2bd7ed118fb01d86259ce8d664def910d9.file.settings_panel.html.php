<?php /* Smarty version Smarty-3.1.8, created on 2013-11-14 01:15:39
         compiled from "X:\home\unique_site_gen_47\www\admin\template\templates\settings_panel.html" */ ?>
<?php /*%%SmartyHeaderCode:2867552801f171f6c81-48820686%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3f5cad2bd7ed118fb01d86259ce8d664def910d9' => 
    array (
      0 => 'X:\\home\\unique_site_gen_47\\www\\admin\\template\\templates\\settings_panel.html',
      1 => 1384377320,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2867552801f171f6c81-48820686',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_52801f17200000_14768806',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52801f17200000_14768806')) {function content_52801f17200000_14768806($_smarty_tpl) {?><div id="settings_panel" class="box_2 settings_panel">

	<h1>Настройки.</h1>
	<div class="distance"></div>
	
	Способ сбора категорий (ключевых фраз)
	<select id="method_grab_cat">
	  <option value="1">Ручной - использовать добавленные вручную</option>
	  <option value="2">Автоматический - собрать автоматом, на основе одной/нескольких добавленных вручную</option>
	</select><br>
	<span class="small">
		При ручном способе используются только категории добавленные вручную, но контент в них собирается автоматически.<br>
		При использовании автоматического способа, добавленные вручную категории не будут отображены на сайте, но на их основе будут собраны категории из wordstat, контент для категорий собирается автоматически.<br>
		Для работы автоматического режима сбора категорий необходимо подключить Selenium сервер и добавить ключ antigate.<br>
		Для сбора контента должно быть включено расширение php - curl.
	</span>
	
	<div class="distance"></div>
	<div class="distance"></div>
	
	Лицензия
	<div>
		<input id="license_code" type="text" class="input_text settings_input" value="" > код лицензии
	</div>
	<div class="distance"></div>
	<div class="distance"></div>
	
	Ключ antigate.com. Используется для распознания каптч при сборе контента в Google. Рекомендуется указывать, иначе Google будет периодически выдавать бан что препятствует сбору контента вплоть до его полной остановки.
	<div>
		<input id="key_antigate" type="text" class="input_text settings_input" value="" > ключ
	</div>
	<div class="distance"></div>
	<div class="distance"></div>
	
	
	Авторизация adwords.google.com (обязательна для автоматического сбора категорий)
	
	<div>
		<input id="ad_mail" type="text" class="input_text settings_input" value="" > Ваша регистрационная почта 
	</div>
	<div class="distance"></div>	
	
	<div>
		<input id="ad_pass" type="text" class="input_text settings_input" value="" > Ваш пароль
	</div>
	<div class="distance"></div>
	<div class="distance"></div>	
	
	Авторизация liveinternet.ru (обязательна для регистрации в liveinternet.ru и сбора статистики)
	
	<div>
		<input id="li_mail" type="text" class="input_text settings_input" value="" > Ваша регистрационная почта 
	</div>
	<div class="distance"></div>
	
	<div>
		<input id="li_pass" type="text" class="input_text settings_input" value="" > Ваш пароль
	</div>
	<div class="distance"></div>
	<div class="distance"></div>
	
	<!--
	Авторизация rambler.ru (обязательна для сбора контента)
	
	<div>
		<input id="rambler_login" type="text" class="input_text settings_input" value="" > Логин
	</div>
	<div class="distance"></div>
	
	<div>
		<input id="rambler_password" type="text" class="input_text settings_input" value="" > Пароль
	</div>
	<div class="distance"></div>
	<div class="distance"></div>
	-->
	
	Биржи ссылок
	
	<div onclick="toggle_hint('settings_link_exchange_hint')" class="hint_title">
		Подсказка к ключам
	</div>
	<div id="settings_link_exchange_hint" class="hint_text">
		Не забываем закачивать файлы бирж ссылок в директорию ROOT\site\money_house<br>
		Файлы биржи ссылок должны находиться в своей папке.<br>
		Ключ это название папки с файлами определённой биржи ссылок.<br>
		Пример: ROOT\site\money_house\[ключ]\[файлы биржи ссылок]<br>
		После сохранения настроек необходимо распространить файл шаблона на все сайты фермы.<br>
		Пример: Обновления->Обновить дизайны.<br>
		Если вы ещё ни разу не запускали "Первичный сбор контента" то обновлять дизайны не нужно.
	</div>
	
	<div>
		<input id="key_sape" type="text" class="input_text settings_input" value="" > Ключ Sape.
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="key_mainlink" type="text" class="input_text settings_input" value="" > Ключ Mainlink.
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="key_setlinks" type="text" class="input_text settings_input" value="" > Ключ Setlinks.
	</div>
	<div class="distance"></div>
	
	
	<div>
		<input id="key_linkfeed" type="text" class="input_text settings_input" value="" > Ключ Linkfeed (ВНИМАНИЕ! установите в файле - linkfeed.php, var $lc_charset = 'UTF-8';).
	</div>
	<div class="distance"></div>
	
	<!--
	<div>
		<input id="key_xap" type="text" class="input_text settings_input" value="" > Ключ Xap (ВНИМАНИЕ! установите в файле - tnx.php, var $_encoding = 'UTF-8';). ! не работает
	</div>
	<div class="distance"></div>
	
	<div>
		<input id="name_xap" type="text" class="input_text settings_input" value="" > Ваше имя в системе Xap. ! не работает
	</div>
	<div class="distance"></div>
	<div class="distance"></div>
	-->

	
	<div>
		<input type="radio" name="logs_grab" id="logs_grab_0" value="0" > Нет <input type="radio" name="logs_grab" id="logs_grab_1" value="1"> Да<br> Вести логгирование сбора контента?
	</div>
	<div class="distance"></div>
	

	<div>
		<span onclick="settings_save()" class="link_imitate" id="settings_sabmit">Сохранить</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span id="settings_indicator" class="indicator"></span>
	</div>

</div><?php }} ?>