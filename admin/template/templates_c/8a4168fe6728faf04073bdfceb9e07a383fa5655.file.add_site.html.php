<?php /* Smarty version Smarty-3.1.8, created on 2014-01-30 13:28:52
         compiled from "X:\home\unique_site_gen_47-1\www\admin\template\templates\add_site.html" */ ?>
<?php /*%%SmartyHeaderCode:1951952ea1b546fe1e2-04530861%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8a4168fe6728faf04073bdfceb9e07a383fa5655' => 
    array (
      0 => 'X:\\home\\unique_site_gen_47-1\\www\\admin\\template\\templates\\add_site.html',
      1 => 1385369113,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1951952ea1b546fe1e2-04530861',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'tags' => 0,
    'value' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_52ea1b547cddd2_69105831',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_52ea1b547cddd2_69105831')) {function content_52ea1b547cddd2_69105831($_smarty_tpl) {?><div class="distance"></div>

<div id="add_site_panel" class="box_2 add_site_panel">

	<h1>Добавление сайта.</h1>
	<div class="distance"></div>
	
	
	<div>
		<input id="name_site" type="text" class="input_text"> <span class="weight-big">Название сайта</span>
	</div>
	<div class="distance"></div>
	

	<div>
		 <input id="domain" type="text" class="input_text" value="site_1"> <span class="weight-big">Домен</span> (без http:// и заключительного /). Не изменяйте, если будете пользоваться автоматическими средствами регистрации доменов и добавления их в cpanel.
	</div>
	<div class="distance"></div>	
	

	<div>
		<span class="weight-big">Ключевые фразы</span> (каждая фраза с новой строки, желательно не менее 2х слов в фразе для придания тематичности сайту), достаточно нескольких фраз, система найдёт подобные и в итоге фраз станет много больше (при автоматическом способе сбора категорий).<br> При ручном способе необходимо добавить все категории которые будут на сайте вручную:<br>
		
		<textarea id="search_phrases" class="textarea"></textarea>
		
	</div>	
	*Обязательное поле.	
	<div class="distance"></div>	
	
	
	<div>
		<span class="weight-big">Индивидуальный список стоп-слов</span> для сайта (статьи, фото и видео в которых найдены эти фразы/слова не будут добавляться в базу), вхождение по корню с суффиксом для отдельных слов (покрываются все словоформы) и точное вхождение для фраз (используйте словоформы в фразах для увеличения покрытия).<br>Каждое слово/фразу с новой строки:<br>
		<textarea id="b_list_words" class="textarea"></textarea>
	</div>	
	<div class="distance"></div>		

	<!--
	Тэги для соц. закладок:<br>
	<select id="id_tag">
		<?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['value']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['tags']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value){
$_smarty_tpl->tpl_vars['value']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['value']->key;
?>
			<?php if ($_smarty_tpl->tpl_vars['value']->value['name']=='Случайная категория'||$_smarty_tpl->tpl_vars['value']->value['name']=='Определение по тегам'){?>
				<option value="<?php echo $_smarty_tpl->tpl_vars['value']->value['id'];?>
"><<?php echo $_smarty_tpl->tpl_vars['value']->value['name'];?>
></option>
			<?php }?>
		<?php } ?>		

		<?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['value']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['tags']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value){
$_smarty_tpl->tpl_vars['value']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['value']->key;
?>
			<?php if ($_smarty_tpl->tpl_vars['value']->value['name']!='Случайная категория'&&$_smarty_tpl->tpl_vars['value']->value['name']!='Определение по тегам'){?>
				<option value="<?php echo $_smarty_tpl->tpl_vars['value']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value['name'];?>
</option>
			<?php }?>
		<?php } ?>
	</select>	
	<div class="distance"></div>
	-->

	<div>
		<span onclick="add_site()" class="link_imitate">Добавить сайт</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span id="add_site_indicator" class="indicator"></span>
	</div>

</div><?php }} ?>