<?
define( '_EXEC', 1 );

/*
require_once(PATH_BASE . DS . '..' . DS . 'classes' . DS . 'smarty' . DS . 'Smarty.class.php');
require_once(PATH_BASE . DS . '..' . DS . 'classes' . DS . 'models' . DS . 'sql_model.class.php');
*/

header("Content-Type: text/html; charset=utf-8");

require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/smarty/Smarty.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/models/sql_model.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/assemble_grab.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/automatize.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/templates.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/parsers.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/helpers.class.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/admin_panels.class.php');

$smarty = new Smarty;
$sql = new Sql_model;
$assemble_grab = new Assemble_grab;
$automatize = new Automatize;
$templates = new Templates;
$parsers = new Parsers;
$helpers = new Helpers;
$admin_panels = new Admin_panels;

$smarty->debugging = false;
$smarty->caching = false;
$smarty->cache_lifetime = 120;

$max_sites = 150;

/*
$smarty->template_dir = PATH_BASE . DS . 'template' . DS . 'templates' . DS;
$smarty->compile_dir = PATH_BASE . DS . 'template' . DS . 'templates_c' . DS;
$smarty->config_dir = PATH_BASE . DS . 'template' . DS . 'configs' . DS;
$smarty->cache_dir = PATH_BASE . DS . 'template' . DS . 'cache' . DS;
*/

$smarty->template_dir = $_SERVER['DOCUMENT_ROOT'] . '/admin/template/templates/';
$smarty->compile_dir = $_SERVER['DOCUMENT_ROOT'] . '/admin/template/templates_c/';
$smarty->config_dir = $_SERVER['DOCUMENT_ROOT'] . '/admin/template/configs/';
$smarty->cache_dir = $_SERVER['DOCUMENT_ROOT'] . '/admin/template/cache/';


//изменение количества комментариев на странице
if(!empty($_POST['command_add_site']) AND !empty($_POST['search_phrases'])){		

	$sites_count = $sql->select_sites_count();
	if($sites_count['count'] < $max_sites){
		$id_site = $sql->insert_site( $_POST['search_phrases'], $_POST['name_site'], $_POST['domain'], $_POST['id_tag']);
		$id_b_list = $sql->insert_b_list($id_site, $_POST['b_list_words']);
		$result = $sql->insert_search_phrases($id_site, $_POST['search_phrases']);
		
		/*
		if($result){
			echo $result;
		}else{
			echo -1;
		}
		*/
	}else{
		echo -2;
	}
	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


if(!empty($_POST['command_add_mass_sites']) AND !empty($_POST['ams_sites_phrases']) ){		

	$ams_sites_phrases = nl2br($_POST['ams_sites_phrases'], false);
	$ams_sites_phrases_arr = explode('<br>', $ams_sites_phrases);
	$loop = 0;
	$sites_count = $sql->select_sites_count();
	if($sites_count['count'] >= $max_sites){
		echo -2;
	}else{
		foreach($ams_sites_phrases_arr as $key => $value){
		
			$sites_count = $sql->select_sites_count();
			if($sites_count['count'] < $max_sites){
				$id_site = $sql->insert_site( $value, $value, 'site_1');
				$result = $sql->insert_search_phrases($id_site, $value);
				$loop++;
			}
					
			
		}
		//print_r($ams_sites_phrases);
		echo $loop;
	}

	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//обновление панели сайтов
if(!empty($_POST['command_update_sites_panel'])){		

	//выборка инфы по сайтам
	$info_sites = $sql->select_info_sites();
	$smarty->assign("info_sites", $info_sites);
	$smarty->display('sites_panel.html');
	
	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//обновление панели настроек
if( !empty($_POST['command_update_settings_panel']) ){		

	//выборка инфы по сайтам
	$settings = $sql->select_settings_by_admin();
	$settings_json =  json_encode ($settings);
	//$smarty->assign("settings", $settings);
	//$smarty->display('settings_panel.html');
	echo $settings_json;
	
	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//первичный сбор контента
if(!empty($_POST['command_primary_grab'])){		

	//выборка инфы по сайтам
	$sites_and_phrases_primary_grab_0 = $sql->select_sites_and_phrases_primary_grab_0();

	//$smarty->assign("sites_and_phrases_primary_grab_0", $sites_and_phrases_primary_grab_0);
	/*
	##отладка
	echo '<pre>';
	print_r($sites_and_phrases_primary_grab_0);
	echo '</pre>';
	*/

	//$smarty->display('sites_panel.html');

	$assemble_grab->grab_main_loop($sites_and_phrases_primary_grab_0);	

	
	/*
	echo '<pre>';
	print_r($result_text);
	echo '</pre>';
	*/
	/*
	//выборка инфы по сайтам
	$info_sites = $sql->select_info_sites();
	$smarty->assign("info_sites", $info_sites);
	$smarty->display('sites_panel.html');	
	*/
	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//первичный сбор контента
if(!empty($_POST['command_fragmentary_grab'])){		

	//выборка инфы по сайтам
	$sites_and_phrases_primary_grab_0 = $sql->select_sites_and_phrases_primary_grab_0();

	//$smarty->assign("sites_and_phrases_primary_grab_0", $sites_and_phrases_primary_grab_0);
	/*
	##отладка
	echo '<pre>';
	print_r($sites_and_phrases_primary_grab_0);
	echo '</pre>';
	*/

	//$smarty->display('sites_panel.html');

	$assemble_grab->grab_main_loop($sites_and_phrases_primary_grab_0, $fragment_grab = 1);	

	
	/*
	echo '<pre>';
	print_r($result_text);
	echo '</pre>';
	*/
	/*
	//выборка инфы по сайтам
	$info_sites = $sql->select_info_sites();
	$smarty->assign("info_sites", $info_sites);
	$smarty->display('sites_panel.html');	
	*/
	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//вторичный, глубокий сбор контента для сайтов с малым количеством текстов
if(!empty($_POST['command_refresh_small_content_sites'])){		

	$automatize->refresh_create_sites_small_content($count_text_censor = 30);	

	//выборка инфы по сайтам
	$sites_and_phrases_primary_grab_0 = $sql->select_sites_and_phrases_primary_grab_0();	

	//увеличиваем глубину сбора сайтов в гугле
	$assemble_grab->set_depth_text_google_grab($depth_text_google_grab = 20);
	$result_text = $assemble_grab->grab_main_loop($sites_and_phrases_primary_grab_0);
	
	//выборка инфы по сайтам
	$info_sites = $sql->select_info_sites();
	$smarty->assign("info_sites", $info_sites);
	$smarty->display('sites_panel.html');	

	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//вторичный, глубокий сбор контента для сайтов с малым количеством текстов
if(!empty($_POST['command_delete_small_content_sites'])){			

	$automatize->delete_small_content_sites();				

	//выборка инфы по сайтам
	$info_sites = $sql->select_info_sites();
	$smarty->assign("info_sites", $info_sites);
	$smarty->display('sites_panel.html');	

	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//назначение прав доступа требуемым папкам
if(!empty($_POST['command_chmod_update'])){			

	$automatize->chmod_update();			

	//выборка инфы по сайтам
	$info_sites = $sql->select_info_sites();
	
	$smarty->assign("info_sites", $info_sites);
	$smarty->display('sites_panel.html');	

	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//создание общего файла соц. закладок
if(!empty($_POST['command_social_bookmark'])){			

	$templates->create_file_for_bagbookmark_common();		

	//выборка инфы по сайтам
	$info_sites = $sql->select_info_sites();
	
	$smarty->assign("info_sites", $info_sites);
	$smarty->display('sites_panel.html');	

	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//add 01_11_12 23_49
if(!empty($_POST['command_liveinternet_reg'])){			

	$result = $automatize->liveinternet_reg();	

	//echo '<pre>';
	print_r($result);
	//echo '</pre>';	

	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//add 02_11_12 22_15
if(!empty($_POST['command_update_files_by_sample'])){			

	$result = $templates->update_files_by_sample();	

	//echo '<pre>';
	print_r($result);
	//echo '</pre>';	

	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//add 05_11_12 22_57
if(!empty($_POST['command_echo_list_sites'])){			

	$result = $automatize->echo_list_sites();

	echo '<style>
		table.sites_list_table{
			border:1px solid black;			
		}
		table.sites_list_table td{
			padding:5px;
			border:1px solid black;
		}
		span.title_site_list{
			font-weight:bold;
		}
	</style>';	

	echo '<span class="title_site_list" >Домены:</span><br>';	
	
	foreach($result as $key => $value){
		echo $value['domain'] . '<br>';
	}	
	
	echo '<br><br><br><br>';
	echo '<span class="title_site_list" >Названия сайтов:</span><br>';
	
	foreach($result as $key => $value){
		echo $value['name'] . '<br>';
	}
	
	echo '<br><br><br><br>';	
	echo '<span class="title_site_list" >Данные в виде таблицы</span>';
	echo '<table class="sites_list_table">';
	echo '<tr> <th>id</th> <th>Название</th> <th>Домен</th> </tr>';
	foreach($result as $key => $value){
		echo '<tr><td>' . $value['id'] . '</td><td>' . $value['name'] . '</td><td>' . $value['domain'] . '</td></tr>';
	}
	echo '</table>';
	//echo '<pre>';
	//print_r($result);
	//echo '</pre>';	

	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//add 07_11_12 01_07
if( !empty($_POST['command_grab_liveinternet']) ){			

	$statistic = $parsers->grab_liveinternet();

	/*
	echo '<pre>';	
	print_r($result);
	echo '</pre>';
	*/
	
	//$statistic = 'dsdsdsdsdsds';
	
	$smarty->assign("statistic", $statistic);
	$smarty->display('statistic.html');

	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//add 22_08_13 15-13
if( !empty($_POST['update_edit_site_panel']) ){			

	$esp_site_search_phrases = $sql->select_search_phrases_by_id_site($_POST['id_site'], $limit = 200, $offset = 0);

	/*
	echo '<pre>';	
	print_r($result);
	echo '</pre>';
	*/
	
	//$statistic = 'dsdsdsdsdsds';
	
	$smarty->assign("esp_site_search_phrases", $esp_site_search_phrases);
	$smarty->display('edit_site_panel.html');

	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//add 26,08,13
if( !empty($_POST['update_esp_search_phrases']) ){			

	$esp_site_search_phrases = $sql->select_search_phrases_by_id_site($_POST['id_site'], $limit = 200, $offset = 0);

	/*
	echo '<pre>';	
	print_r($result);
	echo '</pre>';
	*/
	
	//$statistic = 'dsdsdsdsdsds';
	
	$smarty->assign("esp_site_search_phrases", $esp_site_search_phrases);
	$smarty->display('edit_site_panel_search_phrases.html');

	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//add 24_12_12 20_33
if( !empty($_POST['command_query_index']) ){			

	$statistic = $parsers->query_index();

	/*
	echo '<pre>';	
	print_r($result);
	echo '</pre>';
	*/	

	$smarty->assign("statistic", $statistic);
	$smarty->display('statistic_indexes.html');

	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//add 23_01_13 16_03
if( !empty($_POST['command_activate_mainlink']) ){	

	$sites = $sql->select_all_info_sites_by_primary_grab_and_domain_not_site_1_and_ml_0($primary_grab = 1);
	$statistic = $parsers->query_index($sites);	
	$ids_for_active = null;
	
	foreach($statistic as $key => $value){
		if($value['yandex_index'] != 'no' AND $value['yandex_index'] > 30){
			if( !empty($ids_for_active) ){
				$separator = ',';
			}
			$ids_for_active .= $separator . $value['id'];
		}
	}
	
	/*
	$price_array['dol_main_page'] = trim($_POST['dol_main_page']);
	$price_array['rub_main_page'] = trim($_POST['rub_main_page']);
	$price_array['dol_lvl_1_page'] = trim($_POST['dol_lvl_1_page']);
	$price_array['rub_lvl_1_page'] = trim($_POST['rub_lvl_1_page']);
	$price_array['dol_lvl_2_page'] = trim($_POST['dol_lvl_2_page']);
	$price_array['rub_lvl_2_page'] = trim($_POST['rub_lvl_2_page']);
	$price_array['dol_lvl_3_page'] = trim($_POST['dol_lvl_3_page']);
	$price_array['rub_lvl_3_page'] = trim($_POST['rub_lvl_3_page']);
	*/
	
	/*
	$count_main_links = trim($_POST['count_main_links']);
	$count_other_links = trim($_POST['count_other_links']);
	$multiplication = trim($_POST['multiplication']);
	*/
	$stop_words = trim($_POST['stop_words_mainlink']);
	
	$login_mainlink = trim($_POST['login_mainlink']);
	$password_mainlink = trim($_POST['password_mainlink']);
	
	
	if( !empty($ids_for_active) ){
		$sites = $sql->select_all_info_sites_by_ids($ids_for_active);
	}
	
	/*
	###отладка
	$result = $admin_panels->add_mass_sites_mainlink($sites, $login_mainlink, $password_mainlink, $stop_words);
	echo '<pre>';	
		print_r($result);
	echo '</pre>';	
	*/
	
	
	if( empty($ids_for_active) ){
		echo '<span style="color:red;font-weight:bold;">Среди ещё не добавленных сайтов нет отвечающих условиям добавления в mainlink.ru (больше 30 страниц в индексе Яндекса)!</span>';
	}else{
		$sql->update_sites_mainlink_status($ids_for_active);
		$result = $admin_panels->add_mass_sites_mainlink($sites, $login_mainlink, $password_mainlink, $stop_words);
		
		echo 'Если id_site_mainlink отрицательное число, то это код ошибки обновления сайта в mainlink.ru. Ошибка обновления сайта могла быть вызвана ошибкой добавления сайта, либо иными причинами.<br>';
		echo '<pre>';	
		print_r($result);
		echo '</pre>';	
	}
	
		

	
	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//add 09_07_13 00_15
if( !empty($_POST['command_update_price_mainlink']) ){	

	
	$price_array['dol_main_page'] = trim($_POST['dol_main_page']);
	$price_array['rub_main_page'] = trim($_POST['rub_main_page']);
	$price_array['dol_lvl_1_page'] = trim($_POST['dol_lvl_1_page']);
	$price_array['rub_lvl_1_page'] = trim($_POST['rub_lvl_1_page']);
	$price_array['dol_lvl_2_page'] = trim($_POST['dol_lvl_2_page']);
	$price_array['rub_lvl_2_page'] = trim($_POST['rub_lvl_2_page']);
	$price_array['dol_lvl_3_page'] = trim($_POST['dol_lvl_3_page']);
	$price_array['rub_lvl_3_page'] = trim($_POST['rub_lvl_3_page']);
	
	$count_main_links = trim($_POST['count_main_links']);
	$count_other_links = trim($_POST['count_other_links']);
	$multiplication = trim($_POST['multiplication']);
	//$stop_words = trim($_POST['stop_words']);
	
	$login_mainlink = trim($_POST['login_mainlink']);
	$password_mainlink = trim($_POST['password_mainlink']);
	
	$result = $admin_panels->set_price_and_count_link_sites_mainlink($login_mainlink, $password_mainlink, $price_array, $count_main_links = 3, $count_other_links = 1, $multiplication = 0.5);
	echo 'Обновлённые сайты, ids в системе Mainlink.<br>';
	echo '<pre>';	
		print_r($result);
	echo '</pre>';		
	
	/*
	$smarty->assign("active_mainlink", 1);
	$smarty->assign("ids_for_active_mainlink", $ids_for_active);	
	$smarty->assign("statistic", $statistic);
	$smarty->display('statistic_indexes.html');
	*/
	
	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//add 08_02_13 02_30
if( !empty($_POST['command_activate_add_sape']) ){	

	$sites = $sql->select_all_info_sites_by_primary_grab_and_domain_not_site_1_and_sape_0($primary_grab = 1);	
	
	if( empty($sites) ){
		echo '<span style="color:red;font-weight:bold;">Все сайты уже добавлены в sape, либо в группе сайтов нет ни одного сайта!</span>';
		die;
	}
	$statistic = $parsers->query_index($sites);	
	$ids_for_active = null;
	
	foreach($statistic as $key => $value){
		if($value['yandex_index'] != 'no' AND $value['yandex_index'] > 30){
			if( !empty($ids_for_active) ){
				$separator = ',';
			}
			$ids_for_active .= $separator . $value['id'];
		}
	}
		
	$count_main_links_sape = trim($_POST['count_main_links_sape']);
	$count_other_links_sape = trim($_POST['count_other_links_sape']);
	
	$login_sape = trim($_POST['login_sape']);
	$password_sape = trim($_POST['password_sape']);
	
	$stop_words_sape = trim($_POST['stop_words_sape']);
	$folder_sape = trim($_POST['folder_sape']);
	
	if( empty($ids_for_active) ){
		echo '<span style="color:red;font-weight:bold;">Среди ещё не добавленных сайтов нет отвечающих условиям добавления в sape.ru (больше 30 страниц в индексе Яндекса)!</span>';
	}else{
		$sql->update_sites_sape_status($ids_for_active);
		$id_site_and_id_sape = $admin_panels->add_mass_sites_sape($sites, $login_sape, $password_sape, $count_main_links_sape, $count_other_links_sape, $stop_words_sape, $folder_sape);				
		$sql->update_id_sape_by_id_site($id_site_and_id_sape);
		
		echo '<pre>';	
		print_r($id_site_and_id_sape);
		echo '</pre>';	
	}
	
	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//add 10_02_13 15_55
if( !empty($_POST['command_activate_sape_pages_and_price']) ){	

	$sites = $sql->select_sites_that_have_id_sape();	
	$ids_for_active = null;	
	$separator = null;
	
	foreach($sites as $key => $value){
		
		if( !empty($ids_for_active) ){
			$separator = ',';
		}
		
		$ids_for_active .= $separator . $value['id'];
		
	}
	
	$price_array['rub_main_page_sape'] = trim($_POST['rub_main_page_sape']);
	$price_array['rub_lvl_1_page_sape'] = trim($_POST['rub_lvl_1_page_sape']);
	$price_array['rub_lvl_2_page_sape'] = trim($_POST['rub_lvl_2_page_sape']);
	
	$multiplication_sape = trim($_POST['multiplication_sape']);	
	
	$login_sape = trim($_POST['login_sape']);
	$password_sape = trim($_POST['password_sape']);		
	
		
	if( empty($ids_for_active) ){
		echo '<span style="color:red;font-weight:bold;">Нет сайтов ожидающих активации страниц в Sape, сначала задействуйте процедуру добавления сайтов в Sape!</span>';
	}else{	
		$result = $admin_panels->mass_activate_sape_pages_and_price($sites, $login_sape, $password_sape, $price_array, $multiplication_sape);		
		
		echo '<pre>';	
		print_r($result);
		echo '</pre>';	
	}
	
	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//add 24_12_12 20_33
if( !empty($_POST['command_activate_sape']) ){			

	$statistic = $parsers->query_index();	
	$ids_for_active = null;
	
	foreach($statistic as $key => $value){
		if($value['yandex_index'] != 'no' AND $value['yandex_index'] > 30){
			if( !empty($ids_for_active) ){
				$separator = ',';
			}
			$ids_for_active .= $separator . $value['id'];
		}
	}	
	
	$sql->update_sites_sape_status($ids_for_active);

	/*
	echo '<pre>';	
	print_r($result);
	echo '</pre>';
	*/
	
	//$statistic = 'dsdsdsdsdsds';
	$smarty->assign("active_sape", 1);
	$smarty->assign("ids_for_active_sape", $ids_for_active);	
	$smarty->assign("statistic", $statistic);
	$smarty->display('statistic_indexes.html');

	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//add 08_11_12 01_02
if( !empty($_POST['command_add_ads']) ){			

	$last_insert_ids_array = $automatize->add_ads($_POST['name_ads'], $_POST['ads_position'], $_POST['ads_sorting'], $_POST['code_ads']);

	if($last_insert_ids_array == false){
		echo '<span style="color:red;">Ссылки в базе данных на данный рекламный блок<br>не были созданы т.к. они уже имеются,<br>либо из-за ошибки!</span>';
	}else{
		echo '<pre>';	
		print_r($last_insert_ids_array);
		echo '</pre>';
	}		
	
	/*
	$smarty->assign("statistic", $statistic);
	$smarty->display('statistic.html');
	*/
	
	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//add 07_01_13 04_13
if( !empty($_POST['command_add_sites_blacks']) ){
		
	//$_POST['count_sites_blacks']	- количество сайтов
	$main_cat = $parsers->wiktionary_query();
	$texts_link = $parsers->get_subcats_for_cats($main_cat, $_POST['count_sites_blacks']);
	$automatize->add_sites_blanks($texts_link);

	//выборка инфы по сайтам
	$info_sites = $sql->select_info_sites();
	$smarty->assign("info_sites", $info_sites);
	$smarty->display('sites_panel.html');

	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//add 13_01_13 00_03
if( !empty($_POST['command_create_subdomains']) ){	

	set_time_limit(30*60);
	/*
	$_POST['up_domains']	
	$_POST['number_farm']
	$_POST['theme_cpanel']	
	$_POST['login_cpanel']	
	$_POST['password_cpanel']
	*/

	$up_domains = nl2br($_POST['up_domains'], $is_xhtml = false);
	//выполняем регулярную замену чтобы удалить символы переноса строк
	$up_domains = preg_replace('/\n/ui', '', $up_domains);
	$up_domains_array = explode('<br>', $up_domains);	
	$sites = $sql->select_all_info_sites_by_primary_grab_and_default_domain($primary_grab = 1);	
	
	foreach($sites as $key => $value){
		$site_id_and_domain[$key]['id'] = $value['id'];
		$site_id_and_domain[$key]['domain'] = $helpers->translit_domain($value['name']);
	}
	
	$ids_domains_status = $admin_panels->mass_subdomain($site_id_and_domain, $up_domains_array, $_POST['login_cpanel'], $_POST['password_cpanel'], $_POST['number_farm'], $_POST['theme_cpanel']);
		
	/*
	//формируем массив id сайтов и их домены для обновления доменов в таблице sites
	$count_good_loop = 0;
	foreach($ids_domains_status as $key => $value){
		if($value['status'] == '1'){
		
			$ids_domains_for_update[$count_good_loop]['id'] = $value['id'];
			$ids_domains_for_update[$count_good_loop]['domain'] = $value['domain'];
			
			$count_good_loop++;
		}				
	}
	*/
	
	/*
	###отладка
	echo '<pre>';	
	print_r($sites);
	echo '</pre>';
	
	###отладка
	echo '<pre>';	
	print_r($site_id_and_domain);
	echo '</pre>';
	*/
	
	//выводим отчёт
	//echo '<pre>';	
	print_r($ids_domains_status);
	//echo '</pre>';
	
	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}



//add 13_01_13 00_03
if( !empty($_POST['command_status_sites_update']) ){	

	//set_time_limit(30*60);
	
	if($_POST['status_update_texts'] == 'none'){
		$_POST['status_update_texts'] = 0;
	}
	if($_POST['status_update_videos'] == 'none'){
		$_POST['status_update_videos'] = 0;
	}
	if($_POST['status_update_fotos'] == 'none'){
		$_POST['status_update_fotos'] = 0;
	}
	if($_POST['status_update_translates'] == 'none'){
		$_POST['status_update_translates'] = 0;
	}	
	
	$ids_sites_update = $automatize->status_sites_update($_POST['status_update_texts'], $_POST['status_update_videos'], $_POST['status_update_fotos'], $_POST['status_update_translates']);
				
	//выводим отчёт
	echo '<pre>';	
	print_r($ids_sites_update);
	echo '</pre>';
	
	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//add 13_01_13 00_03
if( !empty($_POST['command_create_subdomains_isp']) ){	

	/*
	$_POST['up_domains']	
	$_POST['number_farm']
	$_POST['hid_user']	
	$_POST['login_panel']	
	$_POST['password_panel']
	*/
	set_time_limit(30*60);
	
	$up_domains = nl2br($_POST['up_domains'], $is_xhtml = false);
	//выполняем регулярную замену чтобы удалить символы переноса строк
	$up_domains = preg_replace('/\n/ui', '', $up_domains);
	$up_domains_array = explode('<br>', $up_domains);	
	$sites = $sql->select_all_info_sites_by_primary_grab_and_default_domain($primary_grab = 1);	
	
	foreach($sites as $key => $value){
		$sites_id_and_domain[$key]['id'] = $value['id'];
		$sites_id_and_domain[$key]['domain'] = $helpers->translit_domain($value['name']);
	}
	
	$ids_domains_status = $admin_panels->mass_subdomain_isp($sites_id_and_domain, $up_domains_array, $_POST['login_panel'], $_POST['password_panel'], $_POST['number_farm'], $_POST['hid_user'], $_POST['hid_email'], $_POST['port']);
		
	//выводим отчёт
	//echo '<pre>';	
	print_r($ids_domains_status);
	//echo '</pre>';
	
	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//add 18_01_13 21_46
if( !empty($_POST['command_create_domains']) ){	

	set_time_limit(30*60);
	
	$_POST['number_farm'] = trim($_POST['number_farm']);
	$_POST['theme_cpanel'] = trim($_POST['theme_cpanel']);
	$_POST['login_cpanel'] = trim($_POST['login_cpanel']);
	$_POST['password_cpanel'] = trim($_POST['password_cpanel']);
	
	$arg_2dom_reg['username'] = trim($_POST['username']);
	$arg_2dom_reg['password'] = trim($_POST['password']);
	$arg_2dom_reg['folder_name'] = trim($_POST['folder_name']);
	$arg_2dom_reg['post_code'] = trim($_POST['post_code']);
	$arg_2dom_reg['region'] = trim($_POST['region']);
	$arg_2dom_reg['town'] = trim($_POST['town']);
	$arg_2dom_reg['street_house_room'] = trim($_POST['street_house_room']);
	$arg_2dom_reg['phone'] = trim($_POST['phone']);
	$arg_2dom_reg['e_mail'] = trim($_POST['e_mail']);
	$arg_2dom_reg['person_en'] = trim($_POST['person_en']);
	$arg_2dom_reg['person_r'] = trim($_POST['person_r']);
	$arg_2dom_reg['birth_date'] = trim($_POST['birth_date']);
	$arg_2dom_reg['private_person_flag'] = trim($_POST['private_person_flag']);
	$arg_2dom_reg['passport_number'] = trim($_POST['passport_number']);
	$arg_2dom_reg['passport_issued_org'] = trim($_POST['passport_issued_org']);
	$arg_2dom_reg['passport_issued_date'] = trim($_POST['passport_issued_date']);
	$arg_2dom_reg['country'] = trim($_POST['country']);
	$arg_2dom_reg['ns0'] = trim($_POST['ns0']);
	$arg_2dom_reg['ns1'] = trim($_POST['ns1']);	
	
	/*
	###отладка
	$result['number_farm'] = $_POST['number_farm'];
	$result['theme_cpanel'] = $_POST['theme_cpanel'];
	$result['login_cpanel'] = $_POST['login_cpanel'];
	$result['password_cpanel'] = $_POST['password_cpanel'];
	$result['username'] = $arg_2dom_reg['username'];
	$result['password'] = $arg_2dom_reg['password'];
	$result['folder_name'] = $arg_2dom_reg['folder_name'];
	$result['post_code'] = $arg_2dom_reg['post_code'];
	$result['region'] = $arg_2dom_reg['region'];
	$result['town'] = $arg_2dom_reg['town'];
	$result['street_house_room'] = $arg_2dom_reg['street_house_room'];
	$result['phone'] = $arg_2dom_reg['phone'];
	$result['e_mail'] = $arg_2dom_reg['e_mail'];
	$result['person_en'] = $arg_2dom_reg['person_en'];
	$result['person_r'] = $arg_2dom_reg['person_r'];
	$result['birth_date'] = $arg_2dom_reg['birth_date'];
	$result['private_person_flag'] = $arg_2dom_reg['private_person_flag'];
	$result['passport_number'] = $arg_2dom_reg['passport_number'];
	$result['passport_issued_org'] = $arg_2dom_reg['passport_issued_org'];
	$result['country'] = $arg_2dom_reg['country'];
	$result['ns0'] = $arg_2dom_reg['ns0'];
	$result['ns1'] = $arg_2dom_reg['ns1'];
	*/
	

	$result = $admin_panels->mass_reg_add_domain_2domain_cpanel($arg_2dom_reg, $_POST['number_farm'], $_POST['login_cpanel'], $_POST['password_cpanel'], $_POST['theme_cpanel']);

	###отладка
	//$result = json_decode($result[0]['result_reg']);
	
	//выводим отчёт
	echo '<pre>';	
	print_r($result);
	echo '</pre>';
	
	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}



//add 02_03_13 15_46
if( !empty($_POST['command_create_domains_isp']) ){		
	
	set_time_limit(30*60);
	
	$_POST['number_farm'] = trim($_POST['number_farm']);
	$_POST['hid_user'] = trim($_POST['hid_user']);
	$_POST['login_panel'] = trim($_POST['login_panel']);
	$_POST['password_panel'] = trim($_POST['password_panel']);
	
	$_POST['port'] = trim($_POST['port']);
	
	$arg_2dom_reg['username'] = trim($_POST['username']);
	$arg_2dom_reg['password'] = trim($_POST['password']);
	$arg_2dom_reg['folder_name'] = trim($_POST['folder_name']);
	$arg_2dom_reg['post_code'] = trim($_POST['post_code']);
	$arg_2dom_reg['region'] = trim($_POST['region']);
	$arg_2dom_reg['town'] = trim($_POST['town']);
	$arg_2dom_reg['street_house_room'] = trim($_POST['street_house_room']);
	$arg_2dom_reg['phone'] = trim($_POST['phone']);
	$arg_2dom_reg['e_mail'] = trim($_POST['e_mail']);
	$arg_2dom_reg['person_en'] = trim($_POST['person_en']);
	$arg_2dom_reg['person_r'] = trim($_POST['person_r']);
	$arg_2dom_reg['birth_date'] = trim($_POST['birth_date']);
	$arg_2dom_reg['private_person_flag'] = trim($_POST['private_person_flag']);
	$arg_2dom_reg['passport_number'] = trim($_POST['passport_number']);
	$arg_2dom_reg['passport_issued_org'] = trim($_POST['passport_issued_org']);
	$arg_2dom_reg['passport_issued_date'] = trim($_POST['passport_issued_date']);
	$arg_2dom_reg['country'] = trim($_POST['country']);
	$arg_2dom_reg['ns0'] = trim($_POST['ns0']);
	$arg_2dom_reg['ns1'] = trim($_POST['ns1']);	
	
	/*
	###отладка
	$result['number_farm'] = $_POST['number_farm'];
	$result['theme_cpanel'] = $_POST['theme_cpanel'];
	$result['login_cpanel'] = $_POST['login_cpanel'];
	$result['password_cpanel'] = $_POST['password_cpanel'];
	$result['username'] = $arg_2dom_reg['username'];
	$result['password'] = $arg_2dom_reg['password'];
	$result['folder_name'] = $arg_2dom_reg['folder_name'];
	$result['post_code'] = $arg_2dom_reg['post_code'];
	$result['region'] = $arg_2dom_reg['region'];
	$result['town'] = $arg_2dom_reg['town'];
	$result['street_house_room'] = $arg_2dom_reg['street_house_room'];
	$result['phone'] = $arg_2dom_reg['phone'];
	$result['e_mail'] = $arg_2dom_reg['e_mail'];
	$result['person_en'] = $arg_2dom_reg['person_en'];
	$result['person_r'] = $arg_2dom_reg['person_r'];
	$result['birth_date'] = $arg_2dom_reg['birth_date'];
	$result['private_person_flag'] = $arg_2dom_reg['private_person_flag'];
	$result['passport_number'] = $arg_2dom_reg['passport_number'];
	$result['passport_issued_org'] = $arg_2dom_reg['passport_issued_org'];
	$result['country'] = $arg_2dom_reg['country'];
	$result['ns0'] = $arg_2dom_reg['ns0'];
	$result['ns1'] = $arg_2dom_reg['ns1'];
	*/
	

	$result = $admin_panels->mass_reg_add_domain_2domain_isp($arg_2dom_reg, $_POST['number_farm'], $_POST['login_panel'], $_POST['password_panel'], $_POST['hid_user'], $_POST['port']);

	###отладка
	//$result = json_decode($result[0]['result_reg']);
	
	//выводим отчёт
	echo '<pre>';	
	print_r($result);
	echo '</pre>';
	
	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//add 23_02_13 13_58
if( !empty($_POST['command_create_domains_cron']) ){		
	
	$sold = 'hgbthj2c';
	
	
	$data['number_farm'] = trim($_POST['number_farm']);
	$data['theme_cpanel'] = trim($_POST['theme_cpanel']);
	$data['login_cpanel'] = trim($_POST['login_cpanel']);
	$data['password_cpanel'] = base64_encode(trim($_POST['password_cpanel']) . $sold);
	
	$data['username'] = trim($_POST['username']);
	$data['password'] = base64_encode(trim($_POST['password']) . $sold);
	$data['folder_name'] = trim($_POST['folder_name']);
	$data['post_code'] = trim($_POST['post_code']);
	$data['region'] = trim($_POST['region']);
	$data['town'] = trim($_POST['town']);
	$data['street_house_room'] = trim($_POST['street_house_room']);
	$data['phone'] = trim($_POST['phone']);
	$data['e_mail'] = trim($_POST['e_mail']);
	$data['person_en'] = trim($_POST['person_en']);
	$data['person_r'] = trim($_POST['person_r']);
	$data['birth_date'] = trim($_POST['birth_date']);
	$data['private_person_flag'] = trim($_POST['private_person_flag']);
	$data['passport_number'] = trim($_POST['passport_number']);
	$data['passport_issued_org'] = trim($_POST['passport_issued_org']);
	$data['passport_issued_date'] = trim($_POST['passport_issued_date']);
	$data['country'] = trim($_POST['country']);
	$data['ns0'] = trim($_POST['ns0']);
	$data['ns1'] = trim($_POST['ns1']);	
	//устанавливает переключатель на регистрацию доменов
	$data['domains_or_sub'] = 1;	
	
	$result = $sql->update_cron_data_reg_domain($data);

	$automatize->add_cronjob_reg_domains();
	/*
	//выводим отчёт
	echo '<pre>';	
	print_r($result);
	echo '</pre>';
	*/
	
	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//add 24_02_13 14_56
if( !empty($_POST['command_create_subdomains_cron']) ){	

	$sold = 'hgbthj2c';
	
	$data['up_domains'] = $_POST['up_domains'];	
	$data['number_farm'] = $_POST['number_farm'];
	$data['theme_cpanel'] = $_POST['theme_cpanel'];	
	$data['login_cpanel'] = $_POST['login_cpanel'];	
	$data['password_cpanel'] = base64_encode(trim($_POST['password_cpanel']) . $sold);	
	//устанавливает переключатель на регистрацию поддоменов
	$data['domains_or_sub'] = 0;
	
	$result = $sql->update_cron_data_reg_domain($data);
	
	$automatize->add_cronjob_reg_subdomains();
	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//add 20_01_13 23_03
if( !empty($_POST['command_fill_fields_cd_from_file']) ){	

	$data = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/words/data_for_domain_reg_add.txt');	
	preg_match_all($pattern = '|\[.*\]|ui', $data, $matches_var);
	preg_match_all($pattern = '|\{.*\}|ui', $data, $matches_ind);	
	
	foreach($matches_ind[0] as $key => $value){
		//$ind = preg_replace('/({|})/uis', '', $value);
		//$value_1 = preg_replace('/([|])/uis', '', $matches_var[0][$key]);
		$value = str_replace('{', '', $value);
		$value = str_replace('}', '', $value);
		$matches_var[0][$key] = str_replace('[', '', $matches_var[0][$key]);
		$matches_var[0][$key] = str_replace(']', '', $matches_var[0][$key]);
		$data_array[$key]['index'] = $value;
		$data_array[$key]['value'] = $matches_var[0][$key];
	}
	
	$json_str = json_encode($data_array);
	
	echo $json_str;
	
	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//add 09_11_12 23_53
if( !empty($_POST['command_update_all_design']) ){	

	$sites = $sql->select_all_info_sites();
	
	foreach($sites as $key => $value){
		$automatize->create_template($value['id']);
	}

	echo '<br><strong>Готово!</strong>';

	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//add 11_11_12 21_35
//обновляет публикацию всего контента
if( !empty($_POST['command_update_content_publish']) ){	

	$sql->unpublish_texts();
	$sql->unpublish_texts_tr();
	$sql->unpublish_videos();
	$sql->unpublish_images();

	$sites = $sql->select_all_info_sites();
	
	foreach($sites as $key => $value){		
		$automatize->updater_status($limit_from = 80, $limit_to = 85, $percent = true, $value['id']);
	}
	//echo '<br><strong>Готово!</strong>';

	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//add 14_02_13 13_32
//сохраняет настройки
if( !empty($_POST['command_settings_save']) ){	

	$settings['key_sape'] = trim($_POST['key_sape']);
	$settings['key_mainlink'] = trim($_POST['key_mainlink']);
	$settings['key_setlinks'] = trim($_POST['key_setlinks']);
	$settings['key_linkfeed'] = trim($_POST['key_linkfeed']);
	$settings['logs_grab'] = trim($_POST['logs_grab']);
	
	$settings['key_xap'] = trim($_POST['key_xap']);
	$settings['name_xap'] = trim($_POST['name_xap']);

	$settings['ad_mail'] = trim($_POST['ad_mail']);
	$settings['ad_pass'] = trim($_POST['ad_pass']);	
	
	$settings['li_mail'] = trim($_POST['li_mail']);
	$settings['li_pass'] = trim($_POST['li_pass']);
	
	$settings['rambler_login'] = trim($_POST['rambler_login']);
	$settings['rambler_password'] = trim($_POST['rambler_password']);
	
	$settings['method_grab_cat'] = trim($_POST['method_grab_cat']);
	$settings['key_antigate'] = trim($_POST['key_antigate']);
	
	$settings['license_code'] = trim($_POST['license_code']);
	
	$sql->update_settings($settings);
	$res = $templates->replace_keys_links_exchange($settings);
	/*
	###отладка
	echo '<pre>';
	print_r($settings);
	echo '<pre>';	
	*/
	if($res){
		echo 1;
	}else{
		echo 0;
	}

	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//add 28_02_13 16_40
if( !empty($_POST['command_query_logs_grab']) ){	
	
	$settings = $sql->select_settings_by_admin();
	$logs_grab = 0;
	foreach($settings as $key => $value){
		if($value['name'] == 'logs_grab'){
			$logs_grab = $value['value'];
		}
	}
	
	if($logs_grab == 1){
		$sql->truncate_logs_grab();
	}
	
	echo $logs_grab;
	
	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//add 28_02_13 14_34
if( !empty($_POST['command_update_logs_view']) ){	
	
	$logs_grab = $sql->select_logs_grab($limit = 20);
	
	if(empty($logs_grab)){
		echo 'Подготовка к сбору контента.';
	}else{
		/*
		echo '<pre>';
		print_r($logs_grab);
		echo '<pre>';	
		*/
		/*
		$time_config['date'] = '%d/%m/%y';
		$time_config['time'] = '%H:%M:%S';
		$smarty->assign("time_config", $time_config);
		*/
		$smarty->assign("logs_grab", $logs_grab);
		$smarty->display('logs_grab.html');
	}
	
	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//24-08-13
//работа с категориями сайта
if( !empty($_POST['command_esp_site_search_phrase_do']) ){	
	
	$res = $automatize->esp_site_search_phrase_do($_POST['site_search_phrase_do_select'], $_POST['ids_search_phrases'], $_POST['esp_site_search_phrase_ar_ser']);	
	
	
	//$ads_blocks_action = serialize($ads_blocks_action);
	/*
	foreach($ads_blocks_action as $key => $value){
		$temp =  explode('_|_', $value);
		$res[] = unserialize($temp[1]);
	}
	*/
	echo $res;
	//echo '<pre>';	
	//print_r($res);
	//echo '</pre>';	
	
	//echo $ads_blocks_action;
	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//24-08-13
//работа с категориями сайта
if( !empty($_POST['command_esp_add_phrases']) ){	
	

	$sql->insert_search_phrases(trim($_POST['id_site']), trim($_POST['esp_add_phrases_field']));
	
	$lines_add = mysql_affected_rows();				
	echo $lines_add;
	//echo '<pre>';	
	//print_r($res);
	//echo '</pre>';		

	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}


//add 23_08_13
if( !empty($_POST['command_esp_update_site_name_a_domain']) ){	
	
	$sql->update_site_name_a_domain_by_id(trim($_POST['id_site']), trim($_POST['site_name']), trim($_POST['site_domain']));
	
	
	//останавливаем выполнение скрипта, всё что нужно мы сделали
	die;
}
?>