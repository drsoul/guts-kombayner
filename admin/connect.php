<?php
if( empty($_POST['login']) OR empty($_POST['password']) ){ 
	echo 'Логин и пароль НЕ переданы';
	die;

}elseif($_GET['command'] == 'statistic_visitors'){	
	
	define( '_EXEC', 1 );	
	define('PATH_BASE', dirname(__FILE__) );
	define( 'DS', DIRECTORY_SEPARATOR );

	set_time_limit(6000);
	
	require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/models/dbconfig.class.php');
	require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/parsers.class.php');
	$config = New Dbconfig();
	$parsers = New Parsers();	
	
	if( $_POST['login'] == $config->user['name'] AND $_POST['password'] == md5($config->user['pass']) ){
		
		$array_statistic = $parsers->grab_liveinternet();
		$array_statistic_serialize = serialize($array_statistic);
		echo $array_statistic_serialize;
		/*
		echo '<pre>';
		print_r($array_statistic);
		echo '<pre>';
		*/
	}
	
}elseif($_GET['command'] == 'inphrases'){	
	
	define( '_EXEC', 1 );	
	define('PATH_BASE', dirname(__FILE__) );
	define( 'DS', DIRECTORY_SEPARATOR );

	set_time_limit(6000);
	
	require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/models/dbconfig.class.php');
	require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/automatize.class.php');
	$config = New Dbconfig();
	$automatize = New Automatize();	
	
	if( $_POST['login'] == $config->user['name'] AND $_POST['password'] == md5($config->user['pass']) ){
		
		//$lines_add = $automatize->get_inphrases($_POST['vis_month_censor']);
		$lines_add = $automatize->get_inphrases($_POST['vis_month_censor']);
		//$lines_add = serialize($lines_add);
		
		//количество вставленных в базу строк
		//echo 'lines_add = ' . $lines_add;
		echo $lines_add;
		/*
		echo '<pre>';
		print_r($lines_add);
		echo '<pre>';
		*/
	}
	
}elseif($_GET['command'] == 'inphrases_by_domain'){	
	
	define( '_EXEC', 1 );	
	define('PATH_BASE', dirname(__FILE__) );
	define( 'DS', DIRECTORY_SEPARATOR );

	set_time_limit(6000);
	
	require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/models/dbconfig.class.php');
	require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/automatize.class.php');
	$config = New Dbconfig();
	$automatize = New Automatize();	
	
	if( $_POST['login'] == $config->user['name'] AND $_POST['password'] == md5($config->user['pass']) ){
		
		//$lines_add = $automatize->get_inphrases($_POST['vis_month_censor']);
		//$lines_add = $automatize->get_inphrases($_POST['vis_month_censor']);
		$lines_add = $automatize->get_inphrases_by_domain($_POST['site_domain'], $_POST['site_id'], $_POST['site_name']);
		//$lines_add = serialize($lines_add);
		
		//количество вставленных в базу строк
		//echo 'lines_add = ' . $lines_add;
		echo $lines_add;
		/*
		echo '<pre>';
		print_r($lines_add);
		echo '<pre>';
		*/
	}
	
}elseif($_GET['command'] == 'get_sites_by_visiters'){	
	
	define( '_EXEC', 1 );	
	define('PATH_BASE', dirname(__FILE__) );
	define( 'DS', DIRECTORY_SEPARATOR );

	set_time_limit(6000);
	
	require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/models/dbconfig.class.php');
	require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/automatize.class.php');
	$config = New Dbconfig();
	$automatize = New Automatize();	
	
	//echo 'ok1';
	if( $_POST['login'] == $config->user['name'] AND $_POST['password'] == md5($config->user['pass']) ){
		//echo 'ok2';	
		//$lines_add = $automatize->get_inphrases($_POST['vis_month_censor']);
		$sites = $automatize->get_sites_by_visiters($_POST['vis_month_censor']);
		$sites = json_encode($sites);
		
		//количество вставленных в базу строк
		//echo 'lines_add = ' . $lines_add;
		echo $sites;
		/*
		echo '<pre>';
		print_r($lines_add);
		echo '<pre>';
		*/
	}
	
}elseif($_GET['command'] == 'trans_inphrases_on_main_bd'){	
	
	define( '_EXEC', 1 );	
	define('PATH_BASE', dirname(__FILE__) );
	define( 'DS', DIRECTORY_SEPARATOR );

	set_time_limit(6000);
	
	require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/models/dbconfig.class.php');
	require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/automatize.class.php');
	$config = New Dbconfig();
	$automatize = New Automatize();	
	
	//echo 'ok1';
	if( $_POST['login'] == $config->user['name'] AND $_POST['password'] == md5($config->user['pass']) ){
		//echo 'ok2';	
		//$lines_add = $automatize->get_inphrases($_POST['vis_month_censor']);
		$inphrases = $automatize->select_all_inphrases();
		//$inphrases = json_encode($inphrases);
		$inphrases = serialize($inphrases);
		//количество вставленных в базу строк
		//echo 'lines_add = ' . $lines_add;
		echo $inphrases;
		/*
		echo '<pre>';
		print_r($lines_add);
		echo '<pre>';
		*/
	}
	
}elseif($_GET['command'] == 'statistic_index'){	
	
	define( '_EXEC', 1 );	
	define('PATH_BASE', dirname(__FILE__) );
	define( 'DS', DIRECTORY_SEPARATOR );

	set_time_limit(6000);
	
	require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/models/dbconfig.class.php');
	require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/parsers.class.php');
	$config = New Dbconfig();
	$parsers = New Parsers();

	if( $_POST['login'] == $config->user['name'] AND $_POST['password'] == md5($config->user['pass']) ){
	
		$yandex_google['yandex'] = $_GET['yandex'];
		$yandex_google['google'] = $_GET['google'];
		
		$array_statistic = $parsers->query_index_by_param($yandex_google);
		$array_statistic_serialize = serialize($array_statistic);
		echo $array_statistic_serialize;	
		
		/*
		#отладка
		echo '<pre>';
		print_r($array_statistic);
		echo '<pre>';
		*/
	}
}elseif($_GET['command'] == 'query_ads_blocks'){	
	
	define( '_EXEC', 1 );	
	define('PATH_BASE', dirname(__FILE__) );
	define( 'DS', DIRECTORY_SEPARATOR );

	set_time_limit(6000);
	
	require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/models/dbconfig.class.php');
	require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/models/sql_model.class.php');
	$config = New Dbconfig();
	$sql = New Sql_model();

	if( $_POST['login'] == $config->user['name'] AND $_POST['password'] == md5($config->user['pass']) ){
	
		$ads_array = $sql->select_ads_all();
		$ads_array_serialize = serialize($ads_array);
		
		echo $ads_array_serialize;
	}
	
}elseif($_GET['command'] == 'ads_blocks_action'){	
	
	define( '_EXEC', 1 );	
	define('PATH_BASE', dirname(__FILE__) );
	define( 'DS', DIRECTORY_SEPARATOR );

	set_time_limit(6000);
	
	require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/models/dbconfig.class.php');
	require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/models/sql_model.class.php');
	$config = New Dbconfig();
	$sql = New Sql_model();		
	
	if( $_POST['login'] == $config->user['name'] AND $_POST['password'] == md5($config->user['pass']) ){
			
			
		$_POST['ids_ads_blocks'] = unserialize($_POST['ids_ads_blocks']);
		$sep = '';
		foreach($_POST['ids_ads_blocks'] as $key => $value){
			$ids_for_in .= $sep . $key;
			if($sep == ''){
				$sep = ',';
			}
		}
		
			
		if($_POST['ads_blocks_select'] == 'unpublish'){
			$sql->update_ads_publish_by_ids($publish_status = 0, $ids_for_in);
		}elseif($_POST['ads_blocks_select'] == 'publish'){
			$sql->update_ads_publish_by_ids($publish_status = 1, $ids_for_in);
		}elseif($_POST['ads_blocks_select'] == 'delete'){
			$sql->delete_ads_by_ids($ids_for_in);
		}
		
		//echo $_POST['ads_blocks_select'] . '_|_' . $_POST['ids_ads_blocks'];
	}
	
}elseif($_GET['command'] == 'ads_blocks_add'){	
	
	define( '_EXEC', 1 );	
	define('PATH_BASE', dirname(__FILE__) );
	define( 'DS', DIRECTORY_SEPARATOR );

	set_time_limit(6000);
	
	require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/models/dbconfig.class.php');
	require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/automatize.class.php');
	$config = New Dbconfig();
	$automatize = New Automatize();		
	
	if( $_POST['login'] == $config->user['name'] AND $_POST['password'] == md5($config->user['pass']) ){
		
		$last_insert_ids_array = $automatize->add_ads($_POST['name_ads'], $_POST['ads_position'], $_POST['ads_sorting'], $_POST['code_ads']);
		$last_insert_ids_array = serialize($last_insert_ids_array);
		
		###отладка
		//echo $_POST['name_ads'] . ' | ' . $_POST['ads_position'] . ' | ' . $_POST['ads_sorting'] . ' | ' . $_POST['code_ads'];
		
		echo $last_insert_ids_array;		
	}
	
}elseif($_GET['command'] == 'work_group_sites_action'){	
	
	define( '_EXEC', 1 );	
	define('PATH_BASE', dirname(__FILE__) );
	define( 'DS', DIRECTORY_SEPARATOR );

	set_time_limit(6000);
	
	require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/models/dbconfig.class.php');
	require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/models/sql_model.class.php');
	require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/automatize.class.php');
	require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/controllers/templates.class.php');
	$config = New Dbconfig();
	$sql = New Sql_model();		
	$automatize = New Automatize();	
	$templates = New Templates();
	
	if( $_POST['login'] == $config->user['name'] AND $_POST['password'] == md5($config->user['pass']) ){		
		
			
		if($_POST['work_group_sites_select'] == 'update_files_sites'){
			$result = $templates->update_files_by_sample();	
			
			echo '<pre>';
			print_r($result);
			echo '</pre>';
			
		}elseif($_POST['work_group_sites_select'] == 'update_design'){		
			$sites = $sql->select_all_info_sites();	
			foreach($sites as $key => $value){
				$automatize->create_template($value['id']);
			}
			echo '<br><strong>Дизайны обновлены.</strong>';
			
		}elseif($_POST['work_group_sites_select'] == 'update_publish'){
			$sql->unpublish_texts();
			$sql->unpublish_videos();
			$sql->unpublish_images();

			$sites = $sql->select_all_info_sites();
			
			foreach($sites as $key => $value){		
				$automatize->updater_status($limit_from = 80, $limit_to = 85, $percent = true, $value['id']);
			}			
			echo '<br><strong>Публикации обновлены.</strong>';
		}elseif($_POST['work_group_sites_select'] == 'sql_query'){
		
			$id_query = $sql->query($_POST['sql_code']);

			echo '<br><strong>SQL запрос выполнен. ID запроса: ' . $id_query . '.</strong>';
		}
		
		//echo $_POST['ads_blocks_select'] . '_|_' . $_POST['ids_ads_blocks'];
	}
	
}
?>