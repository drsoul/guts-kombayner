<?
// no direct access
defined( '_EXEC' ) or die( 'Restricted access' );

require_once($_SERVER['DOCUMENT_ROOT'] . '/classes/models/root_model.class.php');

class Sql_model extends Root_model{

	protected $search_phrase = '';
	protected $id_search_phrase = 0;	
	protected $id_sites = '';	
	protected $b_list_words = '';
	protected $logs_grab = null;
 
	public function logs_grab($object, $title = 'Параметр'){
		
		if( empty($this->logs_grab) ){
			$settings = $this->select_settings_by_admin();
			foreach($settings as $key => $value){
				if($value['name'] == 'logs_grab'){
					$this->logs_grab = $value['value'];
				}
			}
		}
		
		if($this->logs_grab == 1){				
			$serial_object = serialize($object);
			$memory = memory_get_usage();
			$last_error = serialize( error_get_last() );
			$this->insert_logs_grab($title, $serial_object, $memory, $last_error);
			unset($serial_object, $object, $title, $memory, $last_error);		
		}
	}
 
	public function set_id_sites($id_sites){
		$this->id_sites = $id_sites;
	}
	
	public function get_id_sites(){
		return $this->id_sites;
	}
	
	
	public function select_ids_sites_by_params($status_update_texts, $status_update_videos, $status_update_fotos, $status_update_translates){
		
		$query = "
			SELECT DISTINCT sites.id as id,
				COUNT(DISTINCT texts.id) as count_texts,
				COUNT(DISTINCT videos.id) as count_videos,
				COUNT(DISTINCT images.id) as count_images,
				COUNT(DISTINCT texts_translate.id) as count_texts_translate
				
			FROM " . $this->prefix_table . "sites as sites
			
			LEFT JOIN " . $this->prefix_table . "search_phrases as search_phrases
			ON sites.id = search_phrases.id_sites
			
			LEFT JOIN " . $this->prefix_table . "texts as texts
			ON search_phrases.id = texts.id_search_phrase	
			
			LEFT JOIN " . $this->prefix_table . "videos as videos
			ON search_phrases.id = videos.id_search_phrase		
			
			LEFT JOIN " . $this->prefix_table . "images as images
			ON search_phrases.id = images.id_search_phrase	
			
			LEFT JOIN " . $this->prefix_table . " texts_translate as  texts_translate
			ON search_phrases.id =  texts_translate.id_search_phrase						
			
			GROUP BY sites.id 
			HAVING COUNT(DISTINCT texts.id) < " . $status_update_texts . "
			OR COUNT(DISTINCT videos.id) < " . $status_update_videos . "
			OR COUNT(DISTINCT images.id) < " . $status_update_fotos . "
			OR COUNT(DISTINCT texts_translate.id) < " . $status_update_translates . "
			;
		";	

		return $this->sel_rows($query);	
	}
	
	public function update_sites_status_by_ids($ids_for_in){
	
		if( !empty($ids_for_in) ){
			$query = "
				UPDATE " . $this->prefix_table . "sites
				SET primary_grab = 0
				WHERE id IN (" . $ids_for_in . ")		
			";	
			
			$this->query($query);
		}
		
		$lines_add = mysql_affected_rows();
			
		return $lines_add;
	}
	
	
	
	public function select_logs_grab($limit = 10){
		$query = "
			SELECT *
			FROM " . $this->prefix_table . "logs_grab
			ORDER BY id DESC
			LIMIT " . $limit . "
		";	
		
		return $this->sel_rows($query);	
	}
	/*
	public function insert_imgs($texts_and_seach_phrase_array){
		
		$search_phrase = $texts_and_seach_phrase_array['key_phrase'];
		
		//echo '<br><br>!!!' . $texts_and_seach_phrase_array['key_phrase'];
		
		$search_phrase_result = $this->select_search_phrase_by_phrase($search_phrase);
		$id_search_phrase = $search_phrase_result['id'];
		
		if(empty($id_search_phrase)){
			$id_search_phrase = $this->insert_search_phrase($search_phrase);
		}
		
		foreach($texts_and_seach_phrase_array['texts'] as $text){
		
			$url_sourse = 'google.com';
			
			$id_texts[] = $this->insert_text($id_search_phrase, $text, $url_sourse);
		}
		
		return $id_texts;
	}
	*/
	
	
	//add 10_02_13 23_02
	public function update_id_sape_by_id_site($id_site_and_id_sape){
	
		$case_str = null;
		$ids_local = null;
		$separator = '';
		foreach($id_site_and_id_sape as $key => $value){
			if( !empty($value['id_site_sape']) ){
				$case_str .= " WHEN id = " . $value['id_site_local'] . "	THEN " . $value['id_site_sape'];
				
				
				$ids_local .= $separator . $value['id_site_local'];
				
				if( empty($separator) ){
					$separator = ',';
				}
			}
		}
		
		if( !empty($case_str) ){
			$query = "
				UPDATE " . $this->prefix_table . "sites
				SET id_sape = CASE 
							" . $case_str . "
						END
				WHERE id IN (" . $ids_local . ")		
			";	
			
			$this->query($query);
		}
		
	}
	
	
	public function select_sites_that_have_id_sape(){	
		$query = "
			SELECT *
			FROM " . $this->prefix_table . "sites
			WHERE id_sape != 0
		";	
		
		return $this->sel_rows($query);		
	}
	
	
	public function select_sites_count(){	
		$query = "
			SELECT COUNT(*) as count
			FROM " . $this->prefix_table . "sites
			WHERE 1
		";	
		
		return $this->sel_1_row($query);		
	}
	
	
	public function select_ads_all(){	
		$query = "
			SELECT id, name_ads, ads_publish
			FROM " . $this->prefix_table . "ads
		";	
		
		return $this->sel_rows($query);		
	}
	
	
	public function update_ads_publish_by_ids($publish_status = 0, $ids_for_in){
		$query = "
			UPDATE " . $this->prefix_table . "ads
			SET ads_publish = " . $publish_status . "
			WHERE id IN (" . $ids_for_in . ")		
		";	
			
		$this->query($query);
	}
	
	
	public function delete_ads_by_ids($ids_for_in){
		$query = "
			DELETE   ads, ads_links
			FROM  " . $this->prefix_table . "ads as ads
			LEFT JOIN  " . $this->prefix_table . "ads_links as ads_links
			ON (ads.id = ads_links.id_ads)
			WHERE  ads.id IN (" . $ids_for_in . ")		
		";	
			
		$this->query($query);
	}
	
	
	public function delete_search_phrase_by_ids($ids_for_in){
		$query = "
			DELETE 
			FROM  " . $this->prefix_table . "search_phrases		
			WHERE  id IN (" . $ids_for_in . ")		
		";	
			
		$this->query($query);
		
		$lines_add = mysql_affected_rows();
			
		return $lines_add;
	}

	
	//add 04_11_12 20_22
	public function select_ads_liveinternet($ids_sites, $name_ads = 'counter_liveinternet.ru'){				
		
		$query = "
			SELECT ads.id as id_ads, ads.name_ads as name_ads, ads.code as code, ads_links.id_position as id_position, sites.id as id_sites
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "ads_links as ads_links
			ON sites.id = ads_links.id_sites	
			LEFT JOIN " . $this->prefix_table . "ads as ads
			ON ads.id = ads_links.id_ads					
			WHERE sites.id IN($ids_sites)
			AND ads.name_ads = '$name_ads'
		";	
		
		return $this->sel_rows($query);
	}
	
	
	//add 08_11_12 04_44
	public function select_ads_links($ids_sites, $id_ads){				
		
		$query = "
			SELECT *
			FROM " . $this->prefix_table . "ads_links as ads_links							
			WHERE ads_links.id_sites IN($ids_sites)
			AND ads_links.id_ads = '$id_ads'
		";	
		
		return $this->sel_rows($query);
	}
	
	
	//add 08_11_12 04_44
	public function update_id_mainlink_by_site_id($id_local, $id_mainlink){				
		
		$query = "
			UPDATE " . $this->prefix_table . "sites
			SET id_mainlink = $id_mainlink
			WHERE id = $id_local
		";	
		
		return $this->query($query);
	}
	
	
	//add 04_11_12 22_28
	public function select_or_insert_liveinternet($name){
		$liveinternet_result = $this->select_liveinternet($name);
		$id_liveinternet = $liveinternet_result['id'];
		
		if(empty($id_liveinternet)){
			$id_liveinternet = $this->insert_liveinternet($name);
		}
		
		return $id_liveinternet;
	}
	
	
	//add 08_11_12 04_36
	public function select_or_insert_ads($name, $code){
		$ads_result = $this->select_ads($name);
		$id_ads = $ads_result['id'];
		
		if(empty($id_ads)){
			$id_ads = $this->insert_ads($name, $code);
		}
		
		return $id_ads;
	}
	
	
	//add 08_11_12 04_36
	public function select_ads($name){		
		$name = $this->safe($name);
		
		$query = "
			SELECT *
			FROM " . $this->prefix_table . "ads
			WHERE name_ads = '$name'			
		";	
		
		return $this->sel_1_row($query);
	}
	
	
	//add 14_02_13 14_19
	public function select_settings_by_admin(){			
		
		$query = "
			SELECT *
			FROM " . $this->prefix_table . "params
			WHERE admin_panel = 1			
		";	
		
		return $this->sel_rows($query);
	}
	
	
	public function truncate_logs_grab(){
		$query = "
			TRUNCATE TABLE " . $this->prefix_table . "logs_grab			
		";	
		
		return $this->query($query);
	}
	
	
	//add 08_11_12 04_36
	public function insert_ads($name, $code){		
		
		$name = $this->safe($name);	
		$code = $this->safe($code);
		
		//echo '<br><br>!!!' . $search_phrase;
		
		$query = "
			INSERT INTO " . $this->prefix_table . "ads (name_ads, code)
			VALUES ( 
			('$name'),
			('$code')
		)";				
		
		return $this->ins_ret($query);
	}
	
	
	/*
	public function phrases_inter_site($name, $code){		
		
		$name = $this->safe($name);	
		$code = $this->safe($code);
		
		//echo '<br><br>!!!' . $search_phrase;
		
		$query = "
			INSERT INTO " . $this->prefix_table . "ads (name_ads, code)
			VALUES ( 
			('$name'),
			('$code')
		)";				
		
		return $this->ins_ret($query);
	}
	*/
	
	
	//add 08_11_12 04_36
	public function insert_logs_grab($name, $value, $memory, $last_error){		
		
		$name = $this->safe($name);	
		$value = $this->safe($value);	
		$memory = $this->safe($memory);
		$last_error = $this->safe($last_error);
		
		//echo '<br><br>!!!' . $search_phrase;
		
		$query = "
			INSERT INTO " . $this->prefix_table . "logs_grab (name, value, memory, last_error)
			VALUES ( 
			('$name'),
			('$value'),
			('$memory'),
			('$last_error')
		)";				
		
		return $this->ins_ret($query);
	}
	
	
	//add 04_11_12 22_28
	public function select_liveinternet($name){		
		
		$name = $this->safe($name);			
		
		$query = "
			SELECT *
			FROM " . $this->prefix_table . "ads
			WHERE name_ads = '$name'			
		";	
		
		return $this->sel_1_row($query);
	}
	
	
	//add 23_02_13 14_56
	public function select_cron_data(){				
		
		$query = "
			SELECT *
			FROM " . $this->prefix_table . "cron_data			
		";	
		
		return $this->sel_rows($query);
	}
	
	
	//add 04_11_12 22_28
	public function insert_ads_links($id_ads, $ids_sites, $id_position = 3, $ads_sort = 9){		
					
		if( is_array($ids_sites) ){
			$separator = ', 
			';	

			$ids_sites_count = count($ids_sites);
			$loop_counter = 0;
			foreach($ids_sites as $key => $value){		
				$loop_counter++;
				
				if($ids_sites_count == $loop_counter){
					$separator = ';';	
				}
				
				$values .= "($id_ads, $value, $id_position, $ads_sort)" . $separator;
				
				
			}
			
		}else{
			$values = "($id_ads, $value, $id_position, $ads_sort);";
		}
		
		if( !empty($values) ){
		
			$query = "
				INSERT INTO " . $this->prefix_table . "ads_links (id_ads, id_sites, id_position, ads_sort)
				VALUES 
				$values
			";
			
			return $this->sel_1_row($query);
			
		}
	}
	
	
	//add 08_11_12 04_53
	public function insert_mass_ads_links($sites, $id_ads, $ads_position, $ads_sorting){

		$id_ads = $this->safe($id_ads);
		$ads_position = $this->safe($ads_position);
		$ads_sorting = $this->safe($ads_sorting);		
	
		$separator = '';
		foreach($sites as $key => $value){
			$values .= $separator . '(' . $value['id'] . ', ' . $id_ads . ', ' . $ads_position . ', ' .  $ads_sorting . ')';
					
			$separator = ', ';
		}
	
		if( !empty($values) ){
		
			$query = "
				INSERT INTO " . $this->prefix_table . "ads_links (id_sites, id_ads, id_position, ads_sort)
				VALUES 
				$values
			";					
				
			$this->query($query);
			
			$count_sites = count($sites);
			//foreach($sites as $key => $value){
				$last_insert_ids_array['ads_links_id_min'] = Mysql_insert_id();
				$last_insert_ids_array['ads_links_id_max'] = Mysql_insert_id() + $count_sites - 1;
				$last_insert_ids_array['count_ads_links'] = $count_sites;
								
			//}
			
			return $last_insert_ids_array;
		
		}else{
			return false;
		}
	}
	
	
	//add 31_07_13 13_53
	public function truncate_inphrases(){
		$query = "TRUNCATE TABLE  " . $this->prefix_table . "inphrases";
	}
	
	
	//add 21_07_13 18_59
	public function insert_mass_inphrases($inphrases, $id_sites){

		foreach($inphrases as $key => $value){
			$value['phrase'] = $this->safe($value['phrase']);
			$value['link'] = $this->safe($value['link']);
			$value['vis'] = $this->safe($value['vis']);
			$value['%'] = $this->safe($value['%']);
			$value['base'] = $this->safe($value['base']);
			$value['site_name'] = $this->safe($value['site_name']);
			if( empty($value['site_name']) ){
				$value['site_name'] = 0;
			}
		
			$values .= $separator . '(' . $id_sites . ', "' . $value['phrase'] . '", "' .  $value['link'] . '", ' .  $value['vis'] . ', ' .  $value['%'] . ', ' .  $value['base'] . ', ' .  $value['site_name'] . ')';					
			$separator = ', ';
		}

	
		if( !empty($values) ){
		
			$query = "
				INSERT IGNORE INTO " . $this->prefix_table . "inphrases (id_sites, phrase, link, visitors, procent, base, site_name)
				VALUES 
				$values
			";					
				
			$this->query($query);
			
			$lines_add = mysql_affected_rows();
			
			return $lines_add;
		
		}else{
			return false;
		}
	}
	
	
	public function update_sites_ads_status($ids_sites_for_in){
		
		$query = "
			UPDATE " . $this->prefix_table . "sites as sites			
			SET sites.ads = 1
			WHERE sites.id IN (" . $ids_sites_for_in . ")			
		";	
		
		$this->query($query);		
	}
		
	
	//add 14_02_13-13_34
	public function update_settings($settings){
	
		$case_str = null;
		$where_str = null;
		$sep_logic = '';
		$name_column = 'name';
		foreach($settings as $key => $value){			
			$case_str .= "WHEN name = '" . $key . "'	THEN '" . $value . "' 
			";
			
			//переност строки делается для лучшего отображения кода при отладке
			$where_str .= $sep_logic . $name_column . " = '" . $key . "' 
			";			
	
			if( empty($sep_logic) ){
				$sep_logic = ' OR ';
			}				
		}
		
		if( !empty($case_str) ){
			$query = "
				UPDATE " . $this->prefix_table . "params
				SET value = CASE 
							" . $case_str . "
						END
				WHERE " . $where_str . "		
			";				
			$this->query($query);
		}		
	}
	
	
	//add 14_02_13-13_34
	public function update_search_phrase_by_params($esp_site_search_phrase_ar_ser){
	
		$case_str = null;
		$where_str = null;
		$sep_logic = '';
		$name_column = 'id';
		foreach($esp_site_search_phrase_ar_ser as $key => $value){			
			$case_str .= "WHEN " . $name_column. " = '" . $value->id . "'	THEN '" . $value->phrase . "' 
			";
			
			//переност строки делается для лучшего отображения кода при отладке
			$where_str .= $sep_logic . $name_column . " = '" . $value->id . "' 
			";			
	
			if( empty($sep_logic) ){
				$sep_logic = ' OR ';
			}				
		}
		
		if( !empty($case_str) ){
			$query = "
				UPDATE " . $this->prefix_table . "search_phrases
				SET search_phrase = CASE 
							" . $case_str . "
						END
				WHERE " . $where_str . "		
			";				
			$this->query($query);
		}		
		
		$lines_add = mysql_affected_rows();
			
		return $lines_add;		
	}	
	
	
	//add 23_02_13-14_17
	public function update_cron_data_reg_domain($data){
	
		$case_str = null;
		$where_str = null;
		$sep_logic = '';
		$name_column = 'name';
		foreach($data as $key => $value){			
			$case_str .= "WHEN name = '" . $key . "'	THEN '" . $value . "' 
			";
			
			//переност строки делается для лучшего отображения кода при отладке
			$where_str .= $sep_logic . $name_column . " = '" . $key . "' 
			";			
	
			if( empty($sep_logic) ){
				$sep_logic = ' OR ';
			}				
		}
		
		if( !empty($case_str) ){
			$query = "
				UPDATE " . $this->prefix_table . "cron_data
				SET value = CASE 
							" . $case_str . "
						END
				WHERE " . $where_str . "		
			";				
			$this->query($query);
		}		
	}
	
	
	public function update_phrase_publish($ids_sites_for_in, $publish_status = 0){
		
		$query = "
			UPDATE " . $this->prefix_table . "search_phrases as search_phrases			
			SET search_phrases.publish_ph = " . $publish_status . "
			WHERE search_phrases.id IN (" . $ids_sites_for_in . ")			
		";	
		
		$this->query($query);		
	}	
	
	
	public function update_sites_sape_status($ids_sites_for_in){
		
		$query = "
			UPDATE " . $this->prefix_table . "sites as sites			
			SET sites.links_sape = 1
			WHERE sites.id IN (" . $ids_sites_for_in . ")			
		";	
		
		$this->query($query);		
	}
	
	
	public function update_sites_mainlink_status($ids_sites_for_in){
		
		$query = "
			UPDATE " . $this->prefix_table . "sites as sites			
			SET sites.links_mainlink = 1
			WHERE sites.id IN (" . $ids_sites_for_in . ")			
		";	
		
		$this->query($query);		
	}
	
	
	//add 04_11_12 22_28
	public function insert_liveinternet($name){		
		
		$name = $this->safe($name);	
		
		
		$counter = '<!--LiveInternet counter--><script type="text/javascript"><!--
new Image().src = "//counter.yadro.ru/hit?r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random();//--></script><!--/LiveInternet-->';
		$counter = $this->safe($counter);
		
		//echo '<br><br>!!!' . $search_phrase;
		
		$query = "
			INSERT INTO " . $this->prefix_table . "ads (name_ads, code)
			VALUES ( 
			('$name'),
			('$counter')
		)";				
		
		return $this->ins_ret($query);
	}
	
	
	/*
	//add 04_11_12 20_22
	public function insert_ads_liveinternet($ids_sites){				
		
		
		$counter = '<!--LiveInternet counter--><script type="text/javascript"><!--
new Image().src = "//counter.yadro.ru/hit?r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random();//--></script><!--/LiveInternet-->';
		$counter = $this->safe($counter);
		
		
		$query = "
			INSERT INTO
			
			(
				SELECT ads.name_ads as name_ads, ads.code as code, ads_links.id_position as id_position, ads_links.ads_sort as ads_sort
				FROM " . $this->prefix_table . "sites as sites
				LEFT JOIN " . $this->prefix_table . "ads_links as ads_links
				ON sites.id = ads_links.id_sites	
				LEFT JOIN " . $this->prefix_table . "ads as ads
				ON ads.id = ads_links.id_ads
				WHERE sites.id IN($ids_sites)
			)
			
			
			(name_ads, code, id_position, ads_sort)
			VALUES (
				('counter_liveinternet.ru'),
				('$counter'),
				('3'),
				('9')
			)
			
		";	
		
		return $this->sel_rows($query);
	}
	*/
	
	public function insert_video($id_search_phrase, $img, $video, $title, $description, $duration, $time_publish, $source, $user_add){
		/*
		$search_phrase = $this->safe($search_phrase);
		if(empty($this->search_phrase) OR $this->search_phrase != $search_phrase){
			$this->search_phrase = $search_phrase;
			
			$this->id_search_phrase = $this->select_or_insert_search_phrase($this->search_phrase);
		}	
		*/
		//$id_search_phrase = $this->select_or_insert_search_phrase($search_phrase);
		
		
		$thumb = $this->safe($thumb);
		$code = $this->safe($code);
		$title = $this->safe($title);
		$description = $this->safe($description);
		$duration = $this->safe($duration);
		$time_publish = $this->safe($time_publish);
		$source = $this->safe($source);
		//$md5 = md5($title.$description.$duration);
		
		$query = "
			INSERT IGNORE INTO " . $this->prefix_table . "videos (id_search_phrase, thumb, code, title, description, duration, time_publish, source, create_time)
			VALUES ( 
			('$id_search_phrase'),
			('$img'),
			('$video'),
			('$title'),
			('$description'),
			('$duration'),
			('$time_publish'),
			('$source'),
			(NOW())
		)";				
		
		return $this->ins_ret($query);
	}
	
	
	public function insert_img($id_search_phrase, $file_name, $alt, $source_url){

		$file_name = $this->safe($file_name);

		$alt = $this->safe($alt);	
		
		$query = "
			INSERT IGNORE INTO " . $this->prefix_table . "images (id_search_phrase, file_name, alt, create_time, source_url)
			VALUES ( 
			('$id_search_phrase'),
			('$file_name'),
			('$alt'),
			(NOW()),
			('$source_url')
		)";				
		
		return $this->ins_ret($query);
	}
	
	
	public function select_or_insert_search_phrase($search_phrase){
		$search_phrase_result = $this->select_search_phrase_by_phrase($search_phrase);
		$id_search_phrase = $search_phrase_result['id'];
		
		if(empty($id_search_phrase)){
			$id_search_phrase = $this->insert_search_phrase($search_phrase);
		}
		
		return $id_search_phrase;
	}
	
	
	public function insert_texts_and_search_phrases($texts_and_seach_phrase_array){
		
		$search_phrase = $texts_and_seach_phrase_array['key_phrase'];
		
		//echo '<br><br>!!!' . $texts_and_seach_phrase_array['key_phrase'];
		
		$id_search_phrase = $this->select_or_insert_search_phrase($search_phrase);
		
		//$url_sourse = 'google.com';
		
		if(is_array($texts_and_seach_phrase_array['texts'])){
		
			foreach($texts_and_seach_phrase_array['texts'] as $key => $text){
				//если есть вложенные массив с текстом (в случае парса полной статьи по внешней ссылке) то вставляем его
				if(is_array($texts_and_seach_phrase_array['texts'][$key]['text'])){
					foreach($texts_and_seach_phrase_array['texts'][$key]['text'] as $key_1 => $text_1){
						if(!empty($texts_and_seach_phrase_array['texts'][$key]['text']['texts'][$key_1]['text'])){
							$id_texts[] = $this->insert_text($id_search_phrase, $texts_and_seach_phrase_array['texts'][$key]['text']['texts'][$key_1]['text'], $texts_and_seach_phrase_array['texts'][$key]['text']['texts'][$key_1]['url'], $texts_and_seach_phrase_array['texts'][$key]['text']['texts'][$key_1]['title']);
														
						}						
					}
				}else{
					if(!empty($texts_and_seach_phrase_array['texts'][$key]['text'])){
						$id_texts[] = $this->insert_text($id_search_phrase, $texts_and_seach_phrase_array['texts'][$key]['text'], $texts_and_seach_phrase_array['texts'][$key]['url'], $texts_and_seach_phrase_array['texts'][$key]['title']);

					}
				}
							
				//$id_texts[] = $this->insert_text($id_search_phrase, $text, $url_sourse);
			}
		}

		
		return $id_texts;
	}
	
		
	public function insert_texts_and_search_phrases_eng($texts_and_seach_phrase_array){
		
		$search_phrase = $texts_and_seach_phrase_array['key_phrase'];
		
		//echo '<br><br>!!!' . $texts_and_seach_phrase_array['key_phrase'];
		
		$id_search_phrase = $this->select_or_insert_search_phrase($search_phrase);
		
		//$url_sourse = 'google.com';
		
		if(is_array($texts_and_seach_phrase_array['texts'])){
		
			foreach($texts_and_seach_phrase_array['texts'] as $key => $text){
				//если есть вложенные массив с текстом (в случае парса полной статьи по внешней ссылке) то вставляем его
				if(is_array($texts_and_seach_phrase_array['texts'][$key]['text'])){
					foreach($texts_and_seach_phrase_array['texts'][$key]['text'] as $key_1 => $text_1){
						if(!empty($texts_and_seach_phrase_array['texts'][$key]['text']['texts'][$key_1]['text'])){
							$id_texts[] = $this->insert_text_eng($id_search_phrase, $texts_and_seach_phrase_array['texts'][$key]['text']['texts'][$key_1]['text'], $texts_and_seach_phrase_array['texts'][$key]['text']['texts'][$key_1]['url'], $texts_and_seach_phrase_array['texts'][$key]['text']['texts'][$key_1]['title']);
														
						}						
					}
				}else{
					if(!empty($texts_and_seach_phrase_array['texts'][$key]['text'])){
						$id_texts[] = $this->insert_text_eng($id_search_phrase, $texts_and_seach_phrase_array['texts'][$key]['text'], $texts_and_seach_phrase_array['texts'][$key]['url'], $texts_and_seach_phrase_array['texts'][$key]['title']);

					}
				}
							
				//$id_texts[] = $this->insert_text($id_search_phrase, $text, $url_sourse);
			}
		}

		
		return $id_texts;
	}

	
	protected function insert_text($id_search_phrase, $text, $url_sourse, $text_title){
		
		//$hash_text = md5($text);
	
		//$text = mb_convert_encoding($text, 'utf8', 'cp1251');
		//$text = mb_convert_encoding($text, 'cp1251', 'utf8');
		$text = $this->safe($text);	
		$text_title = $this->safe($text_title);	
		$url_sourse = $this->safe($url_sourse);

		//$text = mb_convert_encoding($text, 'cp1251', 'utf8');	
		
		$url_sourse = $this->safe($url_sourse);		
		
		$query = "
			INSERT IGNORE INTO " . $this->prefix_table . "texts (id_search_phrase, text, url_sourse, create_time, text_title)
			VALUES ( 
			('$id_search_phrase'),
			('$text'),
			('$url_sourse'),
			(NOW()),			
			('$text_title')
		)";				
		
		return $this->ins_ret($query);
	}
	
	
		
	public function insert_texts_and_search_phrases_ua($texts_and_seach_phrase_array){
		
		$search_phrase = $texts_and_seach_phrase_array['key_phrase'];
		
		//echo '<br><br>!!!' . $texts_and_seach_phrase_array['key_phrase'];
		
		$id_search_phrase = $this->select_or_insert_search_phrase($search_phrase);
		
		//$url_sourse = 'google.com';
		
		if(is_array($texts_and_seach_phrase_array['texts'])){
		
			foreach($texts_and_seach_phrase_array['texts'] as $key => $text){
				//если есть вложенные массив с текстом (в случае парса полной статьи по внешней ссылке) то вставляем его
				if(is_array($texts_and_seach_phrase_array['texts'][$key]['text'])){
					foreach($texts_and_seach_phrase_array['texts'][$key]['text'] as $key_1 => $text_1){
						if(!empty($texts_and_seach_phrase_array['texts'][$key]['text']['texts'][$key_1]['text'])){
							$id_texts[] = $this->insert_text_ua($id_search_phrase, $texts_and_seach_phrase_array['texts'][$key]['text']['texts'][$key_1]['text'], $texts_and_seach_phrase_array['texts'][$key]['text']['texts'][$key_1]['url'], $texts_and_seach_phrase_array['texts'][$key]['text']['texts'][$key_1]['title']);
														
						}						
					}
				}else{
					if(!empty($texts_and_seach_phrase_array['texts'][$key]['text'])){
						$id_texts[] = $this->insert_text_ua($id_search_phrase, $texts_and_seach_phrase_array['texts'][$key]['text'], $texts_and_seach_phrase_array['texts'][$key]['url'], $texts_and_seach_phrase_array['texts'][$key]['title']);

					}
				}
							
				//$id_texts[] = $this->insert_text($id_search_phrase, $text, $url_sourse);
			}
		}

		
		return $id_texts;
	}
	
	
	public function insert_texts_and_search_phrases_by($texts_and_seach_phrase_array){
		
		$search_phrase = $texts_and_seach_phrase_array['key_phrase'];
		
		//echo '<br><br>!!!' . $texts_and_seach_phrase_array['key_phrase'];
		
		$id_search_phrase = $this->select_or_insert_search_phrase($search_phrase);
		
		//$url_sourse = 'google.com';
		
		if(is_array($texts_and_seach_phrase_array['texts'])){
		
			foreach($texts_and_seach_phrase_array['texts'] as $key => $text){
				//если есть вложенные массив с текстом (в случае парса полной статьи по внешней ссылке) то вставляем его
				if(is_array($texts_and_seach_phrase_array['texts'][$key]['text'])){
					foreach($texts_and_seach_phrase_array['texts'][$key]['text'] as $key_1 => $text_1){
						if(!empty($texts_and_seach_phrase_array['texts'][$key]['text']['texts'][$key_1]['text'])){
							$id_texts[] = $this->insert_text_by($id_search_phrase, $texts_and_seach_phrase_array['texts'][$key]['text']['texts'][$key_1]['text'], $texts_and_seach_phrase_array['texts'][$key]['text']['texts'][$key_1]['url'], $texts_and_seach_phrase_array['texts'][$key]['text']['texts'][$key_1]['title']);
														
						}						
					}
				}else{
					if(!empty($texts_and_seach_phrase_array['texts'][$key]['text'])){
						$id_texts[] = $this->insert_text_by($id_search_phrase, $texts_and_seach_phrase_array['texts'][$key]['text'], $texts_and_seach_phrase_array['texts'][$key]['url'], $texts_and_seach_phrase_array['texts'][$key]['title']);

					}
				}
							
				//$id_texts[] = $this->insert_text($id_search_phrase, $text, $url_sourse);
			}
		}

		
		return $id_texts;
	}
	
	
	protected function insert_text_ua($id_search_phrase, $text, $url_sourse, $text_title){
		
		//$hash_text = md5($text);
	
		//$text = mb_convert_encoding($text, 'utf8', 'cp1251');
		//$text = mb_convert_encoding($text, 'cp1251', 'utf8');
		$text = $this->safe($text);	
		$text_title = $this->safe($text_title);	
		$url_sourse = $this->safe($url_sourse);

		//$text = mb_convert_encoding($text, 'cp1251', 'utf8');	
		
		$url_sourse = $this->safe($url_sourse);		
		
		$query = "
			INSERT IGNORE INTO " . $this->prefix_table . "texts (id_search_phrase, text, url_sourse, create_time, text_title, tr)
			VALUES ( 
			('$id_search_phrase'),
			('$text'),
			('$url_sourse'),
			(NOW()),			
			('$text_title'),
			(1)
		)";				
		
		return $this->ins_ret($query);
	}
	
	
	protected function insert_text_by($id_search_phrase, $text, $url_sourse, $text_title){
		
		//$hash_text = md5($text);
	
		//$text = mb_convert_encoding($text, 'utf8', 'cp1251');
		//$text = mb_convert_encoding($text, 'cp1251', 'utf8');
		$text = $this->safe($text);	
		$text_title = $this->safe($text_title);	
		$url_sourse = $this->safe($url_sourse);

		//$text = mb_convert_encoding($text, 'cp1251', 'utf8');	
		
		$url_sourse = $this->safe($url_sourse);		
		
		$query = "
			INSERT IGNORE INTO " . $this->prefix_table . "texts (id_search_phrase, text, url_sourse, create_time, text_title, tr)
			VALUES ( 
			('$id_search_phrase'),
			('$text'),
			('$url_sourse'),
			(NOW()),			
			('$text_title'),
			(2)
		)";				
		
		return $this->ins_ret($query);
	}
	
		
	protected function insert_text_eng($id_search_phrase, $text, $url_sourse, $text_title){
		
		//$hash_text = md5($text);
	
		//$text = mb_convert_encoding($text, 'utf8', 'cp1251');
		//$text = mb_convert_encoding($text, 'cp1251', 'utf8');
		$text = $this->safe($text);	
		$text_title = $this->safe($text_title);	
		$url_sourse = $this->safe($url_sourse);

		//$text = mb_convert_encoding($text, 'cp1251', 'utf8');	
		
		$url_sourse = $this->safe($url_sourse);		
		
		$query = "
			INSERT IGNORE INTO " . $this->prefix_table . "texts_translate (id_search_phrase, text, url_sourse, create_time, text_title)
			VALUES ( 
			('$id_search_phrase'),
			('$text'),
			('$url_sourse'),
			(NOW()),			
			('$text_title')
		)";				
		
		return $this->ins_ret($query);
	}
	
	
	public function insert_search_phrase($search_phrase){		
		
		$search_phrase = $this->safe($search_phrase);	
		$id_sites = $this->id_sites;
		//echo '<br><br>!!!' . $search_phrase;
		
		$query = "
			INSERT INTO " . $this->prefix_table . "search_phrases (search_phrase, id_sites, create_time)
			VALUES ( 
			('$search_phrase'),
			('$id_sites'),
			(NOW())
		)";				
		
		return $this->ins_ret($query);
	}
	
	
	public function select_ids_phrases_by_id_site($id_site){
		$query = "
			SELECT phrases.id as id
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites			
			WHERE sites.id = " . $id_site . "
			AND phrases.publish_ph = 1
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1
		
		return $this->sel_rows($query);		
	}
	
	
	public function select_all_inphrases(){
		$query = "
			SELECT *
			FROM " . $this->prefix_table . "inphrases
			WHERE 1
		";	
		
		return $this->sel_rows($query);		
	}
	
	
	###админ панель
	public function select_tags(){
		$query = "
			SELECT *
			FROM " . $this->prefix_table . "tags as tags
		";	
		
		return $this->sel_rows($query);		
	}
	###конец админ панель
	
	public function update_videos_status($IN_ids, $limit){
		
		$query = "
			UPDATE " . $this->prefix_table . "videos as videos			
			SET videos.publish = 1
			WHERE videos.id_search_phrase IN (" . $IN_ids . ")
			AND videos.publish = 0 
			LIMIT " . $limit . "
		";	
		
		$this->query($query);		
	}
	
	
	public function update_texts_status($IN_ids, $limit){
		$query = "
			UPDATE " . $this->prefix_table . "texts as texts			
			SET texts.publish = 1
			WHERE texts.id_search_phrase IN (" . $IN_ids . ")
			AND texts.publish = 0 
			LIMIT " . $limit . "
		";	
		
		$this->query($query);		
	}
	
	
	public function update_texts_tr_status($IN_ids, $limit){
		$query = "
			UPDATE " . $this->prefix_table . "texts_translate as texts			
			SET texts.publish = 1
			WHERE texts.id_search_phrase IN (" . $IN_ids . ")
			AND texts.publish = 0 
			LIMIT " . $limit . "
		";	
		
		$this->query($query);		
	}
	
	
	public function update_images_status($IN_ids, $limit){
		$query = "
			UPDATE " . $this->prefix_table . "images as images			
			SET images.publish = 1
			WHERE images.id_search_phrase IN (" . $IN_ids . ")
			AND images.publish = 0			
			LIMIT " . $limit . "
		";	//AND images.id_texts = 0   - убрано из-за того что не хватало фоток в галерею, все тратились на привязку к текстам
		
		$this->query($query);		
	}
	
	
	public function select_count_images_by_id_site_for_updater($id_site){
	
		$query = " 			
			SELECT COUNT(*) as count
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites			
			RIGHT JOIN " . $this->prefix_table . "images as images
			ON images.id_search_phrase = phrases.id
			WHERE sites.id = '" . $id_site . "'	
			AND phrases.publish_ph = 1
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1
		
		return $this->sel_1_row($query);
	}
	
	
	public function select_count_images_by_id_site($id_site){
	
		$query = " 			
			SELECT COUNT(*) as count
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites			
			RIGHT JOIN " . $this->prefix_table . "images as images
			ON images.id_search_phrase = phrases.id
			WHERE sites.id = '" . $id_site . "'				
			AND phrases.publish_ph = 1
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1	
		
		return $this->sel_1_row($query);
	}
		
	
	public function select_sites_update_1(){				
		
		$query = "
			SELECT *
			FROM " . $this->prefix_table . "sites as sites
			WHERE sites.update = 1
		";	
		
		return $this->sel_rows($query);
	}	
	
	
	//обновляет текущий сайт для обновления
	public function update_current_site_for_update($id_site){
		$query = "
			UPDATE " . $this->prefix_table . "params
			SET value = '" . $id_site . "'
			WHERE name = 'site_for_update'
		";	
		
		$this->query($query);		
	}
	
	
	//обновляет текущий сайт для inphrases
	public function update_current_site_for_inphrases($id_site){
		$query = "
			UPDATE " . $this->prefix_table . "params
			SET value = '" . $id_site . "'
			WHERE name = 'site_for_inphrases'
		";	
		
		$this->query($query);		
	}
		
	
	public function select_param($param_name){
		$query = "
			SELECT value
			FROM " . $this->prefix_table . "params as params
			WHERE params.name = '" . $param_name . "'			
		";	
		
		return $this->sel_1_row($query);		
	}
	
	
	public function select_params(){
		$query = "
			SELECT *
			FROM " . $this->prefix_table . "params as params						
		";	
		
		return $this->sel_rows($query);		
	}
		
	
	public function update_param($param_name, $value){
		$query = "
			UPDATE " . $this->prefix_table . "params
			SET value = '" . $value . "'
			WHERE params.name = '" . $param_name . "'			
		";	
		
		$this->query($query);		
	}
	
	
	//add 11_11_12 21_38
	public function unpublish_texts(){
		$query = "
			UPDATE " . $this->prefix_table . "texts
			SET publish = 0
			WHERE publish = 1
		";	
		
		$this->query($query);		
	}
	
	
	public function unpublish_texts_tr(){
		$query = "
			UPDATE " . $this->prefix_table . "texts_translate
			SET publish = 0
			WHERE publish = 1
		";	
		
		$this->query($query);		
	}
	
	
	//add 11_11_12 21_38
	public function unpublish_videos(){
		$query = "
			UPDATE " . $this->prefix_table . "videos
			SET publish = 0
			WHERE publish = 1
		";	
		
		$this->query($query);		
	}
	
	
	//add 11_11_12 21_38
	public function unpublish_images(){
		$query = "
			UPDATE " . $this->prefix_table . "images
			SET publish = 0
			WHERE publish = 1
		";	
		
		$this->query($query);		
	}
	
	
	public function select_search_phrase_by_phrase($search_phrase){		
		
		$search_phrase = $this->safe($search_phrase);	
		$id_sites = $this->id_sites;
		
		$query = "
			SELECT *
			FROM " . $this->prefix_table . "search_phrases
			WHERE search_phrase = '$search_phrase'
			AND id_sites = '$id_sites'
		";	
		
		return $this->sel_1_row($query);
	}
	
	
	## вывод на шаблоны сайтов
	public function select_site($id_site){				
		
		$query = "
			SELECT *
			FROM " . $this->prefix_table . "sites as sites			
			WHERE sites.id = $id_site			
		";	
		
		return $this->sel_1_row($query);
	}
	
	
	public function select_site_ads($id_site){				
		
		$query = "
			SELECT ads.id as id_ads, ads.name_ads as name_ads, ads.code as code, ads_links.id_position as id_position
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "ads_links as ads_links
			ON sites.id = ads_links.id_sites
			LEFT JOIN " . $this->prefix_table . "ads as ads
			ON ads.id = ads_links.id_ads			
			WHERE sites.id = $id_site
			AND ads.ads_publish = 1
			AND ads_links.ads_site_publish = 1
			ORDER BY ads_links.ads_sort 
		";	
		
		return $this->sel_rows($query);
	}

	
	//для обновлятора
	public function select_sites(){				
		
		$query = "
			SELECT 	sites.id as id_sites
			FROM " . $this->prefix_table . "sites as sites
			WHERE sites.update = 1
		";	
		
		return $this->sel_rows($query);
	}
	
	
		//для обновлятора
	public function select_sites_for_inphrases(){				
		
		$query = "
			SELECT 	*
			FROM " . $this->prefix_table . "sites as sites
		";	
		
		return $this->sel_rows($query);
	}
	
	
	public function select_sites_by_primary_grab($primary_grab = 1){				
		
		$query = "
			SELECT 	sites.id as id_sites
			FROM " . $this->prefix_table . "sites as sites
			WHERE sites.primary_grab = $primary_grab
		";	
		
		return $this->sel_rows($query);
	}
	
	
	//add 01_11_12 23_55
	public function select_all_info_sites_by_primary_grab($primary_grab = 1){				
		
		$query = "
			SELECT 	*
			FROM " . $this->prefix_table . "sites as sites
			WHERE sites.primary_grab = $primary_grab
		";	
		
		return $this->sel_rows($query);
	}
	
	
	//add 23_01_13 16_20
	public function select_all_info_sites_by_primary_grab_and_domain_not_site_1($primary_grab = 1){				
		
		$query = "
			SELECT 	*
			FROM " . $this->prefix_table . "sites as sites
			WHERE sites.primary_grab = $primary_grab
			AND domain != 'site_1'
		";	
		
		return $this->sel_rows($query);
	}
	
	
	//add 26_01_13 16_20
	public function select_all_info_sites_by_primary_grab_and_domain_not_site_1_and_ml_0($primary_grab = 1){				
		
		$query = "
			SELECT 	*
			FROM " . $this->prefix_table . "sites as sites
			WHERE sites.primary_grab = $primary_grab
			AND domain != 'site_1'
			AND links_mainlink = 0
		";	
		
		return $this->sel_rows($query);
	}
	
	
	//add 09_07_13 01_52
	public function select_all_info_sites_by_ids($ids){				
		
		$query = "
			SELECT 	*
			FROM " . $this->prefix_table . "sites as sites
			WHERE sites.id IN($ids)
		";	
		
		return $this->sel_rows($query);
	}
	
	
	//add 08_07_13 23_05
	public function select_all_info_sites_by_id_mainlink(){				
		
		$query = "
			SELECT 	*
			FROM " . $this->prefix_table . "sites as sites
			WHERE sites.id_mainlink > 0			
		";	
		
		return $this->sel_rows($query);
	}
	
	
	//add 08_02_13 02_31
	public function select_all_info_sites_by_primary_grab_and_domain_not_site_1_and_sape_0($primary_grab = 1){				
		
		$query = "
			SELECT 	*
			FROM " . $this->prefix_table . "sites as sites
			WHERE sites.primary_grab = $primary_grab
			AND domain != 'site_1'
			AND links_sape = 0
		";	
		
		return $this->sel_rows($query);
	}
	
	
	//add 01_11_12 23_55
	public function select_all_info_sites_by_primary_grab_and_default_domain($primary_grab = 1, $default_domain = 'site_1', $limit = 0){				
		
		$default_domain = $this->safe($default_domain);
		
		$limit_str = '';
		if($limit != 0){
			$limit_str = 'LIMIT ' . $limit;
		}
		
		$query = "
			SELECT 	*
			FROM " . $this->prefix_table . "sites as sites
			WHERE sites.primary_grab = $primary_grab
			AND sites.domain = '$default_domain'" . $limit_str . "
		";	
		
		return $this->sel_rows($query);
	}
	
	
	//add 02_11_12 22_20
	public function select_all_info_sites(){				
		
		$query = "
			SELECT 	*
			FROM " . $this->prefix_table . "sites as sites			
		";	
		
		return $this->sel_rows($query);
	}
	
	
	//для обновлятора
	public function select_count_and_max_id_sites(){				
		
		$query = "
			SELECT 	COUNT(sites.id) as count_sites, MAX(sites.id) as max_sites
			FROM " . $this->prefix_table . "sites as sites
			WHERE sites.update = 1
		";	
		
		return $this->sel_1_row($query);
	}
	
	
	//для inphrases
	public function select_count_and_max_id_sites_for_inphrases(){				
		
		$query = "
			SELECT 	COUNT(sites.id) as count_sites, MAX(sites.id) as max_sites
			FROM " . $this->prefix_table . "sites as sites
		";	
		
		return $this->sel_1_row($query);
	}

	

	//для обновлятора
	public function select_current_site_for_update(){				
		
		$query = "
			SELECT params.value as value
			FROM " . $this->prefix_table . "params as params
			WHERE params.name = 'site_for_update'
		";	
		
		return $this->sel_1_row($query);
	}
	
	
	//для обновлятора
	public function select_current_site_for_inphrases(){				
		
		$query = "
			SELECT params.value as value
			FROM " . $this->prefix_table . "params as params
			WHERE params.name = 'site_for_inphrases'
		";	
		
		return $this->sel_1_row($query);
	}
	
	
	public function select_phrases_texts_images_videos_by_id_site($id_site){				
		
		$query = "
			SELECT *
			FROM " . $this->prefix_table . "links_sites_search_phrases as links
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON links.id_search_phrases = phrases.id
			LEFT JOIN " . $this->prefix_table . "texts as texts
			ON phrases.id = texts.id_search_phrase
			LEFT JOIN " . $this->prefix_table . "images as images
			ON phrases.id = images.id_search_phrase
			LEFT JOIN " . $this->prefix_table . "videos as videos
			ON phrases.id = videos.id_search_phrase		
			WHERE links.id_sites = $id_site
			AND phrases.publish_ph = 1
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1	
		
		return $this->sel_rows($query);
	}	
	
	
	public function select_texts_by_id_site($id_site, $limit = 10, $offset = 0){		
		
		$query = "
			SELECT texts.id as id_texts, texts.text as text, phrases.search_phrase as search_phrase, texts.text_title as text_title, images.alt as alt, images.file_name as file_name, texts.update_time as update_time
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites
			RIGHT JOIN " . $this->prefix_table . "texts as texts
			ON phrases.id = texts.id_search_phrase
			LEFT JOIN " . $this->prefix_table . "images as images
			ON images.id_texts = texts.id
			WHERE sites.id = $id_site
			AND texts.publish = 1
			AND phrases.publish_ph = 1
			GROUP BY texts.id
			ORDER BY texts.update_time DESC
			LIMIT " . $offset . ", " . $limit . "
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1	
		
		return $this->sel_rows($query);
	}
	
	
	public function select_count_all_texts_tr_by_id_site($id_site){		
		
		$query = "
			SELECT COUNT(texts.id) as count
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites
			RIGHT JOIN " . $this->prefix_table . "texts as texts
			ON phrases.id = texts.id_search_phrase
			WHERE sites.id = $id_site	
			AND texts.tr = 2
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1	
		
		return $this->sel_1_row($query);
	}
	
	
	public function select_count_all_texts_tr_ua_id_site($id_site){		
		
		$query = "
			SELECT COUNT(texts.id) as count
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites
			RIGHT JOIN " . $this->prefix_table . "texts as texts
			ON phrases.id = texts.id_search_phrase
			WHERE sites.id = $id_site	
			AND texts.tr = 1
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1	
		
		return $this->sel_1_row($query);
	}
	
	
	public function select_count_all_texts_id_site($id_site){		
		
		$query = "
			SELECT COUNT(texts.id) as count
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites
			RIGHT JOIN " . $this->prefix_table . "texts as texts
			ON phrases.id = texts.id_search_phrase
			WHERE sites.id = $id_site	
			AND texts.tr = 0
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1	
		
		return $this->sel_1_row($query);
	}
	
	
	public function select_count_all_images_id_site($id_site){		
		
		$query = "
			SELECT COUNT(images.id) as count
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites
			RIGHT JOIN " . $this->prefix_table . "images as images
			ON phrases.id = images.id_search_phrase
			WHERE sites.id = $id_site				
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1	
		
		return $this->sel_1_row($query);
	}
	
	
	public function select_count_all_videos_id_site($id_site){		
		
		$query = "
			SELECT COUNT(videos.id) as count
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites
			RIGHT JOIN " . $this->prefix_table . "videos as videos
			ON phrases.id = videos.id_search_phrase
			WHERE sites.id = $id_site				
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1	
		
		return $this->sel_1_row($query);
	}
	
	
	public function select_count_all_texts_tr_id_site($id_site){		
		
		$query = "
			SELECT COUNT(texts.id) as count
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites
			RIGHT JOIN " . $this->prefix_table . "texts_translate as texts
			ON phrases.id = texts.id_search_phrase
			WHERE sites.id = $id_site				
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1	
		
		return $this->sel_1_row($query);
	}
	
	
	public function select_texts_tr_by_id_site($id_site, $limit = 10, $offset = 0){		
		
		$query = "
			SELECT texts.id as id_texts, texts.text as text, phrases.search_phrase as search_phrase, texts.text_title as text_title, texts.update_time as update_time
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites
			RIGHT JOIN " . $this->prefix_table . "texts_translate as texts
			ON phrases.id = texts.id_search_phrase			
			WHERE sites.id = $id_site
			AND texts.publish = 1
			AND phrases.publish_ph = 1
			GROUP BY texts.id
			ORDER BY texts.update_time DESC
			LIMIT " . $offset . ", " . $limit . "
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1	
		
		return $this->sel_rows($query);
	}
		

	public function select_texts_by_id_phrase($id_phrase, $limit = 10, $offset = 0){	

		$id_phrase = $this->safe($id_phrase);
		
		$query = "
			SELECT texts.id as id_texts, texts.text as text, phrases.search_phrase as search_phrase, texts.text_title as text_title, images.alt as alt, images.file_name as file_name, texts.update_time as update_time
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites
			RIGHT JOIN " . $this->prefix_table . "texts as texts
			ON phrases.id = texts.id_search_phrase
			LEFT JOIN " . $this->prefix_table . "images as images
			ON images.id_texts = texts.id
			WHERE phrases.id = $id_phrase	
			AND texts.publish = 1
			AND phrases.publish_ph = 1
			GROUP BY texts.id
			ORDER BY texts.update_time DESC
			LIMIT " . $offset . ", " . $limit . "
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1	
		
		return $this->sel_rows($query);
	}	
	
	
	public function select_texts_tr_by_id_phrase($id_phrase, $limit = 10, $offset = 0){	

		$id_phrase = $this->safe($id_phrase);
		
		$query = "
			SELECT texts.id as id_texts, texts.text as text, phrases.search_phrase as search_phrase, texts.text_title as text_title, texts.update_time as update_time
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites
			RIGHT JOIN " . $this->prefix_table . "texts_translate as texts
			ON phrases.id = texts.id_search_phrase
			WHERE phrases.id = $id_phrase	
			AND texts.publish = 1
			AND phrases.publish_ph = 1
			GROUP BY texts.id
			ORDER BY texts.update_time DESC
			LIMIT " . $offset . ", " . $limit . "
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1	
		
		return $this->sel_rows($query);
	}
	
	
	public function select_count_texts_by_id_site($id_site){		
		
		$query = "
			SELECT COUNT(*) as count
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites
			RIGHT JOIN " . $this->prefix_table . "texts as texts
			ON phrases.id = texts.id_search_phrase
			WHERE sites.id = $id_site
			AND texts.publish = 1
			AND phrases.publish_ph = 1
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1		
		
		return $this->sel_1_row($query);
	}	
	
	
	public function select_count_texts_tr_by_id_site($id_site){		
		
		$query = "
			SELECT COUNT(*) as count
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites
			RIGHT JOIN " . $this->prefix_table . "texts_translate as texts
			ON phrases.id = texts.id_search_phrase
			WHERE sites.id = $id_site
			AND texts.publish = 1
			AND phrases.publish_ph = 1
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1		
		
		return $this->sel_1_row($query);
	}	
	
	
	public function select_count_texts_by_id_site_for_updater($id_site){		
		
		$query = "
			SELECT COUNT(*) as count
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites
			RIGHT JOIN " . $this->prefix_table . "texts as texts
			ON phrases.id = texts.id_search_phrase
			WHERE sites.id = $id_site			
			AND phrases.publish_ph = 1
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1		
		
		return $this->sel_1_row($query);
	}


	public function select_count_texts_tr_by_id_site_for_updater($id_site){		
		
		$query = "
			SELECT COUNT(*) as count
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites
			RIGHT JOIN " . $this->prefix_table . "texts_translate as texts
			ON phrases.id = texts.id_search_phrase
			WHERE sites.id = $id_site			
			AND phrases.publish_ph = 1
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1		
		
		return $this->sel_1_row($query);
	}	
	
	
	public function select_count_texts_by_id_phrase($id_phrase){		
		
		$id_phrase = $this->safe($id_phrase);
		
		$query = "
			SELECT COUNT(*) as count
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites
			RIGHT JOIN " . $this->prefix_table . "texts as texts
			ON phrases.id = texts.id_search_phrase
			WHERE phrases.id = $id_phrase	
			AND texts.publish = 1
		";	
		
		return $this->sel_1_row($query);
	}	
	
	
	public function select_count_texts_tr_by_id_phrase($id_phrase){		
		
		$id_phrase = $this->safe($id_phrase);
		
		$query = "
			SELECT COUNT(*) as count
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites
			RIGHT JOIN " . $this->prefix_table . "texts_translate as texts
			ON phrases.id = texts.id_search_phrase
			WHERE phrases.id = $id_phrase	
			AND texts.publish = 1
		";	
		
		return $this->sel_1_row($query);
	}	
	
	
	public function select_count_texts_for_sites_primary_grab_1(){				
		
		$query = "
			SELECT COUNT(texts.id) as count, sites.id as id_site
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites
			RIGHT JOIN " . $this->prefix_table . "texts as texts
			ON phrases.id = texts.id_search_phrase
			WHERE sites.primary_grab = 1
			AND phrases.publish_ph = 1
			GROUP BY sites.id
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1			
		
		return $this->sel_rows($query);
	}	
	
	
	public function select_count_texts_for_sites_primary_grab_1_left_join_text(){				
		
		$query = "
			SELECT COUNT(texts.id) as count, sites.id as id_site
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites
			LEFT JOIN " . $this->prefix_table . "texts as texts
			ON phrases.id = texts.id_search_phrase
			WHERE sites.primary_grab = 1
			AND phrases.publish_ph = 1
			GROUP BY sites.id
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1	
		
		return $this->sel_rows($query);
	}	
	
	
	public function select_text_and_images_by_id($id_site, $id_article){
		
		$id_article = $this->safe($id_article);
	
		$query = "
			SELECT texts.id as id_texts, texts.text as text, phrases.search_phrase as search_phrase, phrases.id as id_phrase, images.file_name as file_name, images.alt as alt, texts.text_title as text_title, texts.update_time as update_time 
			FROM " . $this->prefix_table . "sites as sites	
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases	
			ON sites.id = phrases.id_sites			
			RIGHT JOIN " . $this->prefix_table . "texts as texts
			ON phrases.id = texts.id_search_phrase
			LEFT JOIN " . $this->prefix_table . "images as images
			ON images.id_texts = texts.id
			WHERE sites.id = $id_site AND texts.id = $id_article
			AND texts.publish = 1
			AND phrases.publish_ph = 1
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1	
		
		return $this->sel_rows($query);	
	}	
	
	
	public function select_text_tr_by_id($id_site, $id_article){
		
		$id_article = $this->safe($id_article);
	
		$query = "
			SELECT texts.id as id_texts, texts.text as text, phrases.search_phrase as search_phrase, phrases.id as id_phrase, texts.text_title as text_title, texts.update_time as update_time 
			FROM " . $this->prefix_table . "sites as sites	
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases	
			ON sites.id = phrases.id_sites			
			RIGHT JOIN " . $this->prefix_table . "texts_translate as texts
			ON phrases.id = texts.id_search_phrase			
			WHERE sites.id = $id_site AND texts.id = $id_article
			AND texts.publish = 1
			AND phrases.publish_ph = 1
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1	
		
		return $this->sel_rows($query);	
	}	
	
	public function select_video_by_id($id_site, $id_video){
		
		$id_video = $this->safe($id_video);
	
		$query = "
			SELECT videos.id as id_video, phrases.search_phrase as search_phrase, phrases.id as id_phrase, videos.thumb as thumb, videos.code as code, videos.title as title, videos.description as description, videos.duration as duration, videos.time_publish as time_publish, videos.update_time as update_time
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites
			RIGHT JOIN " . $this->prefix_table . "videos as videos
			ON phrases.id = videos.id_search_phrase
			WHERE sites.id = $id_site AND videos.id = $id_video
			AND videos.publish = 1
			AND phrases.publish_ph = 1
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1	
		
		return $this->sel_1_row($query);	
	}	
	
	/*
	public function select_phrases_by_id_site($id_site, $limit = 20, $offset = 0){			
		
		$query = "
			SELECT phrases.search_phrase as search_phrase, phrases.id as id
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites
			RIGHT JOIN " . $this->prefix_table . "texts as texts
			ON phrases.id = texts.id_search_phrase
			WHERE sites.id = $id_site
			GROUP BY phrases.id			
			LIMIT " . $offset . ", " . $limit . "
		";	
		
		return $this->sel_rows($query);
	}	
	*/
	
	public function select_phrases_by_id_site($id_site, $limit = 20, $offset = 0){			
		
		$query = "
			SELECT phrases.search_phrase as search_phrase, phrases.id as id
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites		
			LEFT JOIN " . $this->prefix_table . "texts as texts
			ON phrases.id = texts.id_search_phrase
			LEFT JOIN " . $this->prefix_table . "videos as videos
			ON phrases.id = videos.id_search_phrase	
			WHERE sites.id = $id_site
			AND (texts.publish = 1
			OR videos.publish = 1)
			AND phrases.publish_ph = 1
			GROUP BY phrases.id			
			LIMIT " . $offset . ", " . $limit . "
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1	
		
		return $this->sel_rows($query);
	}
	
	
	public function select_phrases_by_id_site_only($id_site, $limit = 20, $offset = 0){			
		
		$query = "
			SELECT phrases.search_phrase as search_phrase, phrases.id as id
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites	
			WHERE sites.id = $id_site
			AND phrases.publish_ph = 1		
			LIMIT " . $offset . ", " . $limit . "
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1	
		
		return $this->sel_rows($query);
	}
	
	
	public function select_search_phrases_by_id_site($id_site, $limit = 20, $offset = 0){			
		
		$query = "
			SELECT phrases.search_phrase as search_phrase, phrases.id as id, sites.name as site_name, sites.domain as site_domain, phrases.primary_add as primary_add
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites					
			WHERE sites.id = $id_site					
			LIMIT " . $offset . ", " . $limit . "
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1	
		
		return $this->sel_rows($query);
	}
	
	
	public function select_phrases_by_id_site_primary_add_0($id_site, $limit = 20, $offset = 0){			
		
		$query = "
			SELECT phrases.search_phrase as search_phrase, phrases.id as id
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites		
			LEFT JOIN " . $this->prefix_table . "texts as texts
			ON phrases.id = texts.id_search_phrase
			LEFT JOIN " . $this->prefix_table . "videos as videos
			ON phrases.id = videos.id_search_phrase	
			WHERE sites.id = $id_site
			AND (texts.publish = 1
			OR videos.publish = 1)
			AND phrases.publish_ph = 1
			AND phrases.primary_add = 0
			GROUP BY phrases.id			
			LIMIT " . $offset . ", " . $limit . "
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1	
		
		return $this->sel_rows($query);
	}

	
	/*###backup
	public function select_phrases_and_site_name_by_id_site($id_site, $limit = 20, $offset = 0){			
		
		$query = "
			SELECT phrases.search_phrase as search_phrase, phrases.id as id, sites.name as name_site, sites.domain as domain, sites.id_tags as id_tags
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites		
			LEFT JOIN " . $this->prefix_table . "texts as texts
			ON phrases.id = texts.id_search_phrase
			LEFT JOIN " . $this->prefix_table . "videos as videos
			ON phrases.id = videos.id_search_phrase	
			WHERE sites.id = $id_site
			AND (texts.publish = 1
			OR videos.publish = 1)
			GROUP BY phrases.id			
			LIMIT " . $offset . ", " . $limit . "
		";	
		
		return $this->sel_rows($query);
	}
	*/	
	
	
	public function select_phrases_and_site_name_by_id_site($id_site, $limit = 20, $offset = 0){			
		
		$query = "
			SELECT phrases.search_phrase as search_phrase, phrases.id as id, sites.name as name_site, sites.domain as domain, sites.id_tags as id_tags, tags.name as name_tag
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites		
			LEFT JOIN " . $this->prefix_table . "texts as texts
			ON phrases.id = texts.id_search_phrase
			LEFT JOIN " . $this->prefix_table . "videos as videos
			ON phrases.id = videos.id_search_phrase	
			LEFT JOIN " . $this->prefix_table . "tags as tags
			ON sites.id_tags = tags.id	
			WHERE sites.id = $id_site
			AND (texts.publish = 1
			OR videos.publish = 1)
			AND phrases.publish_ph = 1
			GROUP BY phrases.id			
			LIMIT " . $offset . ", " . $limit . "
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1	
		
		return $this->sel_rows($query);
	}
	
	
	public function select_images_for_text_by_id_site($id_site, $limit = 60, $offset = 0){			
		
		$query = "
			SELECT images.id_texts as id_texts, images.file_name as file_name, images.alt as alt 
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites
			RIGHT JOIN " . $this->prefix_table . "texts as texts
			ON phrases.id = texts.id_search_phrase
			RIGHT JOIN " . $this->prefix_table . "images as images
			ON phrases.id = images.id_search_phrase
			WHERE sites.id = $id_site AND images.id_texts != 0
			AND phrases.publish_ph = 1
			GROUP BY id_texts
			LIMIT " . $offset . ", " . $limit . "
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1	
		
		return $this->sel_rows($query);
	}	
	
	
	public function select_videos_by_id_site($id_site, $limit = 10, $offset = 0, $id_phrase = ''){		
		
		if(!empty($id_phrase)){
			$and = "AND phrases.id = $id_phrase";
		}
		
		$query = "
			SELECT videos.id as id_video, phrases.search_phrase as search_phrase, videos.thumb as thumb, videos.code as code, videos.title as title, videos.description as description, videos.duration as duration, videos.time_publish as time_publish, videos.update_time as update_time
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites
			RIGHT JOIN " . $this->prefix_table . "videos as videos
			ON phrases.id = videos.id_search_phrase
			WHERE sites.id = $id_site
			AND phrases.publish_ph = 1
			AND videos.publish = 1
			" . $and . "
			ORDER BY videos.update_time DESC
			LIMIT " . $offset . ", " . $limit . "
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1
		
		return $this->sel_rows($query);
	}
	
	
	public function select_count_videos_by_id_site_for_updater($id_site){		
		
		$query = "
			SELECT COUNT(videos.id) as count
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites
			RIGHT JOIN " . $this->prefix_table . "videos as videos
			ON phrases.id = videos.id_search_phrase
			WHERE sites.id = $id_site				
			AND phrases.publish_ph = 1
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1
		
		return $this->sel_1_row($query);
	}
	
	
	public function select_count_videos_by_id_site($id_site){		
		
		$query = "
			SELECT COUNT(videos.id) as count
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites
			RIGHT JOIN " . $this->prefix_table . "videos as videos
			ON phrases.id = videos.id_search_phrase
			WHERE sites.id = $id_site	
			AND videos.publish = 1
			AND phrases.publish_ph = 1
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1	
		
		return $this->sel_1_row($query);
	}
	
	
	public function select_videos_by_id_phrase($id_phrase, $limit = 10, $offset = 0){		
		
		$query = "
			SELECT videos.id as id_video, phrases.search_phrase as search_phrase, videos.thumb as thumb, videos.code as code, videos.title as title, videos.description as description, videos.duration as duration, videos.time_publish as time_publish, videos.update_time as update_time
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites
			RIGHT JOIN " . $this->prefix_table . "videos as videos
			ON phrases.id = videos.id_search_phrase
			WHERE phrases.id = $id_phrase	
			AND videos.publish = 1
			AND phrases.publish_ph = 1
			ORDER BY videos.update_time DESC
			LIMIT " . $offset . ", " . $limit . "
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1
		
		return $this->sel_rows($query);
	}
	
	
	public function select_count_videos_by_id_phrase($id_phrase){		
		
		$query = "
			SELECT COUNT(*) as count
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites
			RIGHT JOIN " . $this->prefix_table . "videos as videos
			ON phrases.id = videos.id_search_phrase
			WHERE phrases.id = $id_phrase	
			AND videos.publish = 1
		";	
		
		return $this->sel_1_row($query);
	}
	
	
	public function select_phrase($id_phrase, $id_site){		
		
		$query = "
			SELECT *
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites
			RIGHT JOIN " . $this->prefix_table . "texts as texts
			ON texts.id_search_phrase = phrases.id
			WHERE phrases.id = $id_phrase
			AND sites.id = $id_site
			AND phrases.publish_ph = 1
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1
		
		
		return $this->sel_rows($query);
	}
	
	
	//13_10_12 add
	public function select_b_list_words(){		
		
		
		if( empty($this->b_list_words[$this->id_sites]) ){
		
			$id_sites = $this->id_sites;
		
			$query = "
			SELECT b_l.id as id_b_l, b_l.id_sites as id_sites, b_l.words as words
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "b_list_words as b_l
			ON sites.id = b_l.id_sites
			WHERE sites.id = $id_sites	
			";	
			
			$this->b_list_words[$this->id_sites]['words'] = $this->sel_rows($query);
			
			return $this->b_list_words[$this->id_sites]['words'];
		}else{
			return $this->b_list_words[$this->id_sites]['words'];
		}		
		
	}	
	
	
	public function select_phrase_for_gal($id_phrase, $id_site){		
		
		$query = "
			SELECT *
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites		
			WHERE phrases.id = $id_phrase
			AND sites.id = $id_site
			AND phrases.publish_ph = 1
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1	
		
		return $this->sel_1_row($query);
	}
	
	
	public function select_phrase_for_cat($id_phrase, $id_site){		
		
		$query = "
			SELECT phrases.search_phrase as search_phrase, phrases.id
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites
			LEFT JOIN " . $this->prefix_table . "texts as texts
			ON texts.id_search_phrase = phrases.id
			LEFT JOIN " . $this->prefix_table . "videos as videos
			ON videos.id_search_phrase = phrases.id
			WHERE phrases.id = $id_phrase
			AND sites.id = $id_site
			AND phrases.publish_ph = 1
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1	
		
		return $this->sel_rows($query);
	}
	

	public function select_images_by_id_site_for_favicon($id_site){		
		
		$query = "
			SELECT *
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites
			RIGHT JOIN " . $this->prefix_table . "images as images
			ON phrases.id = images.id_search_phrase
			WHERE sites.id = $id_site
			AND phrases.publish_ph = 1
			LIMIT 1
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1	
		
		return $this->sel_rows($query);
	}
	
	
	public function select_images_by_id_site($id_site, $limit = 20, $offset = 0){		
		
		$query = "
			SELECT *
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites
			RIGHT JOIN " . $this->prefix_table . "images as images
			ON phrases.id = images.id_search_phrase
			WHERE sites.id = $id_site
			
			AND images.publish = 1
			AND phrases.publish_ph = 1
			GROUP BY phrases.id
			ORDER BY images.update_time DESC
			LIMIT " . $offset . ", " . $limit . "
		";	// AND images.id_texts = 0    -   убрано из-за того что не хватает фоток для хэдера
		//update 28.10.12 03:43 - phrases.publish_ph = 1	
		
		return $this->sel_rows($query);
	}
	
	
	public function select_images_by_id_site_and_phrase($id_site, $id_phrase, $limit = 20, $offset = 0){		
		
		$query = "
			SELECT *
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites
			RIGHT JOIN " . $this->prefix_table . "images as images
			ON phrases.id = images.id_search_phrase
			WHERE sites.id = $id_site
			
			AND phrases.id = $id_phrase
			AND images.publish = 1
			AND phrases.publish_ph = 1
			ORDER BY images.update_time DESC
			LIMIT " . $offset . ", " . $limit . "
		";		//AND id_texts = 0 - убрано из-за того что не хватало изображений для галереи, по причине большого количества текстов
		//update 28.10.12 03:43 - phrases.publish_ph = 1
		
		return $this->sel_rows($query);
	}
	
	
	public function select_count_images_by_id_site_and_phrase($id_site, $id_phrase){		
		
		$query = "
			SELECT COUNT(*) as count
			FROM " . $this->prefix_table . "sites as sites
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites
			RIGHT JOIN " . $this->prefix_table . "images as images
			ON phrases.id = images.id_search_phrase
			WHERE sites.id = $id_site
			
			AND phrases.id = $id_phrase	
			AND images.publish = 1
		";	//AND id_texts = 0 //09_10_12
		
		return $this->sel_1_row($query);
	}
	
	## автоматизатор связок изображений и текстов
	protected function select_images_on_texts(){
		$query = "
			SELECT texts.id as id_text, images.id as id_images, phrases.id as id_phrase
			FROM " . $this->prefix_table . "search_phrases as phrases			
			RIGHT JOIN " . $this->prefix_table . "texts as texts
			ON phrases.id = texts.id_search_phrase
			LEFT JOIN " . $this->prefix_table . "images as images
			ON phrases.id = images.id_search_phrase
			WHERE images.id_texts = 0	
			AND phrases.publish_ph = 1
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1
		
		return $this->sel_rows($query);
	}
	
	
	protected function update_images_on_texts($ids_images, $id_texts){
		$query = "
			UPDATE " . $this->prefix_table . "images
			SET id_texts='" . $id_texts . "'
			WHERE id IN (" . $ids_images . ")
		";	
		
		$this->query($query);
		
		//return $this->num_rows();
	}
	
	
	public function update_site_name_a_domain_by_id($id, $name, $domain){
		$id = $this->safe($id);
		$name = $this->safe($name);
		$domain = $this->safe($domain);
	
		$query = "
			UPDATE " . $this->prefix_table . "sites
			SET name='" . $name . "', domain='" . $domain . "' 
			WHERE id = $id
		";	
		
		$this->query($query);
		
		//return $this->num_rows();
	}	
	
	
	protected function update_domain_by_id_site($id_site, $domain){
		
		$domain = $this->safe($domain);
		
		$query = "
			UPDATE " . $this->prefix_table . "sites
			SET domain='" . $domain . "'
			WHERE id = " . $id_site . "
		";	
		
		$this->query($query);
		
		//return $this->num_rows();
	}
	
	
	##админ панель	
	public function select_info_sites(){
	
		$query = "
			SELECT * FROM
				(SELECT sites.id as id_sites,
				sites.id as id,
				sites.primary_grab as primary_grab,
				sites.name as name,
				sites.domain as domain, 
				sites.links_sape as links_sape,
				sites.links_mainlink as links_mainlink,
				sites.links_setlinks as links_setlinks,
				sites.links_linkfeed as links_linkfeed,
				sites.ads as ads,
				sites.publish as publish,			
				sites.update as update_site,
				sites.update_time as update_time,
				sites.create_time as create_time,

				(SELECT COUNT(DISTINCT sites.id) as sites_primary_grab_0_count
				FROM " . $this->prefix_table . "sites as sites						
				LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
				ON sites.id = phrases.id_sites
				WHERE sites.primary_grab = 0
				) as sites_primary_grab_0_count,				
				
				(SELECT COUNT(DISTINCT sites.id) as sites_primary_grab_1_count
				FROM " . $this->prefix_table . "sites as sites						
				LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
				ON sites.id = phrases.id_sites
				WHERE sites.primary_grab = 1				
				) as sites_primary_grab_1_count,
				
				COUNT(texts.id) as texts_count						
				
				FROM " . $this->prefix_table . "sites as sites			
				LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
				ON sites.id = phrases.id_sites
				LEFT JOIN " . $this->prefix_table . "texts as texts
				ON texts.id_search_phrase = phrases.id
				WHERE phrases.publish_ph = 1
				GROUP BY sites.id) as t1			
			
			LEFT OUTER JOIN		
			
				(SELECT COUNT(texts.id) as texts_publish_count,
				sites.id as id_sites
				
				FROM " . $this->prefix_table . "sites as sites			
				LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
				ON sites.id = phrases.id_sites
				LEFT JOIN " . $this->prefix_table . "texts as texts
				ON texts.id_search_phrase = phrases.id
				WHERE texts.publish = 1
				AND phrases.publish_ph = 1
				GROUP BY sites.id) as t2
			
			ON t1.id_sites = t2.id_sites
			
			
			LEFT OUTER JOIN	
			
				(SELECT COUNT(videos.id) as videos_count,
				sites.id as id_sites
				
				FROM " . $this->prefix_table . "sites as sites			
				LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
				ON sites.id = phrases.id_sites
				LEFT JOIN " . $this->prefix_table . "videos as videos
				ON videos.id_search_phrase = phrases.id		
				WHERE phrases.publish_ph = 1
				GROUP BY sites.id) as t3
				
			ON t1.id_sites = t3.id_sites
						
			
			LEFT OUTER JOIN	
			
				(SELECT COUNT(videos.id) as videos_publish_count,
				sites.id as id_sites
				
				FROM " . $this->prefix_table . "sites as sites			
				LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
				ON sites.id = phrases.id_sites
				LEFT JOIN " . $this->prefix_table . "videos as videos
				ON videos.id_search_phrase = phrases.id		
				WHERE videos.publish = 1
				AND phrases.publish_ph = 1
				GROUP BY sites.id) as t4
				
			ON t1.id_sites = t4.id_sites		
			
			
			
			LEFT OUTER JOIN	
			
				(SELECT COUNT(images.id) as images_count,
				sites.id as id_sites
				
				FROM " . $this->prefix_table . "sites as sites			
				LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
				ON sites.id = phrases.id_sites
				LEFT JOIN " . $this->prefix_table . "images as images
				ON images.id_search_phrase = phrases.id		
				WHERE phrases.publish_ph = 1
				GROUP BY sites.id) as t5
				
			ON t1.id_sites = t5.id_sites
						
			
			LEFT OUTER JOIN	
			
				(SELECT COUNT(images.id) as images_publish_count,
				sites.id as id_sites
				
				FROM " . $this->prefix_table . "sites as sites			
				LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
				ON sites.id = phrases.id_sites
				LEFT JOIN " . $this->prefix_table . "images as images
				ON images.id_search_phrase = phrases.id		
				WHERE images.publish = 1
				AND phrases.publish_ph = 1
				GROUP BY sites.id) as t6
				
			ON t1.id_sites = t6.id_sites
			
			
			LEFT OUTER JOIN	
			
				(SELECT COUNT(images.id) as images_text_count,
				sites.id as id_sites
				
				FROM " . $this->prefix_table . "sites as sites			
				LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
				ON sites.id = phrases.id_sites
				LEFT JOIN " . $this->prefix_table . "images as images
				ON images.id_search_phrase = phrases.id		
				WHERE images.id_texts != 0
				AND phrases.publish_ph = 1
				GROUP BY sites.id) as t7
				
			ON t1.id_sites = t7.id_sites	
			
			
			LEFT OUTER JOIN
			
				(SELECT COUNT(DISTINCT phrases.id) as phrases_active_for_site,
				sites.id as id_sites
				FROM " . $this->prefix_table . "sites as sites						
				LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
				ON sites.id = phrases.id_sites		
				LEFT JOIN " . $this->prefix_table . "texts as texts
				ON texts.id_search_phrase = phrases.id
				LEFT JOIN " . $this->prefix_table . "videos as videos
				ON videos.id_search_phrase = phrases.id
				WHERE (texts.publish = 1 OR videos.publish = 1)
				AND phrases.publish_ph = 1
				GROUP BY sites.id
				) as t8
			
			ON t1.id_sites = t8.id_sites
			
			
			LEFT OUTER JOIN
			
				(SELECT COUNT(phrases.id) as phrases_for_site,
				sites.id as id_sites
				FROM " . $this->prefix_table . "sites as sites						
				LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
				ON sites.id = phrases.id_sites
				WHERE phrases.publish_ph = 1
				GROUP BY sites.id) as t9	
				
			ON t1.id_sites = t9.id_sites
			
			
			LEFT OUTER JOIN		
			
				(SELECT COUNT(texts_tr.id) as texts_tr_publish_count,
				sites.id as id_sites
				
				FROM " . $this->prefix_table . "sites as sites			
				LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
				ON sites.id = phrases.id_sites
				LEFT JOIN " . $this->prefix_table . "texts_translate as texts_tr
				ON texts_tr.id_search_phrase = phrases.id
				WHERE texts_tr.publish = 1
				AND phrases.publish_ph = 1
				GROUP BY sites.id) as t10
			
			ON t1.id_sites = t10.id_sites
			
			
			LEFT OUTER JOIN		
			
				(SELECT COUNT(texts_tr.id) as texts_tr_count,
				sites.id as id_sites
				
				FROM " . $this->prefix_table . "sites as sites			
				LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
				ON sites.id = phrases.id_sites
				LEFT JOIN " . $this->prefix_table . "texts_translate as texts_tr
				ON texts_tr.id_search_phrase = phrases.id
				WHERE phrases.publish_ph = 1				
				GROUP BY sites.id) as t11
			
			ON t1.id_sites = t11.id_sites


			LEFT OUTER JOIN		
			
				(SELECT COUNT(texts_ua.id) as texts_ua_count,
				sites.id as id_sites
				
				FROM " . $this->prefix_table . "sites as sites			
				LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
				ON sites.id = phrases.id_sites
				LEFT JOIN " . $this->prefix_table . "texts as texts_ua
				ON texts_ua.id_search_phrase = phrases.id
				WHERE phrases.publish_ph = 1
				AND texts_ua.publish = 1
				AND texts_ua.tr = 1
				GROUP BY sites.id) as t12
			
			ON t1.id_sites = t12.id_sites			
			
			
			LEFT OUTER JOIN		
			
				(SELECT COUNT(texts_by.id) as texts_by_count,
				sites.id as id_sites
				
				FROM " . $this->prefix_table . "sites as sites			
				LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
				ON sites.id = phrases.id_sites
				LEFT JOIN " . $this->prefix_table . "texts as texts_by
				ON texts_by.id_search_phrase = phrases.id
				WHERE phrases.publish_ph = 1
				AND texts_by.publish = 1
				AND texts_by.tr = 2
				GROUP BY sites.id) as t13
			
			ON t1.id_sites = t13.id_sites	
			
			
			LEFT OUTER JOIN		
			
				(SELECT COUNT(texts_ru.id) as texts_ru_count,
				sites.id as id_sites
				
				FROM " . $this->prefix_table . "sites as sites			
				LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
				ON sites.id = phrases.id_sites
				LEFT JOIN " . $this->prefix_table . "texts as texts_ru
				ON texts_ru.id_search_phrase = phrases.id
				WHERE phrases.publish_ph = 1
				AND texts_ru.publish = 1
				AND texts_ru.tr = 0
				GROUP BY sites.id) as t14
			
			ON t1.id_sites = t14.id_sites	
		";
		
		return $this->sel_rows($query);
	}
	//update 28.10.12 03:43 - phrases.publish_ph = 1
	
	
	public function select_phrases_primary_grab_0($id_site){
		$query = "
			SELECT *			
			FROM " . $this->prefix_table . "search_phrases as phrases	
			WHERE phrases.primary_add = 1	
			AND phrases.id_sites = '$id_site'
			AND phrases.publish_ph = 1
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1
		
		return $this->sel_rows($query);
	}
	
	public function select_sites_and_phrases_primary_grab_0(){
		$query = "
			SELECT *			
			FROM " . $this->prefix_table . "sites as sites			
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites
			WHERE sites.primary_grab = 0
			AND phrases.primary_add = 1
			AND phrases.publish_ph = 1
		";	
		//update 28.10.12 03:43 - phrases.publish_ph = 1	
		
		return $this->sel_rows($query);
	}
	
	
	public function select_sites_primary_grab_1(){
		$query = "
			SELECT sites.id as id_sites			
			FROM " . $this->prefix_table . "sites as sites				
			WHERE sites.primary_grab = 1
		";	
		
		return $this->sel_rows($query);
	}
	
	
	public function update_site_primary_grab($id_sites, $primary_grab = 1){
		$query = "
			UPDATE " . $this->prefix_table . "sites as sites
			SET sites.primary_grab = '$primary_grab'
			WHERE sites.id = '$id_sites'
		";	
		
		$id_query = $this->query($query);
		
		//return $this->num_rows($id_query);		
	}
	
	
	public function update_mass_site_primary_grab($ids_sites, $primary_grab = 1){
	
		if(!empty($ids_sites)){
			$query = "
				UPDATE " . $this->prefix_table . "sites as sites
				SET sites.primary_grab = '$primary_grab'
				WHERE sites.id IN($ids_sites)
			";	
			
			$id_query = $this->query($query);
		}
		//return $this->num_rows($id_query);		
	}
	
	
	public function delete_sites_small_text($ids_sites){
		$query = "
			DELETE videos.*, texts.*, images.*, phrases.*, sites.*
			FROM " . $this->prefix_table . "sites as sites						
			LEFT JOIN " . $this->prefix_table . "search_phrases as phrases
			ON sites.id = phrases.id_sites		
			LEFT JOIN " . $this->prefix_table . "texts as texts
			ON texts.id_search_phrase = phrases.id
			LEFT JOIN " . $this->prefix_table . "videos as videos
			ON videos.id_search_phrase = phrases.id
			LEFT JOIN " . $this->prefix_table . "images as images
			ON images.id_search_phrase = phrases.id			
			WHERE sites.id IN($ids_sites)
		";	
		
		$id_query = $this->query($query);
		
		//return $this->num_rows($id_query);		
	}	
	
	
	public function insert_site($search_phrases, $name_site = '', $domain = '', $id_tags = 27){
		
		if(empty($name_site)){
			$search_phrases = nl2br($search_phrases ,false);
			$dataex = explode('<br>',$search_phrases);
			
			if(is_array($dataex)){
				$name_site = $this->safe($dataex[0]);
			}else{
				$name_site = $this->safe($dataex);
			}
			
		}

		
		$query = "
			INSERT INTO " . $this->prefix_table . "sites (name, domain, create_time, id_tags)
			VALUES ( 
				'$name_site',
				'$domain',
				NOW(),
				'$id_tags'
			)		
		";				
		
		return $this->ins_ret($query);
	}
	
		
	public function insert_search_phrases($id_sites, $search_phrases){
		
		$search_phrases = nl2br($search_phrases ,false);
		$dataex = explode('<br>',$search_phrases);
		
		if(is_array($dataex)){
			foreach($dataex as $key => $value){
			
				//add - if 19_11_12-11_41
				if( !empty($value) AND $value != ' '){
				
					$value = mb_strtolower($value, 'utf-8');
					$value = $this->safe($value);
					
					if(empty($str)){
						$str = "($id_sites, 1, '$value', NOW())";
					}else{
						$str .= ", ($id_sites, 1, '$value', NOW())";
					}
					
				}
				
			}			
		}else{
		
			//add - if 19_11_12-11_41
			if( !empty($dataex) AND $dataex != ' '){
			
				$dataex = mb_strtolower($dataex, 'utf-8');
				$dataex = $this->safe($dataex);	
				$str = "($id_sites, 1, '$dataex', NOW())";
				
			}
		}
				
		//add - if 19_11_12-11_41
		if( !empty($str) ){
		
			$query = "
				INSERT IGNORE INTO " . $this->prefix_table . "search_phrases (id_sites, primary_add, search_phrase, create_time)
				VALUES $str			
			";				
			
			return $this->ins_ret($query);
			
		}else{
			return false;
		}
	}
	
		
	//13_10_12 add
	public function insert_b_list($id_sites, $b_list_words){
		
		$b_list_words = nl2br($b_list_words ,false);
		$dataex = explode('<br>',$b_list_words);
		
		if(is_array($dataex)){
			foreach($dataex as $key => $value){
			
				//add - if 19_11_12-11_41
				if( !empty($value) AND $value != ' '){
				
					$value = mb_strtolower($value, 'utf-8');
					$value = $this->safe($value);
					
					if(empty($str)){
						$str = "($id_sites, '$value')";
					}else{
						$str .= ", ($id_sites, '$value')";
					}
				
				}
				
			}			
		}else{
		
			//add - if 19_11_12-11_41
			if( !empty($dataex) AND $dataex != ' '){
			
				$dataex = mb_strtolower($dataex, 'utf-8');
				$dataex = $this->safe($dataex);	
				$str = "($id_sites, '$dataex')";
				
			}
		}				
		
		//add - if 19_11_12-11_41
		if( !empty($str) ){
		
			$query = "
				INSERT IGNORE INTO " . $this->prefix_table . "b_list_words (id_sites, words)
				VALUES $str			
			";						
			
			return $this->ins_ret($query);
		
		}else{
			return false;
		}
	}
	
}
?>